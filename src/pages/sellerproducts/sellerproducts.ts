import { Component,ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerprodeditPage } from '../sellerprodedit/sellerprodedit';
import { SellerprodviewPage } from '../sellerprodview/sellerprodview';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ExplorePage } from '../explore/explore';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-sellerproducts',
  templateUrl: 'sellerproducts.html',
})
export class SellerproductsPage {
  
  translationLet: any;	
  loading: Loading;	

  userDetails : any = {
    id:'',
    start : 0,
    limit : 10
  };

  loginUserData : any = {'id' : ''}; 

  delProductObj : any ={ 
  	id:'',
    user_id : ''
}

  prodImgPath : any ='';
 isFetchingData :any =true;
 imagePath : any ='';
 token :any ='';
  myorderCount :any =0;
  receiveOrderCount :any =0;

 //For infinite Scrolling 
  loadmoreData=0;
  isData : any = true ;
  ProductListing :any =[];
  tempProductImg : any = "";

   constructor(private socialSharing: SocialSharing,private crop: Crop,private camera: Camera,public platform: Platform,public configProvider : ConfigProvider,public actionSheetCtrl: ActionSheetController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.imagePath = this.configProvider.getProdImgPath();
  this.translateService.get(['select_one_payment_method','unable_to_crop_image','you_can_used_this_promo_code','to_get','discount_on_this_product','share_promo_code','pull_to_refresh','unable_to_share','cancel','post_new_product','take_photo','upload_from_gallery','refreshing','delivery_policies_err','prod_del_success','prod_del_err','delete','delete_product_txt','yes_txt','no_txt','load_more','prod_not_added','my_product','publish','profile_update_success','profile_update_err','email_already_exist','something_wrong_txt','unAuthReq_msg','err_upload_file','cancel','upload_profile_image','take_photo','upload_from_gallery','please_enter_valid_mobile','please_enter_mobile','email_not_valid','error_txt','please_enter_name','please_enter_email','please_wait_txt','basic_info','ok_txt','edit_profile','update_profile','name','mobile_no','email']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
   this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
  });

   //Get details
 this.storage.get('userDetails').then((userDetails) => {
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.userDetails.id=userDetails.id;
      this.loginUserData= userDetails;
      this.delProductObj.user_id =userDetails.id;
      this.initSellerProducts();
  }); 
 
  }


  /*Ionic refresher */

 doSellerListRefresh(refresher) {
    this.userDetails.start= 0;  
    this.userDetails.limit= 10;  
    this.initSellerRefreshProducts(refresher);
  }

initSellerRefreshProducts(refresher)
{

this.userDetails.start=parseInt(this.userDetails.start);
this.userDetails.limit=parseInt(this.userDetails.limit);  
//this.showLoading();
this.serviceProvider.getAllProducts(this.userDetails,this.token).then((result:any) => {
         // this.loading.dismiss();
            if(result.code == 200){
            this.ProductListing =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();
          } else if(result.code == 500) {
             this.ProductListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.ProductListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
       // this.loading.dismiss();
        console.log('err '+err);
        refresher.complete();  
          }); 
}


 /* End */


//Goto social share 

gotoSocialShare(indexValue,code, discount,prodImg)
{

this.ProductListing[indexValue]['isenabled']=true;
  // Share 

this.socialSharing.share(this.translationLet.you_can_used_this_promo_code+code+this.translationLet.to_get+discount+this.translationLet.discount_on_this_product,"",this.imagePath+prodImg).then(() => {
this.ProductListing[indexValue]['isenabled']=false;
}).catch(() => {
 this.ProductListing[indexValue]['isenabled']=false;
  this.toastMsg(this.translationLet.unable_to_share);
}); 

}

initSellerProducts()
{

this.userDetails.start=parseInt(this.userDetails.start);
this.userDetails.limit=parseInt(this.userDetails.limit);	
//this.showLoading();
this.serviceProvider.getAllProducts(this.userDetails,this.token).then((result:any) => {
  this.isFetchingData=false;
  //        this.loading.dismiss();
            if(result.code == 200){
            this.ProductListing =result.data;     
            this.loadmoreData=1;
            this.isData =true;
          } else if(result.code == 500) {
             this.ProductListing=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.ProductListing=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
    //    this.loading.dismiss();
          console.log('err '+err);
          }); 
}

infiniteScrollFun(infiniteScroll) {

    this.userDetails.start = parseInt(this.userDetails.start)+parseInt(this.userDetails.limit);
    this.userDetails.limit= parseInt(this.userDetails.limit)+10;

  setTimeout(() => {

           this.serviceProvider.getAllProducts(this.userDetails,this.token).then((result:any) => {
            if(result.code == 200){
            this.ProductListing = this.ProductListing.concat(result.data);
            this.loadmoreData=1;
            infiniteScroll.complete();
          } else if(result.code == 500) {
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 
 

  //Show loading 

    showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
//End

//Del product 

delProduct(item,indexVal)
{

this.delProductObj.id=item;

  let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.delete_product_txt,
    buttons: [
      
      {
        text: this.translationLet.yes_txt,
        handler: () => {

      //  Delete  
    this.serviceProvider.delProduct(this.delProductObj,this.token).then((result:any) => {
    if(result.code == 200)
    {
     this.ProductListing.splice(indexVal, 1);  
     this.toastMsg(this.translationLet.prod_del_success);
     if(!(this.ProductListing.length >0))
     {
      this.isData =false;
     }

    }else
    {
    this.toastMsg(this.translationLet.prod_del_err);
    }
        }, (err) => {
        });
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        //  console.log('Cancel clicked');
        }
      }
    ]
  });
  alert.present();	

}


//Goto Edit 

 gotoEdit (prodId)
 {
this.navCtrl.push(SellerprodeditPage,{'id' :prodId,'user_id' : this.userDetails.id,'pagePush' : 'seller'}) ;
 }

//End 

// Goto view

gotoDetails(prodId)
{
 this.navCtrl.push(SellerprodviewPage,{'id' :prodId,'user_id' : this.userDetails.id}) 
}
//End 

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerproductsPage');
  }

//gotoHome
 
  gotoHome()
  {
   this.app.getRootNav().setRoot(HomePage); 
  }
 
  gotoAccount()
    {
      this.app.getRootNav().setRoot(MyprofilePage); 
    }
    
    //Go to cover photo
    gotoCoverPhoto()
    {
       if(this.loginUserData.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

       if( (this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null ) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null )  && (this.loginUserData.std_delivery_cost == ''  || this.loginUserData.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else if( (this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
    //Actionsheet 
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    } 
  }
}

 takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
  //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
     //Crop Image 
      this.crop.crop(imageData, {quality: 100})
      .then(
        newImage => this.CropedImg(newImage),
        error =>this.toastMsg(this.translationLet.unable_to_crop_image)
      );
    //End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}

   gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }


//End 
}
