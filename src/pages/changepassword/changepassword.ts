import { Component,ViewChild } from '@angular/core';
import { Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';

@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
  translationLet: any;	
  loading: Loading;
  @ViewChild(Nav) nav: Nav;

  token :any ='';

  changePassObj:any = {
  password:'',
  id : ''
  };
  
  confirm_pass :any ='';


  constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
     this.translateService.get(['pass_change_success','pass_change_err','plese_enter_pass','plese_enter_confirm_pass','pass_more_six','pass_mismatch','error_txt','ok_txt','confirm_pass','new_pass','unAuthReq_msg','something_wrong_txt','please_wait_txt','save','change_pass']).subscribe((translation: [string]) => {
        this.translationLet = translation;
      });  

     this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     });

  	//Get details
 	this.storage.get('userDetails').then((userDetails) => {
      this.changePassObj.id=userDetails.id;
     }); 
    
  }
  
  //Change password functionality 

  save()
  {
   if(this.validateFields())
	{
		 this.showLoading();
		this.serviceProvider.changePassword(this.changePassObj,this.token).then((result:any) => {
		this.loading.dismiss();

		if(result.code == 200) 
    {
     this.toastMsg(this.translationLet.pass_change_success);
     this.navCtrl.pop(); 
    }else if(result.code == 403) 
    {
    this.toastMsg(this.translationLet.unAuthReq_msg);
    }else if(result.code == 500) 
    {
    this.toastMsg(this.translationLet.something_wrong_txt);
    }else
    {
     this.toastMsg(this.translationLet.something_wrong_txt);
    }
		 }, (err) => {
    this.loading.dismiss();
    console.log('err '+err);
    }); 
	}  

  }


//Validations 

 validateFields()
    {
        if (this.changePassObj.password.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_pass);
          return false;
        }

        if (this.changePassObj.password.length < 6) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_more_six);
          return false;
        }

         if (this.confirm_pass.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_confirm_pass);
          return false;
        }

 		 if (this.confirm_pass != this.changePassObj.password) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_mismatch);
          return false;
        }
       
      return true;
    }

  //End 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }

 //Show loading 
 
    showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }

  
//End

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'btn-primary btn-round'}]
    });
    alert.present();
  }
   //End


//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End 
}
