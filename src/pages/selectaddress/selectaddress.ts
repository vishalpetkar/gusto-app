import { Component,NgZone, ElementRef, ViewChild } from '@angular/core';
import { Platform,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { HomePage } from '../home/home';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import moment from 'moment';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

 declare var google;

/**
 * Generated class for the SelectaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-selectaddress',
  templateUrl: 'selectaddress.html',
})
export class SelectaddressPage {
   
   addressList :any =[]; 
   translationLet: any;	
   loading: Loading;

    loginMapData : any = {
	  id : '',
    address : '',
    latitude : '',
    longitude : '',
    created : '',
    device_token : ''
    };


    loginGPSData : any = {
    id : '',
    address : '-',
    latitude : '',
    longitude : '',
    created : '',
    device_token : ''
    };
    locationOptions :any =''; 
    isGPSData : any = false;

    @ViewChild('map') mapElement: ElementRef;
    map: any;
    service = new google.maps.places.AutocompleteService();

   tempAddress : any ='';
    constructor(private zone: NgZone,public platform: Platform,private locationAccuracy: LocationAccuracy,private geolocation: Geolocation,private nativeGeocoder: NativeGeocoder,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.translateService.get(['please_wait_gps_txt','unable_to_fetch_gps','select_delivery_address','error_location_permission','sign_in_success','something_wrong_txt','please_enter_address','unable_to_get_addess','select_current_pos','insert_delivery_add','login_txt_1','login_txt_2','please_wait_txt','error_txt','ok_txt','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
  //Onesignal device_token 
   this.storage.get('player_id').then((player_id) => {
    this.loginMapData.device_token=player_id;
    this.loginGPSData.device_token=player_id;
  }); 
  //End  

   this.loginMapData.id=navParams.get('user_id'); 
   this.loginGPSData.id=navParams.get('user_id'); 
  this.locationViaGPS();

   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectaddressPage');
  }

dismiss()
{
  this.loginMapData.address ="";
  this.addressList =[];
}
//End

//Autocompleted search and get google location from services 

    updateSearch() {

       if (this.loginMapData.address == '') {
       this.addressList = [];
       return;
      }
     
      let me = this;
      this.service.getPlacePredictions({
      input: this.loginMapData.address
     }, (predictions, status) => {
       me.addressList = [];
       me.zone.run(() => {
       if (predictions != null) {
          predictions.forEach((prediction) => {
            me.addressList.push(prediction.description);
          });
         }
       });
     });
    }

     // Select Item from list 

     chooseItem(item: any) {
      this.addressList = [];
      this.loginMapData.address=item;
      this.geoCodeAddress(this.loginMapData.address);
    }
   
   //End

   geoCodeAddress(address) {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': address }, (results, status) => {
      this.loginMapData.latitude = results[0].geometry.location.lat();
      this.loginMapData.longitude = results[0].geometry.location.lng();
      // Call services 
      this.showLoading();
      this.updateDeliveryAddress(this.loginMapData);
      //End  
        });
   }


//Services call 

updateDeliveryAddress(loginData)
{   
    this.serviceProvider.updateDeliveryAddress(loginData).then((result:any) => {
     this.loading.dismiss();
       if(result.code == 200)    
       {  
          this.toastMsg(this.translationLet.sign_in_success);
          this.storage.set('authtoken', result.token);
          this.storage.set('userDetails',result.data);
          this.storage.set('user_id',result.data.id);
          this.storage.set('lang_id',result.data.language_id);
          this.storage.set('cartCount',result.data.cartCount);
          this.app.getRootNav().setRoot(HomePage); 
       }else
       {
        this.toastMsg(this.translationLet.something_wrong_txt);
       }
    }, (err) => {
       this.loading.dismiss();
    }); 
}

//End 




//location via GPS


locationViaGPS()
{

 this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
 
 this.showLoadingGPS();

          if(this.platform.is('ios')){
           this.locationOptions = {timeout: 2000,enableHighAccuracy: true};
          }else{
            this.locationOptions={timeout: 12000, enableHighAccuracy: true};
          } 

          this.geolocation.getCurrentPosition(this.locationOptions).then((resp) => {

          this.loading.dismiss();

             if(resp.coords.latitude && resp.coords.longitude){

             //Get lat n lang  with address
            this.loginGPSData.latitude =resp.coords.latitude;
            this.loginGPSData.longitude =resp.coords.longitude;

            let options: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 1
           };
          this.nativeGeocoder.reverseGeocode(this.loginGPSData.latitude,this.loginGPSData.longitude, options)
          .then((result: NativeGeocoderReverseResult[]) => 

            this.reverseGeo(result))
            
          .catch((error: any) => 
           this.gpsError()
            );
             }else
            {
           this.gpsError()
          
            }
        
        }).catch((error) => {
            this.loading.dismiss();
            this.gpsError();
        });
          
     },
        (error) => {
        this.gpsError()
        } );
} 

reverseGeo(res)
{ 
  this.isGPSData =true;
  this.loginGPSData.address = res[0].thoroughfare+" "+res[0].subThoroughfare+" "+res[0].subLocality+" "+res[0].locality;
}

gpsError()
{
  this.isGPSData =false; 
  this.loginGPSData.address =this.translationLet.unable_to_fetch_gps;
}

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

//Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }

    showLoadingGPS() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_gps_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

//Alert Popups 

    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }

//End 

}
