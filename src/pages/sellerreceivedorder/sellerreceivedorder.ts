import { Component,ViewChild,ElementRef, Renderer } from '@angular/core';
import { Platform,Nav,MenuController ,ActionSheetController,ModalController,normalizeURL,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { SellerviewdeliveryaddressPage } from '../sellerviewdeliveryaddress/sellerviewdeliveryaddress';
import { AcceptnewtimePage } from '../acceptnewtime/acceptnewtime';
import moment from 'moment';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { ExplorePage } from '../explore/explore';
import { SharepeoplelistPage } from '../sharepeoplelist/sharepeoplelist';
import { PaymentTransactionPage } from '../payment-transaction/payment-transaction';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-sellerreceivedorder',
  templateUrl: 'sellerreceivedorder.html',
})
export class SellerreceivedorderPage {
 
  token :any ='';
	translationLet: any;	
	loading: Loading;

   	profileDetails : any = {
    name : '',
    profile_pic : '',
    mobile_no:'',
    address:'',
    email : '',
    id:'',
    min_order_charge:'',
    order_free_amt:'',
   // order_free_range:'',
    max_delivery_range:'',
    std_delivery_cost:'',
    Paypal_method: '',
    COD:'',
    order_take_away:''
  };
  myorderCount :any =0;
  receiveOrderCount :any =0;

  isFetchingData: any =true;
  addressObj :any ={};
  prodImgPath : any ='';
  userImgPath : any ='';
  lang_id :any ='';
  anotherTimeObj : any ={'id' : '','created' : '','is_free' : '' };

  sellerOrderObj : any ={'user_id' : '', 'id' : '' };

  sellerOrder :any = {'seller_id' : '','buyer_id':'','id' : '','created' : '','is_free' : ''};

  orderDetails : any ={ 'orderTrack' : [] };

  tempProductImg : any = "";

  dateTimeOld :any ='';
  dateTimeNew :any ='';
  all_months : any =[];
  newDate:any='';
  tXDate:any ='';

 constructor(public modalCtrl: ModalController,private crop: Crop,public actionSheetCtrl: ActionSheetController,public renderer: Renderer, public platform: Platform,private camera: Camera,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   }
  
//Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}

ionViewDidEnter()
{
   this.prodImgPath = this.configProvider.getProdImgPath();
    this.userImgPath = this.configProvider.getImagePath();

     this.translateService.get(['seller_address_on_map','delivery','take_away','seller_address','pick_up_time','you_got_txt','off_to_all_products','order_reject','buyer','joined_buyer','un_paid','paid','not_added_txt','order_details','order_deliver','do_you_want_deliver_order_to_buyer','order_deliver_to_buyer','rescheduled_time_send_to_buyer','do_you_want_reject_order','reject_order','order_confrim_from_your_side','yes_txt','no_txt','do_you_want_accept_order','accept','reject','order_summary','order_deliver_buyer','order_time_elapsed_can_not_accept_order','select_one_payment_method','unable_to_crop_image','service_error','call_now','order_status','view_all','order_share_with_following_frd','reschedule_order_delivery','accept_with_new_time','reject','accept','order_waiting_for_confirmation','delivery_address_on_map','company','mobile_no','surname','name','order_receiver_details','buyer_order_note','delivery_address','time','order_delivery_on','total_amt','free_delivery_txt','delivery_cost_txt','coupen_applied','pull_to_refresh','receive_orders','please_wait_txt','cancel','post_new_product','take_photo','upload_from_gallery','save','payement_method']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  

      this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;
      if( this.lang_id ==1)
        this.all_months=[{'text' : 'Jan', 'value' : 'Jan'},{'text' : 'Feb', 'value':'Feb'},{'text' : 'Mar', 'value':'March'},{'text' : 'Apr', 'value':'April'},{'text' : 'May', 'value':'May'},{'text' : 'Jun', 'value':'June'},{'text' : 'Jul', 'value':'July'},{'text' : 'Aug', 'value':'Aug'},{'text' : 'Sep', 'value':'Sep'},{'text' : 'Oct', 'value':'Oct'},{'text' : 'Nov', 'value':'Nov'},{'text' : 'Dec', 'value':'Dec'}];
           else
        this.all_months=[{'text' : 'Gennaio', 'value' : 'Jan'},{'text' : 'Febbraio', 'value' : 'Feb'},{'text' : 'Marzo', 'value' : 'Mar'},{'text' : 'Aprile', 'value' : 'Apr'},{'text' : 'Maggio', 'value' : 'May'},{'text' : 'Giugno', 'value' : 'Jun'},{'text' : 'Luglio', 'value' : 'Jul'},{'text' : 'Agosto', 'value' : 'Aug'},{'text' : 'Settembre', 'value' : 'Sep'},{'text' : 'Ottobre', 'value' : 'Oct'},{'text' : 'Novembre', 'value' : 'Nov'},{'text' : 'Dicembre', 'value' : 'Dec'}];
     });  

    this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 

     //Get details
    this.storage.get('userDetails').then((userDetails) => {
      this.profileDetails=userDetails;
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.sellerOrderObj.id=this.navParams.get('order_id');
      this.sellerOrderObj.user_id=this.navParams.get('user_id');
      this.sellerOrder.seller_id = this.navParams.get('user_id');
      this.sellerOrder.id=this.navParams.get('order_id');
      this.sellerOrder.buyer_id = this.navParams.get('buyer_id');
      this.anotherTimeObj.seller_id =this.navParams.get('user_id');
      this.anotherTimeObj.buyer_id =this.navParams.get('buyer_id');

      //Get Buyer order details
     this.sellerOrderInit(this.sellerOrderObj);
     //End 
    }); 
}

  translateDateFun(mixDate)
  {
    for(let i=0;i<this.all_months.length;i++)
{
  if(mixDate.indexOf(this.all_months[i]['text']) != -1)
  {
    this.newDate= mixDate.replace(this.all_months[i]['text'],this.all_months[i]['value']);
  }
}
return this.newDate;

  }

   /*Ionic refresher */

 doSellerOrderRefresh(refresher) {
   this.sellerOrderInitRefresh(this.sellerOrderObj,refresher);
  }

sellerOrderInitRefresh(sellerDetails,refresher)
{

//this.showLoading();

this.serviceProvider.getSellerDetails(sellerDetails,this.token,this.lang_id).then((result:any) => {
  //        this.loading.dismiss();
            if(result.code == 200){
            this.orderDetails =result.data;
             refresher.complete();
          } else
          {
             this.orderDetails=[];
             refresher.complete();
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
    //      this.loading.dismiss();
          refresher.complete();
          console.log('err '+err);
          }); 
}

   //Seller order details section 

sellerOrderInit(sellerDetails)
{

//this.showLoading();

this.serviceProvider.getSellerDetails(sellerDetails,this.token,this.lang_id).then((result:any) => {
          this.isFetchingData=false;
  //        this.loading.dismiss();
            if(result.code == 200){
              this.orderDetails =result.data;
              this.myorderCount = result.userDetails.myorderCount;
              this.receiveOrderCount = result.userDetails.receiveOrderCount;
              this.storage.set('userDetails', result.userDetails); 
          } else
          {
            this.orderDetails=[];
            this.myorderCount = result.userDetails.myorderCount;
            this.receiveOrderCount = result.userDetails.receiveOrderCount;
            this.storage.set('userDetails', result.userDetails); 
            this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
    //      this.loading.dismiss();
          console.log('err '+err);
          }); 
}

 //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  } 
  
//End

	gotoHome()
	{
    
     this.app.getRootNav().setRoot(HomePage);
	}

	gotoAccount()
	{
    this.app.getRootNav().setRoot(MyprofilePage); 
	}
  
   gotoExplore()
   {
    this.app.getRootNav().setRoot(ExplorePage); 
   }

   gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }


//Go to cover photo
    gotoCoverPhoto()
    {

       if(this.profileDetails.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

      if( (this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null ) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null )  && (this.profileDetails.std_delivery_cost == ''  || this.profileDetails.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
    }else if( (this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
       //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
  }
    } 
  }
   
  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

        //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error =>  this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
    
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
//Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   
   //End  

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

gotoAddress(address,lati,logni)
{   
	this.addressObj.address = address;
	this.addressObj.lati = lati;
	this.addressObj.logni = logni;

	this.navCtrl.push(SellerviewdeliveryaddressPage,{ 'delivery_address' : JSON.stringify(this.addressObj)} );
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerreceivedorderPage');
  }

//Ping pong concept 

gotoAccept(order_id)
{
this.tXDate = this.translateDateFun(this.orderDetails.deliveryTime);
this.dateTimeOld =moment(this.tXDate).format('x');
this.dateTimeNew =moment().format('x');

if(this.dateTimeOld < this.dateTimeNew )
{
 this.toastMsg(this.translationLet.order_time_elapsed_can_not_accept_order);
}else
{

//Confirmation 
let alertAccept = this.alertCtrl.create({
    title: this.translationLet.accept,
    message: this.translationLet.do_you_want_accept_order,
    buttons: [
      {
        text: this.translationLet.no_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {
        }
      },
      {
        text: this.translationLet.yes_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {

          //Call service and accept order 
          this.showLoading();
          this.sellerOrder.created = moment().format('YYYY-MM-DD HH:mm:ss');
          this.sellerOrder.is_free= this.orderDetails.isFree;
          this.serviceProvider.acceptOrder(this.sellerOrder,this.token, this.lang_id).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
           this.toastMsg(this.translationLet.order_confrim_from_your_side);
           this.sellerOrderInit(this.sellerOrderObj);
           //this.orderDetails.order_delivery_Status=result.status;
           //this.orderDetails.orderTrack=result.orderTrackStatus;
          } else
          {
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          }); 

          //End 

       }
      }
      
    ]
  });
    alertAccept.present();
 } 

}



//Goto Reject 

gotoReject(order_id)
{
//Rejection 

let alertReject = this.alertCtrl.create({
    title: this.translationLet.reject,
    message: this.translationLet.do_you_want_reject_order,
    buttons: [
      {
        text: this.translationLet.no_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {
        }
      },
      {
        text: this.translationLet.yes_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {

          //Call service and accept order 
          this.showLoading();
          this.sellerOrder.created = moment().format('YYYY-MM-DD HH:mm:ss');
          this.serviceProvider.rejectOrder(this.sellerOrder,this.token,this.lang_id).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
           this.toastMsg(this.translationLet.reject_order);
           this.orderDetails.order_delivery_Status=result.status;
           this.orderDetails.orderTrack=result.orderTrackStatus;
          } else
          {
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          }); 

          //End 

       }
      }
      
    ]
  });
    alertReject.present();

}

//Accept with another time End 
gotoAcceptWithTime(order_id,order_time,order_date)
{
 this.anotherTimeObj.id =this.sellerOrderObj.id;
 this.anotherTimeObj.order_time =order_time;
 this.anotherTimeObj.order_date =order_date;
 this.anotherTimeObj.is_free= this.orderDetails.isFree;
 this.anotherTimeObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
 
  let modalAcceptTime = this.modalCtrl.create(AcceptnewtimePage,{'reschuledJSON' : JSON.stringify(this.anotherTimeObj)});
    modalAcceptTime.present();
    modalAcceptTime.onDidDismiss(data => {
    if(data.status !=0)  
    {
      this.orderDetails.order_delivery_Status=data.status;
      this.orderDetails.orderTrack=data.orderTrackStatus;
      this.sellerOrderInit(this.sellerOrderObj);
      this.toastMsg(this.translationLet.rescheduled_time_send_to_buyer);
    }
    //End 
    });
}
//End 

gotosharedfriends(){
  this.navCtrl.push(SharepeoplelistPage,{ 'orderDetails' : JSON.stringify(this.sellerOrder)});
}

//Order status 

orderSendReq()
{

let alertSendeq = this.alertCtrl.create({
    title: this.translationLet.order_deliver,
    message: this.translationLet.do_you_want_deliver_order_to_buyer,
    buttons: [
      {
        text:  this.translationLet.no_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {
        }
      },
      {
         text:  this.translationLet.yes_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {

          //Call service to deliver order to buyer 
          this.showLoading();
          this.sellerOrder.created = moment().format('YYYY-MM-DD HH:mm:ss');
          this.serviceProvider.deliverOrder(this.sellerOrder,this.token,this.lang_id).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
           this.toastMsg(this.translationLet.order_deliver_to_buyer);
           this.orderDetails.order_delivery_Status=result.status;
           this.orderDetails.orderTrack=result.orderTrackStatus;
           this.storage.set('userDetails',result.userDetails);
           this.app.getRootNav().setRoot(ReceiveUpcomingPage);
          } else
          {
            this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          }); 

          //End 

       }
      }
      
    ]
  });
    alertSendeq.present();
}
//End 

//Goto Order summary 
gotoOrderSummary()
{
  this.navCtrl.push(PaymentTransactionPage,{'ordersSummary' : JSON.stringify(this.sellerOrder)});
}

//End 


}
