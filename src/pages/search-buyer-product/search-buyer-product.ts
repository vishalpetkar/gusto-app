import { Component, NgZone, Renderer, ElementRef, ViewChild } from '@angular/core';
import { ViewController, Platform, normalizeURL, ActionSheetController, Nav, MenuController, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Media, MediaObject } from '@ionic-native/media';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureAudioOptions } from '@ionic-native/media-capture';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import moment from 'moment';
import { Crop } from '@ionic-native/crop';
declare var google;
@Component({
  selector: 'page-search-buyer-product',
  templateUrl: 'search-buyer-product.html',
})
export class SearchBuyerProductPage {
  searchObj: any = {
    category_id: 0,
    delivery_opt: 1,
    takeaway_opt: 1,
    delivery_address: '',
    delivery_lati: '',
    delivery_long: '',
    delivery_addressTemp: '',
    pickup_range: 10,
    cod: 1,
    paypal: 1,
    bancomat: 1,
    user_id: '',
    start: 0,
    limit: 10,
    search_prod: '',
    isFromHome: 0,
    search_datetime: '',
    search_day: '',
    search_hr: ''
  };
  tempCancelArr: any = [];
  loading: Loading;
  isDelivery: any = true;
  isTakeway: any = true;
  addressList: any = [];
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  service = new google.maps.places.AutocompleteService();
  tempAddress: any = '';
  loginUserData: any = [];
  deliveryType: any = [{ id: '1', txt: '', checked: true }, { id: '2', txt: '', checked: true }];
  categoryList: any = [];
  translationLet: any = [];
  lang_id: any = '';

  constructor(public renderer: Renderer, public viewCtrl: ViewController, private crop: Crop, private geolocation: Geolocation, private locationAccuracy: LocationAccuracy, private nativeGeocoder: NativeGeocoder, private zone: NgZone, public platform: Platform, public configProvider: ConfigProvider, private transfer: FileTransfer, public actionSheetCtrl: ActionSheetController, private camera: Camera, private media: Media, private mediaCapture: MediaCapture, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

    //Language Id 

    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id = lang_id;
    });
    //End 

    this.translateService.get(['save', 'all_categories', 'cod', 'payment_method', 'submit', 'km_away_from_seller', 'order_type', 'cancel', 'search', 'ok_txt', 'delivery', 'take_away', 'cod', 'paypal', 'bancomat', 'pull_to_refresh', 'error_txt', 'please_select_atleast_one_payment_method', 'cancel', 'please_select_atleast_one_delivery_type', 'take_photo', 'upload_from_gallery', 'something_wrong_txt', 'unable_to_fetch_gps', 'please_wait_gps_txt', 'prod_del_err', 'prod_del_success', 'yes_txt', 'no_txt', 'delete', 'delete_product_txt', 'likes', 'view_all_comment', 'unAuthReq_msg', 'service_error', 'food_not_avail', 'load_more', 'prod_not_added', 'please_wait_txt', 'delivery_policies_err']).subscribe((translation: [string]) => {
      this.translationLet = translation;
      this.deliveryType[0]['txt'] = this.translationLet.delivery;
      this.deliveryType[1]['txt'] = this.translationLet.take_away;


    });


    this.storage.get('searchJSONObj').then((searchJSONObj) => {
      if (searchJSONObj != null) {
        this.searchObj = searchJSONObj
        this.initCategoryList();
      } else {
        this.storage.get('userDetails').then((userDetails) => {
          this.loginUserData = userDetails;
          this.searchObj.user_id = this.loginUserData.id;
          this.searchObj.delivery_address = this.loginUserData.address;
          this.searchObj.delivery_addressTemp = this.loginUserData.address;
          this.searchObj.delivery_lati = this.loginUserData.latitude;
          this.searchObj.delivery_long = this.loginUserData.longitude;
          this.initCategoryList();

        });
      }
    });
  }

  changeDelivery() {
    if (this.searchObj.delivery_opt == true) {
      this.isDelivery = true;
      this.searchObj.delivery_opt = 1;
    }
    else {
      this.isDelivery = false;
      this.searchObj.delivery_opt = 0;
    }
  }

  changeTakeAway() {
    if (this.searchObj.takeaway_opt == true) {
      this.isTakeway = true;
      this.searchObj.takeaway_opt = 1;
    }
    else {
      this.isTakeway = false;
      this.searchObj.takeaway_opt = 0;
    }
  }

  changeCOD() {
    if (this.searchObj.cod == true)
      this.searchObj.cod = 1;
    else
      this.searchObj.cod = 0;

      // this.searchObj.paypal = 0;
      // this.searchObj.bancomat = 0;
  }


  changePaypal() {
    if (this.searchObj.paypal == true)
      this.searchObj.paypal = 1;
    else
      this.searchObj.paypal = 0;

      // this.searchObj.cod = 0;
      // this.searchObj.bancomat = 0;
  }


  changebancomat() {
    if (this.searchObj.bancomat == true)
      this.searchObj.bancomat = 1;
    else
      this.searchObj.bancomat = 0;

      // this.searchObj.cod = 0;
      // this.searchObj.paypal = 0;
  }


  initCategoryList() {
    this.serviceProvider.getCatList().then((result: any) => {
      if (result.code == 200) {
        this.categoryList = result.data;
      } else if (result.code == 500) {
        //this.showPopup(this.translationLet.error,result.message);
      } else if (result.code == 404) {
        //this.showPopup(this.translationLet.error,result.message);
      }

    }, (err) => {
    });
  }

  // Dismiss
  dismiss() {
    this.tempAddress = '';
    this.addressList = [];
  }

  //Update search
  updateSearch() {
    if (this.searchObj.delivery_address.trim() == '') {
      this.addressList = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions({
      input: this.searchObj.delivery_address
    }, (predictions, status) => {
      me.addressList = [];
      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.addressList.push(prediction.description);
          });
        }
      });
    });
  }
  //End

  // Select Item from list
  chooseItem(item: any) {
    this.addressList = [];
    this.searchObj.delivery_address = item;
    this.searchObj.delivery_addressTemp = item;
    this.geoCodeAddress(this.searchObj.delivery_address);
  }
  //End

  geoCodeAddress(address) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.searchObj.delivery_lati = results[0].geometry.location.lat();
      this.searchObj.delivery_long = results[0].geometry.location.lng();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchBuyerProductPage');
    this.storage.get('searchJSONObj').then((searchJSONObj) => {
      console.log('searchJSONObj searchh', searchJSONObj);
      if (searchJSONObj != null) {
        this.searchObj = searchJSONObj
        this.storage.get('userDetails').then((userDetails) => {
          console.log('userDetails searchh...', userDetails);
          this.searchObj.delivery_address = userDetails.address;
          this.searchObj.delivery_addressTemp = userDetails.address;
        });
        this.initCategoryList();
      } else {
        this.storage.get('userDetails').then((userDetails) => {
          console.log('userDetails searchh', userDetails);
          this.loginUserData = userDetails;
          this.searchObj.user_id = this.loginUserData.id;
          this.searchObj.delivery_address = this.loginUserData.address;
          this.searchObj.delivery_addressTemp = this.loginUserData.address;
          this.searchObj.delivery_lati = this.loginUserData.latitude;
          this.searchObj.delivery_long = this.loginUserData.longitude;
          this.initCategoryList();

        });
      }
    });
  }

  cancel() {
    this.viewCtrl.dismiss(this.tempCancelArr);
  }

  async reset() {
    this.storage.get('userDetails').then(async (userDetails) => {
      // userDetails.address = '';
      userDetails.search_datetime = '';
      userDetails.search_day = '';
      userDetails.search_hr = '';
      userDetails.category_id = 0;
      userDetails.delivery_opt = 0;
      userDetails.takeaway_opt = 0;

      this.searchObj.search_datetime = '';
      this.searchObj.search_day = '';
      this.searchObj.search_hr = '';
      this.searchObj.category_id = 0;
      this.searchObj.delivery_opt = 1;
      this.searchObj.takeaway_opt = 1;

      console.log(this.searchObj);
      
      await this.storage.set('userDetails', userDetails);
      await this.storage.set('searchJSONObj', this.searchObj);
      await this.viewCtrl.dismiss(this.searchObj);
    });
  }

  submit() {
    if (this.validate()) {
      this.showloading();

      this.storage.set('searchJSONObj', this.searchObj);
      this.storage.get('userDetails').then((userDetails) => {
        userDetails.address = this.searchObj.delivery_address;
        userDetails.search_datetime = this.searchObj.search_datetime;
        userDetails.search_day = this.searchObj.search_day;
        userDetails.search_hr = this.searchObj.search_hr;
        this.storage.set('userDetails', userDetails);
      });
      this.loading.dismiss();
      this.viewCtrl.dismiss(this.searchObj);
    }
  }

  validate() {
    if (this.searchObj.delivery_opt == 0 && this.searchObj.takeaway_opt == 0) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_atleast_one_delivery_type);
      return false;
    }
    if (this.searchObj.paypal == 0 && this.searchObj.cod == 0 && this.searchObj.bancomat == 0) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_atleast_one_payment_method);
      return false;
    }

    return true;
  }


  //End
  //Show loading 

  showloading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }

  //End

  //Alert Popups
  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons: [
        {
          text: this.translationLet.ok_txt,
          cssClass: 'button popup-btn'
        }]
    });
    alert.present();
  }
  //End
}