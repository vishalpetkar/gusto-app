import { Component } from '@angular/core';
import { MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { SetpasswordPage } from '../setpassword/setpassword';

@Component({
  selector: 'page-varify',
  templateUrl: 'varify.html',
})
export class VarifyPage {
  translationLet: any;	
  loading: Loading;

loginData : any = {
phoneNumber : '',
otp : ''
};

tempOTP : any ='';

  constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.translateService.get(['otp_send_msg','please_enter_valid_otp','varify_phone_number','varify_your_number','four_digit_txt','please_enter_otp','resend_code','not_receive_code','resend_code','service_error','please_wait_txt','error_txt','ok_txt','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
   this.loginData.phoneNumber=navParams.get('phoneNumber'); 
   this.tempOTP=navParams.get('otp');
  }

  //Next with validations 

  
  submit()
{
  if(this.validate())
  {
   //Set password page
   this.navCtrl.push(SetpasswordPage,{"phoneNumber" : this.loginData.phoneNumber});
 //End 
  }
}

validate()
{

  if (this.loginData.otp =='') {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_otp);
          return false;
        }

   if (this.loginData.otp !=this.tempOTP) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_otp);
          return false;
        }

  return true;
}

resendCode()
{
  this.showLoading();
   this.serviceProvider.resendCode(this.loginData.phoneNumber).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
      this.loginData.otp ='';
     this.tempOTP= result.otp;
     //comment out that code 
     this.toastMsg(this.translationLet.otp_send_msg);
    }else if(result.code == 201){
    this.toastMsg(result.error);
    }
     else{
     this.toastMsg(this.translationLet.service_error);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
}

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

    //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 

  //End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad VarifyPage');
  }

}
