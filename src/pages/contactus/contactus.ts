import { Component,ViewChild } from '@angular/core';
import { Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { MyprofilePage } from '../myprofile/myprofile';

@Component({
  selector: 'page-contactus',
  templateUrl: 'contactus.html',
})
export class ContactusPage {

 translationLet: any;	
  loading: Loading;
  @ViewChild(Nav) nav: Nav;

  token :any ='';

  contactObj:any = {
  subject:'',
  message : '',
  id: '',
  email: '',
  name: ''
  };

  constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
     this.translateService.get(['please_enter_subject','please_enter_message','please_update_email_first','submit','message','subject','contact_us_msg','contact_us','contact_us_success_msg','pass_change_success','pass_change_err','plese_enter_pass','plese_enter_confirm_pass','pass_more_six','pass_mismatch','error_txt','ok_txt','confirm_pass','new_pass','unAuthReq_msg','something_wrong_txt','please_wait_txt','save','change_pass']).subscribe((translation: [string]) => {
        this.translationLet = translation;
      });  

     this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     });

  	//Get details
 	this.storage.get('userDetails').then((userDetails) => {
      this.contactObj.id=userDetails.id;
      this.contactObj.email=userDetails.email;
      this.contactObj.name=userDetails.name;
     }); 
  }

//Contact us functionality 

  save()
  {
    if(this.contactObj.email != '')
    {
   if(this.validateFields())
	{
	 this.showLoading();
		this.serviceProvider.contactus(this.contactObj,this.token).then((result:any) => {
		this.loading.dismiss();

		if(result.code == 200) 
    {
     this.toastMsg(this.translationLet.contact_us_success_msg );
     this.navCtrl.pop(); 
    }else if(result.code == 403) 
    {
    this.toastMsg(this.translationLet.unAuthReq_msg);
    }else if(result.code == 500) 
    {
    this.toastMsg(this.translationLet.something_wrong_txt);
    }else
    {
     this.toastMsg(this.translationLet.something_wrong_txt);
    }
		 }, (err) => {
    this.loading.dismiss();
    console.log('err '+err);
    });  
	}  
  }else
  {
    this.toastMsg(this.translationLet.please_update_email_first);
    this.app.getRootNav().setRoot(MyprofilePage); 
  }
  }


//Validations 

 validateFields()
    {
        if (this.contactObj.subject.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_subject);
          return false;
        }

         if (this.contactObj.message.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_message);
          return false;
        }

      return true;
    }

  //End 
  ionViewDidLoad() {
    console.log('ionViewDidLoad contacts ');
  }

 //Show loading 
 
    showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }

  
//End

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'btn-primary btn-round'}]
    });
    alert.present();
  }
   //End


//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End 
}
