import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ConfigProvider } from '../../providers/config/config';
import { ServiceProvider } from '../../providers/service/service';

@Component({
  selector: 'page-sellerprodview',
  templateUrl: 'sellerprodview.html',
})
export class SellerprodviewPage {
  
  translationLet: any;  
  loading: Loading; 
  token :any ='';

  tempArrList :any = {
  id : '',
  user_id : ''
  };
  
  publishTxt :any ='';
  productDetails : any =[];
  productImgPath : any ='';
  prodImg :any [];

  isValid :any =false;
  
  constructor(public platform: Platform,public configProvider : ConfigProvider,public actionSheetCtrl: ActionSheetController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   
   this.productImgPath = this.configProvider.getProdImgPath();
   this.tempArrList.id =navParams.get('id');
   this.tempArrList.user_id =navParams.get('user_id');

   this.translateService.get(['view_photos','view_product','please_wait_txt','unAuthReq_msg','ok_txt','yes_txt','no_txt','add_product','cover_photo','add_photos','information','category','name_prod','description','price','price_txt','publish_food']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });

   //Get token 
  this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
       this.initProdView();
  });
  }

  initProdView()
  {
	this.showLoading();
	this.serviceProvider.sellerProdDetails(this.tempArrList,this.token).then((result:any) => {
          this.loading.dismiss();

            if(result.code == 200){
            this.productDetails =result.data.Product;     
            this.prodImg=result.data.ProductImage;
           
            if(this.productDetails.status == 0)
        	    this.publishTxt= this.translationLet.no_txt;
            else
            	this.publishTxt= this.translationLet.yes_txt;
           
           } else if(result.code == 500) {
             this.productDetails=[];
          } else
          {
             this.productDetails=[];
          }
          }, (err) => {
        this.loading.dismiss();
          console.log('err '+err);
          }); 
  }

   //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerprodviewPage');
  }

}
