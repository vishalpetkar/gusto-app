import { Component } from '@angular/core';
import { MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {
  
  translationLet: any;	
  loading: Loading;

  resetData : any = {
  id : '',
  password:''
  };

  confirmPass:any ='';

  constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.translateService.get(['pass_mismatch','pass_more_six','plese_enter_pass','plese_enter_confirm_pass','service_error','reset_pass_successfully','confirm_new_pass','enter_new_password','enter_new_password_to_proceed','reset_youre_password','submit','please_wait_txt','error_txt','ok_txt','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
   this.resetData.id= JSON.parse(navParams.get('user_id')); 
   }

 submit()
{
  if(this.validate())
  {
    this.showLoading();
    this.serviceProvider.resetPassword(this.resetData).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
      this.toastMsg(this.translationLet.reset_pass_successfully);	
      this.app.getRootNav().setRoot(LoginPage);
    } else{
       this.toastMsg(this.translationLet.service_error);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
 }
}


validate()
{

  if (this.resetData.password.trim() =='') {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_pass);
          return false;
        }

    if (this.resetData.password.trim().length < 6) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_more_six);
          return false;
        }    

       if (this.confirmPass.trim() =='') {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_confirm_pass);
          return false;
        }

        if (this.resetData.password.trim() != this.confirmPass.trim()) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_mismatch);
          return false;
        }
   

  return true;
}

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

    showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  //End

    //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

}
