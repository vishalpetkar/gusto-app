import { Component, ViewChild, Renderer, ElementRef } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { BuyerorderdetailsPage } from '../buyerorderdetails/buyerorderdetails'; 
import { SellerprofilePage } from '../sellerprofile/sellerprofile';


@Component({
  selector: 'page-pastorder',
  templateUrl: 'pastorder.html',
})

export class PastorderPage {
  translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  orderList : any =[];
  orderObj : any = {
    user_id:'',
    start : 0,
    limit : 100
  };
  
  loadmoreData=0;
  isData : any = true ;
  isFetchingData: any =true;

  constructor(private renderer: Renderer,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

      this.translateService.get(['seller','order_expired','order_completed','order_not_received','no_past_order','order_pending','order_confirm','order_reject','order_rescheduled','view_details','you_got_txt','delivery_cost_txt','free_delivery_txt','order_rescheduled','pull_to_refresh','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();
    this.orderObj.start= 0;  
    this.orderObj.limit= 100;  
     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });
    //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.orderObj.user_id =userDetails.id; 
        this.myPastOrderList();
     }); 
  }

      //Goto User Profile section 

    gotoSellerProfile(userId)
    {
    this.app.getRootNav().push(SellerprofilePage,{'userId' : userId});
    }

    /*Ionic refresher */
 doOrderRefresh(refresher) {
    this.orderObj.start= 0;  
    this.orderObj.limit= 100;  
    this.myPastOrderListRefresher(refresher);
  }
 /* End */

// Buyer details page 
  buyerDetails(order_id,seller_id)
  {
   this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : order_id, 'seller_id' : seller_id});
  }

//End

//Order List 

  myPastOrderList()
  {
    
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);	

this.serviceProvider.myPastOrderList(this.orderObj,this.token).then((result:any) => {
          this.isFetchingData =false ;
            if(result.code == 200){
            this.orderList =result.data;     
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
  }

//End 

myPastOrderListRefresher (refresher)
{
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);  
this.serviceProvider.myPastOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.orderObj.start = parseInt(this.orderObj.start)+parseInt(this.orderObj.limit);
    this.orderObj.limit= parseInt(this.orderObj.limit)+100;

  setTimeout(() => {

           this.serviceProvider.myPastOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList = this.orderList.concat(result.data);
            this.loadmoreData=1;
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}

//End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad Past order page');
  }

 toggleSection(elmViewRef: HTMLElement, event) {
    console.log(event.target.parentElement.classList.add('active'))
    if (!elmViewRef.classList.contains('open')) {
      this.renderer.setElementStyle(elmViewRef, 'height', elmViewRef.children[0].clientHeight + 'px');
      this.renderer.setElementClass(elmViewRef, 'open', true);
      event.target.parentElement.classList.add('active')
    } else {
      this.renderer.setElementStyle(elmViewRef, 'height', '0px');
      this.renderer.setElementClass(elmViewRef, 'open', false);
      event.target.parentElement.classList.remove('active')
    }

  }

}
