import { Component, ViewChild } from '@angular/core';
import { Platform, normalizeURL, ActionSheetController, Nav, MenuController, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Media, MediaObject } from '@ionic-native/media';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureAudioOptions } from '@ionic-native/media-capture';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';
import { Crop } from '@ionic-native/crop';

@Component({
    selector: 'page-editprofile',
    templateUrl: 'editprofile.html',
})
export class EditprofilePage {
    translationLet: any;
    loading: Loading;
    userDetails: any = {
        name: '',
        profile_pic: '',
        mobile_no: '',
        email: '',
        id: '',
        delivery_name: '',
        delivery_surname: '',
        delivery_phone: '',
        delivery_comp: '',
        isPromoCode: '',
        promoCode: '',
        promoDiscount: '',
        order_delivery: '',
        order_take_away: '',
        pick_address:''
    };
    tempArr: any = {
        name: '',
        profile_pic: ''
    }
    imagePath: any = '';
    apiPath: any = '';
    isphoto: any = false;
    token: any = '';
    uniqueuserObj: any = {
        user_id: '',
        name: ''
    };

    constructor(private crop: Crop, public platform: Platform, public configProvider: ConfigProvider, private transfer: FileTransfer, public actionSheetCtrl: ActionSheetController, private camera: Camera, private media: Media, private mediaCapture: MediaCapture, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
        this.imagePath = this.configProvider.getImagePath();
        this.apiPath = this.configProvider.getAPIpath();
        this.translateService.get(['order_policies', 'please_select_atleast_one_order_policies', 'delivery', 'take_away', 'discount_less_than', 'please_add_promo_code', 'please_add_discount', 'amt_must_positive', 'allow_promo_code_for_all_items', 'discount', 'promo_code', 'allow_promo_code', 'please_enter_surname', 'please_enter_name', 'please_enter_user_name', 'unable_to_crop_image', 'name', 'surname', 'company', 'username', 'please_enter_valid_phone', 'user_name_avail', 'user_name_already_exist', 'mobile_already_exist', 'emiil_already_exist', 'user_name_not_more_than', 'profile_update_success', 'profile_update_err', 'email_already_exist', 'something_wrong_txt', 'unAuthReq_msg', 'err_upload_file', 'cancel', 'order_type', 'upload_profile_image', 'take_photo', 'upload_from_gallery', 'please_enter_valid_mobile', 'please_enter_mobile', 'email_not_valid', 'error_txt', 'please_enter_email', 'please_wait_txt', 'basic_info', 'ok_txt', 'edit_profile', 'update_profile', 'name', 'mobile_no', 'email']).subscribe((translation: [string]) => {
            this.translationLet = translation;
        });

        //Get details
        this.storage.get('userDetails').then((userDetails) => {
            this.userDetails = userDetails;
            if (this.userDetails.delivery_name == null || this.userDetails.delivery_name == '')
                this.userDetails.delivery_name = '';
            if (this.userDetails.delivery_surname == null || this.userDetails.delivery_surname == '')
                this.userDetails.delivery_surname = '';
            if (this.userDetails.delivery_comp == null || this.userDetails.delivery_comp == '')
                this.userDetails.delivery_comp = '';

            this.uniqueuserObj.user_id = this.userDetails.id;
            if (userDetails['profile_pic'] != '') {
                this.userDetails.profile_pic = userDetails['profile_pic'];
                this.tempArr.profile_pic = userDetails['profile_pic'];
            } else {
                this.userDetails.profile_pic = '';
                this.tempArr.profile_pic = '';
            }
            this.tempArr.name = userDetails.name;
            //For Promo code
            if (this.userDetails.isPromoCode == 1)
                this.userDetails.isPromoCode = true;
            else
                this.userDetails.isPromoCode = false;
            if (this.userDetails.order_delivery == 1)
                this.userDetails.order_delivery = true;
            else
                this.userDetails.order_delivery = false;
            if (this.userDetails.order_take_away == 1)
                this.userDetails.order_take_away = true;
            else
                this.userDetails.order_take_away = false;
        });

        //Get token
        this.storage.get('authtoken').then((authtoken) => {
            this.token = authtoken;
        });
    }
    //Upload image
    changePic() {
        let actionSheet = this.actionSheetCtrl.create({
            title: this.translationLet.upload_profile_image,
            buttons: [
                {
                    text: this.translationLet.take_photo,
                    handler: () => {
                        this.takePhoto();
                    }
                }, {
                    text: this.translationLet.upload_from_gallery,
                    handler: () => {
                        this.takePhotoGallery();
                    }
                }, {
                    text: this.translationLet.cancel,
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }
    //Take a photo
    takePhoto() {
        const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
        //Get picture function
        this.camera.getPicture(options).then((imageData) => {
            //Crop Image
            this.crop.crop(imageData, { quality: 100 })
                .then(
                    newImage => this.CropedImg(newImage),
                    error => this.toastMsg(this.translationLet.unable_to_crop_image)
                );
            //End
        }, (err) => {
        });
    }
    takePhotoGallery() {
        const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
        //Get picture function
        this.camera.getPicture(options).then((imageData) => {
            //Crop Image
            this.crop.crop(imageData, { quality: 100 })
                .then(
                    newImage => this.CropedImg(newImage),
                    error => this.toastMsg(this.translationLet.unable_to_crop_image)
                );
            //End
        }, (err) => {
        });
    }
    CropedImg(imageData) {
        this.isphoto = true;
        if (this.platform.is('ios')) {
            this.tempArr.profile_pic = normalizeURL(imageData);
        } else {
            this.tempArr.profile_pic = imageData;
        }
    }
    //Update profile
    submit() {

        if (this.validateFields()) {
            //For Promo code
            // if (this.userDetails.isPromoCode == true) {
            //     this.userDetails.isPromoCode = 1;
            // }
            // else {
            //     this.userDetails.isPromoCode = 0;
            //     this.userDetails.promoCode = "";
            //     this.userDetails.promoDiscount = "";
            // }
            //Update order policies
            if (this.userDetails.order_delivery == true)
                this.userDetails.order_delivery = 1;
            else
                this.userDetails.order_delivery = 0;

            if (this.userDetails.order_take_away == true)
                this.userDetails.order_take_away = 1;
            else
                this.userDetails.order_take_away = 0;
            //Update order policies End

            if (this.isphoto == false) {
                this.showLoading();
                this.serviceProvider.updateProfile(this.userDetails, this.token).then((result: any) => {
                    this.loading.dismiss();

                    if (result.code == 200) {
                        this.toastMsg(this.translationLet.profile_update_success);
                        this.storage.set('userDetails', result.user_detail);
                        this.navCtrl.pop();
                    } else if (result.code == 500) {
                        if (result.status == 1)
                            this.toastMsg(this.translationLet.user_name_already_exist);
                        else if (result.status == 2)
                            this.toastMsg(this.translationLet.email_already_exist);
                        else if (result.status == 5)
                            this.toastMsg(this.translationLet.mobile_already_exist);
                        else
                            this.toastMsg(this.translationLet.profile_update_err);
                    }
                    else if (result.code == 401) {
                        this.toastMsg(this.translationLet.email_already_exist);
                    }
                    else {
                        this.toastMsg(this.translationLet.something_wrong_txt);
                    }
                }, (err) => {
                    this.loading.dismiss();
                    console.log('err ' + err);
                });
            } else {
                this.uploadImage(this.tempArr.profile_pic);

            }
        }
    }
    public uploadImage(file_uri) {

        var url = this.apiPath + "profileImageUpdate";
        // File for Upload
        var targetPath = file_uri;
        // File name only
        var filename = 'image.jpg';
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename, 'id': this.userDetails.id },
            headers: { 'Authorization': 'Bearer ' + this.token }
        };
        const fileTransfer: FileTransferObject = this.transfer.create();
        // Use the FileTransfer to upload the image
        this.showLoading();

        fileTransfer.upload(targetPath, url, options).then(data => {
            var jsonObject: any = JSON.parse(data.response);
            // this.loading.dismiss();
            //this.toastMsg(jsonObject.message);
            let imgResponse: any;
            if (jsonObject.code == 200) {
                this.userDetails.profile_pic = jsonObject.data;
                // this.showLoading();
                this.serviceProvider.updateProfile(this.userDetails, this.token).then((result: any) => {
                    this.loading.dismiss();
                    if (result.code == 200) {
                        this.toastMsg(this.translationLet.profile_update_success);
                        this.storage.set('userDetails', result.user_detail);
                        this.navCtrl.pop();
                    } else if (result.code == 500) {
                        if (result.status == 1)
                            this.toastMsg(this.translationLet.user_name_already_exist);
                        else if (result.status == 2)
                            this.toastMsg(this.translationLet.email_already_exist);
                        else if (result.status == 5)
                            this.toastMsg(this.translationLet.mobile_already_exist);
                        else
                            this.toastMsg(this.translationLet.profile_update_err);
                    } else if (result.code == 401) {
                        this.toastMsg(this.translationLet.email_already_exist);
                    } else {
                        this.toastMsg(this.translationLet.something_wrong_txt);
                    }
                }, (err) => {
                    this.loading.dismiss();
                    this.toastMsg(this.translationLet.something_wrong_txt);
                });
            } else if (jsonObject.code == 500) {
                this.loading.dismiss();
                this.toastMsg(this.translationLet.something_wrong_txt);
            } else if (jsonObject.code == 403) {
                this.loading.dismiss();
                this.toastMsg(this.translationLet.unAuthReq_msg);
            }

        }, err => {
            this.loading.dismiss();
            this.toastMsg(this.translationLet.err_upload_file);
        });
    }
    //validation starts
    validateFields() {
        let emailValid = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        //let phoneno = /^([0|+[0-9]{1,5})?([7-9][0-9]{9})$/;

        if (this.userDetails.name.trim() == "") {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_user_name);
            return false;
        }

        if (this.userDetails.name.length > 30) {
            this.showPopup(this.translationLet.error_txt, this.translationLet.user_name_not_more_than);
            return false;
        }
        if (this.userDetails.delivery_name.trim() == "") {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_name);
            return false;
        }
        if (this.userDetails.delivery_surname.trim() == "") {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_surname);
            return false;
        }
        if (this.userDetails.email.trim() == "") {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_email);
            return false;
        }
        if (this.userDetails.mobile_no.trim() == "") {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_mobile);
            return false;
        }
        if ((this.userDetails.mobile_no.length < 8) || (this.userDetails.mobile_no.length > 14)) {
            this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_valid_mobile);
            return false;
        }
        if (!emailValid.test(this.userDetails.email) && (this.userDetails.email != '')) {
            this.showPopup(this.translationLet.error_txt, this.translationLet.email_not_valid);
            return false;
        }

        // if (this.userDetails.order_delivery == false && this.userDetails.order_take_away == false) {
        //     this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_atleast_one_order_policies);
        //     return false;
        // }
        // if (this.userDetails.isPromoCode == true) {

        //     if (this.userDetails.promoCode.trim() == "") {
        //         this.showPopup(this.translationLet.error_txt, this.translationLet.please_add_promo_code);
        //         return false;
        //     }
        //     if (this.userDetails.promoDiscount.trim() == "") {
        //         this.showPopup(this.translationLet.error_txt, this.translationLet.please_add_discount);
        //         return false;
        //     }
        //     if (Math.sign(this.userDetails.promoDiscount.trim()) == -1) {
        //         this.showPopup(this.translationLet.error_txt, this.translationLet.amt_must_positive);
        //         return false;
        //     }
        //     if (this.userDetails.promoDiscount.trim() > 100) {
        //         this.showPopup(this.translationLet.error_txt, this.translationLet.discount_less_than);
        //         return false;
        //     }
        // }



        return true;
    }
    //Toast Msg
    toastMsg(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present(toast);
    }
    //End
    //Show loading
    showLoading() {
        this.loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loading-section',
            content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
        });
        this.loading.present();
    }

    //End
    //Alert Popups
    showPopup(title, text) {
        let alert = this.alertCtrl.create({
            title: title,
            message: text,
            buttons: [
                {
                    text: this.translationLet.ok_txt,
                    cssClass: 'btn-primary btn-round'
                }]
        });
        alert.present();
    }
    //End
    ionViewDidLoad() {
        console.log('ionViewDidLoad EditprofilePage');
    }
    //Check User name unqiueness
    uniqueName(item) {

        this.uniqueuserObj.name = item.trim();
        if (this.uniqueuserObj.name.length > 0) {
            this.showLoading();
            this.serviceProvider.uniqueLoginUserName(this.uniqueuserObj, this.token).then((result: any) => {
                this.loading.dismiss();
                if (result.code == 200) {
                    this.toastMsg(this.translationLet.user_name_already_exist);
                } else {
                    this.toastMsg(this.translationLet.user_name_avail);
                }
            }, (err) => {
                this.loading.dismiss();
            });
        }

    }
}