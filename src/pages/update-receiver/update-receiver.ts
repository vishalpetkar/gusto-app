import { Component } from '@angular/core';
import { ViewController,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { SelectaddressPage } from '../selectaddress/selectaddress';
import { HomePage } from '../home/home';
import moment from 'moment';

@Component({
  selector: 'page-update-receiver',
  templateUrl: 'update-receiver.html',
})
export class UpdateReceiverPage {
  
  translationLet: any;	
  loading: Loading;

    receiverDetails : any = {
   	id : '',
    delivery_surname: '',
    delivery_name : '',
    delivery_mobile : '',
    delivery_company : '',
    buyer_id: ''
    };
    tempObj :any =[]; 
    token :any ='';
    tempArr :any =[];

constructor(public viewCtrl: ViewController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
 
     this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 

   this.translateService.get(['unable_to_update_receiver_details','receiver_details_update_success','order_receiver_details','order_receiver_details','please_enter_four_digit_for_year','ok_txt','please_select_prod_delivery_date','please_select_prod_delivery_time','please_enter_delivery_receiver_name','please_enter_credit_card_number','please_enter_expire_month','card_year_expire_numeric','card_cvv_numeric','please_enter_valid_phone','please_enter_cvv','card_month_expire_numeric','please_enter_expire_year','card_number_numeric','please_select_card_type','please_select_payment_method','date_must_greter_than_current_date','please_enter_delivery_receiver_mobile_no','please_enter_deliver_receiver_surname','service_error','please_select_date','please_select_time','deliver_to','name','surname','mobile_no','company','note','amt','with_delivery_cost','deduct_amount_seller_accept_txt1','reaches_delivery_policies_amt','deduct_amount_seller_accept_txt2','select_payment_method','credit_card_number','expire_month','expire_year','card_cvv','credit_card_details','update','next_txt','seller_not_selected_any_payment_method_yet','schedule_order','delivery_address','error_txt','please_wait_txt']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        });

	 this.tempObj=JSON.parse(navParams.get('receiverDetails')); 
		 this.receiverDetails.id=this.tempObj.orderIds;
		 this.receiverDetails.delivery_surname= this.tempObj.delivery_surname;
		 this.receiverDetails.delivery_name=this.tempObj.delivery_name;
		 this.receiverDetails.delivery_mobile=this.tempObj.delivery_mobile;
		 this.receiverDetails.delivery_company=this.tempObj.delivery_company;
		 this.receiverDetails.buyer_id= this.tempObj.buyerIds;
  }
  
//Submit and validtions 

submit()
{
 if(this.validate())
  {
  this.showLoading();
    this.serviceProvider.updateReceiverDetails(this.receiverDetails,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     this.toastMsg(this.translationLet.receiver_details_update_success);
     this.viewCtrl.dismiss(this.receiverDetails);
    }
    else
    {
     this.viewCtrl.dismiss(this.tempArr);
    this.toastMsg(this.translationLet.unable_to_update_receiver_details);
    }
        }, (err) => {
          this.loading.dismiss();
        });
  }
}



   //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End


//Alert Popups 

    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'btn-primary btn-round'}]
    });
    alert.present();
  }

//End 


validate()
{

if (this.receiverDetails.delivery_name.trim()== "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_delivery_receiver_name);
          return false;
        }
  if (this.receiverDetails.delivery_surname.trim()== "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_deliver_receiver_surname);
          return false;
        }
  if (this.receiverDetails.delivery_mobile.trim()== "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_delivery_receiver_mobile_no);
          return false;
        }
         if ( (this.receiverDetails.delivery_mobile.length < 8) || (this.receiverDetails.delivery_mobile.length > 14) ) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_phone);
          return false;
        }
 return true;
}

  //Show loading 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

  cancel()
  {
  this.viewCtrl.dismiss(this.tempArr);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad updatereceiverpage');
  }

}
