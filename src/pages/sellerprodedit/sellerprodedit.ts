import { Component,NgZone, ElementRef, ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Media, MediaObject } from '@ionic-native/media';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions,CaptureAudioOptions } from '@ionic-native/media-capture';
import { FileTransfer,FileUploadOptions,FileTransferObject} from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { HomePage } from '../home/home';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import moment from 'moment';
import { Crop } from '@ionic-native/crop';

declare var google;
@Component({
  selector: 'page-sellerprodedit',
  templateUrl: 'sellerprodedit.html',
})
export class SellerprodeditPage {
  translationLet: any;  
  loading: Loading; 
  apiPath :any =''; 
  productImgPath :any ='';
  token :any ='';
  tempCoverPhoto :any ='';

  productDetails : any = {
    user_id : '',
    cover_photo : '',
    id: '',
    description : '',
    name : '',
    category_id : '',
    amount:'',
    status:'',
    created:'',
    address:'',
    latitude:'',
    longitude:'',
    promoCode:'',
    isPromoCode:'',
    promoDiscount:''
  };

  addressList :any =[];
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  service = new google.maps.places.AutocompleteService(); 
  
  isphoto:any = false;
  isFetchingData :any =true;
  prodContainer : any =[];

  prodDelObj:any = {
    user_id: '',
    name : '',
    id:'',
   };
  
   tempObj :any ={
  	id: '',
  	user_id : ''
  }; 

  publishProd : any = [{'val' : '1','txt' : ''},{'val' : '0','txt' : ''}];

  categoryList : any =[];
  
 constructor(private crop: Crop,private nativeGeocoder: NativeGeocoder,private zone: NgZone,public platform: Platform,public configProvider : ConfigProvider,private transfer: FileTransfer,public actionSheetCtrl: ActionSheetController,private camera: Camera,private media: Media,private mediaCapture: MediaCapture,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   
   this.apiPath = this.configProvider.getAPIpath();
   this.productImgPath = this.configProvider.getProdImgPath();
   this.productDetails.id =navParams.get('id');
   
   this.tempObj.user_id=navParams.get('user_id');
   this.tempObj.id=navParams.get('id');
   this.prodDelObj.user_id=navParams.get('user_id');


   this.translateService.get(['save','unable_to_crop_image','amt_must_positive','allow_promo_code','promo_code','discount','please_add_discount','discount_less_than','please_add_promo_code','location','please_enter_location','please_enter_address','product_name_not_more_than','please_enter_number_only','product_cover_photo','prod_details_not','product_update_sucess','view_photos','edit_product','error_txt','please_select_category','please_enter_name_product','please_enter_price','please_select_prod_status','product_add_success','service_error','please_wait_txt','delete','delete_txt','unable_to_del','prod_del_image','err_while_upload_file','unable_capture_image','unAuthReq_msg','err_upload_file','product_img_sucess','upload_product_photo','take_photo','upload_from_gallery','cancel','ok_txt','yes_txt','no_txt','add_product','cover_photo','add_photos','information','category','name_prod','description','price','price_txt','publish_food','submit','']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });

   this.publishProd[0]['txt'] =this.translationLet.yes_txt;
   this.publishProd[1]['txt'] =this.translationLet.no_txt;
 
   //Get token 
  this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
      this.initEditProduct();
  });

  }

  //Functionality started 

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 
  
  
  //Autocompleted search and get google location from services 
    
    dismiss()
      {
        this.productDetails.address ="";
        this.addressList =[];
      }

    updateSearch() {

       if (this.productDetails.address.trim() == '') {
       this.addressList = [];
       return;
      }
     
      let me = this;
      this.service.getPlacePredictions({
      input: this.productDetails.address
     }, (predictions, status) => {
       me.addressList = [];
       me.zone.run(() => {
       if (predictions != null) {
          predictions.forEach((prediction) => {
            me.addressList.push(prediction.description);
          });
         }
       });
     });
    }

     // Select Item from list 

     chooseItem(item: any) {
      this.addressList = [];
      this.productDetails.address=item;
      this.geoCodeAddress(this.productDetails.address);
    }
   
   //End

   geoCodeAddress(address) {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': address }, (results, status) => {
      this.productDetails.latitude = results[0].geometry.location.lat();
      this.productDetails.longitude = results[0].geometry.location.lng();
        });
   }


  //Edit product   

  initEditProduct()
  {

   this.serviceProvider.getProductRecords(this.tempObj,this.token).then((result:any) => {
    if(result.code == 200){
      this.isFetchingData=false;
      this.categoryList =result.data.categoryList;
      this.prodContainer=result.data.ProductImage; 
      this.productDetails =result.data.Product;
 
      if(this.productDetails.status ==1)
       this.productDetails.status = true;
      else
       this.productDetails.status = false;

     //For Promo code 

     if(this.productDetails.isPromoCode ==1)
       this.productDetails.isPromoCode = true;
      else
       this.productDetails.isPromoCode = false;

     //End 

      this.tempCoverPhoto =this.productDetails.cover_photo;
    } else if(result.code == 500) {
      this.toastMsg(this.translationLet.prod_details_not);
    } else if(result.code == 403) {
      this.toastMsg(this.translationLet.unAuthReq_msg);
    } 
    
    }, (err) => {
    });
  }
  //End 

//Multiple image code 

//Take picture 

gotoPhoto ()
{

 let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.upload_product_photo,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
} 
//End 

//Take a photo 

takePhoto ()
{

const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
//Get picture function 

this.camera.getPicture(options).then((imageData) => {
   //Crop Image 
  this.crop.crop(imageData, {quality: 100})
    .then(
      newImage => this.CropedProdImg(newImage),
      error =>  this.toastMsg(this.translationLet.unable_to_crop_image)
    );
  //End 

 }, (err) => {
console.log(this.translationLet.unable_capture_image);
}); 
}

takePhotoGallery ()
{
const options: CameraOptions = {
      quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,                                                                                                         
            correctOrientation: true,
            targetWidth: 900,                                       
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
  //Get picture function 
  this.camera.getPicture(options).then((imageData) => {

   //Crop Image 
  this.crop.crop(imageData, {quality: 100})
    .then(
      newImage => this.CropedProdImg(newImage),
     error => this.toastMsg(this.translationLet.unable_to_crop_image)
    );
  //End 
}, (err) => {
 console.log(this.translationLet.unable_capture_image);
})
}

//Crop Product image 

CropedProdImg(imageData)
{
 this.productDetails.user_id =this.navParams.get('user_id'); 
 var url = this.apiPath+"UploadImgSave";
  // File for Upload
  var targetPath = imageData;
 
  // File name only
  var filename = 'image.jpg';
  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename,'id':this.productDetails.user_id,'product_id' :  this.productDetails.id},
    headers: {'Authorization': 'Bearer '+this.token}
  };
 
const fileTransfer: FileTransferObject = this.transfer.create();

 this.showLoading();
 
  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismiss();
    var jsonObject : any = JSON.parse(data.response);
    let imgResponse:any;
  
    if(jsonObject.code == 200){
       this.prodContainer.push({'name' : jsonObject.data.name, 'id' :jsonObject.data.id }); 
      this.toastMsg(this.translationLet.product_img_sucess);
    }else if(jsonObject.code == 400){
     this.toastMsg(this.translationLet.service_error);
    }
     else if(jsonObject.code == 500){
      this.loading.dismiss();
    this.toastMsg(this.translationLet.err_upload_file);
    } else if(jsonObject.code == 403){
      this.loading.dismiss();
      this.toastMsg(this.translationLet.unAuthReq_msg);
    }
  }, err => {
    this.loading.dismiss();
   this.toastMsg(this.translationLet.err_while_upload_file);
  }); 
}

//Delete Image from container


delImg(indexVal,resImg)
{
  let alertDel = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.delete_txt,
    buttons: [
    {
        text: this.translationLet.yes_txt,
        cssClass:'button popup-btn',
        handler: () => {
     
//Delete API 

this.prodDelObj.id=resImg.id;
this.prodDelObj.name=resImg.name;

this.serviceProvider.delEditProdImage(this.prodDelObj,this.token).then((res:any) => {
           if(res.code == 200)
           {
            this.prodContainer.splice(indexVal, 1);
             this.toastMsg(this.translationLet.prod_del_image);
           } else if(res.code == 403){
             this.toastMsg(this.translationLet.unAuthReq_msg);
           } else if(res.code == 500) {
             this.toastMsg(this.translationLet.unable_to_del);
           }
          }); 
   
        }
      },
      { 
        text: this.translationLet.no_txt,
        cssClass:'button popup-btn',
        handler: () => {
        }
      }
      
    ]
  });
    alertDel.present();
}


//Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End


//Edit product Details  

submit()
{
if(this.validate())
  {
  	 if(this.isphoto == true){
         this.uploadCoverPhoto(this.tempCoverPhoto);
  	 }else
  	 {

      if(this.productDetails.status == true )
          this.productDetails.status= 1;
        else
        this.productDetails.status= 0;

      //For Promo code 

      if(this.productDetails.isPromoCode == true )
          this.productDetails.isPromoCode= 1;
        else
        this.productDetails.isPromoCode= 0;

      //ENd

        this.productDetails.user_id =this.navParams.get('user_id');
  	 	this.productDetails.created = moment().format('YYYY-MM-DD HH:mm:ss');
        this.serviceProvider.editproductData(this.productDetails, this.token).then((result:any) => {
          if(result.code == 200 ){
           this.toastMsg(this.translationLet.product_update_sucess);
             if(this.navParams.get('pagePush') == 'seller')
            this.navCtrl.push(SellerproductsPage);
            else
            this.navCtrl.push(HomePage);  
          } else if(result.code == 403){
             this.toastMsg(this.translationLet.unAuthReq_msg);
          } else {
               this.toastMsg(this.translationLet.service_error);
          }
          }); 
  	 }
  
  }

} 

//End 


public uploadCoverPhoto(file_uri) {

this.productDetails.user_id =this.navParams.get('user_id');
  //this.showLoading();

var url =this.apiPath+"productCoverPhoto";
  // File for Upload
  var targetPath = file_uri;
  // File name only
  var filename = 'image.jpg';
  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename,'id':this.productDetails.user_id},
    headers: {'Authorization': 'Bearer '+this.token}
  };

  const fileTransfer: FileTransferObject = this.transfer.create();
  // Use the FileTransfer to upload the image
  
  fileTransfer.upload(targetPath, url, options).then(data => {

    var jsonObject : any = JSON.parse(data.response);
    let imgResponse:any;

    if(jsonObject.code == 200){

      this.productDetails.cover_photo=jsonObject.data;
      
      //Call services and END
        this.productDetails.user_id =this.navParams.get('user_id');
  	 	this.productDetails.created = moment().format('YYYY-MM-DD HH:mm:ss');
        if(this.productDetails.status == true )
          this.productDetails.status= 1;
        else
        this.productDetails.status= 0;

       if(this.productDetails.isPromoCode == true )
          this.productDetails.isPromoCode= 1;
        else
        this.productDetails.isPromoCode= 0;

      
       

        this.serviceProvider.editproductData(this.productDetails, this.token).then((result:any) => {
      //    this.loading.dismiss(); 
          if(result.code == 200 ){
           this.toastMsg(this.translationLet.product_update_sucess);
           if(this.navParams.get('pagePush') == 'seller')
            this.navCtrl.push(SellerproductsPage);
            else
            this.navCtrl.push(HomePage);  
          } else if(result.code == 403){
             this.toastMsg(this.translationLet.unAuthReq_msg);
          } else {
               this.toastMsg(this.translationLet.service_error);
          }
          }); 
      //End 
       
    } else if(jsonObject.code == 500){
    //  this.loading.dismiss(); 
      this.toastMsg(this.translationLet.service_error);
    } else if(jsonObject.code == 403){
    //  this.loading.dismiss();	
      this.toastMsg(this.translationLet.unAuthReq_msg);
    } 
   
  }, err => {
   // this.loading.dismiss();
    this.toastMsg(this.translationLet.err_upload_file);
  });

}


  ionViewDidLoad() {
    console.log('ionViewDidLoad AddproductformPage');
  }

validate()
{
  if (this.productDetails.category_id == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_select_category);
          return false;
        }

  if (this.productDetails.name.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_name_product);
          return false;
        }

    if (this.productDetails.name.length > 30) {
           this.showPopup(this.translationLet.error_txt,this.translationLet.product_name_not_more_than);
          return false;
        }
   
    if (this.productDetails.address.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_location);
          return false;
        }


  if (this.productDetails.amount.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_price);
          return false;
        }     
      
        if (Math.sign(this.productDetails.amount.trim()) == -1) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.amt_must_positive);
          return false;
        }

   if (isNaN(this.productDetails.amount)) {
         this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_number_only);
          return false;
        }

         if (this.productDetails.isPromoCode == true) {
         
            if (this.productDetails.promoCode.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_add_promo_code);
          return false;
        }

         if (this.productDetails.promoDiscount.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_add_discount);
          return false;
        }

        if (Math.sign(this.productDetails.promoDiscount.trim()) == -1) {
           this.showPopup(this.translationLet.error_txt,this.translationLet.amt_must_positive);
          return false;
        }

         if (this.productDetails.promoDiscount.trim() > 100) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.discount_less_than);
          return false;
        }

        }
        
  return true;
} 

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

//Edit cover photo 

changeCoverPic()
{
	let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.product_cover_photo,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takeCoverPhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takeCoverPhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
}


//Take a photo 

takeCoverPhoto ()
{
const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
           targetWidth: 700,
            targetHeight: 700,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
//Get picture function 
this.camera.getPicture(options).then((imageData) => {

 //Crop Image 
this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedCoverImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
 }, (err) => {
console.log(this.translationLet.unable_capture_image);
});

}
//Croped cover photo 

CropedCoverImg(imageData)
{
  this.isphoto=true;

   if(this.platform.is('ios'))
  {
  this.tempCoverPhoto  = normalizeURL(imageData);
  }else
  {
  this.tempCoverPhoto =imageData;
  }
}

takeCoverPhotoGallery ()
{
const options: CameraOptions = {
      quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,                                                                                                         
            correctOrientation: true,
            targetWidth: 700,                                       
            targetHeight: 700,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
//Get picture function 
this.camera.getPicture(options).then((imageData) => {
  //Crop Image 
this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedCoverImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

}, (err) => {
console.log(this.translationLet.unable_capture_image);
});

}
  
}
