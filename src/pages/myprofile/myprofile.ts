import { Component, NgZone, ViewChild, ElementRef, Renderer } from '@angular/core';
import { ModalController, Platform, Nav, MenuController, ActionSheetController, normalizeURL, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { EditprofilePage } from '../editprofile/editprofile';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MycartPage } from '../mycart/mycart';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import { NotificationPage } from '../notification/notification';
import { ExplorePage } from '../explore/explore';
import { SharedfriendsPage } from '../sharedfriends/sharedfriends';
import { TransactionsPage } from '../transactions/transactions';
import { JoinorderhistoryPage } from '../joinorderhistory/joinorderhistory';
import { PrivacyPoliciesPage } from '../privacy-policies/privacy-policies';
import { DisclaimerPage } from '../disclaimer/disclaimer';
import { ContactusPage } from '../contactus/contactus';
import { elementAt } from 'rxjs/operator/elementAt';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
declare var google;
@Component({
  selector: 'page-myprofile',
  templateUrl: 'myprofile.html',
})
export class MyprofilePage {
  morning = "morning"; /*Segment active*/
  translationLet: any;
  loading: Loading;
  @ViewChild(Nav) nav: Nav;
  @ViewChild('toggledivelm', { read: ElementRef }) toggledivelm;
  @ViewChild('togglebtnelmDivTime', { read: ElementRef }) togglebtnelmDivTime;
  @ViewChild('togglebtnelmTime', { read: ElementRef }) togglebtnelmTime;
  @ViewChild('insetcontainer') insetcontainer;
  @ViewChild('insetTimecontainer') insetTimecontainer;
  @ViewChild('togglebtnelm', { read: ElementRef }) togglebtnelm;
  @ViewChild('togglebtnelmOrderType', { read: ElementRef }) togglebtnelmOrderType;
  @ViewChild('togglebtnelmOrderTypeDiv', { read: ElementRef }) togglebtnelmOrderTypeDiv;
  @ViewChild('ordertypecontainer') ordertypecontainer;
  @ViewChild('togglebtnelmOrderPromo', { read: ElementRef }) togglebtnelmOrderPromo;
  @ViewChild('togglebtnelmOrderPromoDiv', { read: ElementRef }) togglebtnelmOrderPromoDiv;
  @ViewChild('orderpromocontainer') orderpromocontainer;
  public hideElement: boolean = false;
  public hideTimeElement: boolean = false;
  public hideDeliveryType: boolean = false;
  public hidePromoCode: boolean = false;
  public hidePickupAddr: boolean = false;

  langList = [{ id: 1, txt: 'assets/images/english.png', value: 'en' }, { id: 2, txt: 'assets/images/italian.png', value: 'it' }];

  paymentMethod = [{ id: 1, txt: 'assets/images/cod.png' }, { id: 2, txt: 'assets/images/paypal.png' }, { id: 3, txt: 'assets/images/bancomat.png' }];

  profileDetails: any = {
    name: '',
    profile_pic: '',
    mobile_no: '',
    address: '',
    email: '',
    id: '',
    min_order_charge: '',
    order_free_amt: '',
    // order_free_range:'',
    max_delivery_range: '',
    std_delivery_cost: '',
    Paypal_method: '',
    paypal_client_id: '',
    paypal_secret_key: '',
    COD: '',
    order_delivery: '',
    order_take_away: '',
    pick_address: '',
    delivery_lati: '',
    delivery_long: '',
    delivery_addressTemp: '',
    isPromoCode: '',
    promoCode: '',
    promoDiscount: ''
  };
  cartCount: any = 0;
  productCount: any = 0;
  orderCount: any = 0;
  orderReceiveCount: any = 0;
  notificationCount: any = 0;
  shareFriendCount: any = 0;
  txHistoryCount: any = 0;
  joinOrderCount: any = 0;
  token: any = '';
  langBtn: any = '';
  imagePath: any = '';

  paypalItem: any = "";
  myorderCount: any = 0;
  receiveOrderCount: any = 0;
  tempImg: any = '';
  tempProductImg: any = "";
  timeSLots: any = { 'user_id': '', 'isTimePolicy': '', 'sellerOrderTime': '', 'avaibility': [{ 'from': '', 'to': '' }, { 'from': '', 'to': '' }, { 'from': '', 'to': '' }, { 'from': '', 'to': '' }, { 'from': '', 'to': '' }, { 'from': '', 'to': '' }, { 'from': '', 'to': '' }] };
  timeSlotList: any = [];
  addressList: any = [];
  service = new google.maps.places.AutocompleteService();

  constructor(public modalCtrl: ModalController, private crop: Crop, public actionSheetCtrl: ActionSheetController, public renderer: Renderer, public platform: Platform, private camera: Camera, public configProvider: ConfigProvider, private fb: Facebook, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private zone: NgZone) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyprofilePage');
    this.hideDeliveryType = true;
    this.hidePromoCode = true;
    this.closeOrderType();
    this.closePromoType();
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter");
    this.hideDeliveryType = true;
    this.hidePromoCode = true;
    this.imagePath = this.configProvider.getImagePath();

    this.translateService.get(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'dat', 'from', 'to', 'schedule_time_policy_updated', 'seller_availability_to_deliver_order', 'time', 'do_you_want_to_set_scheduler_policies', 'set_hour_policy', 'please_set_hour', 'ok_txt', 'goto_url', 'copy_n_use', 'on_right_side', 'client_id', 'check', 'live', 'my_apps_n_credentials', 'merchant_credentials', 'login_with', 'login_into_dashboard', 'click_on', 'how_to_get_paypal_client_id', 'contact_us', 'disclaimer', 'privacy_policy', 'please_enter_valid_paypal_client_id', 'paypal_client_id', 'paypal_secret_key', 'please_enter_paypal_client_id', 'please_enter_paypal_secret_key', 'paypal_account_details', 'paypal_details_updated_successfull', 'select_one_payment_method', 'unable_to_crop_image', 'submit', 'cancel', 'paypal_account_details', 'update', 'tx_history', 'friend_request_join_order', 'join_order_history', 'receive_order_from_buyer', 'cancel', 'post_new_product', 'take_photo', 'upload_from_gallery', 'min_order_for_home', 'min_order_for_free_delivery', 'order_within_meter', 'max_delivery_radius', 'cost_std_delivery', 'payment_change_success', 'COD', 'paypal', 'delivery_policies_success', 'please_enter_min_order', 'please_enter_number_only', 'please_enter_amt_free', 'please_enter_range_free', 'please_enter_max_range', 'please_enter_std_cost', 'error_txt', 'delivery_policies_err', 'my_product_list', 'view_product_list', 'not_mention_yet', 'unAuthReq_msg', 'something_wrong_txt', 'please_wait_txt', 'english_txt', 'italian_txt', 'sign_out_success', 'sign_out_msg', 'lang_change_success', 'sign_out', 'yes_txt', 'no_txt', 'user_account', 'my_order_history', 'view_order_history', 'cart', 'view_cart', 'notification', 'view_all_notification', 'delivery_polices', 'min_order_delivery', 'min_order_free_delivery', 'amount', 'range_in_meter', 'max_delivery_range', 'stadard_delivery_cost', 'save', 'payement_method', 'change_lang', 'change_pass', 'logout', 'please_select_atleast_one_order_policies', 'date_valid_error', 'time_valid_error', 'pick_up_addr_err']).subscribe((translation: [string]) => {
      this.translationLet = translation;
      console.log("this.translationLet", this.translationLet);
    });

    this.storage.get('authtoken').then((authtoken) => {
      this.token = authtoken;
    });

    this.storage.get('cartCount').then((cartCount) => {
      if (cartCount != null) {
        this.cartCount = cartCount;
      }
    });


    this.storage.get('lang_id').then((lang_id) => {
      this.langBtn = lang_id;
    });

    //Get details
    this.storage.get('userDetails').then((userDetails) => {
      console.log("userDetails", userDetails);
      
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.profileDetails = userDetails;
      this.timeSLots.user_id = userDetails.id;
      this.timeSLots.isTimePolicy = userDetails.isTimePolicy;
      this.timeSLots.sellerOrderTime = userDetails.sellerOrderTime;
      this.timeSlotList = userDetails.avaibility;
      this.timeSLots.avaibility = userDetails.avaibility;
      //Display Policies 
      if ((this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null) && (this.profileDetails.std_delivery_cost == '' || this.profileDetails.std_delivery_cost == null)) {
        this.hideElement = false;
        this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', this.ordertypecontainer.nativeElement.offsetHeight + 'px');
        this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', this.orderpromocontainer.nativeElement.offsetHeight + 'px');
        this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', this.insetcontainer.nativeElement.offsetHeight + 'px');
        this.renderer.setElementAttribute(this.togglebtnelm.nativeElement, 'class', 'arrow-up');
      } else {
        this.hideElement = true;
        this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', "0");
        this.renderer.setElementAttribute(this.togglebtnelm.nativeElement, 'class', 'arrow-down');
      }

      this.hideTimeElement = true;
      this.renderer.setElementStyle(this.togglebtnelmDivTime.nativeElement, 'height', "0");
      this.renderer.setElementAttribute(this.togglebtnelmTime.nativeElement, 'class', 'arrow-down');
      //End 

      if (this.profileDetails.COD == 0)
        this.profileDetails.COD = false;
      else
        this.profileDetails.COD = true;

      if (this.timeSLots.isTimePolicy == 0)
        this.timeSLots.isTimePolicy = false;
      else
        this.timeSLots.isTimePolicy = true;

      if (this.profileDetails.Paypal_method == 0)
        this.profileDetails.Paypal_method = false;
      else
        this.profileDetails.Paypal_method = true;

      if (this.profileDetails.Bancomat == 0)
        this.profileDetails.Bancomat = false;
      else
        this.profileDetails.Bancomat = true;

      if (userDetails['profile_pic'] != '' && userDetails['profile_pic'] != null) {
        this.tempImg = this.imagePath + userDetails['profile_pic'];
      } else {
        this.tempImg = "assets/images/user-profile-default-img.jpg";
      }

      if (this.profileDetails.order_delivery == 1)
          this.profileDetails.order_delivery = true;
      else
          this.profileDetails.order_delivery = false;
      if (this.profileDetails.order_take_away == 1)
          this.profileDetails.order_take_away = true;
      else
          this.profileDetails.order_take_away = false;

      if (this.profileDetails.order_delivery == 0 && this.profileDetails.order_take_away == 1) {
        this.hidePickupAddr = true;
      }

      //Get My productlist count 
      this.getProductCount();
      //End 
    });

    //End  
    this.closeOrderType();
    this.closePromoType();
  }


  getProductCount() {
    this.serviceProvider.getProductCount(this.profileDetails.id, this.token).then((result: any) => {
      if (result.code == 200) {
        console.log(JSON.stringify(result));
        this.productCount = result.prodCount;
        this.orderCount = result.orderCountRes;
        this.orderReceiveCount = result.orderReceiveCount;
        this.notificationCount = result.notificationCount;
        this.shareFriendCount = result.shareFriendCount;
        this.txHistoryCount = result.txHistoryCount;
        this.joinOrderCount = result.joinOrderCount;
      } else {
        this.productCount = 0;
        this.orderCount = 0;
        this.orderReceiveCount = 0;
        this.notificationCount = 0;
        this.shareFriendCount = 0;
        this.txHistoryCount = 0;
        this.joinOrderCount = 0;
      }
    }, (err) => {
    });
  }

  gotosharewithfriends() {
    this.navCtrl.push(SharedfriendsPage);
  }


  gotoEdit() {
    this.navCtrl.push(EditprofilePage);
  }

  gotoJoinOrderHistory() {
    this.navCtrl.push(JoinorderhistoryPage);
  }

  gotoMyCart() {
    this.navCtrl.push(MycartPage);
  }

  //Bancomat payment method 

  BancomatPaymentMethod() {
    if (this.profileDetails.Bancomat == true)
      this.profileDetails.Bancomat = 1;
    else
      this.profileDetails.Bancomat = 0;

    this.showLoading();

    //Update in DB with token
    this.serviceProvider.updatePaymentMethod(this.profileDetails, this.token).then((result: any) => {

      this.loading.dismiss();
      if (result.code == 200) {
        this.toastMsg(this.translationLet.payment_change_success);
        this.storage.set('userDetails', result.user_detail);
      } else if (result.code == 403) {
        this.toastMsg(this.translationLet.unAuthReq_msg);
      } else {
        this.toastMsg(this.translationLet.something_wrong_txt);
      }
    }, (err) => {
    });
    //End 
  }

  //Change Payment method 

  changePaymentMethod() {

    if (this.profileDetails.COD == true)
      this.profileDetails.COD = 1;
    else
      this.profileDetails.COD = 0;

    this.showLoading();

    //Update in DB with token
    this.serviceProvider.updatePaymentMethod(this.profileDetails, this.token).then((result: any) => {

      this.loading.dismiss();
      if (result.code == 200) {
        this.toastMsg(this.translationLet.payment_change_success);
        this.storage.set('userDetails', result.user_detail);
      } else if (result.code == 403) {
        this.toastMsg(this.translationLet.unAuthReq_msg);
      } else {
        this.toastMsg(this.translationLet.something_wrong_txt);
      }
    }, (err) => {
    });
    //End 
  }


  //Changed payment method for Paypal 

  paypalPaymentMethod() {
    if (this.profileDetails.Paypal_method == true) {
      let alertPaypal = this.alertCtrl.create({
        title: this.translationLet.paypal_account_details,
        enableBackdropDismiss: false,
        inputs: [
          {
            name: 'paypal_client_id',
            placeholder: this.translationLet.please_enter_paypal_client_id,
            type: 'text'
          }/*,
          {
        name: 'paypal_secret_key',
        placeholder:  this.translationLet.please_enter_paypal_secret_key,
        type: 'text'
         }*/
        ],
        buttons: [
          {
            text: this.translationLet.cancel,
            cssClass: 'btn-primary btn-round',
            handler: data => {
              this.profileDetails.Paypal_method = 0;
            }
          },
          {
            text: this.translationLet.submit,
            cssClass: 'btn-primary btn-round',
            handler: data => {

              if (this.validate(data.paypal_client_id)) {
                this.profileDetails.Paypal_method = 1;
                this.profileDetails.paypal_client_id = data.paypal_client_id;
                this.profileDetails.paypal_secret_key = '';
                //this.profileDetails.paypal_secret_key = data.paypal_secret_key;
                this.showLoading();

                //Update in DB with token
                this.serviceProvider.updatePaymentMethod(this.profileDetails, this.token).then((result: any) => {

                  this.loading.dismiss();
                  if (result.code == 200) {
                    this.toastMsg(this.translationLet.payment_change_success);
                    this.storage.set('userDetails', result.user_detail);
                  } else if (result.code == 403) {
                    this.toastMsg(this.translationLet.unAuthReq_msg);
                  } else {
                    this.toastMsg(this.translationLet.something_wrong_txt);
                  }
                }, (err) => {
                });
                //End 
              } else {
                return false;
              }

            }
          }

        ]
      });
      alertPaypal.present();

    }
    else {
      this.profileDetails.Paypal_method = 0;
      this.profileDetails.paypal_client_id = "";

      this.showLoading();

      //Update in DB with token
      this.serviceProvider.updatePaymentMethod(this.profileDetails, this.token).then((result: any) => {

        this.loading.dismiss();
        if (result.code == 200) {
          this.toastMsg(this.translationLet.payment_change_success);
          this.storage.set('userDetails', result.user_detail);
        } else if (result.code == 403) {
          this.toastMsg(this.translationLet.unAuthReq_msg);
        } else {
          this.toastMsg(this.translationLet.something_wrong_txt);
        }
      }, (err) => {
      });
      //End 
    }


  }

  validate(items) {
    let emailValid = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (items.trim().length == 0) {
      this.toastMsg(this.translationLet.please_enter_paypal_client_id);
      return false;
    }

    if (emailValid.test(items) == true) {
      this.toastMsg(this.translationLet.please_enter_valid_paypal_client_id);
      return false;
    }

    return true;
  }

  //End 

  //Language Translate 

  changeLang(langItem) {
    this.langBtn = langItem.id;
    this.profileDetails.language_id = langItem.id;
    //Update in DB with token
    // this.showLoading();

    this.serviceProvider.updatelanguage(this.profileDetails, this.token).then((result: any) => {
      //  this.loading.dismiss();

      if (result.code == 200) {
        this.translateService.use(langItem.value);
        this.translateService.get(['ok_txt', 'please_enter_paypal_client_id', 'please_enter_paypal_secret_key', 'paypal_account_details', 'paypal_details_updated_successfull', 'select_one_payment_method', 'unable_to_crop_image', 'submit', 'cancel', 'paypal_account_details', 'update', 'tx_history', 'friend_request_join_order', 'join_order_history', 'receive_order_from_buyer', 'cancel', 'post_new_product', 'take_photo', 'upload_from_gallery', 'min_order_for_home', 'min_order_for_free_delivery', 'order_within_meter', 'max_delivery_radius', 'cost_std_delivery', 'payment_change_success', 'COD', 'paypal', 'delivery_policies_success', 'please_enter_min_order', 'please_enter_number_only', 'please_enter_amt_free', 'please_enter_range_free', 'please_enter_max_range', 'please_enter_std_cost', 'error_txt', 'delivery_policies_err', 'my_product_list', 'view_product_list', 'not_mention_yet', 'unAuthReq_msg', 'something_wrong_txt', 'please_wait_txt', 'english_txt', 'italian_txt', 'sign_out_success', 'sign_out_msg', 'lang_change_success', 'sign_out', 'yes_txt', 'no_txt', 'user_account', 'my_order_history', 'view_order_history', 'cart', 'view_cart', 'notification', 'view_all_notification', 'delivery_polices', 'min_order_delivery', 'min_order_free_delivery', 'amount', 'range_in_meter', 'max_delivery_range', 'stadard_delivery_cost', 'save', 'payement_method', 'change_lang', 'change_pass', 'logout', 'please_select_atleast_one_order_policies', 'date_valid_error', 'time_valid_error', 'pick_up_addr_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
          this.storage.set('lang_id', langItem.id);
          this.toastMsg(this.translationLet.lang_change_success);
        });

      } else if (result.code == 403) {
        this.toastMsg(this.translationLet.unAuthReq_msg);
      } else {
        this.toastMsg(this.translationLet.something_wrong_txt);
      }
    }, (err) => {
      // this.loading.dismiss();
    });
    //End 

  }

  //End 


  //Go to cover photo
  gotoCoverPhoto() {

    if (this.profileDetails.order_take_away == 1) {
      //Actionsheet start
      let actionSheet = this.actionSheetCtrl.create({
        title: this.translationLet.post_new_product,
        buttons: [
          {
            text: this.translationLet.take_photo,
            handler: () => {
              this.takePhoto();
            }
          }, {
            text: this.translationLet.upload_from_gallery,
            handler: () => {
              this.takePhotoGallery();
            }
          }, {
            text: this.translationLet.cancel,
            handler: () => {
            }
          }
        ]
      });
      actionSheet.present();
      //Actionsheet end
    } else {
      if ((this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null) && (this.profileDetails.std_delivery_cost == '' || this.profileDetails.std_delivery_cost == null)) {
        this.toastMsg(this.translationLet.delivery_policies_err);
      } else if ((this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0)) {
        this.toastMsg(this.translationLet.select_one_payment_method);
        this.app.getRootNav().setRoot(MyprofilePage);
      } else {
        //Actionsheet start
        let actionSheet = this.actionSheetCtrl.create({
          title: this.translationLet.post_new_product,
          buttons: [
            {
              text: this.translationLet.take_photo,
              handler: () => {
                this.takePhoto();
              }
            }, {
              text: this.translationLet.upload_from_gallery,
              handler: () => {
                this.takePhotoGallery();
              }
            }, {
              text: this.translationLet.cancel,
              handler: () => {
              }
            }
          ]
        });
        actionSheet.present();
        //Actionsheet end
      }
    }

  }

  takePhoto() {
    //End 
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }
  //End  

  takePhotoGallery() {
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }

  //End  

  CropedImg(imageData) {
    if (this.platform.is('ios')) {
      this.tempProductImg = normalizeURL(imageData);
    } else {
      this.tempProductImg = imageData;
    }
    this.app.getRootNav().setRoot(CoverphotoPage, { 'tempProductImg': this.tempProductImg });

  }


  //gotoHome

  gotoHome() {
    this.app.getRootNav().setRoot(HomePage);
  }

  //gotoOrder 

  gotoMyOrder() {
    this.navCtrl.push(UpcomingPage);
  }

  //gotoReceiveOrder

  gotoOrder() {
    this.navCtrl.push(ReceiveUpcomingPage);
  }

  //gotoNotification

  gotoNotification() {
    this.navCtrl.push(NotificationPage);
  }

  //gotoChangepass

  gotoChangepass() {
    this.navCtrl.push(ChangepasswordPage);
  }

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }

  //End

  //Logout functionality 

  logout() {
    let alertLogout = this.alertCtrl.create({
      title: this.translationLet.sign_out,
      message: this.translationLet.sign_out_msg,
      buttons: [
        {
          text: this.translationLet.yes_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {
            this.storage.remove('user_id');
            this.storage.remove('userDetails');
            this.storage.remove('authtoken');
            this.storage.remove('searchJSONObj');
            this.storage.remove('lang_id');
            this.storage.remove('cartCount');

            //Facebook Login #2
            //Instagram Login #3

            if (this.profileDetails.login_by == 2) {
              this.fb.logout()
                .then(res => console.log('success'))
                .catch(e => console.log('Error logout from Facebook'));
            }

            this.app.getRootNav().setRoot(LoginPage);
            this.toastMsg(this.translationLet.sign_out_success);
          }
        },
        {
          text: this.translationLet.no_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {
          }
        }

      ]
    });
    alertLogout.present();
  }
  //End 

  //Toast Msg 

  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  //End 

  //Submit Time policies 
  async validateDatetime() {

    for(const element of this.timeSlotList ) {
      let arrFrom = await element.av_from.split(":");
      let arrTo = await element.av_to.split(":");
      if (await (arrFrom[0] > arrTo[0])) {
        await this.showPopup(this.translationLet.error_txt, this.translationLet.date_valid_error + ' ( ' + element.day + ' )');
        return false;
      }
      if (await (arrFrom[0] === arrTo[0])) {
        if (await (arrFrom[1] > arrTo[1])) {
          await this.showPopup(this.translationLet.error_txt, this.translationLet.time_valid_error + ' ( ' + element.day + ' )');
          return false;
        }
      }
      if (await (arrFrom[0] < arrTo[0])) {
      }
    }
    return true;
  }

  async submitTimePolicies() {
    if (await this.validateDatetime()) {
      if (this.validatePolicies()) {
        if (this.timeSLots.isTimePolicy == true)
          this.timeSLots.isTimePolicy = 1;
        else
          this.timeSLots.isTimePolicy = 0;

        this.showLoading();

        this.serviceProvider.updateTimePolicies(this.timeSLots, this.token).then((result: any) => {
          this.loading.dismiss();

          if (result.code == 200) {
            this.closeOrderType();
            this.closePromoType();
            this.hidePromoCode = true;
            this.renderer.setElementStyle(this.togglebtnelmDivTime.nativeElement, 'height', "0");
            this.renderer.setElementAttribute(this.togglebtnelmTime.nativeElement, 'class', 'arrow-down');
            if (result.userRes.isTimePolicy == 1) {
              this.timeSLots.isTimePolicy = true;
            } else {
              this.timeSLots.isTimePolicy = false;
            }

            this.timeSLots.sellerOrderTime = result.userRes.sellerOrderTime;
            this.timeSlotList = result.userRes.avaibility;
            this.timeSLots.avaibility = result.userRes.avaibility;

            this.storage.set('userDetails', result.userRes);
            this.toastMsg(this.translationLet.schedule_time_policy_updated);
          } else if (result.code == 403) {
            this.toastMsg(this.translationLet.unAuthReq_msg);
          } else {
            this.toastMsg(this.translationLet.something_wrong_txt);
          }
        }, (err) => {
          this.loading.dismiss();
        });
        //End  */
      }
    }
  }


   validatePolicies() {
    if (this.timeSLots.isTimePolicy == true) {
      if (this.timeSLots.sellerOrderTime.trim() == '') {
        this.showPopup(this.translationLet.error_txt, this.translationLet.please_set_hour);
        return false;
      }
    }
    return true;
  }


  //Submit delivery policies 

  submitPolicies() {
    if (this.validateFields()) {

      if (this.profileDetails.isPromoCode == true) {
          this.profileDetails.isPromoCode = 1;
      }
      else {
          this.profileDetails.isPromoCode = 0;
          this.profileDetails.promoCode = "";
          this.profileDetails.promoDiscount = "";
      }

      this.showLoading();
      console.log('this.profileDetails', this.profileDetails);
      this.serviceProvider.updatePolicies(this.profileDetails, this.token).then((result: any) => {
        this.loading.dismiss();

        if (result.code == 200) {
          this.hideElement = true;
          this.hideDeliveryType = true;
          this.hidePromoCode = true;
          this.closeOrderType();
          this.closePromoType();
          this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', "0");
          this.renderer.setElementAttribute(this.togglebtnelm.nativeElement, 'class', 'arrow-down');
          this.storage.set('userDetails', result.userDetails);
          this.toastMsg(this.translationLet.delivery_policies_success);
        } else if (result.code == 403) {
          this.toastMsg(this.translationLet.unAuthReq_msg);
        } else {
          this.toastMsg(this.translationLet.something_wrong_txt);
        }
      }, (err) => {
        this.loading.dismiss();
      });
      //End 
    }

  }

  //validation starts

  validateFields() {
    console.log(this.profileDetails);
    
    if (this.profileDetails.min_order_charge && this.profileDetails.min_order_charge.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_min_order);
      return false;
    }

    if (this.profileDetails.min_order_charge && Math.sign(this.profileDetails.min_order_charge.trim()) == -1) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.amt_must_positive);
      return false;
    }


    if (this.profileDetails.min_order_charge && isNaN(this.profileDetails.min_order_charge)) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_number_only);
      return false;
    }

    if (this.profileDetails.order_free_amt && this.profileDetails.order_free_amt.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_amt_free);
      return false;
    }

    if (this.profileDetails.order_free_amt && Math.sign(this.profileDetails.order_free_amt.trim()) == -1) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.amt_must_positive);
      return false;
    }

    if (this.profileDetails.order_free_amt && isNaN(this.profileDetails.order_free_amt)) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_number_only);
      return false;
    }

    if (this.profileDetails.max_delivery_range && this.profileDetails.max_delivery_range.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_max_range);
      return false;
    }

    if (this.profileDetails.max_delivery_range && isNaN(this.profileDetails.max_delivery_range)) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_number_only);
      return false;
    }

    if (this.profileDetails.std_delivery_cost && this.profileDetails.std_delivery_cost.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_std_cost);
      return false;
    }

    if (this.profileDetails.std_delivery_cost && Math.sign(this.profileDetails.std_delivery_cost.trim()) == -1) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.amt_must_positive);
      return false;
    }

    if (this.profileDetails.std_delivery_cost && isNaN(this.profileDetails.std_delivery_cost)) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_number_only);
      return false;
    }

    if (this.profileDetails.order_delivery == false && this.profileDetails.order_take_away == false) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_atleast_one_order_policies);
      return false;
    }

    if (this.profileDetails.isPromoCode == true) {

      if (this.profileDetails.promoCode && this.profileDetails.promoCode.trim() == "") {
          this.showPopup(this.translationLet.error_txt, this.translationLet.please_add_promo_code);
          return false;
      }
      if (this.profileDetails.promoDiscount && this.profileDetails.promoDiscount.trim() == "") {
          this.showPopup(this.translationLet.error_txt, this.translationLet.please_add_discount);
          return false;
      }
      if (this.profileDetails.promoDiscount && Math.sign(this.profileDetails.promoDiscount.trim()) == -1) {
          this.showPopup(this.translationLet.error_txt, this.translationLet.amt_must_positive);
          return false;
      }
      if (this.profileDetails.promoDiscount && this.profileDetails.promoDiscount.trim() > 100) {
          this.showPopup(this.translationLet.error_txt, this.translationLet.discount_less_than);
          return false;
      }
    }

    if (this.profileDetails.order_delivery == 0 && this.profileDetails.order_take_away == 1) {
      this.profileDetails.hidePickupAddr = true;
      if (this.profileDetails.pick_address === '') {
        this.showPopup(this.translationLet.error_txt, this.translationLet.pick_up_addr_err);
        return false;
      }
    } else {
      this.profileDetails.hidePickupAddr = false;
    }

  return true;
  }

  //End

  togglePickAddress() {
    console.log("pick address..");
    if (this.profileDetails.order_delivery == 0 && this.profileDetails.order_take_away == 1) {
      console.log("pick address.. if");
      this.hidePickupAddr = true;
      
      this.closeOrderType();
      this.hideDeliveryType = true;
      setTimeout(() => {
        this.openOrderType();
        this.hideDeliveryType = false;
      }, 500);
    } else {
      console.log("pick address.. else");
      this.hidePickupAddr = false;

      this.closeOrderType();
      this.hideDeliveryType = true;
      setTimeout(() => {
        this.openOrderType();
        this.hideDeliveryType = false;
      }, 500);
    }
  }

  //Alert Popups 

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons: [
        {
          text: this.translationLet.ok_txt,
          cssClass: 'btn-primary btn-round'
        }]
    });
    alert.present();
  }

  //End 

  //My product list 

  gotoMyProduct() {
    this.navCtrl.push(SellerproductsPage);
  }

  public togglePolicies() {
    this.hideElement = !this.hideElement;
    if (this.hideElement) {
      this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', "0");
      this.renderer.setElementAttribute(this.togglebtnelm.nativeElement, 'class', 'arrow-down');

    } else {
      this.renderer.setElementAttribute(this.togglebtnelm.nativeElement, 'class', 'arrow-up');
      this.renderer.setElementStyle(this.toggledivelm.nativeElement, 'height', this.insetcontainer.nativeElement.offsetHeight + 'px');
    }
  }

  public closeOrderType() {
    console.log("closeOrderType");
    this.renderer.setElementStyle(this.togglebtnelmOrderTypeDiv.nativeElement, 'height', "0");
    this.renderer.setElementAttribute(this.togglebtnelmOrderType.nativeElement, 'class', 'arrow-down');
  }

  public openOrderType() {
    this.renderer.setElementAttribute(this.togglebtnelmOrderType.nativeElement, 'class', 'arrow-up');
    this.renderer.setElementStyle(this.togglebtnelmOrderTypeDiv.nativeElement, 'height', this.ordertypecontainer.nativeElement.offsetHeight + 'px');
  }

  public closePromoType() {
    console.log("closePromoType");
    this.renderer.setElementStyle(this.togglebtnelmOrderPromoDiv.nativeElement, 'height', "0");
    this.renderer.setElementAttribute(this.togglebtnelmOrderPromo.nativeElement, 'class', 'arrow-down');
  }

  public toggleOrderType() {
    this.hideElement = !this.hideElement;
    if (this.hideElement) {
      this.renderer.setElementStyle(this.togglebtnelmOrderTypeDiv.nativeElement, 'height', "0");
      this.renderer.setElementAttribute(this.togglebtnelmOrderType.nativeElement, 'class', 'arrow-down');

    } else {
      this.renderer.setElementAttribute(this.togglebtnelmOrderType.nativeElement, 'class', 'arrow-up');
      this.renderer.setElementStyle(this.togglebtnelmOrderTypeDiv.nativeElement, 'height', this.ordertypecontainer.nativeElement.offsetHeight + 'px');
    }
  }

  public toggleOrderPromo() {
    this.hideElement = !this.hideElement;
    if (this.hideElement) {
      this.renderer.setElementStyle(this.togglebtnelmOrderPromoDiv.nativeElement, 'height', "0");
      this.renderer.setElementAttribute(this.togglebtnelmOrderPromo.nativeElement, 'class', 'arrow-down');

    } else {
      this.renderer.setElementAttribute(this.togglebtnelmOrderPromo.nativeElement, 'class', 'arrow-up');
      this.renderer.setElementStyle(this.togglebtnelmOrderPromoDiv.nativeElement, 'height', this.orderpromocontainer.nativeElement.offsetHeight + 'px');
    }
  }

  public toggleTimePolicies() {

    this.hideTimeElement = !this.hideTimeElement;
    if (this.hideTimeElement) {
      this.renderer.setElementStyle(this.togglebtnelmDivTime.nativeElement, 'height', "0");
      this.renderer.setElementAttribute(this.togglebtnelmTime.nativeElement, 'class', 'arrow-down');

    } else {
      this.renderer.setElementAttribute(this.togglebtnelmTime.nativeElement, 'class', 'arrow-up');
      this.renderer.setElementStyle(this.togglebtnelmDivTime.nativeElement, 'height', this.insetTimecontainer.nativeElement.offsetHeight + 'px');
    }
  }

  gotosalesTransaction() {
    this.navCtrl.push(TransactionsPage);
  }

  gotoUserOrder() {
    this.app.getRootNav().setRoot(ReceiveUpcomingPage);
  }

  //Update paypal client id 

  updateClientId() {
    let alertPaypal = this.alertCtrl.create({
      title: this.translationLet.paypal_account_details,
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'paypal_client_id',
          placeholder: this.translationLet.please_enter_paypal_client_id,
          type: 'text',
          value: this.profileDetails.paypal_client_id
        }
      ],
      buttons: [
        {
          text: this.translationLet.cancel,
          cssClass: 'btn-primary btn-round',
          handler: data => {
          }
        },
        {
          text: this.translationLet.submit,
          cssClass: 'btn-primary btn-round',
          handler: data => {

            if (this.validate(data.paypal_client_id)) {
              this.profileDetails.paypal_client_id = data.paypal_client_id;
              this.profileDetails.paypal_secret_key = '';
              //this.profileDetails.paypal_secret_key = data.paypal_secret_key;
              this.showLoading();
              //Update in DB with token
              this.serviceProvider.updatePaymentMethod(this.profileDetails, this.token).then((result: any) => {

                this.loading.dismiss();
                if (result.code == 200) {
                  this.toastMsg(this.translationLet.paypal_details_updated_successfull);
                  this.storage.set('userDetails', result.user_detail);
                } else if (result.code == 403) {
                  this.toastMsg(this.translationLet.unAuthReq_msg);
                } else {
                  this.toastMsg(this.translationLet.something_wrong_txt);
                }
              }, (err) => {
              });
              //End 
            }
            else {
              return false;
            }
          }
        }

      ]
    });
    alertPaypal.present();
  }

  gotMyOrder() {
    this.app.getRootNav().setRoot(UpcomingPage);
  }

  gotoContact() {
    this.navCtrl.push(ContactusPage);
  }

  gotoDisclaimer() {
    this.navCtrl.push(DisclaimerPage);
  }

  gotoPrivacy() {
    this.navCtrl.push(PrivacyPoliciesPage);
  }

  //Update search
  updateSearch() {

    if (this.profileDetails.pick_address && this.profileDetails.pick_address.trim() == '') {
      this.addressList = [];
      return;
    }

    if (this.profileDetails.pick_address != '') {
      let me = this;
      this.service.getPlacePredictions({
        input: this.profileDetails.pick_address
      }, (predictions, status) => {
        me.addressList = [];
        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach((prediction) => {
              me.addressList.push(prediction.description);
            });
          }
        });
      });
    }

  }
  //End

  // Select Item from list
  chooseItem(item: any = '') {
    this.addressList = [];
    this.profileDetails.pick_address = item;
    this.profileDetails.delivery_addressTemp = item;
    this.geoCodeAddress(this.profileDetails.pick_address);
  }
  //End

  geoCodeAddress(address) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.profileDetails.delivery_lati = results[0].geometry.location.lat();
      this.profileDetails.delivery_long = results[0].geometry.location.lng();
    });
  }
}
