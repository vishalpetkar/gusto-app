import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController, ToastController,AlertController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../providers/config/config';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-country-code',
  templateUrl: 'country-code.html',
})
export class CountryCodePage {
  data : any = { 'country_code' : '' } ;
  tempArrList :any =[];
  flagImgPath : any =''; 
  translationLet: any;

  constructor(private translateService: TranslateService,public configProvider : ConfigProvider,private storage: Storage,public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,public toastCtrl: ToastController,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    
   this.translateService.get(['error_txt','ok_txt','select_dailing_code']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });

    this.flagImgPath=this.configProvider.getFlagImgpath();
    this.tempArrList=navParams.get('countryListObj');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountryCodePage');
  }

   dismiss() {
 	this.data = { 'country_code': '','country_icon' : '' };
    this.viewCtrl.dismiss(this.data);
   }

  setCountryData(item){
	this.data.country_code=item.dailing_code;
  this.data.country_icon=this.flagImgPath+item.icon;
	this.viewCtrl.dismiss(this.data);
  }



}
