import { Component } from '@angular/core';
import { MenuController ,IonicPage,App, NavController,ModalController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { LoginPhoneNumberPage } from '../login-phone-number/login-phone-number';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Instagram } from "ng2-cordova-oauth/core";  
import { OauthCordova } from 'ng2-cordova-oauth/platform/cordova';
import { ServiceProvider } from '../../providers/service/service';
import { SelectaddressPage } from '../selectaddress/selectaddress';
import { HomePage } from '../home/home';
import moment from 'moment';
import { CountryCodePage } from '../country-code/country-code';
import { VarifyPage } from '../varify/varify';
import { SetpasswordPage } from '../setpassword/setpassword';
import { RegistrationPage } from '../registration/registration';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

loginData : any = {
phoneNumber : '',
login_by:'',
device_token:'',
facebook_id:'',
instagram_id:'',
countryCode : '+39',
tempNumber:'',
password:''
};

countryList :any =[];
defaultFlag : any ='';
translationLet: any;
loading: Loading;
userList: any =[];
private oauth: OauthCordova = new OauthCordova();

    private instagramProvider: Instagram = new Instagram({
        clientId: "55d8dc9cd799404092dec049072e698e",      // Register you client id from https://www.instagram.com/developer/
        redirectUri: 'http://localhost',  // Let is be localhost for Mobile Apps
        appScope: ['basic'] 
    });

   private apiResponse;

  constructor(public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private fb: Facebook,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {  this.apiResponse = [];
  
   this.defaultFlag = 'assets/images/country-icon1.png'; 

 this.translateService.get(['please_wait_txt','forgot_password','please_enter_valid_phone','plese_enter_pass','signup','do_not_have_an_account','wrong_pass','instagram_error','sign_in_success','something_wrong_txt','facebook_error','error_txt','enter_phone_no_val','ok_txt','login_txt_1','login_txt_2','login_txt_3','facebook','instagram','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
 //End 
 
  //Onesignal device_token 
   this.storage.get('player_id').then((player_id) => {
    this.loginData.device_token=player_id;
  }); 
  //End  
   this.initCountryList();
 }

//User List 

 initUserList()
 {

  this.serviceProvider.getAppUserList().then((result:any) => {
    if(result.code == 200){
      this.userList =result.data;
    } else if(result.code == 500) {
      //this.showPopup(this.translationLet.error,result.message);
    } else if(result.code == 404) {
      //this.showPopup(this.translationLet.error,result.message);
    } 
    
    }, (err) => {
    });
 }

   //Get Country List 
 initCountryList()
 {

  this.serviceProvider.getCountryList().then((result:any) => {
    if(result.code == 200){
      this.countryList =result.data;
    } else if(result.code == 500) {
      //this.showPopup(this.translationLet.error,result.message);
    } else if(result.code == 404) {
      //this.showPopup(this.translationLet.error,result.message);
    } 
    
    }, (err) => {
    });
 }
  //End
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
//End 

//Country model 

 openCountryCodeModal()
 {

 let modalCountry = this.modalCtrl.create(CountryCodePage, {'countryListObj':this.countryList});

  modalCountry.onDidDismiss(data => {
     if(data.country_code != ''){
      this.loginData.countryCode =data.country_code;
      this.defaultFlag =data.country_icon;
     }
   });

    modalCountry.present();
 }


  //Submit 

submit()
{
  if(this.validate())
  {
  this.showLoading();
  this.loginData.mobile_no=this.loginData.countryCode+this.loginData.tempNumber;
  this.loginData.created = moment().format('YYYY-MM-DD HH:mm:ss');
  this.serviceProvider.loginUser(this.loginData).then((result:any) => {
     this.loading.dismiss();

    if(result.code == 200){

      if(result.status == 1)
      {
        this.navCtrl.push(SelectaddressPage,{'user_id' : result.data });

      }else
      {
          this.toastMsg(this.translationLet.sign_in_success);
          this.storage.set('authtoken', result.token);
          this.storage.set('userDetails',result.userRes);
          this.storage.set('user_id',result.userRes.id);
          this.storage.set('lang_id',result.userRes.language_id);
          this.storage.set('cartCount',result.userRes.cartCount);
          this.app.getRootNav().setRoot(HomePage);
       }

    } else{
       this.toastMsg(this.translationLet.wrong_pass);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
 }
}

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
        content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

validate()
{
  if (this.loginData.tempNumber.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.enter_phone_no_val);
          return false;
        }
  if (this.loginData.tempNumber.length < 8 || this.loginData.tempNumber.length > 12  ) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_phone);
          return false;
        }
  if (this.loginData.password.trim() == '') {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_pass);
          return false;
        }

  return true;
}

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 



//Login with Instagram

 loginWithInsta()
 {
  this.showLoading();
  this.oauth.logInVia(this.instagramProvider).then((success) => {
     this.serviceProvider.getInstagramUserInfo(success).then((resultInsta:any) => {
      //Call services
      this.loginData.created = moment().format('YYYY-MM-DD HH:mm:ss');
      this.loginData.login_by =3;
      this.loginData.instagram_id = resultInsta['data']['id'];
      this.loginData.name = (resultInsta['data']['full_name'] === '') ? '' : resultInsta['data']['full_name'];
      this.loginData.email = (resultInsta['data']['email'] === '') ? '' : resultInsta['data']['email'];

      this.serviceProvider.instaGramLogin(this.loginData).then((result:any) => {
      this.loading.dismiss();  
        if(result.code == 200)
        {
         if(result.isAddress == false)
         {
          //Go to adress page
          this.navCtrl.push(SelectaddressPage,{'user_id' : result.data });
          //End 
         }else
         {
          //Go to sign in page
          this.toastMsg(this.translationLet.sign_in_success);
          this.storage.set('authtoken', result.token);
          this.storage.set('userDetails',result.userRes);
          this.storage.set('user_id',result.userRes.id);
          this.storage.set('cartCount',result.userRes.cartCount);
          this.app.getRootNav().setRoot(HomePage);
          //End 
         }
        }else
        {
        this.toastMsg(this.translationLet.something_wrong_txt);
        }
    }, (err) => {
        this.loading.dismiss(); 
        this.toastMsg(this.translationLet.something_wrong_txt);
    })
      //End
    }, (err) => {
      this.loading.dismiss();   
     this.toastMsg(this.translationLet.instagram_error);
    })
      }, (error) => {
        this.loading.dismiss(); 
       this.toastMsg(this.translationLet.instagram_error);
      });
  }
 
 //End 
 

   gotoRegistrationPage ()
 {
  this.navCtrl.push(RegistrationPage);
 }

 //Login with fb 

 loginWithFB()
 {
 this.fb.login(['public_profile', 'email'])
    .then(res => {
      if(res.status === "connected") {
        this.getUserDetail(res.authResponse.userID);
      } else {
       this.toastMsg(this.translationLet.facebook_error);
      }
    })
    .catch(e => this.toastMsg(this.translationLet.facebook_error));

 }

//Get User FB details 

 getUserDetail(userid) {
  this.fb.api("/"+userid+"/?fields=id,name,first_name,email",["public_profile"])
    .then(resFB => {
      this.loginData.created = moment().format('YYYY-MM-DD HH:mm:ss');
      this.loginData.login_by =2;
      this.loginData.facebook_id = resFB['id'];
      this.loginData.name = (resFB['name'] === '') ? '' : resFB['name'];
      this.loginData.email = (resFB['email'] === '') ? '' : resFB['email'];
     
     this.showLoading();

      this.serviceProvider.facebookLogin(this.loginData).then((result:any) => {
      this.loading.dismiss();  
        if(result.code == 200)
        {
         if(result.isAddress == false)
         {
          //Go to adress page
          this.navCtrl.push(SelectaddressPage,{'user_id' : result.data });
          
          //End 
         }else
         {
          //Go to sign in page
          this.toastMsg(this.translationLet.sign_in_success);
          this.storage.set('authtoken', result.token);
          this.storage.set('userDetails',result.userRes);
          this.storage.set('user_id',result.userRes.id);
          this.storage.set('cartCount',result.userRes.cartCount);
          this.app.getRootNav().setRoot(HomePage);
          //End 
         }
        }else
        {
        this.toastMsg(this.translationLet.something_wrong_txt);
        }
    }, (err) => {
        this.loading.dismiss(); 
        this.toastMsg(this.translationLet.something_wrong_txt);
    })

    })
    .catch(e => {
      this.toastMsg(this.translationLet.facebook_error);
    });
}

//End 

//Forgot password 

gotoForgot()
{
  this.navCtrl.push(ForgotpasswordPage);
}


}
