import { Component,ViewChild,ElementRef, Renderer } from '@angular/core';
import { Content ,Slides,Platform,Nav,MenuController ,ActionSheetController,normalizeURL,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerreceivedorderPage } from '../sellerreceivedorder/sellerreceivedorder'; 
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { UpcomingPage } from '../upcoming/upcoming';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import moment from 'moment';

@Component({
  selector: 'page-receive-upcoming',
  templateUrl: 'receive-upcoming.html',
})
export class ReceiveUpcomingPage {
  
  translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  isRefresh :any =0;
 /*Upcoming receive orders */
  orderList : any =[];
  orderObj : any = {
    user_id:'',
    start : 0,
    limit : 100
  };
  isFetchingData: any =true;
  returnVal:any ='';
  loadmoreData=0;
  isData : any = true ;
    lang_id: any ='';
  orderPastList:any =[];
  isFetchingPastData: any =true;
  loadmorePastData=0;
  isPastData : any = true ;
  all_months : any =[];
  newDate:any='';
  tXDate:any ='';

  
  /*End */
  tempProductImg :any ='';
  dateTimeOld: any ='';
  dateTimeNew: any ='';
  acceptOrderObj : any ={'id' : '','seller_id' : '','buyer_id' : '','created':'','is_free':'','lang_id' : ''};
  acceptList :any =[];
  selectedAll : any =false;
  orderCounts :any =0;

   profileDetails : any = {
    name : '',
    profile_pic : '',
    mobile_no:'',
    address:'',
    email : '',
    id:'',
    min_order_charge:'',
    order_free_amt:'',
    //order_free_range:'',
    max_delivery_range:'',
    std_delivery_cost:'',
    Paypal_method: '',
    COD:'',
    order_take_away: ''
  };

   myorderCount :any =0;
  receiveOrderCount :any =0;
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any ="0";

 constructor(private crop: Crop,public actionSheetCtrl: ActionSheetController,public renderer: Renderer, public platform: Platform,private camera: Camera,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
 

   ionViewDidEnter()
  {    
       // this.page= 0;
        this.isRefresh = 0;
        this.orderObj.start= 0;  
        this.orderObj.limit= 100;  

         
        this.userImgPath = this.configProvider.getImagePath();
        this.prodImgPath = this.configProvider.getProdImgPath();
         this.storage.get('authtoken').then((authtoken) => {
            this.token=authtoken;
        });
   
        this.storage.get('lang_id').then((lang_id) => {
        this.lang_id=lang_id;
         if( this.lang_id ==1)
        this.all_months=[{'text' : 'Jan', 'value' : 'Jan'},{'text' : 'Feb', 'value':'Feb'},{'text' : 'Mar', 'value':'March'},{'text' : 'Apr', 'value':'April'},{'text' : 'May', 'value':'May'},{'text' : 'Jun', 'value':'June'},{'text' : 'Jul', 'value':'July'},{'text' : 'Aug', 'value':'Aug'},{'text' : 'Sep', 'value':'Sep'},{'text' : 'Oct', 'value':'Oct'},{'text' : 'Nov', 'value':'Nov'},{'text' : 'Dec', 'value':'Dec'}];
           else
        this.all_months=[{'text' : 'Gennaio', 'value' : 'Jan'},{'text' : 'Febbraio', 'value' : 'Feb'},{'text' : 'Marzo', 'value' : 'Mar'},{'text' : 'Aprile', 'value' : 'Apr'},{'text' : 'Maggio', 'value' : 'May'},{'text' : 'Giugno', 'value' : 'Jun'},{'text' : 'Luglio', 'value' : 'Jul'},{'text' : 'Agosto', 'value' : 'Aug'},{'text' : 'Settembre', 'value' : 'Sep'},{'text' : 'Ottobre', 'value' : 'Oct'},{'text' : 'Novembre', 'value' : 'Nov'},{'text' : 'Dicembre', 'value' : 'Dec'}];
        });

      this.translateService.get(['delivery','take_away','seller_address','pick_up_time','order_move_to_done','do_you_want_to_mark_order_to_done','done_orders','open_orders','done_txt','delivery_policies_err','buyer','total','post_new_product','cancel','unable_to_crop_image','upload_from_gallery','take_photo','upcoming','past_orders','order_deliver','order_expired','no_past_order','please_select_one_order','service_error','order_confrim_from_your_side','accept','do_you_want_accept_order','order_time_elapsed_can_not_accept_order','order_completed','order_not_received','no_orders_from_buyers','receive_orders','delivery_time','delivery_place','buyer_got','no_upcoming_join_orders','order_pending','off_to_all_products','order_confirm','order_reject','order_rescheduled','view_details','you_got_txt','delivery_cost_txt','free_delivery_txt','order_rescheduled','pull_to_refresh','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

    //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.profileDetails=userDetails;
        this.orderObj.user_id =userDetails.id; 
        this.myorderCount = userDetails.myorderCount;
        this.receiveOrderCount = userDetails.receiveOrderCount;
        this.receiveOrderList();
     }); 
        
  }
 
   translateDateFun(mixDate)
  {
    for(let i=0;i<this.all_months.length;i++)
{
  if(mixDate.indexOf(this.all_months[i]['text']) != -1)
  {
    this.newDate= mixDate.replace(this.all_months[i]['text'],this.all_months[i]['value']);
  }
}
return this.newDate;

  }

  /*************************************/
/*Scroll sliding tabs start*/



  // On segment click
  selectedTab(index) {
    this.slider.slideTo(index);
  }

  // On slide changed
  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    let slides_count = this.segments.nativeElement.childElementCount;

    this.page = currentIndex.toString();
    if(this.page >= slides_count)
      this.page = (slides_count-1).toString();

   //In proogress completed nd in requested orders 

    if(this.page == 0)
    {
      this.isRefresh =0;
     this.receiveOrderList();
    }
   else
   {
     this.isRefresh =1;
     this.orderObj.limit =100;
     this.orderObj.start = 0;
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.receivePastList(); 
   }
    this.centerScroll();
  }

  // Center current scroll
  centerScroll(){
    if(!this.segments || !this.segments.nativeElement)
      return;

    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent/2) ;

    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }

  // Get size start to current
  sizeLeft(){
    let size = 0;
    for(let i = 0; i < this.page; i++){
      size+= this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }

  // Easing function
  easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }

  // Animate scroll
  smoothScrollTo(endX){
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;

    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }
/*************************************/
/*Scroll sliding tabs End*/

   /*Common Ionic refresher */

 doRefresh(refresher) {
    this.orderObj.start= 0;  
    this.orderObj.limit= 100;  
    if(this.isRefresh ==1)
    this.receivePastListRefresh(refresher);
    else
    this.receiveOrderListRefresh(refresher);
  }

gotoSellerProfile(userId)
{
this.app.getRootNav().push(SellerprofilePage,{'userId' : userId});
}


receivePastListRefresh (refresher)
{
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);  
this.serviceProvider.receivePastList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderPastList =result.data;     
            this.commonProductList(this.orderPastList);
            this.loadmorePastData=1;
            this.isPastData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.orderPastList=[];
             this.isPastData =false;
             this.loadmorePastData=0;
             refresher.complete();
          } else
          {
             this.orderPastList=[];
             this.isPastData =false;
             this.loadmorePastData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}


//Order List 

  receivePastList()
  {
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);  

this.serviceProvider.receivePastList(this.orderObj,this.token).then((result:any) => {
 this.isFetchingPastData=false;
            if(result.code == 200){
            this.orderPastList =result.data;   
            this.commonProductList(this.orderPastList);  
            this.loadmorePastData=1;
            this.isPastData =true;
          } else if(result.code == 500) {
             this.orderPastList=[];
             this.isPastData =false;
             this.loadmorePastData=0;
          } else
          {
             this.orderPastList=[];
             this.isPastData =false;
             this.loadmorePastData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
  }


//Order List 

  receiveOrderList()
  {
    this.orderObj.start=parseInt(this.orderObj.start);
    this.orderObj.limit=parseInt(this.orderObj.limit);	

this.serviceProvider.receiveOrderList(this.orderObj,this.token).then((result:any) => {
 this.isFetchingData=false;
            if(result.code == 200){
            this.orderList =result.data;     
            this.commonProductList(this.orderList);
            this.orderCounts = result.orderCounts;     
            this.loadmoreData=1;
            this.isData =true;
            this.myorderCount = result.userDetails.myorderCount;
            this.receiveOrderCount = result.userDetails.receiveOrderCount;
            this.storage.set('userDetails',result.userDetails);

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             this.orderCounts =0;
            this.myorderCount = result.userDetails.myorderCount;
            this.receiveOrderCount = result.userDetails.receiveOrderCount;
            this.storage.set('userDetails',result.userDetails);
          } else
          {
             this.orderList=[];
             this.isData =false;
              this.orderCounts =0;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
  }

//End 

receiveOrderListRefresh (refresher)
{
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);  
this.serviceProvider.receiveOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList =result.data;     
            this.commonProductList(this.orderList);
            this.orderCounts = result.orderCounts; 
            this.loadmoreData=1;
            this.isData =true;
            this.myorderCount = result.userDetails.myorderCount;
            this.receiveOrderCount = result.userDetails.receiveOrderCount;
            this.storage.set('userDetails',result.userDetails);
            refresher.complete();

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
              this.myorderCount = result.userDetails.myorderCount;
            this.receiveOrderCount = result.userDetails.receiveOrderCount;
            this.storage.set('userDetails',result.userDetails);
             refresher.complete();
          } else
          {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}


//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.orderObj.start = parseInt(this.orderObj.start)+parseInt(this.orderObj.limit);
    this.orderObj.limit= parseInt(this.orderObj.limit)+100;

  setTimeout(() => {

           this.serviceProvider.receiveOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList = this.orderList.concat(result.data);
            this.commonProductList(this.orderList);
            this.orderCounts = result.orderCounts; 
            this.loadmoreData=1;
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}

//SellerDetails

sellerDetails(order_id,buyerId)
{
this.app.getRootNav().push(SellerreceivedorderPage,{'order_id' : order_id, 'user_id' : this.orderObj.user_id, 'buyer_id' : buyerId});
}

//End 



//Upcoming order functionality 
 
  toggleUpcomingSection(elmViewRef: HTMLElement, event) {
    console.log(event.target.parentElement.classList.add('active'))
    if (!elmViewRef.classList.contains('open')) {
      this.renderer.setElementStyle(elmViewRef, 'height', elmViewRef.children[0].clientHeight + 'px');
      this.renderer.setElementClass(elmViewRef, 'open', true);
      event.target.parentElement.classList.add('active')
    } else {
      this.renderer.setElementStyle(elmViewRef, 'height', '0px');
      this.renderer.setElementClass(elmViewRef, 'open', false);
      event.target.parentElement.classList.remove('active')
    }
  }

  togglePastSection(elmViewRef: HTMLElement, event) {
    console.log(event.target.parentElement.classList.add('active'))
    if (!elmViewRef.classList.contains('open')) {
      this.renderer.setElementStyle(elmViewRef, 'height', elmViewRef.children[0].clientHeight + 'px');
      this.renderer.setElementClass(elmViewRef, 'open', true);
      event.target.parentElement.classList.add('active')
    } else {
      this.renderer.setElementStyle(elmViewRef, 'height', '0px');
      this.renderer.setElementClass(elmViewRef, 'open', false);
      event.target.parentElement.classList.remove('active')
    }

  }



checkedAll()
{
  this.acceptList =[];
  if(this.selectedAll == true)
  { 
    for(let i=0;i<this.orderList.length;i++)
    {
     if(this.orderList[i]['order_delivery_Status'] == 2) 
     {
      this.acceptOrderObj = {'id' :this.orderList[i]['orderId'],'seller_id' : this.orderList[i]['seller_id'],'buyer_id' :this.orderList[i]['buyerIds'],'created':moment().format('YYYY-MM-DD HH:mm:ss'),'is_free':this.orderList[i]['isFree'],'lang_id' : this.lang_id};
      this.acceptList.push(this.acceptOrderObj);   
      this.orderList[i]['checked'] =true;
       }
     }
  }else
  {  
     this.acceptList =[];
     for(let j=0;j<this.orderList.length;j++)
    {
      if(this.orderList[j]['order_delivery_Status'] == 2) 
     {
     this.orderList[j]['checked'] =false;
     }
    
  }
}
}
//End 

updateItem(evt,orderItem,orderIndex)
{
         if(!this.isKeyExist(orderItem.orderId,this.acceptList))
         {
                  this.acceptOrderObj = {'id' :orderItem.orderId,'seller_id' : orderItem.seller_id,'buyer_id' :orderItem.buyerIds,'created':moment().format('YYYY-MM-DD HH:mm:ss'),'is_free':orderItem.isFree,'lang_id' : this.lang_id};
                  this.acceptList.push(this.acceptOrderObj);   
                   orderItem.checked =true;
                  //Check all functionality started here 
                  if(this.acceptList.length == this.orderCounts)
                    this.selectedAll =true;
                  else
                    this.selectedAll =false;
        }
         else
         { 
                this.orderList[orderIndex]['checked'] =false;
                this.updateKeyValue(orderItem.orderId,this.acceptList); 
                this.selectedAll =false   ;
         }

}

updateKeyValue(order_id,listArr)
{
  for (var i = listArr.length - 1; i >= 0; --i) {
    if (listArr[i].id == order_id) {
        listArr.splice(i,1);
    }
}
}


isKeyExist(order_id,listArr)
{
  for(var i = 0, len = listArr.length; i < len; i++) {
        if( listArr[i].id == order_id )
        {   
            return true;
        }
    }
    return false
}

//Accpet all orders 

acceptAllOrders()
{
  
 if(this.acceptList.length > 0) 
 {
   //Confirmation 
let alertAccept = this.alertCtrl.create({
    title: this.translationLet.done_txt,
    message: this.translationLet.do_you_want_to_mark_order_to_done,
    buttons: [
      {
        text: this.translationLet.no_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {
        }
      },
      {
        text: this.translationLet.yes_txt,
        cssClass:'btn-primary btn-round',
        handler: () => {
          //Call service and accept order 
         this.showLoading();
          this.serviceProvider.acceptAllDoneOrder(this.acceptList,this.token).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
           this.toastMsg(this.translationLet.order_move_to_done);
           this.selectedAll =false;
           this.orderObj.start=0;
           this.orderObj.limit=100;
           this.receiveOrderList();
          } else
          {
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          }); 
          //End */
       }
      }
      
    ]
  });
    alertAccept.present();
 }
 else
 {
   this.toastMsg(this.translationLet.please_select_one_order);
 }
}
//End functionlaity 


 //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  } 
  
//End

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  //Common functionality 

  gotoHome()
  {
    
     this.app.getRootNav().setRoot(HomePage);
  }

  gotoAccount()
  {
    this.app.getRootNav().setRoot(MyprofilePage); 
  }

gotoMyorder()
    {
     this.app.getRootNav().setRoot(UpcomingPage);
    }


//Go to cover photo
    gotoCoverPhoto()
    {
      if(this.profileDetails.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

      if( (this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null ) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null )  && (this.profileDetails.std_delivery_cost == ''  || this.profileDetails.std_delivery_cost == null))
 {
     this.toastMsg(this.translationLet.delivery_policies_err);
    }else if( (this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
       //Actionsheet 
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
  }
    } 
   }
  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

        //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error =>  this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
    
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
//Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   
   //End  

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}



commonProductList(orderData)
{
if(orderData.length > 0)
{
for(let i=0;i<orderData.length;i++)
{
if(orderData[i].productList.length > 0)
{
orderData[i].productTempList =[];

for(let k=0; k<orderData[i].productList.length; k++)
{
if(orderData[i].productTempList.length > 0)
{
this.returnVal= this.isProductExist(orderData[i].productTempList,orderData[i].productList[k]['product_id']);
if( this.returnVal == 'notExist')
{
orderData[i].productTempList.push(orderData[i].productList[k]);
}else{
orderData[i].productTempList[this.returnVal]['quantity']  = parseInt(orderData[i].productTempList[this.returnVal]['quantity']) + parseInt
(orderData[i].productList[k]['quantity']);
}
}else
{
orderData[i].productTempList.push(orderData[i].productList[k]);
}

} 
}
}
}
}

isProductExist(prodData,prodId)
{
for(let j=0;j<prodData.length;j++)
  {
  if(prodData[j]['product_id'] == prodId)
  return j;
  }
  return 'notExist';
  }
}
