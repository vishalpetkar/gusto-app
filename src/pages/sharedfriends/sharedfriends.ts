import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { JoinorderPage } from '../joinorder/joinorder';
import { JoinhistroydetailPage } from '../joinhistroydetail/joinhistroydetail';
import { BuyerorderdetailsPage } from '../buyerorderdetails/buyerorderdetails';
import moment from 'moment';

@Component({
  selector: 'page-sharedfriends',
  templateUrl: 'sharedfriends.html',
})
export class SharedfriendsPage {
  
  translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  token :any ='';
  shareFriendList :any =[];
  shareFriendObj : any = {
    user_id:'',
    start : 0,
    limit : 10
  };
  isFetchingData: any =true;
  loadmoreData=0;
  isData : any = true;
  dateTimeNew:any ='';
  dateTimeOld : any ='';
  all_months : any =[];
  newDate:any='';
  tXDate:any ='';
  lang_id:any ='';

  constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    
    this.shareFriendObj.start= 0;  
    this.shareFriendObj.limit= 10;  

    this.userImgPath = this.configProvider.getImagePath();
    
    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;

       if( this.lang_id ==1)
        this.all_months=[{'text' : 'Jan', 'value' : 'Jan'},{'text' : 'Feb', 'value':'Feb'},{'text' : 'Mar', 'value':'March'},{'text' : 'Apr', 'value':'April'},{'text' : 'May', 'value':'May'},{'text' : 'Jun', 'value':'June'},{'text' : 'Jul', 'value':'July'},{'text' : 'Aug', 'value':'Aug'},{'text' : 'Sep', 'value':'Sep'},{'text' : 'Oct', 'value':'Oct'},{'text' : 'Nov', 'value':'Nov'},{'text' : 'Dec', 'value':'Dec'}];
           else
        this.all_months=[{'text' : 'Gennaio', 'value' : 'Jan'},{'text' : 'Febbraio', 'value' : 'Feb'},{'text' : 'Marzo', 'value' : 'Mar'},{'text' : 'Aprile', 'value' : 'Apr'},{'text' : 'Maggio', 'value' : 'May'},{'text' : 'Giugno', 'value' : 'Jun'},{'text' : 'Luglio', 'value' : 'Jul'},{'text' : 'Agosto', 'value' : 'Aug'},{'text' : 'Settembre', 'value' : 'Sep'},{'text' : 'Ottobre', 'value' : 'Oct'},{'text' : 'Novembre', 'value' : 'Nov'},{'text' : 'Dicembre', 'value' : 'Dec'}];
     });  


     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

      this.translateService.get(['order_time_elapsed_can_not_join_order','share_friend_req','pull_to_refresh','no_one_share_order_yet','want_to_share_order','rejected','joined','pending','buyer_send_order_to_seller','joined_order','join_order','product_cart_deleted','delete_cart_msg','unable_update_product_note','product_note_updated','pleae_add_note','write_note','add_product_note','unable_update_quantity','product_update_quantity','unable_to_delete_cart','delete','submit','select_quantity','cancel','cart','add_notes','add_promo','proceed','prod_cart_empty','delivery_polices','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     this.storage.get('userDetails').then((userDetails) => {
        this.shareFriendObj.user_id=userDetails.id;
        this.shareFriendRequest();
     });


    
  }

translateDateFun(mixDate)
  {
    for(let i=0;i<this.all_months.length;i++)
{
  if(mixDate.indexOf(this.all_months[i]['text']) != -1)
  {
    this.newDate= mixDate.replace(this.all_months[i]['text'],this.all_months[i]['value']);
  }
}
return this.newDate;

  }

 /*Ionic refresher */
 doFriendRefresh(refresher) {
    this.shareFriendObj.start= 0;  
    this.shareFriendObj.limit= 10;  
    this.shareFriendRequestRefresh(refresher);
  }
 /* End */



//Share friend list Refresh 
shareFriendRequestRefresh (refresher)
{
this.shareFriendObj.start=parseInt(this.shareFriendObj.start);
this.shareFriendObj.limit=parseInt(this.shareFriendObj.limit);  
this.serviceProvider.shareFriendOrderList(this.shareFriendObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.shareFriendList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.shareFriendList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.shareFriendList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

//Cart list 

shareFriendRequest()
{
this.shareFriendObj.start=parseInt(this.shareFriendObj.start);
this.shareFriendObj.limit=parseInt(this.shareFriendObj.limit);	
this.serviceProvider.shareFriendOrderList(this.shareFriendObj,this.token).then((result:any) => {
  this.isFetchingData=false;
            if(result.code == 200){
            this.shareFriendList =result.data;     
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.shareFriendList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.shareFriendList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.shareFriendObj.start = parseInt(this.shareFriendObj.start)+parseInt(this.shareFriendObj.limit);
    this.shareFriendObj.limit= parseInt(this.shareFriendObj.limit)+10;

  setTimeout(() => {

           this.serviceProvider.shareFriendOrderList(this.shareFriendObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.shareFriendList = this.shareFriendList.concat(result.data);
            this.loadmoreData=1;
            if(this.shareFriendList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.shareFriendList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.shareFriendList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 

//Toast Msg 

	toastMsg (msg){
	 let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 2000,
	      position: 'bottom'
	    });
	    toast.present(toast);
	  }

   //End


//Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SharedfriendsPage');
  }

  gotoJoinOrder(order_id,distance,deliveryTime)
  {
    this.tXDate = this.translateDateFun(deliveryTime);
    this.dateTimeOld =moment(this.tXDate).format('x');
    
    this.dateTimeNew =moment().format('x');
    if(this.dateTimeOld < this.dateTimeNew )
    {
     this.toastMsg(this.translationLet.order_time_elapsed_can_not_join_order);
    }else
    {
    this.navCtrl.push(JoinorderPage,{'order_id' : order_id});
   }
  }

 gotoJoinOrderDetails(order_id,seller_id,buyer_id)
 {
  this.navCtrl.push(BuyerorderdetailsPage,{'order_id' : order_id, 'seller_id' : seller_id});
 }
}
