import { Component, ViewChild } from '@angular/core';
import { Platform, ActionSheetController, Nav, MenuController, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import moment from 'moment';
import { BuyerorderdetailsPage } from '../buyerorderdetails/buyerorderdetails';


@Component({
  selector: 'page-deliveryaddress',
  templateUrl: 'deliveryaddress.html'
})

export class DeliveryaddressPage {

  translationLet: any = [];
  loading: Loading;
  token: any = '';
  lang_id: any = '';
  deliveryObj: any = {
    'user_id': "",
    'seller_id': "",
    'cart_id': "",
    'delivery_address': "",
    'latitude': "",
    'longitude': "",
    'delivery_date': "",
    'delivery_time': "",
    'schedule_datetime': "",
    'created': "",
    'modified': "",
    'payment_mode': "",
    'delivery_name': "",
    'delivery_surname': "",
    'delivery_mobile': "",
    'delivery_company': "",
    'credit_card_number': "",
    'temp_card_number': "",
    'expire_month': "",
    'expire_year': "",
    'cvv': "",
    'card_type': "",
    'totalCartAmount': "",
    'delivery_cost': "",
    'free_amt': "",
    'promoCode': "",
    'discount': "",
    'sellerUsedCode': "",
    'order_type': "",
    'productList': []
  };
  isAvailable: any = false;
  isSet: any = false;
  tempEventData: any = [];
  timeSlots: any = [];
  updateDate: any = '';
  calLang: any = '';
  tempCalDate: any = { 'user_id': '', 'sellerId': '', 'event_date': '', 'event_time': '', 'event_value': '' };
  /*Event calender variables*/
  eventSource: any = [];
  viewTitle: any = '';
  calendar = {
    mode: 'month',
    currentDate: ''
  };

  dateTime: any = '';
  dateOne: any = '';
  dateTwo: any = '';
  isPaypal: any = false;
  tempProdData: any = {
    'delivery_cost': '',
    'totalCartAmount': ''
  };
  isDelivery: any = '';
  isUpdateCreateCard: any = true;
  sellerObj: any = { 'user_id': '', 'sellerId': '' };
  paymentMethod: any = [];
  tempArr: any = [];
  sellerDelivery: any = '';
  sellertakeWay: any = '';
  sellerPolicies: any = [];
  buyerAddress: any = {
    'delivery_address': "",
    'latitude': "",
    'longitude': ""
  };
  takeAwayOpt: any = '';
  deliveryOpt: any = '';
  lastElement: any = [];
  eventTimeNew: any = '';
  eventTimeOld: any = '';
  eventOne: any = '';
  eventTwo: any = '';


  cardTypeObj: any = [{ 'id': 1, 'txt': 'visa', 'img': 'assets/images/visa-card.png' }, { 'id': 2, 'txt': 'mastercard', 'img': 'assets/images/master-card.png' }];
  constructor(public platform: Platform, public configProvider: ConfigProvider, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

    this.storage.get('authtoken').then((authtoken) => {
      this.token = authtoken;
    });

    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id = lang_id;
    });

    this.translateService.get(['January', 'December', 'November', 'October', 'September', 'August', 'July', 'June', 'May', 'April', 'March', 'February', 'available', 'selected', 'time_slot_not_available', 'seller_delivery_policy', 'order_type', 'delivery', 'take_away', 'seller_address', 'cancel', 'Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'done_txt', 'please_enter_valid_expire_year', 'please_enter_valid_cvv', 'please_enter_four_digit_for_year', 'ok_txt', 'please_select_prod_delivery_date', 'please_select_prod_delivery_time', 'please_enter_delivery_receiver_name', 'please_enter_credit_card_number', 'please_enter_expire_month', 'card_year_expire_numeric', 'card_cvv_numeric', 'please_enter_valid_phone', 'please_enter_cvv', 'card_month_expire_numeric', 'please_enter_expire_year', 'card_number_numeric', 'please_select_card_type', 'please_select_payment_method', 'date_must_greter_than_current_date', 'please_enter_delivery_receiver_mobile_no', 'please_enter_deliver_receiver_surname', 'service_error', 'please_select_date', 'please_select_time', 'deliver_to', 'name', 'surname', 'mobile_no', 'company', 'note', 'amt', 'with_delivery_cost', 'deduct_amount_seller_accept_txt1', 'reaches_delivery_policies_amt', 'deduct_amount_seller_accept_txt2', 'select_payment_method', 'credit_card_number', 'expire_month', 'expire_year', 'card_cvv', 'credit_card_details', 'update', 'next_txt', 'seller_not_selected_any_payment_method_yet', 'schedule_order', 'delivery_address', 'error_txt', 'please_wait_txt']).subscribe((translation: [string]) => {
      this.translationLet = translation;
      this.takeAwayOpt = this.translationLet.take_away;
      this.deliveryOpt = this.translationLet.delivery;
    });
    this.sellerPolicies = JSON.parse(this.navParams.get('sellerPolicies'));
    //Get details
    this.storage.get('userDetails').then(async (userDetails) => {
      this.deliveryObj.user_id = userDetails.id;
      this.deliveryObj.delivery_name = userDetails.delivery_name;
      this.deliveryObj.delivery_surname = userDetails.delivery_surname;
      this.deliveryObj.delivery_mobile = userDetails.mobile_no;
      this.deliveryObj.delivery_company = userDetails.delivery_comp;
      this.deliveryObj.delivery_address = userDetails.address;
      this.deliveryObj.latitude = userDetails.latitude;
      this.deliveryObj.longitude = userDetails.longitude;
      this.buyerAddress.delivery_address = userDetails.address;
      this.buyerAddress.latitude = userDetails.latitude;
      this.buyerAddress.longitude = userDetails.longitude;
      this.deliveryObj.cart_id = this.navParams.get('cart_id');
      this.deliveryObj.delivery_date = moment().format('YYYY-MM-DD');
      let ROUNDING = 1000 * 60 * 5;
      let start = moment();
      let testDate = moment().format('HH:mm');
      let lastDigit = testDate.toString().split('').pop();
      if ((lastDigit == '0') || (lastDigit == '10') || (lastDigit == '5'))
        this.deliveryObj.delivery_time = testDate;
      else
        this.deliveryObj.delivery_time = moment(Math.ceil((+start) / ROUNDING) * ROUNDING).format('HH:mm');

      this.sellerObj.user_id = userDetails.id;
      this.sellerObj.sellerId = await this.navParams.get('sellerId');
      console.log("this.sellerObj.sellerId", this.sellerObj.sellerId);

      setTimeout(() => {
        this.applyAddress();
      }, 500);

      this.deliveryObj.seller_id = this.navParams.get('sellerId');
      this.tempCalDate.sellerId = this.navParams.get('sellerId');
      this.tempCalDate.user_id = userDetails.id;
      this.tempProdData.delivery_cost = this.navParams.get('delivery_cost');
      this.tempProdData.totalCartAmount = this.navParams.get('totalCartAmount');
      this.deliveryObj.totalCartAmount = this.navParams.get('totalCartAmount');
      this.deliveryObj.delivery_cost = this.navParams.get('delivery_cost');
      this.deliveryObj.free_amt = this.navParams.get('free_delivery_amount');
      this.deliveryObj.promoCode = this.navParams.get('promoCode');
      this.deliveryObj.discount = this.navParams.get('discount');

      console.log("this.deliveryObj", this.deliveryObj);

      //Call calender functionality 
      if (this.sellerPolicies.isTimePolicy == 1) {
        this.gotoCalender();
      }

      //Order policies functionality 
      if (this.sellerPolicies.order_delivery == 1 && this.sellerPolicies.order_take_away == 1) {
        this.tempArr = [{ 'id': 1, 'txt': this.deliveryOpt, 'img': 'assets/images/take-away.png' }, { 'id': 2, 'txt': this.takeAwayOpt, 'img': 'assets/images/store-icon.png' }];
        this.deliveryObj.order_type = 1;
        this.isDelivery = true;
      } else if (this.sellerPolicies.order_delivery == 1 && this.sellerPolicies.order_take_away == 0) {        
        this.tempArr = [{ 'id': 1, 'txt': this.deliveryOpt, 'img': 'assets/images/take-away.png' }];
        this.deliveryObj.order_type = 1;
        this.isDelivery = true;
      } else if (this.sellerPolicies.order_delivery == 0 && this.sellerPolicies.order_take_away == 1) {
        this.tempArr = [{ 'id': 2, 'txt': this.takeAwayOpt, 'img': 'assets/images/store-icon.png' }];
        this.deliveryObj.order_type = 2;
        this.isDelivery = false;
        this.deliveryObj.delivery_address = this.sellerPolicies.address;
        // this.storage.get('userDetails').then((userDetails) => {
        //   console.log(userDetails);
        //   this.deliveryObj.delivery_address = userDetails.pick_address;
        // });
        this.deliveryObj.latitude = this.sellerPolicies.latitude;
        this.deliveryObj.longitude = this.sellerPolicies.longitude;
      }

      //End 

      if (this.navParams.get('sellerUsedCode') == 0) {
        this.deliveryObj.sellerUsedCode = 0;
        this.deliveryObj.promoCode = '';
        this.deliveryObj.discount = '';
      } else {
        this.deliveryObj.sellerUsedCode = this.navParams.get('sellerUsedCode');
        this.deliveryObj.promoCode = this.navParams.get('promoCode');
        this.deliveryObj.discount = this.navParams.get('discount');
      }

      this.deliveryObj.productList = JSON.parse(this.navParams.get('cartList'));

      // Call service to get payment methods from seller 
      this.serviceProvider.sellerPaymentMethod(this.sellerObj, this.token, this.lang_id).then((result: any) => {
        if (result.code == 200) {

          if (result.sellerPayments.length > 0) {
            if (result.sellerPayments.length == 1) {
              this.deliveryObj.payment_mode = result.sellerPayments[0]['id'];
              this.paymentMethod = result.sellerPayments;
            } else {
              this.paymentMethod = result.sellerPayments;
            }
          }
          else {
            this.paymentMethod = [];
          }
          //Is credit card details 
          if (result.cardDetails.length > 0) {
            this.isUpdateCreateCard = false;
            this.deliveryObj.credit_card_number = result.cardDetails[0].credit_card_number;
            this.deliveryObj.temp_card_number = result.cardDetails[0].credit_card_number.replace(/.(?=.{4})/g, 'X');
            this.deliveryObj.expire_month = result.cardDetails[0].expire_month;
            this.deliveryObj.expire_year = result.cardDetails[0].expire_year;
            this.deliveryObj.cvv = '';
            this.deliveryObj.card_type = result.cardDetails[0].card_type;
          } else {
            this.isUpdateCreateCard = true;
          }

          //End 

        } else {
          this.toastMsg(this.translationLet.service_error);
        }

      }, (err) => {
      });
      // End

    });

  }

  applyAddress() {
    console.log("applyAddress");
    console.log("this.deliveryObj", this.deliveryObj);

    if (this.deliveryObj.order_type == 2) {
      this.serviceProvider.getSellerProfile(this.sellerObj.sellerId).then((result: any) => {
        console.log("seller details", result[0].users.pick_address);
        this.deliveryObj.delivery_address = result[0].users.pick_address;
      });
    }
  }

  updateCardDetails() {
    this.isUpdateCreateCard = true;
  }
  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }

  //End

  //Toast Msg 

  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 4000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  //End

  //Submit 

  submit() {
    if (this.validate()) {
      this.showLoading();
      this.deliveryObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
      if (this.sellerPolicies.isTimePolicy == 0)
        this.dateTime = this.deliveryObj.delivery_date + " " + this.deliveryObj.delivery_time;
      else
        this.dateTime = moment(this.tempCalDate.event_date).format('YYYY-MM-DD') + " " + this.tempCalDate.event_time + ":00";

      this.deliveryObj.schedule_datetime = moment(this.dateTime).format('YYYY-MM-DD HH:mm:ss');

      this.serviceProvider.addAsTempOrder(this.deliveryObj, this.token).then((result: any) => {
        this.loading.dismiss();
        if (result.code == 200) {
          this.storage.set('cartCount', result.userDetails.cartCount);
          this.storage.set('userDetails', result.userDetails);
          this.app.getRootNav().setRoot(BuyerorderdetailsPage, { 'order_id': result.order_id, 'seller_id': result.seller_id });
        } else {
          this.toastMsg(this.translationLet.service_error);
        }
      }, (err) => {
        this.loading.dismiss();
      });
    }
  }

  validate() {
    console.log("tempCalDate", this.tempCalDate);
    console.log("deliveryObj", this.deliveryObj);
    if (this.sellerPolicies.isTimePolicy == 1) {
      if (this.tempCalDate.event_date == '') {
        this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_date);
        return false;
      }

      if (this.tempCalDate.event_value == '') {
        console.log("1", this.tempCalDate);
        
        this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_time);
        return false;
      }
    } else {
      if (this.deliveryObj.delivery_date == "") {
        this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_date);
        return false;
      }
      if (this.deliveryObj.delivery_time == "") {
        console.log("3", this.deliveryObj);

        this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_time);
        return false;
      }
    }
    console.log(this.deliveryObj);

    if (this.deliveryObj.delivery_name !== null && this.deliveryObj.delivery_name.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_delivery_receiver_name);
      return false;
    }
    if (this.deliveryObj.delivery_name !== null && this.deliveryObj.delivery_surname.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_deliver_receiver_surname);
      return false;
    }
    if (this.deliveryObj.delivery_name !== null && this.deliveryObj.delivery_mobile.trim() == "") {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_delivery_receiver_mobile_no);
      return false;
    }
    if ((this.deliveryObj.delivery_mobile.length < 8) || (this.deliveryObj.delivery_mobile.length > 14)) {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_enter_valid_phone);
      return false;
    }

    //Check date validation 

    if (this.sellerPolicies.isTimePolicy == 0) {
      this.dateTime = this.deliveryObj.delivery_date + " " + this.deliveryObj.delivery_time;
      this.dateOne = moment(this.dateTime).format('x');
      this.dateTwo = moment().format('x');

      if (this.dateOne < this.dateTwo) {
        this.showPopup(this.translationLet.error_txt, this.translationLet.date_must_greter_than_current_date);
        return false;
      }

    } else {
      this.dateTime = moment(this.tempCalDate.event_date).format('YYYY-MM-DD') + " " + this.tempCalDate.event_time + ":00";
      this.dateOne = moment(this.dateTime).format('x');
      this.dateTwo = moment().format('x');

      if (this.dateOne < this.dateTwo) {
        this.showPopup(this.translationLet.error_txt, this.translationLet.date_must_greter_than_current_date);
        return false;
      }

      if (this.isSet == false) {
        this.showPopup(this.translationLet.error_txt, this.translationLet.seller_delivery_policy);
        return false;
      }
    }
    //End     

    if (this.deliveryObj.payment_mode == '') {
      this.showPopup(this.translationLet.error_txt, this.translationLet.please_select_payment_method);
      return false;
    }

    /*if(this.isPaypal == true)
    {
           
           if (this.deliveryObj.card_type.trim()== "") {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_select_card_type); 
              return false;
            }
    
            if (this.deliveryObj.credit_card_number.trim()== "") {
              this.showPopup(this.translationLet.error_txt,this.translationLet.credit_card_number); 
              return false;
            }
            
           if (isNaN(this.deliveryObj.credit_card_number.trim())) {
             this.showPopup(this.translationLet.error_txt,this.translationLet.card_number_numeric);
              return false;
            }
    
            if (this.deliveryObj.expire_month.trim()== "") {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_expire_month);
              return false;
            }
    
            if (isNaN(this.deliveryObj.expire_month.trim())) {
             this.showPopup(this.translationLet.error_txt,this.translationLet.card_month_expire_numeric);
              return false;
            }
    
            console.log(this.deliveryObj.expire_year.trim());
    
            if (this.deliveryObj.expire_year.trim()== "") {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_expire_year);
              return false;
            }
    
            if (isNaN(this.deliveryObj.expire_year.trim())) {
             this.showPopup(this.translationLet.error_txt,this.translationLet.card_year_expire_numeric);
              return false;
            }
    
            if (this.deliveryObj.expire_year.trim().length < 4  && this.deliveryObj.expire_year.trim() !='') {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_four_digit_for_year);
              return false;
            }
            if (this.deliveryObj.expire_year.trim() < new Date().getFullYear()) {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_expire_year);
              return false;
            }
       
            if (this.deliveryObj.cvv.trim()== "") {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_cvv);
              return false;
            }
    
             if (this.deliveryObj.cvv.trim().length < 3) {
              this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_cvv);
              return false;
            }
    
            if (isNaN(this.deliveryObj.cvv.trim())) {
             this.showPopup(this.translationLet.error_txt,this.translationLet.card_cvv_numeric);
              return false;
            }
    
    } */

    return true;
  }


  //Alert Popups 

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons: [
        {
          text: this.translationLet.ok_txt,
          cssClass: 'btn-primary btn-round'
        }]
    });
    alert.present();
  }

  //End 


  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryaddressPage');
  }

  paymentOpt(paymentId) {

    if (paymentId == 2)
      this.isPaypal = true;
    else
      this.isPaypal = false;
  }

  deliveryOptFun(deliveryId) {

    if (deliveryId == 1) {
      this.isDelivery = true;
      this.deliveryObj.delivery_address = this.buyerAddress.delivery_address;
      this.deliveryObj.latitude = this.buyerAddress.latitude;
      this.deliveryObj.longitude = this.buyerAddress.longitude;
    }
    else {
      this.isDelivery = false;
      this.deliveryObj.delivery_address = this.sellerPolicies.address;
      // this.storage.get('userDetails').then((userDetails) => {
      //   this.deliveryObj.delivery_address = userDetails.pick_address;
      // });
      this.deliveryObj.latitude = this.sellerPolicies.latitude;
      this.deliveryObj.longitude = this.sellerPolicies.longitude;

      this.applyAddress();
    }

  }

  //Calender functionality 
  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    if (date < current) {
      this.timeSlots = [];
      return date < current;
    }
  };

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onTimeBookSelected(ev) {
    if (ev.disabled == true) {
      this.timeSlots = [];
      this.isAvailable = false;
      this.tempCalDate.event_time = '';
      this.tempCalDate.event_value = '';
      this.tempCalDate.event_date = '';
    } else {
      if (ev.events != undefined && ev.events.length != 0) {
        this.timeSlots = [];
        //this.isAvailable =false;
        this.tempCalDate.event_time = '';
        this.tempCalDate.event_value = '';
        this.tempCalDate.event_date = '';
        this.tempCalDate.event_date = moment(ev.selectedTime).format('MM/DD/YYYY');
        this.serviceProvider.getTimeSlots(this.tempCalDate, this.token).then((result: any) => {
          let slot_data
          if (result.result.availableTime && result.result.availableTimeEv && result.result.availableTimeEv.length && result.result.availableTime.length) {
            slot_data = result.result.availableTime.concat(result.result.availableTimeEv);
          } else if (result.result.availableTimeEv && result.result.availableTimeEv.length) {
            slot_data = result.result.availableTimeEv;
          } else if (result.result.availableTime && result.result.availableTime.length) {
            slot_data = result.result.availableTime;
          }

          console.log(result);
          if (result.code == 200) {
            this.timeSlots = slot_data;
            this.setCheckedFalse(this.timeSlots);
          } else if (result.code == 500) {
            this.timeSlots = [];
          } else {
            this.timeSlots = [];
          }
        }, (err) => {
        });
      } else {
        this.timeSlots = [];
        this.tempCalDate.event_time = '';
        this.tempCalDate.event_value = '';
        this.tempCalDate.event_date = '';
      }
    }
  }

  getEventTime(times, timeObj, indexVal) {
    console.log(times, timeObj, indexVal);
    timeObj = this.timeSlots;
    this.tempCalDate.event_time = '';
    this.tempCalDate.event_value = '';
    this.eventTimeNew = '';
    this.eventTimeOld = '';
    this.eventOne = '';
    this.eventTwo = '';
    this.isSet = false;
    //Calculation part start 
    this.lastElement = timeObj[timeObj.length - 1];
    this.eventTimeNew = moment(this.tempCalDate.event_date).format('YYYY-MM-DD') + " " + times.id + ":00";
    this.eventTimeOld = moment(this.tempCalDate.event_date).format('YYYY-MM-DD') + " " + this.lastElement['id'] + ":00";
    this.eventTimeNew = moment(this.eventTimeNew).add(this.sellerPolicies.sellerOrderTime, 'hours').format('YYYY-MM-DD HH:mm');
    this.eventOne = moment(this.eventTimeNew).format('x');
    this.eventTwo = moment(this.eventTimeOld).format('x');

    this.tempCalDate.event_time = times.id;
    this.tempCalDate.event_value = times.value;
    this.timePush(timeObj, indexVal);

    if (this.eventOne <= this.eventTwo)
      this.isSet = true;
    else
      this.isSet = false;

  }


  timePush(timeList, indexVal) {
    timeList = this.timeSlots;
    for (var f = 0, len = timeList.length; f < len; f++) {
      if (f == indexVal) {
        timeList[f].checked = true;
      } else {
        timeList[f].checked = false;
      }
    }
  }

  async setCheckedFalse(timeSlotsData) {
    console.log('timeSlotsData', timeSlotsData);
    let newData = [];
    await timeSlotsData.forEach(element => {
      if (element !== undefined && element.value !== undefined) {
        newData.push(element);
      }
    });
    timeSlotsData = newData;
    this.timeSlots = newData;
    timeSlotsData = timeSlotsData.filter(function( element ) {
      return element !== undefined;
    });
   
    for (let f = 0; f < timeSlotsData.length; f++) {
      if (timeSlotsData !== undefined && timeSlotsData[f] !== undefined && timeSlotsData[f].value !== undefined) {
        timeSlotsData[f].checked = false;
      }
    }
  }

  //Goto Calender 

  gotoCalender() {
    this.serviceProvider.getSellerAvailDates(this.sellerObj, this.token).then((resultDates: any) => {

      if (resultDates.code == 200) {
        if (resultDates.data.length > 0) {
          for (let i = 0; i < resultDates.data.length; i++) {
            this.tempEventData.push({ 'startTime': new Date(resultDates.data[i]), 'endTime': new Date(resultDates.data[i]) });
          }
          this.eventSource = this.tempEventData;
        }
      } else {
        this.eventSource = [];
      }
    }, (err) => {
      console.log('err ' + err);
    });
  }
  //End 


  removeChars(val) {
    return this.convertTime12to24(val);
  }

  convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');
  
    let [hours, minutes] = time.split(':');
  
    if (hours === '12') {
      hours = '00';
    }
  
    if (modifier === 'PM') {
      hours = parseInt(hours, 10) + 12;
    }
  
    return `${hours}:${minutes}`;
  }
}
