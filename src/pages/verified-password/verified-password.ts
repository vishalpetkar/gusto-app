import { Component } from '@angular/core';
import { MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ResetpasswordPage } from '../resetpassword/resetpassword';

@Component({
  selector: 'page-verified-password',
  templateUrl: 'verified-password.html',
})
export class VerifiedPasswordPage {

 translationLet: any;	
  loading: Loading;

  loginData : any = {
  user_id : ''
  };

tempOTP : any ='';
otp :any ='';

tempObj :any =[];

 constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.translateService.get(['otp_send_msg','please_enter_valid_otp','varify_phone_number','varify_your_number','four_digit_txt','please_enter_otp','resend_code','not_receive_code','resend_code','service_error','please_wait_txt','error_txt','ok_txt','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
   this.tempObj= JSON.parse(navParams.get('ResetObj')); 
   this.tempOTP=this.tempObj.otp;
   this.loginData.user_id = this.tempObj.user_id;
   this.tempObj.phoneNumber = this.tempObj.mobile_no;
  }

  //Next with validations 

  
  submit()
{
  if(this.validate())
  {
   this.navCtrl.push(ResetpasswordPage,{"user_id" : this.loginData.user_id});
 }
}

validate()
{

  if (this.otp =='') {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_otp);
          return false;
        }

   if (this.otp !=this.tempOTP) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_otp);
          return false;
        }

  return true;
}

resendCode()
{
  this.showLoading();
   this.serviceProvider.resendCodeReset(this.tempObj.phoneNumber).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
     this.otp = ''; 
     this.tempOTP= result.otp;
     this.toastMsg(this.translationLet.otp_send_msg);
    }
    else if(result.code == 201){
       this.toastMsg(result.error);
    } else{
     this.toastMsg(this.translationLet.service_error);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
}

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

    //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 

  //End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad VarifyPage');
  }

}
