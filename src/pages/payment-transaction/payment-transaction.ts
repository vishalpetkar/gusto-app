import { Component,ViewChild } from '@angular/core';
import { IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';


@Component({
  selector: 'page-payment-transaction',
  templateUrl: 'payment-transaction.html',
})
export class PaymentTransactionPage {
  
  token :any ='';
	translationLet: any;	
	loading: Loading;
    tempObj :any = {'seller_id' : '','buyer_id':'','id' : '','user_id':''};

    orderSummary : any ={ 
     delivery_date_time : '',
     delivery_address : '',
     delivery_cost:'',
     devliery_total_amount:'',
     productList : []
    };

    userImgPath : any ='';
    prodImgPath : any ='';

 constructor(public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();
    

     this.translateService.get(['delivery','take_away','seller_address','pick_up_time','buyer','joined_buyer','un_paid','paid','please_wait_txt','service_error','coupen_applied','total_amt','free_delivery_txt','delivery_cost_txt','order_details','delivery_date_time','delivery_address','delivery_cost_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  

   this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 
   
  	this.storage.get('userDetails').then((userDetails) => {
  	  this.tempObj= JSON.parse(this.navParams.get('ordersSummary'));  
      this.tempObj.user_id=userDetails.id;
      this.getOrderSummary();
    }); 
 }


 getOrderSummary()
 {
this.showLoading();

this.serviceProvider.getOrderSummary(this.tempObj,this.token).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
            this.orderSummary =result.result;
          } else
          {
             this.orderSummary=[];
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          }); 

 }

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

//Toast Message

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentTransactionPage');
  }

}
