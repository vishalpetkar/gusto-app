import { Component,ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { Media, MediaObject } from '@ionic-native/media';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions,CaptureAudioOptions } from '@ionic-native/media-capture';
import { FileTransfer,FileUploadOptions,FileTransferObject} from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config/config';
import { AddproductformPage } from '../addproductform/addproductform';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { Crop } from '@ionic-native/crop';
import { ExplorePage } from '../explore/explore';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-coverphoto',
  templateUrl: 'coverphoto.html',
})
export class CoverphotoPage {
  
  translationLet: any;	
  loading: Loading;	
  apiPath :any =''; 
  token :any ='';

  productDetails : any = {
    user_id : '',
    product_image : ''
  };
  
  tempImg :any ='';
  
  myorderCount :any =0;
  receiveOrderCount :any =0;

    constructor(private crop: Crop,public platform: Platform,public configProvider : ConfigProvider,private transfer: FileTransfer,public actionSheetCtrl: ActionSheetController,private camera: Camera,private media: Media,private mediaCapture: MediaCapture,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   
   this.apiPath = this.configProvider.getAPIpath();
   this.tempImg =navParams.get('tempProductImg');
   this.productDetails.product_image=navParams.get('tempProductImg');

   this.translateService.get(['unable_to_crop_image','err_upload_file','please_select_image','ok_txt','please_wait_txt','unAuthReq_msg','service_error','unable_capture_image','cancel','product_cover_photo','change_cover_photo','next_txt','take_photo','upload_from_gallery']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });

 
   //Get details
 this.storage.get('userDetails').then((userDetails) => {
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.productDetails.user_id=userDetails.id;
  }); 
 
	 //Get token 
	this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
  });
  }
 

 //Change image 

coverPhoto ()
{

	let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.product_cover_photo,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
}



//Take a photo 

takePhoto ()
{
const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
//Get picture function 
this.camera.getPicture(options).then((imageData) => {
 //Crop Image 
this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
 }, (err) => {
 this.toastMsg(this.translationLet.unable_capture_image);
});

}

CropedImg (imageData)
{
// File for Upload
  if(this.platform.is('ios'))
  {
  this.productDetails.product_image =normalizeURL(imageData);
  this.tempImg = normalizeURL(imageData);
  }else
  {
  this.productDetails.product_image =imageData;
  this.tempImg = imageData;
  }
}



takePhotoGallery ()
{
const options: CameraOptions = {
      quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,                                                                                                         
            correctOrientation: true,
            targetWidth: 900,                                       
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
	//Get picture function 
	this.camera.getPicture(options).then((imageData) => {
	 //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
	}, (err) => {
	 this.toastMsg(this.translationLet.unable_capture_image);
	});

}

//Submit 

submit()
{

if(this.productDetails.product_image != '')
{

 this.showLoading();

var url =this.apiPath+"productCoverPhoto";
  // File for Upload
  var targetPath = this.productDetails.product_image;
  // File name only
  var filename = 'image.jpg';
  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename,'id':this.productDetails.user_id},
    headers: {'Authorization': 'Bearer '+this.token}
  };

  const fileTransfer: FileTransferObject = this.transfer.create();
  // Use the FileTransfer to upload the image
  
  fileTransfer.upload(targetPath, url, options).then(data => {

    var jsonObject : any = JSON.parse(data.response);
    let imgResponse:any;
    if(jsonObject.code == 200){
      this.productDetails.product_image=jsonObject.data;
         this.serviceProvider.addProductCover(this.productDetails,this.token).then((result:any) => {

         this.loading.dismiss();
            if(result.code == 200){
           this.navCtrl.push(AddproductformPage,{'id' : result.product_id,'cover_photo' : result.cover_photo });
          } else if(result.code == 500) {
            this.toastMsg(this.translationLet.service_error);
          }else{
          	  this.toastMsg(this.translationLet.service_error);
          } 
          }, (err) => {
          this.loading.dismiss();
           this.toastMsg(this.translationLet.service_error);
          });
    } else if(jsonObject.code == 500){
      this.loading.dismiss(); 
      this.toastMsg(this.translationLet.service_error);
    } else if(jsonObject.code == 403){
      this.loading.dismiss();	
      this.toastMsg(this.translationLet.unAuthReq_msg);
    } 
   
  }, err => {
    this.loading.dismiss();
    this.toastMsg(this.translationLet.err_upload_file);
  });


}else {
this.toastMsg(this.translationLet.please_select_image);
}

}

//End 

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

//Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

  ionViewDidLoad() {
    console.log('ionViewDidLoad CoverphotoPage');
  }
 

 //Foote section
  
  gotoHome()
  {
  this.app.getRootNav().setRoot(HomePage); 
  }
  
  gotoAccount()
  {
  this.app.getRootNav().setRoot(MyprofilePage); 
  }
  
          gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }



}
