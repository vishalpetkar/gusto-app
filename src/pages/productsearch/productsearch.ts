import { Component,ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { Crop } from '@ionic-native/crop';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { ExplorePage } from '../explore/explore';
import { TransactionsPage } from '../transactions/transactions';
import { HomePage } from '../home/home';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-productsearch',
  templateUrl: 'productsearch.html',
})
export class ProductsearchPage {
  
  translationLet: any = [];	
  loading: Loading;	
  tempProductImg : any = "";
  loginUserData : any = {'id' : '',address:'',latitude:'',longitude:'',order_take_away : ''}; 
    isFetchingData: any =true;
   productDetails : any = {
    user_id:'',
    search_prod : '',
    start : 0,
    limit : 10
  };

   userImgPath : any ='';
   prodImgPath : any ='';

   token :any ='';
  myorderCount :any =0;
  receiveOrderCount :any =0;


  loadmoreData=0;
  isData : any = true ;
  productListing :any =[];

  constructor(private crop: Crop,public actionSheetCtrl: ActionSheetController,public platform: Platform,public configProvider : ConfigProvider,private camera: Camera,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
  
/*Ionic refresher */

 doProdRefresh(refresher) {
    this.productDetails.start= 0;  
    this.productDetails.limit= 10;  
    this.initproductsRefresh(refresher);
  }
 /* End */

gotoSearch()
{
  this.productDetails.start = 0;
  this.productDetails.limit = 10;
  this.initProducts();
}

initproductsRefresh (refresher)
{
this.productDetails.user_id=this.loginUserData.id;
this.productDetails.start=parseInt(this.productDetails.start);
this.productDetails.limit=parseInt(this.productDetails.limit);  
//this.showLoading();
this.serviceProvider.getProducts(this.productDetails,this.token).then((result:any) => {

          //this.loading.dismiss();
            if(result.code == 200){
            this.productListing =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
      //  this.loading.dismiss();
          console.log('err '+err);
          refresher.complete();
          }); 
}


ionViewDidEnter(){
    
    this.productDetails.start= 0;  
    this.productDetails.limit= 10;  

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

    this.translateService.get(['select_one_payment_method','unable_to_crop_image','product_search','search_by_name','pull_to_refresh','cancel','post_new_product','take_photo','upload_from_gallery','something_wrong_txt','unable_to_fetch_gps','please_wait_gps_txt','prod_del_err','prod_del_success','yes_txt','no_txt','delete','delete_product_txt','likes','view_all_comment','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     //Get details

    this.storage.get('userDetails').then((userDetails) => {

        this.myorderCount = userDetails.myorderCount;
        this.receiveOrderCount = userDetails.receiveOrderCount;
        this.loginUserData=userDetails;
        this.initProducts();
     }); 
      
      //End 
  }


 dismiss()
      {
      this.productDetails.search_prod ="";
      this.productDetails.start= 0;  
      this.productDetails.limit= 10;  
       this.initProducts();
      }

  initProducts()
{

this.productDetails.user_id=this.loginUserData.id;
this.productDetails.start=parseInt(this.productDetails.start);
this.productDetails.limit=parseInt(this.productDetails.limit);	
//this.showLoading();
this.serviceProvider.getProducts(this.productDetails,this.token).then((result:any) => {
   this.isFetchingData=false;
          //this.loading.dismiss();
            if(result.code == 200){
            this.productListing =result.data;     
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
      //  this.loading.dismiss();
          console.log('err '+err);
          }); 
}

 
infiniteScrollFun(infiniteScroll) {

    this.productDetails.start = parseInt(this.productDetails.start)+parseInt(this.productDetails.limit);
    this.productDetails.limit= parseInt(this.productDetails.limit)+10;

  setTimeout(() => {

           this.serviceProvider.getProducts(this.productDetails,this.token).then((result:any) => {
            if(result.code == 200){
            this.productListing = this.productListing.concat(result.data);
            this.loadmoreData=1;
            if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 

//Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

gotoProductDetailPage(product_id,p_user_id,product_distance){
  
  if(p_user_id != this.productDetails.user_id)
		this.navCtrl.push(ProductdetailsPage,{'product_id' : product_id, 'product_distance' : product_distance });
	}

//Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}

 //Common Pages 
   
    gotoExplore()
   {
    this.app.getRootNav().setRoot(ExplorePage); 
   }

    gotoAccount()
    {
      this.app.getRootNav().setRoot(MyprofilePage); 
    }
    
    //Go to cover photo

    gotoCoverPhoto()
    {
     if(this.loginUserData.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

   if( (this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null ) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null )  && (this.loginUserData.std_delivery_cost == ''  || this.loginUserData.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else if( (this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method)
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {

    //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
 }
  }
}

  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

  //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

CropedImg(imageData)
{
if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg });
}

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
 //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }

   //Toast Msg 

	toastMsg (msg){
	 let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 2000,
	      position: 'bottom'
	    });
	    toast.present(toast);
	  }

   //End


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsearchPage');
  }

  gotoHome()
 {
  this.app.getRootNav().setRoot(HomePage); 
 }

          gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }



}
