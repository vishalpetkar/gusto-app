import { Component,ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { DeliverypolicyPage } from '../deliverypolicy/deliverypolicy'; 
import { PaymentTransactionPage } from '../payment-transaction/payment-transaction';

@Component({
  selector: 'page-transactiondetails',
  templateUrl: 'transactiondetails.html',
})
export class TransactiondetailsPage {

  translationLet: any;  
  loading: Loading; 
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  isFetchingData: any =true;
  userObj :any ={
    id: '',
    tx_id : ''
  };

  txItems : any ={'joinOrders' : []};
  buyerOrderObj : any ={'buyer_id' : '','seller_id' : '' , 'id' : '', 'is_free' : '' };

constructor(public platform: Platform,public configProvider : ConfigProvider,public actionSheetCtrl: ActionSheetController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

  this.translateService.get(['pick_up_time','orders_list','ordered_date_time','deliverd_date_time','free_delivery_txt','joined_order','total','delivery_cost_txt','payment_summary','order_place_time','delivery_time','tx_details','unable_to_share','cancel','post_new_product','take_photo','upload_from_gallery','refreshing','delivery_policies_err','prod_del_success','prod_del_err','delete','delete_product_txt','yes_txt','no_txt','load_more','prod_not_added','my_product','publish','profile_update_success','profile_update_err','email_already_exist','something_wrong_txt','unAuthReq_msg','err_upload_file','cancel','upload_profile_image','take_photo','upload_from_gallery','please_enter_valid_mobile','please_enter_mobile','email_not_valid','error_txt','please_enter_name','please_enter_email','please_wait_txt','basic_info','ok_txt','edit_profile','update_profile','name','mobile_no','email']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
   this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
  });

   //Get details
 this.storage.get('userDetails').then((userDetails) => {
      this.userObj.id=userDetails.id;
      this.userObj.tx_id = this.navParams.get('tx_id');
       this.txDetails();
  }); 
  }

 txDetails()
 {
//  this.showLoading();
  this.serviceProvider.txDetails(this.userObj,this.token).then((result:any) => {
    this.isFetchingData =false; 
       //   this.loading.dismiss();
            if(result.code == 200){
              this.txItems =result.data[0];     
  //            console.log(JSON.stringify(this.txItems));
          } else if(result.code == 500) {
             this.txItems=[];
          } else
          {
             this.txItems=[];
          }
          }, (err) => {
    //      this.loading.dismiss();
          console.log('err '+err);
          }); 
 }


  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactiondetailsPage');
  }

 
  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
//End

  gotoDeliveryPolicy(){
  	this.navCtrl.push(DeliverypolicyPage);
  }

  gotoPaymentTransaction(seller_id,buyer_id,order_id,delivery_cost){
      this.buyerOrderObj.is_free =delivery_cost;
      this.buyerOrderObj.buyer_id =buyer_id;
      this.buyerOrderObj.seller_id =seller_id;
      this.buyerOrderObj.id =order_id;
      this.navCtrl.push(PaymentTransactionPage,{'ordersSummary' : JSON.stringify(this.buyerOrderObj)});
  }

gotoBuyerProfile(buyerId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : buyerId});
}


}
