import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import { UpcomingPage } from '../upcoming/upcoming';
import { SharedfriendsPage } from '../sharedfriends/sharedfriends';
import { SellerreceivedorderPage } from '../sellerreceivedorder/sellerreceivedorder'; 
import { BuyerorderdetailsPage } from '../buyerorderdetails/buyerorderdetails';
import { JoinorderPage } from '../joinorder/joinorder';
import { MyprofilePage } from '../myprofile/myprofile';
import { JoinhistroydetailPage } from '../joinhistroydetail/joinhistroydetail';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
 
  translationLet: any = []; 
  loading: Loading; 
  token :any ='';
  notiList : any =[];
  userImgPath : any ='';
  notiObj : any = {
    user_id:'',
    start : 0,
    limit : 10
  };
  isFetchingData: any =true;
  loadmoreData=0;
  isData : any = true ;
  lang_id: any ='';

  delNotiObj : any ={ 'id' : '' ,'user_id' :''};


   constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }

ionViewDidEnter()
{ 
    this.notiObj.start= 0;  
    this.notiObj.limit= 10;  
    
     this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;
     });  

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

    this.userImgPath = this.configProvider.getImagePath();

      this.translateService.get(['notification_del_successfully','unable_to_del_notification','delete','do_you_want_to_del_noti','notification','pull_to_refresh','notification_not_found','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

    //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.notiObj.user_id =userDetails.id; 
        this.delNotiObj.user_id =userDetails.id; 
        this.getNotification();
     }); 

}
getNotification()
{

this.notiObj.start=parseInt(this.notiObj.start);
this.notiObj.limit=parseInt(this.notiObj.limit);  
this.serviceProvider.getNotification(this.notiObj,this.token,this.lang_id).then((result:any) => {
this.isFetchingData=false;
            if(result.code == 200){
            this.notiList =result.data;     
             this.loadmoreData=1;
            this.isData =true;
          } else if(result.code == 500) {
             this.notiList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.notiList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
}

//Redirection for Order 

redirctPage(notiItem)
{
  console.log(notiItem.type);
  //Seller
  if(notiItem.type == 1 || notiItem.type == 5 || notiItem.type == 6 )
  {
    this.navCtrl.push(SellerreceivedorderPage,{'order_id' : notiItem.order_id, 'user_id' : notiItem.seller_id, 'buyer_id' : notiItem.buyer_id});
  //Buyer
  }else if(notiItem.type == 16 || notiItem.type == 2 || notiItem.type == 3 || notiItem.type == 4 || notiItem.type == 7 || notiItem.type == 10)
  {
   this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : notiItem.order_id, 'seller_id' : notiItem.seller_id});
  }else if(notiItem.type == 8)
  {
    this.navCtrl.push(JoinorderPage,{'order_id' : notiItem.order_id});
  }
  else if(notiItem.type == 9)
  {
    this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : notiItem.order_id, 'seller_id' : notiItem.seller_id});
  }
  else if(notiItem.type == 17 || notiItem.type == 11 || notiItem.type ==12)
  {
   this.navCtrl.push(SellerreceivedorderPage,{'order_id' : notiItem.order_id, 'user_id' : notiItem.seller_id, 'buyer_id' : notiItem.buyer_id});
  }
  else if(notiItem.type == 15)
  {
   this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : notiItem.order_id, 'seller_id' : notiItem.seller_id});
  }
  //Join order 
}

//End 

//Delet Notification

deleteNoti(item,indexVal)
{

this.delNotiObj.id=item;

  let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.do_you_want_to_del_noti,
    buttons: [
      {
        text: this.translationLet.yes_txt,
        handler: () => {
      //  Delete  
    this.showLoading();
    this.serviceProvider.delNotification(this.delNotiObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     this.notiList.splice(indexVal, 1);  
     this.toastMsg(this.translationLet.notification_del_successfully);
     if(!(this.notiList.length >0))
     {
      this.isData =false;
     }

    }else
    {
   this.toastMsg(this.translationLet.unable_to_del_notification);
    }
        }, (err) => {
          this.loading.dismiss();
        });
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        }
      }
    ]
  });
  alert.present();  

}

  /*Ionic refresher */
 doNotiRefresh(refresher) {
    this.notiObj.start= 0;  
    this.notiObj.limit= 10;  
    this.getNotificationRefresher(refresher);
  }
 /* End */


getNotificationRefresher(refresher)
{
this.notiObj.start=parseInt(this.notiObj.start);
this.notiObj.limit=parseInt(this.notiObj.limit);  
this.serviceProvider.getNotification(this.notiObj,this.token,this.lang_id).then((result:any) => {
            if(result.code == 200){
            this.notiList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.notiList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.notiList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.notiObj.start = parseInt(this.notiObj.start)+parseInt(this.notiObj.limit);
    this.notiObj.limit= parseInt(this.notiObj.limit)+10;

  setTimeout(() => {

           this.serviceProvider.getNotification(this.notiObj,this.token,this.lang_id).then((result:any) => {
            if(result.code == 200){
            this.notiList = this.notiList.concat(result.data);
            this.loadmoreData=1;
            if(this.notiList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.notiList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.notiList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}


  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

    //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
//End

//Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

   //End
  
}

 
