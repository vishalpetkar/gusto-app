import { Component} from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { HomePage } from '../home/home';
import { Crop } from '@ionic-native/crop';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { DeliverypolicyPage } from '../deliverypolicy/deliverypolicy';
import { SellerjoinproductsPage } from '../sellerjoinproducts/sellerjoinproducts';
import { SellerreceivedorderPage } from '../sellerreceivedorder/sellerreceivedorder'; 
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal'
import { UpcomingPage } from '../upcoming/upcoming';
import moment from 'moment';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-joinorder',
  templateUrl: 'joinorder.html',
})
export class JoinorderPage {
  
  translationLet: any = []; 
  loading: Loading; 
  tempProductImg : any = "";
  userImgPath : any ='';
  prodCatImgPath : any ='';
  prodImgPath:any ='';
  tempArr :any =[];
   isFetchingData: any =true;
  token :any ='';
  loginUserData : any = {'id' : '',address:'',latitude:'',longitude:'', order_take_away : ''}; 
  exploreDetailsObj : any = { 'order_type' : '','user_id' : '','order_id' : '', 'isSellerPromoCodeUsed' : '', 'discount_percentage' : '' };

  orderDetails :any = { 'distance' : '', joinBuyer : [],userPolicies : ''};
  distance :any ='0';
  dateTimeOld:any ='';
  dateTimeNew:any ='';
  myorderCount :any =0;
  receiveOrderCount :any =0;
  all_months : any =[];
  newDate:any='';
  tXDate:any ='';
  lang_id: any ='';


   constructor(private payPal: PayPal,private crop: Crop,public actionSheetCtrl: ActionSheetController,public platform: Platform,public configProvider : ConfigProvider,private camera: Camera,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }

  ionViewDidEnter()
  {
       this.prodImgPath = this.configProvider.getProdImgPath();
        this.userImgPath = this.configProvider.getImagePath();
        this.exploreDetailsObj.order_id = this.navParams.get('order_id');
        //this.distance = this.navParams.get('distance');

        this.translateService.get(['pick_up_time','delivery','take_away','seller_address','seller','buyer','not_added_txt','order_time_elapsed_can_not_join_order','unable_to_crop_image','order_time_elapsed','select_one_payment_method','coupen_applied_buyer_got','buyer_joined_with_other_prod','delivery_polices','free_delivery_all','delivery_time','delivery_place','coupen_applied_txt','view_order','joined_order','join_order','order_details','cancel','post_new_product','take_photo','upload_from_gallery','something_wrong_txt','unable_to_fetch_gps','please_wait_gps_txt','prod_del_err','prod_del_success','yes_txt','no_txt','delete','delete_product_txt','likes','view_all_comment','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
       
      this.storage.get('lang_id').then((lang_id) => {
         this.lang_id=lang_id;

       if( this.lang_id ==1)
        this.all_months=[{'text' : 'Jan', 'value' : 'Jan'},{'text' : 'Feb', 'value':'Feb'},{'text' : 'Mar', 'value':'March'},{'text' : 'Apr', 'value':'April'},{'text' : 'May', 'value':'May'},{'text' : 'Jun', 'value':'June'},{'text' : 'Jul', 'value':'July'},{'text' : 'Aug', 'value':'Aug'},{'text' : 'Sep', 'value':'Sep'},{'text' : 'Oct', 'value':'Oct'},{'text' : 'Nov', 'value':'Nov'},{'text' : 'Dec', 'value':'Dec'}];
           else
        this.all_months=[{'text' : 'Gennaio', 'value' : 'Jan'},{'text' : 'Febbraio', 'value' : 'Feb'},{'text' : 'Marzo', 'value' : 'Mar'},{'text' : 'Aprile', 'value' : 'Apr'},{'text' : 'Maggio', 'value' : 'May'},{'text' : 'Giugno', 'value' : 'Jun'},{'text' : 'Luglio', 'value' : 'Jul'},{'text' : 'Agosto', 'value' : 'Aug'},{'text' : 'Settembre', 'value' : 'Sep'},{'text' : 'Ottobre', 'value' : 'Oct'},{'text' : 'Novembre', 'value' : 'Nov'},{'text' : 'Dicembre', 'value' : 'Dec'}];
     });  


       this.storage.get('authtoken').then((authtoken) => {
            this.token=authtoken;
        });
      
        this.storage.get('userDetails').then((userDetails) => {
            this.myorderCount = userDetails.myorderCount;
            this.receiveOrderCount = userDetails.receiveOrderCount;
            this.loginUserData=userDetails;
            this.exploreDetailsObj.user_id = this.loginUserData.id;
            this.getExploreDetails(this.exploreDetailsObj);
         }); 
  }

  translateDateFun(mixDate)
  {
    for(let i=0;i<this.all_months.length;i++)
{
  if(mixDate.indexOf(this.all_months[i]['text']) != -1)
  {
    this.newDate= mixDate.replace(this.all_months[i]['text'],this.all_months[i]['value']);
  }
}
return this.newDate;

  }

//Get explore details 

getExploreDetails(exploreObj)
{
//this.showLoading();
this.serviceProvider.getExploreDetails(exploreObj,this.token).then((result:any) => {
this.isFetchingData =false;
  //        this.loading.dismiss();
            if(result.code == 200){
            //code success
            this.orderDetails = result.data;
            this.distance=result.data.distance;
            this.orderDetails.distance =result.data.distance;
            this.exploreDetailsObj.isSellerPromoCodeUsed = result.data.isSellerPromoCodeUsed;
            this.exploreDetailsObj.discount_percentage = result.data.discount_percentage;      
            this.exploreDetailsObj.order_type = result.data.order_type;      
          } else
          {
            //code error
            this.orderDetails =[];
          }
          }, (err) => {
    //      this.loading.dismiss();
          console.log('err '+err);
          }); 
}


//Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End


//Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

//End

//Common Pages 
   
    gotoHome()
   {
    this.app.getRootNav().setRoot(HomePage); 
   }

          gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }


    gotoAccount()
    {
      this.app.getRootNav().setRoot(MyprofilePage); 
    }
    
    //Go to cover photo

    gotoCoverPhoto()
    {

       if(this.loginUserData.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

    if( (this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null ) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null )  && (this.loginUserData.std_delivery_cost == ''  || this.loginUserData.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else if( (this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {

    //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
 }
  }
}

  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

  //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

CropedImg(imageData)
{
if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg });
}

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
 //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }

  //Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}

gotoDelPolicies(deliveryPolicies)
{
 this.navCtrl.push(DeliverypolicyPage, {'userPolicies' : JSON.stringify(deliveryPolicies)});
}

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad JoinorderPage');
  }

//Goto Join order section
gotoJoins(deliveryTime)
{
this.tXDate = this.translateDateFun(deliveryTime);
this.dateTimeOld =moment(this.tXDate).format('x');
this.dateTimeNew =moment().format('x');
if(this.dateTimeOld < this.dateTimeNew )
{
 this.toastMsg(this.translationLet.order_time_elapsed_can_not_join_order);
}else
{
  this.navCtrl.push(SellerjoinproductsPage,{'order_type' : this.exploreDetailsObj.order_type,'isSellerPromoCodeUsed':this.exploreDetailsObj.isSellerPromoCodeUsed,'discount_percentage': this.exploreDetailsObj.discount_percentage,'order_id' : this.exploreDetailsObj.order_id, 'seller_id' : this.orderDetails.seller_id,'buyer_id' : this.orderDetails.buyer_id,'delivery_lat' : this.orderDetails.delivery_lat,'delivery_long' : this.orderDetails.delivery_long});
}
}



//SellerDetails

gotoReceiveOrder()
{
this.navCtrl.push(SellerreceivedorderPage,{'order_id' : this.exploreDetailsObj.order_id, 'user_id' : this.loginUserData.id, 'buyer_id' : this.orderDetails.buyer_id});
}


//End



}
