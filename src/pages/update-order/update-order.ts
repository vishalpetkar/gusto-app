import { Component } from '@angular/core';
import { ViewController,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { SelectaddressPage } from '../selectaddress/selectaddress';
import { HomePage } from '../home/home';
import moment from 'moment';

@Component({
  selector: 'page-update-order',
  templateUrl: 'update-order.html',
})
export class UpdateOrderPage {
    translationLet: any;	
    loading: Loading;
    orderObj : any ={'id' : '','seller_id':'','buyer_id':'','role':'','user_id' : '','record_id' : '','notes' : '','quantity' : ''};
    token :any ='';
    quantityList : any = [];
    selectOptions : any ={};
    dismissVal :any = {'status' : '','tempArr' : [],'action' : ''};

 constructor(public viewCtrl: ViewController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    
          this.selectOptions = {
          title: ''
          };
        this.quantityList = [{'text' : '1', 'val' : '1'},{'text' : '2', 'val' : '2'},{'text' : '3', 'val' : '3'},
   {'text' : '4', 'val' : '4'},{'text' : '5', 'val' : '5'},{'text' : '6', 'val' : '6'},
   {'text' : '7', 'val' : '7'},{'text' : '8', 'val' : '8'},{'text' : '9', 'val' : '9'},{'text' : '10', 'val' : '10'}];

         this.storage.get('authtoken').then((authtoken) => {
          this.token=authtoken;
         }); 
      this.translateService.get(['save','qty','unable_to_update_order','order_updated_success','edit_order','add_product_note','promo_code_successfully_applied','service_error','add_promo_code','add_product_note','enter_promo_code','please_add_promo_code','please_enter_valid_promo_code','used_promo_code','you_got_txt','pull_to_refresh','product_cart_deleted','delete_cart_msg','unable_update_product_note','product_note_updated','pleae_add_note','write_note','add_product_note','unable_update_quantity','product_update_quantity','unable_to_delete_cart','delete','submit','select_quantity','cancel','cart','add_notes','add_promo','proceed','prod_cart_empty','delivery_polices','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
     this.selectOptions.title= this.translationLet.select_quantity;
	   this.orderObj=navParams.get('orderDetails'); 
  }

  //Quantity changes 

onSelectChange(events)
{
  this.orderObj.quantity = events;
}

  
//Submit and validtions 


submit()
{
  this.showLoading();
    this.serviceProvider.updateOrder(this.orderObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     this.toastMsg(this.translationLet.order_updated_success);
     this.dismissVal.tempArr =this.orderObj;
     this.dismissVal.status =1;
     this.dismissVal.action =1;
     this.viewCtrl.dismiss(this.dismissVal);
    }
    else
    {
     this.dismissVal.tempArr =[];
     this.dismissVal.status =1;
     this.dismissVal.action =1;
     this.viewCtrl.dismiss(this.dismissVal);
    this.toastMsg(this.translationLet.unable_to_update_order);
    }
        }, (err) => {
          this.loading.dismiss();
        });
}

   //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

  cancel()
  {
     this.dismissVal.tempArr =[];
     this.dismissVal.status =0;
     this.dismissVal.action =1;
     this.viewCtrl.dismiss(this.dismissVal);
  }


}
