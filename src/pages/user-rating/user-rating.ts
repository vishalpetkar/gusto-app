import { Component, Renderer } from '@angular/core';
import { ViewController,IonicPageModule,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';



@Component({
  selector: 'page-user-rating',
  templateUrl: 'user-rating.html',
})
export class UserRatingPage {

  translationLet: any = []; 
  loading: Loading; 
  token :any ='';
  userObj : any = {'product_id' : '' ,'user_id' : '' ,'rating' : 0,'buyer_id' : '' };
  tempObj : any = {};


 constructor(public renderer: Renderer, public viewCtrl: ViewController,public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'custom-popup', true);
     
      this.translateService.get(['please_provide_seller_ratings','thanks_rate_to_seller','cancel','seller_ratings','please_rate_on_seller_services','submit','unAuthReq_msg','service_error','please_wait_txt']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

     this.userObj.user_id= this.navParams.get('user_id');
     this.userObj.product_id=this.navParams.get('product_id');
     this.userObj.buyer_id=this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad User Rating page');
  }

  onModelChange(event){
  this.userObj.rating = event;
  }


cancel()
{
this.viewCtrl.dismiss();
}

//Submit and save ratings 

submit()
{

if(this.userObj.rating > 0)
{
  this.showLoading();
  this.serviceProvider.addUserRate(this.userObj,this.token).then((result:any) => {
            this.loading.dismiss();
            if(result.code == 200)
              {
             this.toastMsg(this.translationLet.thanks_rate_to_seller);   
             this.viewCtrl.dismiss();
             }
           else
           {
            this.toastMsg(this.translationLet.service_error);
            }
          }, (err) => {
             this.loading.dismiss();
          }); 

}else{
this.toastMsg(this.translationLet.please_provide_seller_ratings);  
}

}

 //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

}