import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

declare var google;
@Component({
  selector: 'page-sellerviewdeliveryaddress',
  templateUrl: 'sellerviewdeliveryaddress.html',
})
export class SellerviewdeliveryaddressPage {
  
  tempAddress : any =[];
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  translationLet: any;  
  constructor(private translateService: TranslateService,public navCtrl: NavController, public navParams: NavParams) {
  this.tempAddress=JSON.parse(this.navParams.get('delivery_address'));

  console.log(JSON.stringify(this.tempAddress));
   this.translateService.get(['view_on_map']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  
}

ionViewDidLoad() {
    this.locationMap();
  }

  //Display map

    locationMap(){
    let latLng = new google.maps.LatLng(this.tempAddress.lati, this.tempAddress.logni);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    setTimeout(() => {this.addMarker()}, 300);
  }

// Add marker 

  addMarker(){
 
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: this.map.getCenter()
  });     
 
  let content = "<p class='info-window'>"+ this.tempAddress.address +"</p>";         
 
  this.addInfoWindow(marker, content);
 
}

//End 


addInfoWindow(marker, content){
 
  let infoWindow = new google.maps.InfoWindow({
    content: content
  });
 
  google.maps.event.addListener(marker, 'click', () => {
    infoWindow.open(this.map, marker);
  });
}

}
