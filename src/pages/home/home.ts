import { Component, ViewChild } from '@angular/core';
import { Platform, normalizeURL, ActionSheetController, Nav, MenuController, ModalController, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { CommentsPage } from '../comments/comments';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { LocationPage } from '../location/location';
import { SellerprodeditPage } from '../sellerprodedit/sellerprodedit';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Crop } from '@ionic-native/crop';
import { MycartPage } from '../mycart/mycart';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { ExplorePage } from '../explore/explore';
import { TransactionsPage } from '../transactions/transactions';
import moment from 'moment';
import { Keyboard } from '@ionic-native/keyboard';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import { SearchBuyerProductPage } from '../search-buyer-product/search-buyer-product';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  translationLet: any = [];
  loading: Loading;
  tempProductImg: any = "";
  loginUserData: any = { 'id': '', address: '', latitude: '', longitude: '', order_take_away: '' };

  productDetails: any = {
    category_id: 0,
    delivery_opt: 1,
    takeaway_opt: 1,
    delivery_address: '',
    delivery_lati: '',
    delivery_long: '',
    delivery_addressTemp: '',
    pickup_range: 10,
    cod: 1,
    paypal: 1,
    bancomat: 1,
    user_id: '',
    start: 0,
    limit: 10,
    search_prod: '',
    isFromHome: 1,
    search_datetime: new Date()
  };

  userImgPath: any = '';
  prodImgPath: any = '';

  token: any = '';

  loadmoreData = 0;
  isData: any = true;
  productListing: any = [];

  tempProdLikes: any = {
    'user_id': '',
    'product_id': ''
  };

  isFetchingData: any = true;
  locationOptions: any = '';

  delProductObj: any = {
    id: '',
    user_id: ''
  }

  isHideSearchbar: any = false;


  addtoCartObj: any =
    {
      'product_id': '',
      'user_id': '',
      'created': '',
      'quantity': '1'
    }
  cartCount: any = 0;
  myorderCount: any = 0;
  receiveOrderCount: any = 0;
  refreshFilter: any = 0;
  downRefreshFilter: any = 0;

  constructor(public modalCtrl: ModalController, public keyboard: Keyboard, private crop: Crop, public actionSheetCtrl: ActionSheetController, private nativeGeocoder: NativeGeocoder, private geolocation: Geolocation, private locationAccuracy: LocationAccuracy, public platform: Platform, public configProvider: ConfigProvider, private camera: Camera, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    //Check language 
    this.storage.get('lang_id').then((lang_id) => {

      if (lang_id > 0) {
        if (lang_id == 1) {
          this.translateService.use('en');
        } else {
          this.translateService.use('it');
        }

        this.translateService.get(['product_search', 'search_by_name', 'product_already_in_cart', 'unable_to_crop_image', 'select_one_payment_method', 'pull_to_refresh', 'prd_added_cart', 'prod_cart_err', 'cancel', 'post_new_product', 'take_photo', 'upload_from_gallery', 'something_wrong_txt', 'unable_to_fetch_gps', 'please_wait_gps_txt', 'prod_del_err', 'prod_del_success', 'yes_txt', 'no_txt', 'delete', 'delete_product_txt', 'likes', 'view_all_comment', 'unAuthReq_msg', 'service_error', 'food_not_avail', 'load_more', 'prod_not_added', 'please_wait_txt', 'delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        });

      } else {
        this.translateService.setDefaultLang('en');
        this.translateService.use('en');
        this.translateService.get(['product_search', 'search_by_name', 'product_already_in_cart', 'unable_to_crop_image', 'select_one_payment_method', 'pull_to_refresh', 'prd_added_cart', 'prod_cart_err', 'cancel', 'post_new_product', 'take_photo', 'upload_from_gallery', 'something_wrong_txt', 'unable_to_fetch_gps', 'please_wait_gps_txt', 'prod_del_err', 'prod_del_success', 'yes_txt', 'no_txt', 'delete', 'delete_product_txt', 'likes', 'view_all_comment', 'unAuthReq_msg', 'service_error', 'food_not_avail', 'load_more', 'prod_not_added', 'please_wait_txt', 'delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        });
      }


    });
    //End 
  }



  /*Ionic refresher */

  async doHomeRefresh(refresher) {

    /*if(this.refreshFilter == 0)
    { 

      this.isHideSearchbar = true;  
      this.refreshFilter=1;
      this.downRefreshFilter=1;
      this.productDetails.search_prod ="";
    }else
    {
      this.isHideSearchbar = false;  
      this.refreshFilter=0;
      this.downRefreshFilter=0;
      this.productDetails.search_prod ="";
    } */

    this.productDetails = {
      category_id: 0,
      delivery_opt: 1,
      takeaway_opt: 1,
      pickup_range: 10,
      cod: 1,
      paypal: 1,
      bancomat: 1,
      user_id: '',
      start: 0,
      limit: 10,
      search_prod: '',
      isFromHome: 1
    };
    this.productDetails.delivery_address = this.loginUserData.address;
    this.productDetails.delivery_addressTemp = this.loginUserData.address;
    this.productDetails.delivery_lati = this.loginUserData.latitude;
    this.productDetails.delivery_long = this.loginUserData.longitude;
    await this.storage.get('searchJSONObj').then((searchObj) => {
      this.productDetails = searchObj;
      this.productDetails.isFromHome = 0;
      this.productDetails.start = 0;
      this.productDetails.limit = 10;
      this.initHomeRefreshproducts(refresher);
    });
  }

  /* End */

  ionViewDidEnter() {
    this.downRefreshFilter = 0;
    this.productDetails.start = 0;
    this.productDetails.limit = 10;

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

    this.storage.get('authtoken').then((authtoken) => {
      this.token = authtoken;
    });


    //Get details

    this.storage.get('cartCount').then((cartCount) => {
      if (cartCount != null) {
        this.cartCount = cartCount;
      }
    });

    this.storage.get('userDetails').then((userDetails) => {
      console.log('get data of user.. home 2', userDetails);
      this.loginUserData = userDetails;
      this.productDetails.delivery_address = this.loginUserData.address;
      this.productDetails.delivery_addressTemp = this.loginUserData.address;
      this.productDetails.delivery_lati = this.loginUserData.latitude;
      this.productDetails.delivery_long = this.loginUserData.longitude;
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.initHomeproducts();
    });

    //End 
  }


  //Search product functionality started 

  dismiss() {
    this.productDetails.search_prod = "";
    this.productDetails.start = 0;
    this.productDetails.limit = 10;
    this.initHomeproducts();
  }

  //End 

  gotoSearch() {
    this.productDetails.start = 0;
    this.productDetails.limit = 10;
    this.initHomeproducts();
  }


  initHomeRefreshproducts(refresher) {
    this.productDetails.user_id = this.loginUserData.id;
    this.productDetails.start = parseInt(this.productDetails.start);
    this.productDetails.limit = parseInt(this.productDetails.limit);
    //this.showLoading();
    this.serviceProvider.getHomeProducts(this.productDetails, this.token).then((result: any) => {
      //this.loading.dismiss();
      if (result.code == 200) {
        this.productListing = result.data;
        this.loadmoreData = 1;
        this.isData = true;
        refresher.complete();

      } else if (result.code == 500) {
        this.productListing = [];
        this.isData = false;
        this.loadmoreData = 0;
        refresher.complete();
      } else {
        this.productListing = [];
        this.isData = false;
        this.loadmoreData = 0;
        refresher.complete();
      }
    }, (err) => {
      //  this.loading.dismiss();
      console.log('err ' + err);
      refresher.complete();
    });
  }

  initHomeproducts() {

    this.productDetails.user_id = this.loginUserData.id;
    this.productDetails.start = parseInt(this.productDetails.start);
    this.productDetails.limit = parseInt(this.productDetails.limit);
    //this.showLoading();
    console.log('params', this.productDetails);

    this.serviceProvider.getHomeProducts(this.productDetails, this.token).then((result: any) => {
      this.isFetchingData = false;
      //this.loading.dismiss();
      if (result.code == 200) {
        this.productListing = result.data;
        this.loadmoreData = 1;
        this.isData = true;

      } else if (result.code == 500) {
        this.productListing = [];
        this.isData = false;
        this.loadmoreData = 0;
      } else {
        this.productListing = [];
        this.isData = false;
        this.loadmoreData = 0;
      }
    }, (err) => {
      //  this.loading.dismiss();
      console.log('err ' + err);
    });
  }

  //Goto likes 

  gotoLikes(prodIndex, prodLikes) {
    //Call service to update in DB 

    if (prodLikes == true) {
      this.productListing[prodIndex]['isLikeUser'] = false;
      this.productListing[prodIndex]['likeCount'] -= 1;
    }
    else {
      this.productListing[prodIndex]['isLikeUser'] = true;
      this.productListing[prodIndex]['likeCount'] += 1;
    }

    this.tempProdLikes.user_id = this.productDetails.user_id;
    this.tempProdLikes.product_id = this.productListing[prodIndex]['id'];
    this.serviceProvider.updateProdLikes(this.tempProdLikes, this.token).then((result: any) => {
      //this.loading.dismiss();
      if (result.code == 200) {
      } else if (result.code == 500) {
        this.toastMsg(this.translationLet.service_error);
      } else {
        this.toastMsg(this.translationLet.unAuthReq_msg);
      }
    }, (err) => {
      //this.loading.dismiss();
      console.log('err ' + err);
    });
    //End

  }

  //End 

  infiniteScrollFun(infiniteScroll) {

    /*if(this.downRefreshFilter == 0)
      { 
        this.isHideSearchbar = true;  
        this.downRefreshFilter=1;
        this.refreshFilter =1;
        this.productDetails.search_prod ="";
        this.loadmoreData=0;
        infiniteScroll.complete();
      }else
      { 
        this.isHideSearchbar = false;  
        this.downRefreshFilter=0;
        this.refreshFilter =0;
        this.productDetails.search_prod =""; */

    this.productDetails.start = parseInt(this.productDetails.start) + parseInt(this.productDetails.limit);
    this.productDetails.limit = parseInt(this.productDetails.limit) + 10;

    setTimeout(() => {

      this.serviceProvider.getHomeProducts(this.productDetails, this.token).then((result: any) => {
        if (result.code == 200) {
          this.productListing = this.productListing.concat(result.data);
          this.loadmoreData = 1;
          if (this.productListing.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }

          infiniteScroll.complete();
        } else if (result.code == 500) {
          if (this.productListing.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }
          this.loadmoreData = 0;
          infiniteScroll.complete();
        } else {
          if (this.productListing.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }

          this.loadmoreData = 0;
          infiniteScroll.complete();
        }
      }, (err) => {
        console.log('err ' + err);
      });

    }, 500);

    // }
  }
  //End 

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }
  //End



  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  gotoComments(indexVal, product_id) {
    this.navCtrl.push(CommentsPage, { 'product_id': product_id });
  }

  gotoLocationPage() {
    this.navCtrl.push(LocationPage);
  }

  gotoProductDetailPage(product_id, p_user_id, product_distance) {

    if (p_user_id != this.productDetails.user_id)
      this.navCtrl.push(ProductdetailsPage, { 'product_id': product_id, 'product_distance': product_distance });
  }

  //Product edit section 

  gotoEdit(prodId) {
    this.navCtrl.push(SellerprodeditPage, { 'id': prodId, 'user_id': this.productDetails.user_id, 'pagePush': 'home' });
  }

  //Product delete section 

  delProduct(item, indexVal) {
    this.delProductObj.id = item;
    this.delProductObj.user_id = this.productDetails.user_id;

    let alert = this.alertCtrl.create({
      title: this.translationLet.delete,
      message: this.translationLet.delete_product_txt,
      buttons: [

        {
          text: this.translationLet.yes_txt,
          handler: () => {

            //  Delete  
            this.serviceProvider.delProduct(this.delProductObj, this.token).then((result: any) => {
              if (result.code == 200) {
                this.productListing.splice(indexVal, 1);
                this.toastMsg(this.translationLet.prod_del_success);
                if (!(this.productListing.length > 0)) {
                  this.isData = false;
                }

              } else {
                this.toastMsg(this.translationLet.prod_del_err);
              }
            }, (err) => {
            });
          }
          //End
        }, {
          text: this.translationLet.no_txt,
          handler: () => {
            //  console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();

  }


  //End 

  //Goto User Profile section 

  gotoSellerProfile(userId) {
    this.navCtrl.push(SellerprofilePage, { 'userId': userId });
  }

  //Common Pages 

  gotMyOrder() {
    this.app.getRootNav().setRoot(UpcomingPage);
  }

  gotoAccount() {
    this.app.getRootNav().setRoot(MyprofilePage);
  }

  gotoUserOrder() {
    this.app.getRootNav().setRoot(ReceiveUpcomingPage);
  }

  //Go to cover photo

  gotoCoverPhoto() {

    if (this.loginUserData.order_take_away == 1) {
      //Actionsheet start
      let actionSheet = this.actionSheetCtrl.create({
        title: this.translationLet.post_new_product,
        buttons: [
          {
            text: this.translationLet.take_photo,
            handler: () => {
              this.takePhoto();
            }
          }, {
            text: this.translationLet.upload_from_gallery,
            handler: () => {
              this.takePhotoGallery();
            }
          }, {
            text: this.translationLet.cancel,
            handler: () => {
            }
          }
        ]
      });
      actionSheet.present();
      //Actionsheet end
    } else {

      if ((this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null) && (this.loginUserData.std_delivery_cost == '' || this.loginUserData.std_delivery_cost == null)) {
        this.toastMsg(this.translationLet.delivery_policies_err);
        this.app.getRootNav().setRoot(MyprofilePage);

      } else if ((this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0)) {
        this.toastMsg(this.translationLet.select_one_payment_method);
        this.app.getRootNav().setRoot(MyprofilePage);
      } else {

        //Actionsheet 

        let actionSheet = this.actionSheetCtrl.create({
          title: this.translationLet.post_new_product,
          buttons: [
            {
              text: this.translationLet.take_photo,
              handler: () => {
                this.takePhoto();
              }
            }, {
              text: this.translationLet.upload_from_gallery,
              handler: () => {
                this.takePhotoGallery();
              }
            }, {
              text: this.translationLet.cancel,
              handler: () => {
              }
            }
          ]
        });
        actionSheet.present();
      }
    }
  }

  takePhoto() {
    //End 
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }
  //End  

  CropedImg(imageData) {
    if (this.platform.is('ios')) {
      this.tempProductImg = normalizeURL(imageData);
    } else {
      this.tempProductImg = imageData;
    }
    this.app.getRootNav().setRoot(CoverphotoPage, { 'tempProductImg': this.tempProductImg });
  }

  takePhotoGallery() {
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }

  //Toast Msg 

  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  //End


  showLoadingGPS() {
    this.loading = this.loadingCtrl.create({
      content: this.translationLet.please_wait_gps_txt,
      spinner: 'bubbles'
    });
    this.loading.present();
  }


  //Goto GPS

  gotoGPS() {

    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {

        this.showLoadingGPS();

        if (this.platform.is('ios')) {
          this.locationOptions = { timeout: 2000, enableHighAccuracy: true };
        } else {
          this.locationOptions = { timeout: 12000, enableHighAccuracy: true };
        }

        this.geolocation.getCurrentPosition(this.locationOptions).then((resp) => {
          if (resp.coords.latitude && resp.coords.longitude) {

            //Get lat n lang  with address
            this.loginUserData.latitude = resp.coords.latitude;
            this.loginUserData.longitude = resp.coords.longitude;

            let options: NativeGeocoderOptions = {
              useLocale: true,
              maxResults: 1
            };
            this.nativeGeocoder.reverseGeocode(this.loginUserData.latitude, this.loginUserData.longitude, options)
              .then((result: NativeGeocoderReverseResult[]) =>

                this.reverseGeo(result))

              .catch((error: any) =>
                this.gpsError()
              );
          } else {
            this.gpsError()
          }

        }).catch((error) => {
          this.loading.dismiss();
          this.gpsError();
        });

      },
      (error) => {
        this.gpsError()
      });



  }

  reverseGeo(res) {
    this.loading.dismiss();
    this.loginUserData.address = res[0].thoroughfare + " " + res[0].subThoroughfare + " " + res[0].subLocality + " " + res[0].locality;

    //Update data into db and display on home page

    this.serviceProvider.buyerDeliveryAddress(this.loginUserData, this.token).then((result: any) => {
      //this.loading.dismiss();
      if (result.code == 200) {
        this.storage.set('userDetails', result.userDetails);
        this.initHomeproducts();
      } else {
        this.toastMsg(this.translationLet.something_wrong_txt);
      }
    }, (err) => {
      console.log('err ' + err);
    });

    //End 
  }


  gpsError() {
    this.loading.dismiss();
    this.toastMsg(this.translationLet.unable_to_fetch_gps);
  }

  //Go to cart 

  gotoCart() {
    this.navCtrl.push(MycartPage);
  }


  //Add to cart from Home page

  addToCart(cartIndex, product_id) {
    //Service call 
    this.showLoading();
    this.addtoCartObj.product_id = product_id;
    this.addtoCartObj.user_id = this.loginUserData.id;
    this.addtoCartObj.created = moment().format('YYYY-MM-DD HH:mm:ss');

    this.serviceProvider.addToCart(this.addtoCartObj, this.token).then((result: any) => {
      this.loading.dismiss();
      if (result.code == 200) {
        this.productListing[cartIndex].isProductCart = true;
        this.storage.set('cartCount', result.data);
        this.cartCount = result.data;
        this.toastMsg(this.translationLet.prd_added_cart);
      }
      else {
        this.toastMsg(this.translationLet.prod_cart_err);
      }
    }, (err) => {
      this.loading.dismiss();
      console.log('err ' + err);
    });
  }
  //End 

  //Product already in cart
  existInCart() {
    this.toastMsg(this.translationLet.product_already_in_cart);
  }

  //close keyboard

  /*onScrollStart(event: any) {
      if (event === null) {
          return;
      }
     this.keyboard.hide();
  }*/

  searchProdFun() {
    let modalSearch = this.modalCtrl.create(SearchBuyerProductPage, {}, { cssClass: 'search-modal' });
    modalSearch.present();
    modalSearch.onDidDismiss(data => {
      if (data.length != 0) {
        this.productDetails = data;
        this.initHomeproducts();
      }
      //End 

      this.storage.get('userDetails').then((userDetails) => {
        console.log('get data of user.. home 1', userDetails);
        this.loginUserData = userDetails;
        this.productDetails.delivery_address = this.loginUserData.address;
        this.productDetails.delivery_addressTemp = this.loginUserData.address;
        this.productDetails.delivery_lati = this.loginUserData.latitude;
        this.productDetails.delivery_long = this.loginUserData.longitude;
        this.productDetails.search_datetime = this.loginUserData.search_datetime;
        this.myorderCount = userDetails.myorderCount;
        this.receiveOrderCount = userDetails.receiveOrderCount;
        this.initHomeproducts();
      });
    });

  }
}
