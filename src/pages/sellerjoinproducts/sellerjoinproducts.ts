import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,ModalController,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductDetailsPage } from '../sellerproduct-details/sellerproduct-details';
import { JoinsharedorderPage } from '../joinsharedorder/joinsharedorder';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';


@Component({
  selector: 'page-sellerjoinproducts',
  templateUrl: 'sellerjoinproducts.html',
})
export class SellerjoinproductsPage {

  translationLet: any = [];	
  loading: Loading;	
  productDetails : any = {
    user_id:'',
    start : 0,
    limit : 10,
    seller_id : '',
    'delivery_long' : '',
    'delivery_lat' : ''
  };

   isFetchingData: any =true;
   userImgPath : any ='';
   prodImgPath : any ='';
   selectedAll : any =false;
   token :any ='';

  loadmoreData=0;
  isData : any = true ;
  productListing :any =[];
  
  joinObj :any ={
  	'order_id' :'',
  	'user_id' : '',
    'buyer_id' : '',
    'seller_id' : '',
  	'product_id' : '',
  	'quantity' : '1',
  	'payment_method' : '',
    'amount' : '',
    'discount_percentage':'' ,
    'isPromocodeused' : '',
    'total_amount' : '',
    'promoCode' : '',
    'isPromoCode' : '',
    'credit_card_number' : '',
    'expire_month' : '',
    'expire_year' : '',
    'cvv' : '',
    'prod_pic' : '',
    'seller_name' : '',
    'prod_name' : ''
    
  };
  joinProductList :any =[]; 

  commonObj:any =
  { 'order_id' :'',
    'user_id' : '',
    'buyer_id' : '',
    'seller_id' : '',
    'isSellerPromoCodeUsed' : '',
    'discount_percentage' : '',
    'order_type' : ''
  };

 constructor(public modalCtrl: ModalController,public actionSheetCtrl: ActionSheetController,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
 

updateItemInCart(evt,orderItem,prodIndex)
{
         if(!this.isKeyExist(orderItem.id,this.joinProductList))
         {
                  this.joinObj = {'prod_name' :orderItem.name,'prod_pic' : orderItem.cover_photo,'seller_name' :orderItem.sellerName,'product_id' :orderItem.id,'amount' :orderItem.amount,'promoCode' :orderItem.promoCode,'isPromoCode' :orderItem.isPromoCode,'discount_percentage' :orderItem.promoDiscount,'quantity' : 1,'std_cost' :orderItem.DeliveryCost,'isPromocodeused' : 0,'notes' : '','order_id' : this.commonObj.order_id,'deductedAmt' :orderItem.amount,
        'total_amount' :orderItem.amount};
                  this.joinProductList.push(this.joinObj);   
                  orderItem.checked =true;
                   if(this.joinProductList.length == this.productListing.length)
                    this.selectedAll =true;
                  else
                    this.selectedAll =false;
        }
         else
         {      
                this.productListing[prodIndex]['checked'] =false;
                this.updateKeyValue(orderItem.id,this.joinProductList); 
                this.selectedAll =false   ;
         }

}


updateKeyValue(order_id,listArr)
{
  for (var i = listArr.length - 1; i >= 0; --i) {
    if (listArr[i].product_id == order_id) {
        listArr.splice(i,1);
    }
}
}


isKeyExist(product_id,listArr)
{
  for(var i = 0, len = listArr.length; i < len; i++) {
        if( listArr[i].product_id == product_id )
        {   
            return true;
        }
    }
    return false
}



checkedAll()
{
  if(this.selectedAll == true)
  { 
    this.joinProductList =[];
    for(let i=0;i<this.productListing.length;i++)
    {
        this.joinObj = {
        'prod_name' :this.productListing[i]['name'],
        'prod_pic' :this.productListing[i]['cover_photo'],
        'seller_name' : this.productListing[i]['sellerName'],
        'product_id' : this.productListing[i]['id'],
        'amount' :  this.productListing[i]['amount'],
        'promoCode' : this.productListing[i]['promoCode'],
        'isPromoCode' : this.productListing[i]['isPromoCode'],
        'discount_percentage' : this.productListing[i]['promoDiscount'],
        'quantity' : 1,
        'std_cost' : this.productListing[i]['DeliveryCost'],
        'isPromocodeused' : 0,
        'notes' : '',
        'order_id' : this.commonObj.order_id,
        'deductedAmt' :this.productListing[i]['amount'],
        'total_amount' :this.productListing[i]['amount']
      };
      this.joinProductList.push(this.joinObj);  
      this.productListing[i]['checked'] =true;
   }
  }else
  {  
     this.joinProductList =[];
     for(let j=0;j<this.productListing.length;j++)
    {
     this.productListing[j]['checked'] =false;
    }
}
}
//End 

//Submit product

submitProduct()
{
  if(this.joinProductList.length > 0)
  {
    this.navCtrl.push(JoinsharedorderPage, {'joinorderDetails' : JSON.stringify(this.joinProductList), 'commonField' : JSON.stringify(this.commonObj)});
  }else
  {
    this.toastMsg("Please select atleast one product to join");
  }
}

//End 


  /*Ionic refresher */

 doSellerRefresh(refresher) {
    this.productDetails.start= 0;  
    this.productDetails.limit= 10;  
    this.initSellerRefreshproducts(refresher);
  }

ionViewDidEnter(){
    this.productDetails.start= 0;  
    this.productDetails.limit= 10;  
    this.commonObj.order_id= this.navParams.get('order_id');
    this.commonObj.order_type= this.navParams.get('order_type');
    this.commonObj.seller_id=this.navParams.get('seller_id');
    this.commonObj.buyer_id = this.navParams.get('buyer_id'); 
    this.commonObj.isSellerPromoCodeUsed = this.navParams.get('isSellerPromoCodeUsed'); 
    this.commonObj.discount_percentage = this.navParams.get('discount_percentage'); 
    this.joinObj.order_id= this.navParams.get('order_id');
    this.productDetails.seller_id=this.navParams.get('seller_id');
    this.joinObj.buyer_id = this.navParams.get('buyer_id'); 
    this.joinObj.seller_id = this.navParams.get('seller_id');
    this.productDetails.delivery_lat = this.navParams.get('delivery_lat'); 
    this.productDetails.delivery_long = this.navParams.get('delivery_long'); 
    this.selectedAll =false;
    this.joinProductList =[];
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

    this.translateService.get(['select_all','join_order','seller','seller_products','pull_to_refresh','join_order','cancel','post_new_product','take_photo','upload_from_gallery','something_wrong_txt','unable_to_fetch_gps','please_wait_gps_txt','prod_del_err','prod_del_success','yes_txt','no_txt','delete','delete_product_txt','likes','view_all_comment','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

    this.storage.get('userDetails').then((userDetails) => {
        this.joinObj.user_id = userDetails.id;
        this.productDetails.user_id = userDetails.id;
        this.commonObj.user_id =userDetails.id;
        this.initSellerproducts();
     }); 
      
      //End 
  }
 

 initSellerRefreshproducts (refresher)
{
this.productDetails.start=parseInt(this.productDetails.start);
this.productDetails.limit=parseInt(this.productDetails.limit);  
//this.showLoading();
this.serviceProvider.getSellerProducts(this.productDetails,this.token).then((result:any) => {

          //this.loading.dismiss();
            if(result.code == 200){
            this.productListing =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
      //  this.loading.dismiss();
          console.log('err '+err);
          refresher.complete();
          }); 
}
 /* End */

 initSellerproducts()
{
this.productDetails.start=parseInt(this.productDetails.start);
this.productDetails.limit=parseInt(this.productDetails.limit);	
//this.showLoading();
this.serviceProvider.getSellerProducts(this.productDetails,this.token).then((result:any) => {
  this.isFetchingData=false;
          //this.loading.dismiss();
            if(result.code == 200){
            this.productListing =result.data; 
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.productListing=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
      //  this.loading.dismiss();
          console.log('err '+err);
          }); 
}

infiniteScrollFun(infiniteScroll) {

    this.productDetails.start = parseInt(this.productDetails.start)+parseInt(this.productDetails.limit);
    this.productDetails.limit= parseInt(this.productDetails.limit)+10;

    setTimeout(() => {

           this.serviceProvider.getSellerProducts(this.productDetails,this.token).then((result:any) => {
            if(result.code == 200){
            this.productListing = this.productListing.concat(result.data);
            this.loadmoreData=1;
            if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.productListing.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}

 //Show loading 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

//Toast Msg 

	toastMsg (msg){
	 let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 2000,
	      position: 'bottom'
	    });
	    toast.present(toast);
	  }

//End

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerproductsPage');
  }

  gotoProductDetailPage(product_id,product_distance){
    this.navCtrl.push(SellerproductDetailsPage,{'product_id' : product_id, 'product_distance' : product_distance });
  }


 //Goto User Profile section 

    gotoSellerProfile(userId)
    {
    this.app.getRootNav().push(SellerprofilePage,{'userId' : userId});
    }


}
