import { Component,ViewChild } from '@angular/core';
import { Platform,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { TransactiondetailsPage } from '../transactiondetails/transactiondetails';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { ExplorePage } from '../explore/explore';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';

@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class TransactionsPage {
 
  translationLet: any;	
  loading: Loading;	

  userDetails : any = {
    id:'',
    start : 0,
    limit : 10
  };
  myorderCount :any =0;
  receiveOrderCount :any =0;
  loginUserData : any = {'id' : ''}; 

  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  isFetchingData : any = true;
 //For infinite Scrolling 

  loadmoreData=0;
  isData : any = true ;
  txList :any =[];
  tempProductImg : any = "";
  delObj : any ={'user_id' : '','tx_id' : ''};

  constructor(private crop: Crop,private camera: Camera,public platform: Platform,public configProvider : ConfigProvider,public actionSheetCtrl: ActionSheetController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

  this.translateService.get(['delivery','take_away','seller_address','pick_up_time','off_to_used_seller_promo_code','buyer','delete_transaction','delete_tx_msg','delete','deliverd_date_time','total_amt','delivery_address','unable_to_crop_image','select_one_payment_method','tx_history','pull_to_refresh','no_tx_history','delivered','delivery_time','delivery_cost_txt','unable_to_share','cancel','post_new_product','take_photo','upload_from_gallery','refreshing','delivery_policies_err','prod_del_success','prod_del_err','delete','delete_product_txt','yes_txt','no_txt','load_more','prod_not_added','my_product','publish','profile_update_success','profile_update_err','email_already_exist','something_wrong_txt','unAuthReq_msg','err_upload_file','cancel','upload_profile_image','take_photo','upload_from_gallery','please_enter_valid_mobile','please_enter_mobile','email_not_valid','error_txt','please_enter_name','please_enter_email','please_wait_txt','basic_info','ok_txt','edit_profile','update_profile','name','mobile_no','email']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
   this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
  });

   //Get details
 this.storage.get('userDetails').then((userDetails) => {
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.userDetails.id=userDetails.id;
      this.delObj.user_id = userDetails.id;
      this.loginUserData= userDetails;
       this.initTxList();
  }); 
 
  }

/*Ionic refresher */

 doListRefresh(refresher) {
    this.userDetails.start= 0;  
    this.userDetails.limit= 10;  
    this.initTxListRefresh(refresher);
  }


initTxListRefresh(refresher)
{

this.userDetails.start=parseInt(this.userDetails.start);
this.userDetails.limit=parseInt(this.userDetails.limit);  
//this.showLoading();
this.serviceProvider.getAllTx(this.userDetails,this.token).then((result:any) => {
  this.isFetchingData =false;
         // this.loading.dismiss();
            if(result.code == 200){
            this.txList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();
          } else if(result.code == 500) {
             this.txList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.txList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
       // this.loading.dismiss();
       this.isFetchingData =false;
        console.log('err '+err);
        refresher.complete();  
          }); 
}


initTxList()
{

this.userDetails.start=parseInt(this.userDetails.start);
this.userDetails.limit=parseInt(this.userDetails.limit);	
//this.showLoading();
this.serviceProvider.getAllTx(this.userDetails,this.token).then((result:any) => {
this.isFetchingData=false;
         // this.loading.dismiss();
            if(result.code == 200){
            this.txList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
          } else if(result.code == 500) {
             this.txList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.txList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
       // this.loading.dismiss();
        this.isFetchingData=false; 
          console.log('err '+err);
          }); 
}


infiniteScrollFun(infiniteScroll) {

    this.userDetails.start = parseInt(this.userDetails.start)+parseInt(this.userDetails.limit);
    this.userDetails.limit= parseInt(this.userDetails.limit)+10;

  setTimeout(() => {

           this.serviceProvider.getAllTx(this.userDetails,this.token).then((result:any) => {
            if(result.code == 200){
            this.txList = this.txList.concat(result.data);
            this.loadmoreData=1;
            infiniteScroll.complete();
          } else if(result.code == 500) {
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End


  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionsPage');
  }

	


//Goto footer 

//gotoHome
 
  gotoHome()
  {
   this.app.getRootNav().setRoot(HomePage); 
  }
 
  gotoAccount()
    {
      this.app.getRootNav().setRoot(MyprofilePage); 
    }
    
    //Go to cover photo
    gotoCoverPhoto()
    {
       if(this.loginUserData.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

       if( (this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null ) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null )  && (this.loginUserData.std_delivery_cost == ''  || this.loginUserData.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else if( (this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
    //Actionsheet 
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    } 
  }
}

 takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
  //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
     //Crop Image 
      this.crop.crop(imageData, {quality: 100})
      .then(
        newImage => this.CropedImg(newImage),
        error =>this.toastMsg(this.translationLet.unable_to_crop_image)
      );
    //End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}

  gotoExplore()
   {
    this.app.getRootNav().setRoot(ExplorePage); 
   }

  gotoTransactionDetails(tx_id){
		this.navCtrl.push(TransactiondetailsPage,{'tx_id' : tx_id});
	} 

gotoBuyerProfile(buyerId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : buyerId});
}

 gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }

//Del trans 

delTrans(tx_id,indexVal)
{
 this.delObj.tx_id =tx_id;
 let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.delete_transaction,
    buttons: [
      {
        text: this.translationLet.yes_txt,
        handler: () => {
      //  Delete  
    this.showLoading();
    this.serviceProvider.delTransaction(this.delObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     this.txList.splice(indexVal,1);  
     if(this.txList.length > 0)
      this.isData =true;
      else
      this.isData =false;
     this.toastMsg(this.translationLet.delete_tx_msg);

    }else
    {
    this.toastMsg(this.translationLet.something_wrong_txt);
    }
        }, (err) => {
          this.loading.dismiss();
        });
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        }
      }
    ]
  });
  alert.present();  

}

//End 
}

