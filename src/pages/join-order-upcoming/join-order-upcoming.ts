import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { JoinhistroydetailPage } from '../joinhistroydetail/joinhistroydetail';


@Component({
  selector: 'page-join-order-upcoming',
  templateUrl: 'join-order-upcoming.html',
})
export class JoinOrderUpcomingPage {

  translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  orderList : any =[];
  orderObj : any = {
    user_id:'',
    start : 0,
    limit : 50
  };
  
  loadmoreData=0;
  isData : any = true ;
  isFetchingData: any =true;

  constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }

      //Goto User Profile section 

    gotoSellerProfile(userId)
    {
    this.app.getRootNav().push(SellerprofilePage,{'userId' : userId});
    }


  ionViewDidEnter()
  {  

    this.orderObj.start= 0;  
    this.orderObj.limit= 50;  

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

     this.translateService.get(['seller','delivery_time','delivery_place','buyer_got','no_upcoming_join_orders','order_pending','order_confirm','order_reject','order_rescheduled','view_details','you_got_txt','delivery_cost_txt','free_delivery_txt','order_rescheduled','pull_to_refresh','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

    //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.orderObj.user_id =userDetails.id; 
        this.myOrderList();
     }); 
  }


    /*Ionic refresher */
 doOrderRefresh(refresher) {
    this.orderObj.start= 0;  
    this.orderObj.limit= 50;  
    this.myOrderListRefresher(refresher);
  }
 /* End */

// Buyer details page 
  buyerDetails(order_id,seller_id,mainBuyerId)
  {
   this.app.getRootNav().push(JoinhistroydetailPage,{'order_id' : order_id, 'seller_id' : seller_id,'mainBuyerId' : mainBuyerId});
  }

//End

//Order List 

  myOrderList()
  {
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);	

this.serviceProvider.myJoinOrderList(this.orderObj,this.token).then((result:any) => {
  this.isFetchingData=false;
            if(result.code == 200){
            this.orderList =result.data;     
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
  }

//End 

myOrderListRefresher (refresher)
{
this.orderObj.start=parseInt(this.orderObj.start);
this.orderObj.limit=parseInt(this.orderObj.limit);  
this.serviceProvider.myJoinOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.orderList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.orderObj.start = parseInt(this.orderObj.start)+parseInt(this.orderObj.limit);
    this.orderObj.limit= parseInt(this.orderObj.limit)+50;

  setTimeout(() => {

           this.serviceProvider.myJoinOrderList(this.orderObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.orderList = this.orderList.concat(result.data);
            this.loadmoreData=1;
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.orderList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}

//End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpcomingPage');
  }

}
