import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Content, Slides, Platform, Nav, MenuController, ActionSheetController, normalizeURL, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { BuyerorderdetailsPage } from '../buyerorderdetails/buyerorderdetails';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import moment from 'moment';
@Component({
  selector: 'page-upcoming',
  templateUrl: 'upcoming.html',
})
export class UpcomingPage {
  translationLet: any = [];
  loading: Loading;
  userImgPath: any = '';
  prodImgPath: any = '';
  token: any = '';
  //Upcoming variables
  orderList: any = [];
  orderPastList: any = [];
  orderObj: any = {
    user_id: '',
    start: 0,
    limit: 20,
    created: ''
  };
  tempArr: any = [];
  isRefresh: any = 0;
  loadmoreData = 0;
  isData: any = true;
  isFetchingData: any = true;
  returnVal: any = '';
  //End

  isPastData: any = true;
  isFetchingPastData: any = true;
  loadmorePastData = 0;
  /*End*/
  tempProductImg: any = '';
  profileDetails: any = {
    name: '',
    profile_pic: '',
    mobile_no: '',
    address: '',
    email: '',
    id: '',
    min_order_charge: '',
    order_free_amt: '',
    //order_free_range:'',
    max_delivery_range: '',
    std_delivery_cost: '',
    Paypal_method: '',
    COD: '',
    order_take_away: ''
  };
  /*End*/
  myorderCount: any = 0;
  receiveOrderCount: any = 0;
  delObject: any = { 'user_id': '', 'order_id': '' };
  constructor(
    private crop: Crop,
    public actionSheetCtrl: ActionSheetController,
    public renderer: Renderer,
    public platform: Platform,
    private camera: Camera,
    public configProvider: ConfigProvider,
    public serviceProvider: ServiceProvider,
    private menu: MenuController,
    public app: App,
    private translateService: TranslateService,
    public events: Events,
    public navCtrl: NavController,
    private storage: Storage,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {
  }

  /*************************************/
  /*Scroll sliding tabs start*/
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any = "0";

  // On segment click
  selectedTab(index) {
    this.slider.slideTo(index);
  }
  // On slide changed
  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    let slides_count = this.segments.nativeElement.childElementCount;
    this.page = currentIndex.toString();
    if (this.page >= slides_count)
      this.page = (slides_count - 1).toString();

    if (this.page == "0") {
      this.isRefresh = 0;
      this.orderObj.limit = 20;
      this.orderObj.start = 0;
      this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
      this.myOrderList();
    }
    else {
      this.isRefresh = 1;
      this.orderObj.limit = 20;
      this.orderObj.start = 0;
      this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
      this.myPastOrderList();
    }
    this.centerScroll();
  }
  // Center current scroll
  centerScroll() {
    if (!this.segments || !this.segments.nativeElement)
      return;
    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent / 2);
    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }
  // Get size start to current
  sizeLeft() {
    let size = 0;
    for (let i = 0; i < this.page; i++) {
      size += this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }
  // Easing function
  easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }
  // Animate scroll
  smoothScrollTo(endX) {
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;
    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }
  /*************************************/
  /*Scroll sliding tabs End*/
  myPastOrderList() {
    this.orderObj.start = 0;
    this.orderObj.limit = 20;
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.serviceProvider.myPastOrderList(this.orderObj, this.token).then((result: any) => {
      this.isFetchingPastData = false;
      if (result.code == 200) {
        this.orderPastList = result.data;
        this.commonProductList(this.orderPastList);
        this.loadmorePastData = 1;
        this.isPastData = true;
      } else if (result.code == 500) {
        this.orderPastList = [];
        this.isPastData = false;
        this.loadmorePastData = 0;
      } else {
        this.orderPastList = [];
        this.isPastData = false;
        this.loadmorePastData = 0;
      }
    }, (err) => {
      console.log('err ' + err);
    });
  }
  /*Common Ionic refresher */
  doRefresh(refresher) {
    this.orderObj.start = 0;
    this.orderObj.limit = 20;
    if (this.isRefresh == 1)
      this.myPastOrderListRefresher(refresher);
    else
      this.myOrderListRefresher(refresher);
  }
  myPastOrderListRefresher(refresher) {
    this.orderObj.start = parseInt(this.orderObj.start);
    this.orderObj.limit = parseInt(this.orderObj.limit);
    this.serviceProvider.myPastOrderList(this.orderObj, this.token).then((result: any) => {
      if (result.code == 200) {
        this.orderPastList = result.data;
        this.commonProductList(this.orderPastList);
        this.loadmorePastData = 1;
        this.isPastData = true;
        refresher.complete();
      } else if (result.code == 500) {
        this.orderPastList = [];
        this.isPastData = false;
        this.loadmorePastData = 0;
        refresher.complete();
      } else {
        this.orderPastList = [];
        this.isPastData = false;
        this.loadmorePastData = 0;
        refresher.complete();
      }
    }, (err) => {
      console.log('err ' + err);
      refresher.complete();
    });
  }
  /* End */
  ionViewDidEnter() {
    this.orderObj.start = 0;
    this.orderObj.limit = 20;
    this.isRefresh = 0;
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();
    this.storage.get('authtoken').then((authtoken) => {
      this.token = authtoken;
    });
    this.translateService.get(['delivery', 'take_away', 'seller_address', 'pick_up_time', 'done_txt', 'off_to_all_products', 'seller', 'something_wrong_txt', 'order_deleted_success', 'delete', 'do_you_want_to_delete_order', 'total', 'upcoming', 'past_orders', 'my_order_history', 'no_past_order', 'unable_to_crop_image', 'take_photo', 'upload_from_gallery', 'cancel', 'post_new_product', 'select_one_payment_method', 'delivery_policies_err', 'no_upcoming_order', 'delivery_time', 'order_not_received', 'order_expired', 'delivery_place', 'buyer_got', 'no_upcoming_join_orders', 'order_completed', 'order_deliver', 'order_pending', 'order_confirm', 'order_reject', 'order_rescheduled', 'view_details', 'you_got_txt', 'delivery_cost_txt', 'free_delivery_txt', 'order_rescheduled', 'pull_to_refresh', 'yes_txt', 'no_txt', 'refreshing', 'please_wait_txt', 'load_more']).subscribe((translation: [string]) => {
      this.translationLet = translation;
    });
    //Get details
    this.storage.get('userDetails').then((userDetails) => {
      this.profileDetails = userDetails;
      this.delObject.user_id = userDetails.id;
      this.orderObj.user_id = userDetails.id;
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.myOrderList();
    });
  }
  //Goto User Profile section
  gotoSellerProfile(userId) {
    this.app.getRootNav().push(SellerprofilePage, { 'userId': userId });
  }
  /*Ionic refresher */
  doOrderRefresh(refresher) {
    this.orderObj.start = 0;
    this.orderObj.limit = 20;
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.myOrderListRefresher(refresher);
  }
  /* End */
  // Buyer details page
  buyerDetails(order_id, seller_id) {
    this.app.getRootNav().push(BuyerorderdetailsPage, { 'order_id': order_id, 'seller_id': seller_id });
  }
  //End
  //Order List
  myOrderList() {
    this.orderObj.start = parseInt(this.orderObj.start);
    this.orderObj.limit = parseInt(this.orderObj.limit);
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.serviceProvider.myOrderList(this.orderObj, this.token).then((result: any) => {
      this.isFetchingData = false;
      if (result.code == 200) {
        this.orderList = result.data;
        this.commonProductList(this.orderList);
        this.loadmoreData = 1;
        this.isData = true;
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
      } else if (result.code == 500) {
        this.orderList = [];
        this.isData = false;
        this.loadmoreData = 0;
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
      } else {
        this.orderList = [];
        this.isData = false;
        this.loadmoreData = 0;
      }
    }, (err) => {
      console.log('err ' + err);
    });
  }
  //End
  myOrderListRefresher(refresher) {
    this.orderObj.start = parseInt(this.orderObj.start);
    this.orderObj.limit = parseInt(this.orderObj.limit);
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    this.serviceProvider.myOrderList(this.orderObj, this.token).then((result: any) => {
      if (result.code == 200) {
        this.orderList = result.data;
        this.commonProductList(this.orderList);
        this.loadmoreData = 1;
        this.isData = true;
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
        refresher.complete();
      } else if (result.code == 500) {
        this.orderList = [];
        this.isData = false;
        this.loadmoreData = 0;
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
        refresher.complete();
      } else {
        this.orderList = [];
        this.isData = false;
        this.loadmoreData = 0;
        refresher.complete();
      }
    }, (err) => {
      console.log('err ' + err);
      refresher.complete();
    });
  }
  //infiniteScrollFun
  infiniteScrollPastFun(infiniteScroll) {
    this.orderObj.start = parseInt(this.orderObj.start) + parseInt(this.orderObj.limit);
    this.orderObj.limit = parseInt(this.orderObj.limit) + 20;
    setTimeout(() => {
      this.serviceProvider.myPastOrderList(this.orderObj, this.token).then((result: any) => {
        if (result.code == 200) {
          this.orderPastList = this.orderPastList.concat(result.data);
          this.commonProductList(this.orderPastList);
          this.loadmorePastData = 1;
          if (this.orderPastList.length > 0) {
            this.isPastData = true;
          } else {
            this.isPastData = false;
          }
          infiniteScroll.complete();
        } else if (result.code == 500) {
          if (this.orderPastList.length > 0) {
            this.isPastData = true;
          } else {
            this.isPastData = false;
          }
          this.loadmorePastData = 0;
          infiniteScroll.complete();
        } else {
          if (this.orderPastList.length > 0) {
            this.isPastData = true;
          } else {
            this.isPastData = false;
          }
          this.loadmorePastData = 0;
          infiniteScroll.complete();
        }
      }, (err) => {
        console.log('err ' + err);
      });
    }, 500);
  }
  //End
  //infiniteScrollFun
  infiniteScrollFun(infiniteScroll) {
    this.orderObj.start = parseInt(this.orderObj.start) + parseInt(this.orderObj.limit);
    this.orderObj.limit = parseInt(this.orderObj.limit) + 20;
    this.orderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
    setTimeout(() => {
      this.serviceProvider.myOrderList(this.orderObj, this.token).then((result: any) => {
        if (result.code == 200) {
          this.orderList = this.orderList.concat(result.data);
          this.commonProductList(this.orderList);
          this.loadmoreData = 1;
          if (this.orderList.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }
          infiniteScroll.complete();
        } else if (result.code == 500) {
          if (this.orderList.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }
          this.loadmoreData = 0;
          infiniteScroll.complete();
        } else {
          if (this.orderList.length > 0) {
            this.isData = true;
          } else {
            this.isData = false;
          }
          this.loadmoreData = 0;
          infiniteScroll.complete();
        }
      }, (err) => {
        console.log('err ' + err);
      });
    }, 500);
  }
  //End
  ionViewDidLoad() {
    console.log('ionViewDidLoad UpcomingPage');
  }
  togglePastSection(elmViewRef: HTMLElement, event) {
    console.log(event.target.parentElement.classList.add('active'))
    if (!elmViewRef.classList.contains('open')) {
      this.renderer.setElementStyle(elmViewRef, 'height', elmViewRef.children[0].clientHeight + 'px');
      this.renderer.setElementClass(elmViewRef, 'open', true);
      event.target.parentElement.classList.add('active')
    } else {
      this.renderer.setElementStyle(elmViewRef, 'height', '0px');
      this.renderer.setElementClass(elmViewRef, 'open', false);
      event.target.parentElement.classList.remove('active')
    }
  }


  toggleUpcomingSection(elmViewRef: HTMLElement, event) {
    console.log(event.target.parentElement.classList.add('active'))
    if (!elmViewRef.classList.contains('open')) {
      this.renderer.setElementStyle(elmViewRef, 'height', elmViewRef.children[0].clientHeight + 'px');
      this.renderer.setElementClass(elmViewRef, 'open', true);
      event.target.parentElement.classList.add('active')
    } else {
      this.renderer.setElementStyle(elmViewRef, 'height', '0px');
      this.renderer.setElementClass(elmViewRef, 'open', false);
      event.target.parentElement.classList.remove('active')
    }
  }

  //Common functionality
  gotoHome() {

    this.app.getRootNav().setRoot(HomePage);
  }
  gotoAccount() {
    this.app.getRootNav().setRoot(MyprofilePage);
  }


  //End
  gotoUserOrder() {
    this.app.getRootNav().setRoot(ReceiveUpcomingPage);
  }
  //Go to cover photo
  gotoCoverPhoto() {
    if (this.profileDetails.order_take_away == 1) {
      //Actionsheet start
      let actionSheet = this.actionSheetCtrl.create({
        title: this.translationLet.post_new_product,
        buttons: [
          {
            text: this.translationLet.take_photo,
            handler: () => {
              this.takePhoto();
            }
          }, {
            text: this.translationLet.upload_from_gallery,
            handler: () => {
              this.takePhotoGallery();
            }
          }, {
            text: this.translationLet.cancel,
            handler: () => {
            }
          }
        ]
      });
      actionSheet.present();
      //Actionsheet end
    } else {

      if ((this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null) && (this.profileDetails.std_delivery_cost == '' || this.profileDetails.std_delivery_cost == null)) {
        this.toastMsg(this.translationLet.delivery_policies_err);
      } else if ((this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0)) {
        this.toastMsg(this.translationLet.select_one_payment_method);
        this.app.getRootNav().setRoot(MyprofilePage);
      } else {
        //Actionsheet

        let actionSheet = this.actionSheetCtrl.create({
          title: this.translationLet.post_new_product,
          buttons: [
            {
              text: this.translationLet.take_photo,
              handler: () => {
                this.takePhoto();
              }
            }, {
              text: this.translationLet.upload_from_gallery,
              handler: () => {
                this.takePhotoGallery();
              }
            }, {
              text: this.translationLet.cancel,
              handler: () => {
              }
            }
          ]
        });
        actionSheet.present();
      }
    }
  }
  takePhoto() {
    //End
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };
    //Get picture function
    this.camera.getPicture(options).then((imageData) => {
      //Crop Image
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });
  }
  //End
  takePhotoGallery() {
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };
    //Get picture function
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End
    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });
  }

  //End
  CropedImg(imageData) {
    if (this.platform.is('ios')) {
      this.tempProductImg = normalizeURL(imageData);
    } else {
      this.tempProductImg = imageData;
    }
    this.app.getRootNav().setRoot(CoverphotoPage, { 'tempProductImg': this.tempProductImg });

  }
  //Toast Msg
  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }
  //End
  //Code started
  //Show loading
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }

  //End
  //Delete orders
  deleteOrder(orderId, indexVal) {
    this.delObject.order_id = orderId;
    let alert = this.alertCtrl.create({
      title: this.translationLet.delete,
      message: this.translationLet.do_you_want_to_delete_order,
      buttons: [
        {
          text: this.translationLet.yes_txt,
          handler: () => {
            //  Delete
            this.showLoading();
            this.serviceProvider.delPastOrders(this.delObject, this.token).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.orderPastList.splice(indexVal, 1);
                if (this.orderPastList.length > 0)
                  this.isPastData = true;
                else
                  this.isPastData = false;
                this.toastMsg(this.translationLet.order_deleted_success);
              } else {
                this.toastMsg(this.translationLet.something_wrong_txt);
              }
            }, (err) => {
              this.loading.dismiss();
            });
          }
          //End
        }, {
          text: this.translationLet.no_txt,
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  commonProductList(orderData) {
    if (orderData.length > 0) {
      for (let i = 0; i < orderData.length; i++) {
        if (orderData[i].productList.length > 0) {
          orderData[i].productTempList = [];

          for (let k = 0; k < orderData[i].productList.length; k++) {
            if (orderData[i].productTempList.length > 0) {
              this.returnVal = this.isProductExist(orderData[i].productTempList, orderData[i].productList[k]['product_id']);
              if (this.returnVal == 'notExist') {
                orderData[i].productTempList.push(orderData[i].productList[k]);
              } else {
                orderData[i].productTempList[this.returnVal]['quantity'] = parseInt(orderData[i].productTempList[this.returnVal]['quantity']) + parseInt
                  (orderData[i].productList[k]['quantity']);
              }
            } else {
              orderData[i].productTempList.push(orderData[i].productList[k]);
            }

          }
          console.log(JSON.stringify(orderData[i].productTempList));
        }
      }
    }
  }

  isProductExist(prodData, prodId) {
    for (let j = 0; j < prodData.length; j++) {
      if (prodData[j]['product_id'] == prodId)
        return j;
    }
    return 'notExist';
  }
}