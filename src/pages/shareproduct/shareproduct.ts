import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import moment from 'moment';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';


@Component({
  selector: 'page-shareproduct',
  templateUrl: 'shareproduct.html',
})

export class ShareproductPage {
 
  translationLet: any = [];
  loading: Loading;
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  friendList :any =[];

  friendObj : any = {
    user_id:'',
    search_frd : '',
    start : 0,
    limit : 100,
    order_id : ''
  };
  userList :any =[];
  collectUsers : any =[];
  tempContacts : any =[];
  tempScrollData :any =[];
  loadmoreData=0;
  isData : any = true;
  user_id :any ='';
  lang_id : any ='';

  tempArr :any ={buyer_id: '',seller_id:'',friendCollect:[]};

  constructor(private contacts: Contacts,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   
    this.friendList =[];
    this.isData =true;
    this.userList =[];
    this.userImgPath = this.configProvider.getImagePath();

    this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

    this.translateService.get(['order_share_frd_successfully','please_select_atleast_one_frd','service_error','share_with_frd','search_by_name_phone_email','pull_to_refresh','frd_not_found','submit','cancel','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        });
      
    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;
     });  

      this.tempArr= JSON.parse(this.navParams.get('orderDetails'));
      //this.fetchAllPhoneData();
       this.storage.get('userDetails').then((userDetails) => {
        this.friendObj.user_id= userDetails.id;
        this.friendObj.search_frd= '';
        this.friendObj.order_id= this.tempArr.id;
        this.friendObj.seller_id= this.tempArr.seller_id;
        this.user_id =userDetails.id;
        this.getContactDataFun();
      });

    
  }

  getContactDataFun()
  {
    //Call contact users 
    this.showLoading();
     this.serviceProvider.getContactData(this.friendObj,this.token).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
            this.userList = result.userList;
            this.fetchAllPhoneData();
          } else
          {
            this.friendList =[];
            this.isData=false;
          //this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          });

  }


  fetchAllPhoneData()
  {

  var opts = {  
          filter: '',
          desiredFields: ['phoneNumbers'],
          fields: ['phoneNumbers'],
          multiple: true,        
          hasPhoneNumber:true
        };

  this.contacts.find(['displayName','phoneNumbers'],opts).then((contacts) => {
        contacts.filter((item) => {
          //Fetch all contacts
          if(item.phoneNumbers != null)
          {
           const userIndex  =  this.userList.findIndex( role =>
              role.mobile_no == item.phoneNumbers[0].value.replace(/[ (-)- ]/g,'') || item.phoneNumbers[0].value.replace(/[ (-)- ]/g,'').includes(role.mobile_no) == true || role.mobile_no.includes(item.phoneNumbers[0].value.replace(/[ (-)- ]/g,'')) == true
           );
           if(userIndex != -1)
            this.friendList.push({'name': this.userList[userIndex]['name'], 'profile_pic' : this.userList[userIndex]['profile_pic'], 'id' : this.userList[userIndex]['id']})
          }
       });

        if(this.friendList.length > 0)
           this.isData =true;
         else
          this.isData=false;

       // this.loading.dismiss();
        //End
     });
   } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShareproductPage');
  }


//Submit records

submit()
{
  if(this.collectUsers.length > 0 )
  {
    this.tempArr.friendCollect = this.collectUsers;
    this.tempArr.created = moment().format('YYYY-MM-DD HH:mm:ss');

    this.showLoading();

    this.serviceProvider.shareFriendOrder(this.tempArr,this.token,this.lang_id).then((result:any) => {
          this.loading.dismiss();
            if(result.code == 200){
            this.navCtrl.pop();
            this.toastMsg(this.translationLet.order_share_frd_successfully);
          } else
          {
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
          this.loading.dismiss();
          console.log('err '+err);
          });
  }else
  {
   this.toastMsg(this.translationLet.please_select_atleast_one_frd);

  }
}

//Show loading

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }

updateItem(evt,friend_id)
{
   
     if(!this.isKeyExist(friend_id,this.collectUsers))
          {
              this.collectUsers.push(friend_id);  
          }else
          {
            this.updateKeyValue(friend_id,this.collectUsers);    
          }
}

isKeyExist(friend_id,friendArr)
{
  for(var i = 0, len = friendArr.length; i < len; i++) {
        if( friendArr[i] == friend_id )
            return true;
    }
    return false;  
}

updateKeyValue(friend_id,friendArr)
{
let index: number = friendArr.indexOf(friend_id);
    if(index > -1){
        friendArr.splice(index, 1);
    }
}

//End

//Toast Msg

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

}