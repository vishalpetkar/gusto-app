import { Component, Renderer } from '@angular/core';
import { ViewController,IonicPageModule,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import moment from 'moment';

@Component({
  selector: 'page-updatedeliverytime',
  templateUrl: 'updatedeliverytime.html',
})

export class UpdatedeliverytimePage {
  translationLet: any = []; 
  loading: Loading; 
  token :any ='';
  tempObj :any ={};
  OrderObj : any = {'id' : '' ,'user_id' : '','schedule_datetime' : '','delivery_time' : '','delivery_date' : '' };
  tempTime :any ='';
  oldDateTime :any =''
  oldDate : any ='' ;
  newDateTime :any =''
  newDate : any ='';
  schedule_datetime: any ='';

constructor(public renderer: Renderer, public viewCtrl: ViewController,public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'acceptnewtime-popup', true);
     
      this.translateService.get(['done_txt','save','delivery_time_greater_than_current_time','delivery_time','rescheduled_delivery_greater_than_buyer','rescheduled_delivery_differ_than_buyer','unAuthReq_msg','service_error','please_wait_txt','accept_with_new_time','time','cancel','submit']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

     this.tempObj= JSON.parse(this.navParams.get('deliveryJSON'));
     this.tempTime =this.tempObj.delivery_time;
     this.OrderObj.id= this.tempObj.id;
     this.OrderObj.user_id= this.tempObj.user_id;
     this.OrderObj.schedule_datetime= this.tempObj.delivery_time;
     this.OrderObj.delivery_date= this.tempObj.delivery_date;
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AcceptnewtimePage');
  }

cancel()
{
this.viewCtrl.dismiss(0);
}

submit(){

	if(this.OrderObj.schedule_datetime != this.tempTime)
	{
        this.newDateTime = this.OrderObj.delivery_date + " " + this.OrderObj.schedule_datetime;
        this.newDate =moment(this.newDateTime).format('x');
        this.oldDate =moment().format('x');
         if(this.newDate < this.oldDate )
         {
         this.toastMsg(this.translationLet.delivery_time_greater_than_current_time);
          return false;
         }else
         {
      		  this.showLoading();
      		  this.OrderObj.delivery_time = this.OrderObj.schedule_datetime;
      		  this.schedule_datetime = this.OrderObj.delivery_date + " " + this.OrderObj.schedule_datetime;
      		  this.OrderObj.schedule_datetime =moment(this.schedule_datetime).format('YYYY-MM-DD HH:mm:ss');
        
      		  this.serviceProvider.updateDeliveryTime(this.OrderObj,this.token).then((result:any) => {
      		            this.loading.dismiss();
      		            if(result.code == 200)
      		            { 
      		              this.viewCtrl.dismiss(this.OrderObj);
      		            }
      		           else
      		           {
      		            this.viewCtrl.dismiss(0);
      		            this.toastMsg(this.translationLet.service_error);
      		            }
      		          }, (err) => {
      		             this.loading.dismiss();
      		          }); 
}
}else
{
	        this.viewCtrl.dismiss(0);
}
  }

  //Show loading 
   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

}
