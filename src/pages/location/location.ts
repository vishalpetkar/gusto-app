import { Component,NgZone, ElementRef, ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import moment from 'moment';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

declare var google;

@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  
  addressList :any =[]; 
  translationLet: any = [];	
  loading: Loading;	
  loginUserData : any = {'id' : '','address' : '','latitude' : '','longitude' : ''}; 
  token :any ='';
  tempAddress :any ='';
  locationOptions :any =''; 

  @ViewChild('map') mapElement: ElementRef;
  map: any;
 
    service = new google.maps.places.AutocompleteService();

  constructor(private locationAccuracy: LocationAccuracy,private zone: NgZone,private geolocation: Geolocation,private nativeGeocoder: NativeGeocoder,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
  this.translateService.get(['save','delivery_address','enter_delivery_address','update_delivery_address','please_enter_address','error_location_permission','please_wait_gps_txt','unable_to_fetch_gps','error_txt','unAuthReq_msg','service_error','please_wait_txt']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
    
     this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     });

     //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.loginUserData.address=userDetails.address;
        this.loginUserData.id=userDetails.id;
        this.tempAddress= userDetails.address;
     }); 
  }
  

//Autocompleted search and get google location from services 
    
    dismiss()
      {
        this.loginUserData.address ="";
        this.addressList =[];
      }

    updateSearch() {

       if (this.loginUserData.address.trim() == '') {
       this.addressList = [];
       return;
      }
     
      let me = this;
      this.service.getPlacePredictions({
      input: this.loginUserData.address
     }, (predictions, status) => {
       me.addressList = [];
       me.zone.run(() => {
       if (predictions != null) {
          predictions.forEach((prediction) => {
            me.addressList.push(prediction.description);
          });
         }
       });
     });
    }

     // Select Item from list 

     chooseItem(item: any) {
      this.addressList = [];
      this.loginUserData.address=item;
      this.geoCodeAddress(this.loginUserData.address);
    }
   
   //End

   geoCodeAddress(address) {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': address }, (results, status) => {
      this.loginUserData.latitude = results[0].geometry.location.lat();
      this.loginUserData.longitude = results[0].geometry.location.lng();
        });
   }


  //Submit request 

  submit()
  {

if(this.validateFields())
  {
   
    this.showLoading();

   if(this.tempAddress == this.loginUserData.address)
   {
   this.loading.dismiss();
   this.navCtrl.pop();
   }else{
    this.serviceProvider.buyerDeliveryAddress(this.loginUserData,this.token).then((result:any) => {
     this.loading.dismiss();
       
       if(result.code == 200)    
       { 
         this.storage.set('userDetails',result.userDetails);
         this.toastMsg(this.translationLet.update_delivery_address);
         this.navCtrl.pop();
       }else if(result.code == 403)
       {
        this.toastMsg(this.translationLet.unAuthReq_msg);
       }else
       {
        this.toastMsg(this.translationLet.something_wrong_txt);
       }
    }, (err) => {
      this.loading.dismiss();
    }); 
}
     //End
   }
  
  }

  //Call GPS location 

  getGPS()
  {

  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
 
 this.showLoadingGPS();

          if(this.platform.is('ios')){
           this.locationOptions = {timeout: 2000,enableHighAccuracy: true};
          }else{
            this.locationOptions={timeout: 12000, enableHighAccuracy: true};
          } 

          this.geolocation.getCurrentPosition(this.locationOptions).then((resp) => {

          this.loading.dismiss();

             if(resp.coords.latitude && resp.coords.longitude){

             //Get lat n lang  with address
            this.loginUserData.latitude =resp.coords.latitude;
            this.loginUserData.longitude =resp.coords.longitude;

            let options: NativeGeocoderOptions = {
            useLocale: true,
            maxResults: 1
           };
          this.nativeGeocoder.reverseGeocode(this.loginUserData.latitude,this.loginUserData.longitude, options)
          .then((result: NativeGeocoderReverseResult[]) => 

            this.reverseGeo(result))
            
          .catch((error: any) => 
           this.gpsError()
            );
             }else
            {
           this.gpsError()
            }
        
        }).catch((error) => {
            this.loading.dismiss();
            this.gpsError();
        });
          
     },
        (error) => {
        this.gpsError()
        } );


  }

reverseGeo(res)
{ 
  this.loginUserData.address =res[0].thoroughfare+" "+res[0].subThoroughfare+" "+res[0].subLocality+" "+res[0].locality;
}


gpsError()
{
  this.toastMsg(this.translationLet.unable_to_fetch_gps);
}


  //validation starts

   validateFields()
    {
    if (this.loginUserData.address.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_address);
          return false;
        }
      return true;
    }

    //End

//Alert Popups 

    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: "Ok",
        cssClass:'btn-primary btn-round'}]
    });
    alert.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }

  //Show loading 

    showLoadingGPS() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_gps_txt+'</div></div></div>'
    });
    this.loading.present();
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

   //End


}
