import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-privacy-policies',
  templateUrl: 'privacy-policies.html',
})
export class PrivacyPoliciesPage {
 translationLet: any;	
  constructor(private translateService: TranslateService,public navCtrl: NavController, public navParams: NavParams) {
       this.translateService.get(['privacy_policy']).subscribe((translation: [string]) => {
        this.translationLet = translation;
      });  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivacyPoliciesPage');
  }

}
