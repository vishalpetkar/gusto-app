import { Component, Renderer } from '@angular/core';
import { ViewController,IonicPageModule,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { UpcomingPage } from '../upcoming/upcoming';
import moment from 'moment';
import { ConfigProvider } from '../../providers/config/config';

@Component({
  selector: 'page-joinsharedorder',
  templateUrl: 'joinsharedorder.html',
})
export class JoinsharedorderPage {

 translationLet: any = []; 
 loading: Loading; 
 token :any ='';
 quantityList : any = [];
 selectOptions : any ={};
 tempArr : any =[];
 cardType: any ='';
 totalCalAmt:any ='';

 isUpdateCreateCard :any =true;
 isPaypal :any =false;
 paymentMethod = [];
 sellerObj :any ={ 'user_id' : '', 'sellerId' : '','lang_id' : '' };
 prmoCodeItem : any ='';
 std_cost : any ='';
 prodImgPath : any ='';
 cardTypeObj : any = [{'id' : 1, 'txt' : 'visa','img':'assets/images/visa-card.png'},{'id' : 2,'txt' : 'mastercard', 'img':'assets/images/master-card.png'}];
 lang_id :any ='';
 commonFieldList :any=[]; 
 shareProductList :any ={'order_type' : '','order_id': '','buyer_id' : '' ,'seller_id' : '','user_id':'','created' : '', 'payment_method' : '','productList' : []}; 
 tempFields: any = { 'isSellerPromoCodeUsed' : '', 'discount_percentage' : '' };

constructor(public configProvider : ConfigProvider,public renderer: Renderer, public viewCtrl: ViewController,public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
//this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'joinsharedorder-popup', true);
this.prodImgPath = this.configProvider.getProdImgPath();
 this.translateService.get(['off_to_all_products','delivery','take_away','seller_address','you_got_txt','prod_del_success','delete','yes_txt','no_txt','delete_cart_msg','save','seller','please_enter_valid_expire_year','please_enter_valid_cvv','qty','not_added_txt','product_note_updated','product_update_quantity','please_enter_four_digit_for_year','pleae_add_note','write_note','add_product_note','add_notes','select_quantity','add_promo_code','add_promo','order_joined_successfully','please_enter_valid_promo_code','please_add_promo_code','ok_txt','please_select_prod_delivery_date','please_select_prod_delivery_time','please_enter_delivery_receiver_name','please_enter_credit_card_number','please_enter_expire_month','card_year_expire_numeric','card_cvv_numeric','please_enter_valid_phone','please_enter_cvv','card_month_expire_numeric','please_enter_expire_year','card_number_numeric','please_select_card_type','please_select_payment_method','date_must_greter_than_current_date','please_enter_delivery_receiver_mobile_no','please_enter_deliver_receiver_surname','service_error','please_select_date','please_select_time','deliver_to','name','surname','mobile_no','company','note','amt','with_delivery_cost','deduct_amount_seller_accept_txt1','reaches_delivery_policies_amt','deduct_amount_seller_accept_txt2','select_payment_method','credit_card_number','expire_month','expire_year','card_cvv','credit_card_details','update','next_txt','seller_not_selected_any_payment_method_yet','schedule_order','delivery_address','cancel','submit','note','seller_not_added_payment_method_yet','promo_code_successfully_applied','used_promo_code','unAuthReq_msg','service_error','please_wait_txt']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;
      this.sellerObj.lang_id;
     });  

this.quantityList = [{'text' : '1', 'val' : '1'},{'text' : '2', 'val' : '2'},{'text' : '3', 'val' : '3'},
   {'text' : '4', 'val' : '4'},{'text' : '5', 'val' : '5'},{'text' : '6', 'val' : '6'},
   {'text' : '7', 'val' : '7'},{'text' : '8', 'val' : '8'},{'text' : '9', 'val' : '9'},{'text' : '10', 'val' : '10'}];

this.shareProductList.productList = JSON.parse(this.navParams.get('joinorderDetails')) ; 
this.commonFieldList = JSON.parse(this.navParams.get('commonField')) ; 
this.tempFields.isSellerPromoCodeUsed = this.commonFieldList.isSellerPromoCodeUsed;
this.tempFields.discount_percentage = this.commonFieldList.discount_percentage;

this.totalCal();
this.sellerObj.sellerId = this.commonFieldList.seller_id;
this.sellerObj.user_id = this.commonFieldList.user_id; 
this.shareProductList.seller_id = this.commonFieldList.seller_id;
this.shareProductList.user_id = this.commonFieldList.user_id; 
this.shareProductList.buyer_id = this.commonFieldList.buyer_id;
this.shareProductList.order_id = this.commonFieldList.order_id; 
this.shareProductList.order_type = this.commonFieldList.order_type; 

this.std_cost= this.shareProductList.productList[0].std_cost;

     this.selectOptions = {
        title: this.translationLet.select_quantity
        };
 
 this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;

          // Call service to get payment methods from seller 
          this.serviceProvider.sellerPaymentMethod(this.sellerObj,this.token,this.lang_id).then((result:any) => {
          if(result.code == 200){
          if(result.sellerPayments.length > 0 )
          {
            if(result.sellerPayments.length == 1)
            {
             this.shareProductList.payment_method= result.sellerPayments[0]['id'];
             this.paymentMethod = result.sellerPayments;
            }else
            {
            this.paymentMethod = result.sellerPayments;
            }
          //End 
          }
            else
            {
            this.paymentMethod = [];
            }
          } else{
             this.toastMsg(this.translationLet.service_error);
          } 
          }, (err) => {
          }); 
        // End
    });
}

//cal total for status
totalCal()
{
this.totalCalAmt =0;

if(this.shareProductList.productList.length > 0)
  {
  for(let i=0;i<this.shareProductList.productList.length;i++)
  {
    this.totalCalAmt += parseFloat(this.shareProductList.productList[i]['deductedAmt'])  * this.shareProductList.productList[i]['quantity'];
   }

   if(this.tempFields.isSellerPromoCodeUsed ==1)
   {
    this.totalCalAmt= (this.totalCalAmt-(this.totalCalAmt * (this.tempFields.discount_percentage/100)));
   }
   } 
}

//On quntity selections 

onSelectChange(events,indexVal)
{
this.shareProductList.productList[indexVal]['quantity'] = events;
this.totalCal();
this.toastMsg(this.translationLet.product_update_quantity);
}

 //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

//Cancel modal 

cancel()
{
this.viewCtrl.dismiss();
}

submit(){

   if(this.validate())
  {
   this.shareProductList.created = moment().format('YYYY-MM-DD HH:mm:ss');
   this.showLoading();
     this.serviceProvider.joinOrders(this.shareProductList,this.token,this.lang_id).then((result:any) => {
            this.loading.dismiss();
            if(result.code == 200)
            { 
              this.viewCtrl.dismiss();
              this.storage.set('userDetails', result.userDetails); 
              this.toastMsg(this.translationLet.order_joined_successfully);
              this.app.getRootNav().setRoot(UpcomingPage); 
            }
           else
           {
            this.viewCtrl.dismiss();
            this.toastMsg(this.translationLet.service_error);
            }
          }, (err) => {
             this.loading.dismiss();
          }); 
  } 
  }

  validate()
{
 
  if (this.shareProductList.payment_method == "") {
          this.toastMsg(this.translationLet.please_select_payment_method);
          return false;
        }
  return true;
}

//Alert Popups 

    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'btn-primary btn-round'}]
    });
    alert.present();
  }

//End 

paymentOpt(paymentId)
{
if(paymentId ==2)
  this.isPaypal =true;
else
  this.isPaypal =false;
}


updateCardDetails()
{
  this.isUpdateCreateCard =true;
}

//goto promo code 

gotoPromo(prodData,indexValue)
{
 let alertPromo = this.alertCtrl.create({
    title : this.translationLet.add_promo_code,
    enableBackdropDismiss:false,
    inputs: [
      {
        name: 'add_promo',
        placeholder:  this.translationLet.please_add_promo_code,
        type: 'text'
      }
    ],
    buttons: [
    {
        text: this.translationLet.cancel,
        cssClass:'btn-primary btn-round',
        handler: data => {
        }
      },
      {
        text: this.translationLet.submit,
        cssClass:'btn-primary btn-round',
        handler: data => {

        if(data.add_promo.trim().length == 0)
          {
          this.toastMsg(this.translationLet.please_add_promo_code);
           return false;
          }else
          {
          if(data.add_promo.trim() != prodData.promoCode )
          {
           this.toastMsg(this.translationLet.please_enter_valid_promo_code);
           return false;
          }else
          {
          prodData.isPromocodeused ='1';
          prodData.total_amount =prodData.amount;
          prodData.deductedAmt = prodData.amount-prodData.amount * (prodData.discount_percentage/100);
          this.totalCal();
          this.toastMsg(this.translationLet.promo_code_successfully_applied);
          }

                  }
      }
    }
      
    ]
  });

  alertPromo.present();  

}

//gotoNote

gotoNote(prod_notes,indexValues) {

 let alert = this.alertCtrl.create({
    title : this.translationLet.add_product_note,
    enableBackdropDismiss:false,
    inputs: [
      {
        name: 'add_note',
        placeholder:  this.translationLet.write_note,
        type: 'text',
        value : prod_notes
      }
    ],
    buttons: [
    {
        text: this.translationLet.cancel,
        cssClass:'btn-primary btn-round',
        handler: data => {
        }
      },
      {
        text: this.translationLet.submit,
        cssClass:'btn-primary btn-round',
        handler: data => {
        if(data.add_note.length == 0)
          {
           this.toastMsg(this.translationLet.pleae_add_note);
           return false;
          }else
          {
           this.shareProductList.productList[indexValues]['notes'] = data.add_note;
           this.toastMsg(this.translationLet.product_note_updated);
          } 
                  }
      }
      
    ]
  });
  alert.present();
}


delFromCart(item,indexVal)
{
  let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.delete_cart_msg,
     buttons: [
      {
        text: this.translationLet.yes_txt,
        handler: () => {
          this.shareProductList.productList.splice(indexVal, 1); 
          if(this.shareProductList.productList.length == 0)
          {
            this.navCtrl.pop();
          }
          this.totalCal();
          this.toastMsg(this.translationLet.prod_del_success);
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        }
      }
    ]
  });
  alert.present();  

}

}
