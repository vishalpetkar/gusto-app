import { Component,ViewChild,ElementRef, Renderer } from '@angular/core';
import { Platform,Nav,MenuController ,ModalController,ActionSheetController,normalizeURL,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import moment from 'moment';
import { ExplorePage } from '../explore/explore';
import { ShareproductPage } from '../shareproduct/shareproduct';
import { SharepeoplelistPage } from '../sharepeoplelist/sharepeoplelist';
import { PaymentTransactionPage } from '../payment-transaction/payment-transaction';
import { UserRatingPage } from '../user-rating/user-rating';
import { UpcomingPage } from '../upcoming/upcoming';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
@Component({
  selector: 'page-joinhistroydetail',
  templateUrl: 'joinhistroydetail.html',
})
export class JoinhistroydetailPage {

	token :any ='';
	translationLet: any;	
	loading: Loading;

   	profileDetails : any = {
    name : '',
    profile_pic : '',
    mobile_no:'',
    address:'',
    email : '',
    id:'',
    min_order_charge:'',
    order_free_amt:'',
   // order_free_range:'',
    max_delivery_range:'',
    std_delivery_cost:'',
    Paypal_method: '',
    COD:'',
    order_take_away : ''
  };

  myorderCount :any =0;
  receiveOrderCount :any =0;

  prodImgPath : any ='';
  userImgPath : any ='';
  isFetchingData: any =true;
  buyerOrderObj : any ={'buyer_id' : '','seller_id' : '' , 'id' : '', 'created' : '','is_free' : '','userId':'' };
  orderDetails : any = {'orderTrack' : [] ,'itemList' :[] };

  tempProductImg : any = "";

  dateTimeOld :any ='';
  dateTimeNew :any ='';

	 constructor(public modalCtrl: ModalController,private crop: Crop,public actionSheetCtrl: ActionSheetController,public renderer: Renderer, public platform: Platform,private camera: Camera,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }
  
   /*Ionic refresher */

 doJoinBuyerOrderRefresh(refresher) {
   this.buyerOrderInitRefresh(this.buyerOrderObj,refresher);
  }


buyerOrderInitRefresh(buyerDetails,refresher)
{

//this.showLoading();

this.serviceProvider.getJoinBuyerDetails(buyerDetails,this.token).then((result:any) => {
  //        this.loading.dismiss();
            if(result.code == 200){
            this.orderDetails =result.data;
            refresher.complete();
          } else
          {
             this.orderDetails=[];
             refresher.complete();
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
    //      this.loading.dismiss();
          refresher.complete();
          console.log('err '+err);
          }); 
}

 /* End */
      ionViewDidEnter()
    {
   this.prodImgPath = this.configProvider.getProdImgPath();
    this.userImgPath = this.configProvider.getImagePath();

      this.translateService.get(['coupen_applied','buyer','joined_buyer','delivery_cost','buyer','joined_buyer','paid','un_paid','unable_to_crop_image','yes_txt','no_txt','reject_received_order_from_your_side','do_you_want_reject_order','reject_order','send_order','sender_order_to_seller','order_send_to_seller_successfully','select_one_payment_method','accept_received_order','do_you_want_to_accept_order','order_received_sucessfully','do_you_want_to_accept_rescheduled_time','order_confrim_from_your_side','reject_received_order','do_you_want_to_reject_received_order','order_time_elapsed','service_error','mobile_no','refreshing','time','order_details','pull_to_refresh','delivery_cost_txt','free_delivery_txt','total_amt','order_delivery_on','buyer_product_notes','not_added_txt','order_receiver_details','name','surname','company','seller_info','share_order_with_frd','order_share_with_following_frd','view_all','order_summary','send_order_to_seller','order_not_send_due_to_min_policies','order_not_accept_yet','order_rescheduled_by_seller','seller_suggest_new_time','accept','reject','received_order_from_seller','accepted','rejected','order_status','please_wait_txt','cancel','post_new_product','take_photo','upload_from_gallery','save','payement_method']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  

    this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 

     //Get details
    this.storage.get('userDetails').then((userDetails) => {
      this.profileDetails=userDetails;
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.buyerOrderObj.id=this.navParams.get('order_id');
      this.buyerOrderObj.userId=userDetails.id;
      this.buyerOrderObj.buyer_id=this.navParams.get('mainBuyerId'); 
      this.buyerOrderObj.seller_id=this.navParams.get('seller_id');
      //Get Buyer order details
     this.buyerOrderInit(this.buyerOrderObj);
     //End 
    }); 
    }
  

  //Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}

//Buyer order details section 

buyerOrderInit(buyerDetails)
{

//this.showLoading();

this.serviceProvider.getJoinBuyerDetails(buyerDetails,this.token).then((result:any) => {
  this.isFetchingData =false;
  //        this.loading.dismiss();
            if(result.code == 200){
            this.orderDetails =result.data;
          } else
          {
             this.orderDetails=[];
             this.toastMsg(this.translationLet.service_error);
          }
          }, (err) => {
    //      this.loading.dismiss();
          console.log('err '+err);
          }); 
}

 //Show loading 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

    gotoExplore()
   {
    this.app.getRootNav().setRoot(ExplorePage); 
   }

	gotoHome()
	{
    
     this.app.getRootNav().setRoot(HomePage);
	}

	gotoAccount()
	{
    this.app.getRootNav().setRoot(MyprofilePage); 
	}

   


//Go to cover photo
    gotoCoverPhoto()
    {
       if(this.profileDetails.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

        if( (this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null ) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null )  && (this.profileDetails.std_delivery_cost == ''  || this.profileDetails.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
    }else if( (this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
       //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
  }
    } 
  }
   
  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

        //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
    
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
//Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   
   //End  

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyerorderdetailsPage');
  }


//Goto Share a friend 

gotoShareAfriend()
{
  this.navCtrl.push(ShareproductPage,{ 'orderDetails' : JSON.stringify(this.buyerOrderObj)});
}
//End 

gotosharedfriends(){
this.navCtrl.push(SharepeoplelistPage,{ 'orderDetails' : JSON.stringify(this.buyerOrderObj)});
}

       gotoUserOrder()
    {
     this.app.getRootNav().setRoot(ReceiveUpcomingPage);
    }

     gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }

//Goto Order summary 
gotoOrderSummary()
{
  this.navCtrl.push(PaymentTransactionPage,{'ordersSummary' : JSON.stringify(this.buyerOrderObj)});
}

//End 


}
