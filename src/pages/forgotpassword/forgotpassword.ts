import { Component } from '@angular/core';
import { MenuController ,IonicPage,App, NavController,ModalController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { CountryCodePage } from '../country-code/country-code';
import { VerifiedPasswordPage } from '../verified-password/verified-password';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
  
countryList :any =[];
defaultFlag : any ='';
translationLet: any;
loading: Loading;

forgotData : any = {
phoneNumber : '',
countryCode : '+39',
tempNumber:''
};

 constructor(public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) 
 {  
   this.defaultFlag = 'assets/images/country-icon1.png'; 
    this.translateService.get(['please_wait_txt','enter_mobile_not_exist','submit','forgot_password','enter_phone_to_reset_pass','back_to_login','please_enter_valid_phone','plese_enter_pass','signup','do_not_have_an_account','wrong_pass','instagram_error','sign_in_success','something_wrong_txt','facebook_error','error_txt','enter_phone_no_val','ok_txt','login_txt_1','login_txt_2','login_txt_3','facebook','instagram','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
 //End 
   this.initCountryList();
 }


   //Get Country List 
 initCountryList()
 {

  this.serviceProvider.getCountryList().then((result:any) => {
    if(result.code == 200){
      this.countryList =result.data;
    } else if(result.code == 500) {
      //this.showPopup(this.translationLet.error,result.message);
    } else if(result.code == 404) {
      //this.showPopup(this.translationLet.error,result.message);
    } 
    }, (err) => {
    });
 }
  //End

  //Country model 

 openCountryCodeModal()
 {

 let modalCountry = this.modalCtrl.create(CountryCodePage, {'countryListObj':this.countryList});

  modalCountry.onDidDismiss(data => {
     if(data.country_code != ''){
      this.forgotData.countryCode =data.country_code;
      this.defaultFlag =data.country_icon;
     }
   });

    modalCountry.present();
 }

 //Submit 

submit()
{
  if(this.validate())
  {
  this.showLoading();
  this.forgotData.mobile_no=this.forgotData.countryCode+this.forgotData.tempNumber;
  this.serviceProvider.forgotPass(this.forgotData).then((result:any) => {
     this.loading.dismiss();

    if(result.code == 200){
      this.navCtrl.push(VerifiedPasswordPage,{'ResetObj' : JSON.stringify(result)});
    }
    else if(result.code == 201){
       this.toastMsg(result.error);
    }
     else{
       this.toastMsg(this.translationLet.enter_mobile_not_exist);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
 }
}

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

validate()
{
  if (this.forgotData.tempNumber.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.enter_phone_no_val);
          return false;
        }
  if (this.forgotData.tempNumber.length < 8 || this.forgotData.tempNumber.length > 12  ) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_phone);
          return false;
        }

  return true;
}

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 



  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }

gotoLogin()
{
	 this.app.getRootNav().setRoot(LoginPage);
}
}
