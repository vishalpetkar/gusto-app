import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { DeliverypolicyPage } from '../deliverypolicy/deliverypolicy';
import { DeliveryaddressPage } from '../deliveryaddress/deliveryaddress'; 
import { SellerprofilePage } from '../sellerprofile/sellerprofile';


@Component({
  selector: 'page-mycart',
  templateUrl: 'mycart.html',
})
export class MycartPage {
 
  translationLet: any = []; 
  loading: Loading; 
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  productList :any =[];
  productObj : any = {
    user_id:'',
    start : 0,
    limit : 10
  };
  user_id :any ='';
   isFetchingData : any = true;
  
  loadmoreData=0;
  isData : any = true ;

   delProdCartObj : any ={ 
    id:'',
    user_id : ''
   }
   tempArr: any =[]; 
   add_note : any ='';
   
   notesObj : any = {'id' : '', 'note' : '','user_id' : ''};

   promoObj : any = {'id' : '', 'promo_code' : '','user_id' : '','discount' : '', 'isSellerPromocodeUsed' : ''};
   sellerPromoObj : any = {'cartList' : [], 'promo_code' : '','user_id' : '','discount' : '', 'isSellerPromocodeUsed' : ''};

   quanityObj : any = {'id' : '', 'quantity' : '','user_id' : ''};

   deliveryPolicies :any =[];

   quantityList : any = [];
 
   selectOptions : any ={};
   commonArr :any =[];

  constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
  this.selectOptions = {
    title: ''
    };

  this.quantityList = [{'text' : '1', 'val' : '1'},{'text' : '2', 'val' : '2'},{'text' : '3', 'val' : '3'},
   {'text' : '4', 'val' : '4'},{'text' : '5', 'val' : '5'},{'text' : '6', 'val' : '6'},
   {'text' : '7', 'val' : '7'},{'text' : '8', 'val' : '8'},{'text' : '9', 'val' : '9'},{'text' : '10', 'val' : '10'}];

    
    this.productObj.start= 0;  
    this.productObj.limit= 10;  

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

      this.translateService.get(['used_seller_promocode','off_to_all_products','save','seller','qty','not_added_txt','total','promo_code_successfully_applied','service_error','add_promo_code','add_product_note','enter_promo_code','please_add_promo_code','please_enter_valid_promo_code','used_promo_code','you_got_txt','pull_to_refresh','product_cart_deleted','delete_cart_msg','unable_update_product_note','product_note_updated','pleae_add_note','write_note','add_product_note','unable_update_quantity','product_update_quantity','unable_to_delete_cart','delete','submit','select_quantity','cancel','cart','add_notes','add_promo','proceed','prod_cart_empty','delivery_polices','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
    this.selectOptions.title= this.translationLet.select_quantity;
     //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.user_id =userDetails.id; 
        this.mycartList();
     }); 

  }
   
    /*Ionic refresher */
 doCartRefresh(refresher) {
    this.productObj.start= 0;  
    this.productObj.limit= 10;  
    this.mycartListRefresher(refresher);
  }
 /* End */

//Cart list Refresh 
mycartListRefresher (refresher)
{
this.productObj.user_id=this.user_id;
this.productObj.start=parseInt(this.productObj.start);
this.productObj.limit=parseInt(this.productObj.limit);  
this.serviceProvider.mycartList(this.productObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.productList =result.data;     
            this.discountWithAmount(this.productList);
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.productList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.productList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

//Cart list 

mycartListDelete()
{
this.productObj.start = 0;
this.productObj.limit =10;
this.productObj.user_id=this.user_id;
this.productObj.start=parseInt(this.productObj.start);
this.productObj.limit=parseInt(this.productObj.limit);  
this.serviceProvider.mycartList(this.productObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.productList =result.data; 
            this.discountWithAmount(this.productList);
            this.loadmoreData=1;
         } else if(result.code == 500) {
             this.productList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.productList=[];
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
}

mycartList()
{
this.productObj.user_id=this.user_id;
this.productObj.start=parseInt(this.productObj.start);
this.productObj.limit=parseInt(this.productObj.limit);  
this.serviceProvider.mycartList(this.productObj,this.token).then((result:any) => {
  this.isFetchingData=false;
            if(result.code == 200){
            this.productList =result.data; 
            this.discountWithAmount(this.productList);
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.productList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.productList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
}


discountWithAmount(resultData)
{
 for(let i=0;i<resultData.length;i++)
 {
      for(let k =0; k<resultData[i]['cartList'].length;k++)
      {
        if(resultData[i]['cartList'][k]['isSellerPromocodeUsed'] == 2)
         {
          resultData[i]['isSellerPromocodeUsed'] =2;
         }else if(resultData[i]['cartList'][k]['isSellerPromocodeUsed'] == 1)
         {
          resultData[i]['isSellerPromocodeUsed'] =1;
          
         }else
         {
          resultData[i]['isSellerPromocodeUsed'] =0;
         }
     }
     if(resultData[i]['isSellerPromocodeUsed']  ==1)
     {
      resultData[i]['totalCartAmt']= (resultData[i]['totalCartAmt']-(resultData[i]['totalCartAmt'] * (resultData[i]['userPolicies']['promoDiscount']/100)));
     }
 }
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.productObj.start = parseInt(this.productObj.start)+parseInt(this.productObj.limit);
    this.productObj.limit= parseInt(this.productObj.limit)+10;

  setTimeout(() => {

           this.serviceProvider.mycartList(this.productObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.productList = this.productList.concat(result.data);
            this.discountWithAmount(this.productList);
            this.loadmoreData=1;
            if(this.productList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.productList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.productList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 

//Goto Del policies 

gotoDelPolicies (deliveryPolicies)
{
this.navCtrl.push(DeliverypolicyPage, {'userPolicies' : JSON.stringify(deliveryPolicies)});
}

//Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 4000,
        position: 'bottom'
      });
      toast.present(toast);
    }

   //End

//Del product 

delFromCart(item,indexVal,cartVal)
{
this.delProdCartObj.id=item;
this.delProdCartObj.user_id=this.user_id;

  let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.delete_cart_msg,
    buttons: [
      {
        text: this.translationLet.yes_txt,
        handler: () => {
      //  Delete  
    this.showLoading();
    this.serviceProvider.delCartProduct(this.delProdCartObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     
     this.storage.set('cartCount', result.data); 
     this.productList[cartVal]['cartList'].splice(indexVal, 1);  
     this.productList[cartVal]['totalCartAmt'] = this.getCartAmountCount(this.productList[cartVal]['cartList']);
     if(this.productList[cartVal]['cartList'].length == 0)
      this.productList.splice(cartVal, 1);
     if(this.productList.length == 0)
      this.isData = false;
      this.mycartListDelete();
     this.toastMsg(this.translationLet.product_cart_deleted);
    }else
    {
    this.toastMsg(this.translationLet.unable_to_delete_cart);
    }
        }, (err) => {
          this.loading.dismiss();
        });
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        }
      }
    ]
  });
  alert.present();  

}

  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End


//Quantity changes 

onSelectChange(events,cartItem,indexVal,cartVal)
{
//Update Cart quantity 
this.quanityObj.id = cartItem;
this.quanityObj.user_id = this.user_id;
this.quanityObj.quantity = events;

this.showLoading();
    this.serviceProvider.updateQuantity(this.quanityObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     
     this.productList[cartVal]['cartList'][indexVal]['cQuant'] =events;
     this.productList[cartVal]['totalCartAmt'] = this.getCartAmountCount(this.productList[cartVal]['cartList']);
    //if(this.productList[cartVal]['isSellerPromocodeUsed'] ==1)
    // {
    // this.productList[cartVal]['totalCartAmt']= (this.productList[cartVal]['totalCartAmt']-(this.productList[cartVal]['totalCartAmt'] * (this.productList[cartVal]['cartList'][indexVal]['cDiscount']/100)));
    // }
     this.discountWithAmount(this.productList);
     this.toastMsg(this.translationLet.product_update_quantity);
    }else
    {
    this.toastMsg(this.translationLet.unable_update_quantity);
    }
        }, (err) => {
          this.loading.dismiss();
        });

//End 
}

//End

getCartAmountCount(cartListObj)
{
  let tempObj =0;
 for(let i=0; i< cartListObj.length ; i++)
 {
  if(cartListObj[i]['cIsPromocodeused'] ==1)

  tempObj +=   (cartListObj[i]['pAmt']-(cartListObj[i]['pAmt'] * (cartListObj[i]['cDiscount']/100))) * cartListObj[i]['cQuant'];
else
  tempObj +=  cartListObj[i]['cQuant'] * cartListObj[i]['pAmt'] ;
 }
 return tempObj;
}


//Goto Seller promo 
gotoSellerPromo(promoCode,discount,itemData,indexVal)
{
 let alertSellerPromo = this.alertCtrl.create({
    title : this.translationLet.add_promo_code,
    enableBackdropDismiss:false,
    inputs: [
      {
        name: 'add_promo',
        placeholder:  this.translationLet.enter_promo_code,
        type: 'text'
      }
    ],
    buttons: [
    {
        text: this.translationLet.cancel,
        cssClass:'btn-primary btn-round',
        handler: data => {
        }
      },
      {
        text: this.translationLet.submit,
        cssClass:'btn-primary btn-round',
        handler: data => {

        if(data.add_promo.trim().length == 0)
          {
          this.toastMsg(this.translationLet.please_add_promo_code);
           return false;
          }else
          {
          
          if(data.add_promo.trim() != promoCode )
          {
           this.toastMsg(this.translationLet.please_enter_valid_promo_code);
           return false;
          }else
          {
               this.sellerPromoObj.user_id =this.user_id;
               this.sellerPromoObj.cartList =itemData.cartList;
               this.sellerPromoObj.promo_code =data.add_promo;
               this.sellerPromoObj.discount =discount;
               this.sellerPromoObj.isSellerPromocodeUsed =1;
               this.showLoading();     
              this.serviceProvider.updateSellerPromo(this.sellerPromoObj,this.token).then((result:any) => {
              this.loading.dismiss();
              if(result.code == 200)
              {
               
               this.productList[indexVal]['discount'] = discount;
               this.productList[indexVal]['isSellerPromocodeUsed'] =1;
               this.productList[indexVal]['totalCartAmt']= (this.productList[indexVal]['totalCartAmt']-(this.productList[indexVal]['totalCartAmt'] * (discount/100)));
               this.mycartList();
               this.toastMsg(this.translationLet.promo_code_successfully_applied);
              }else
              {
               this.toastMsg(this.translationLet.service_error);
              }
                  }, (err) => {
                    this.loading.dismiss();
                  });  
          }

                  }
      }
    }
      
    ]
  });

  alertSellerPromo.present();  

}

//End 

//goto promo code 

gotoPromo(cId,indexVal,promoCode,discount,cartVal)
{
 let alertPromo = this.alertCtrl.create({
    title : this.translationLet.add_promo_code,
    enableBackdropDismiss:false,
    inputs: [
      {
        name: 'add_promo',
        placeholder:  this.translationLet.enter_promo_code,
        type: 'text'
      }
    ],
    buttons: [
    {
        text: this.translationLet.cancel,
        cssClass:'btn-primary btn-round',
        handler: data => {
        }
      },
      {
        text: this.translationLet.submit,
        cssClass:'btn-primary btn-round',
        handler: data => {

        if(data.add_promo.trim().length == 0)
          {
          this.toastMsg(this.translationLet.please_add_promo_code);
           return false;
          }else
          {
          
          if(data.add_promo.trim() != promoCode )
          {
           this.toastMsg(this.translationLet.please_enter_valid_promo_code);
           return false;
          }else
          {

          //Call service and Update promo code 
          this.promoObj.id = cId;
          this.promoObj.user_id = this.user_id;
          this.promoObj.promo_code = data.add_promo;
          this.promoObj.discount = discount;
          this.promoObj.isSellerPromocodeUsed = 2;
          
          this.showLoading();
              this.serviceProvider.updatePromo(this.promoObj,this.token).then((result:any) => {
              this.loading.dismiss();
              if(result.code == 200)
              {
               this.discountWithAmount(this.productList); 
               this.productList[cartVal]['cartList'][indexVal]['cIsPromocodeused'] =1;
               this.productList[cartVal]['cartList'][indexVal]['cDiscount'] =discount;
               this.productList[cartVal]['isSellerPromocodeUsed'] =2;
               this.productList[cartVal]['totalCartAmt'] = this.getCartAmountCount(this.productList[cartVal]['cartList']);
               this.toastMsg(this.translationLet.promo_code_successfully_applied);
              }else
              {
               this.toastMsg(this.translationLet.service_error);
              }
                  }, (err) => {
                    this.loading.dismiss();
                  });

       
          }

                  }
      }
    }
      
    ]
  });

  alertPromo.present();  

}


//End 

//gotoNote

gotoNote(notesVal,cId,indexVal,cartVal) {

 let alert = this.alertCtrl.create({
    title : this.translationLet.add_product_note,
    enableBackdropDismiss:false,
    inputs: [
      {
        name: 'add_note',
        placeholder:  this.translationLet.write_note,
        type: 'text',
        value : notesVal
      }
    ],
    buttons: [
    {
        text: this.translationLet.cancel,
        cssClass:'btn-primary btn-round',
        handler: data => {
        }
      },
      {
        text: this.translationLet.submit,
        cssClass:'btn-primary btn-round',
        handler: data => {

        if(data.add_note.trim().length == 0)
          {
           this.toastMsg(this.translationLet.pleae_add_note);
           return false;
          }else
          {
          //Call service and Update notes 
          this.notesObj.id = cId;
          this.notesObj.user_id = this.user_id;
          this.notesObj.note = data.add_note;
          this.showLoading();
              this.serviceProvider.updateNotes(this.notesObj,this.token).then((result:any) => {
              this.loading.dismiss();
              if(result.code == 200)
              {
               this.productList[cartVal]['cartList'][indexVal]['cNotes'] =data.add_note;
               this.toastMsg(this.translationLet.product_note_updated);
              }else
              {
              this.toastMsg(this.translationLet.unable_update_product_note);
              }
                  }, (err) => {
                    this.loading.dismiss();
                  });
          }
                  }
      }
      
    ]
  });
  alert.present();
}

//GotoProceed 

gotoProceed(promoCode,discount,sellerUsedCode,sellerId,delivery_cost,cartList,totalCartAmount,free_delivery_amount,sellerPolicies)
{
if(sellerUsedCode == 0)  
{
 promoCode =""; 
 discount =""; 
}
this.navCtrl.push(DeliveryaddressPage,{'sellerPolicies' : JSON.stringify(sellerPolicies),'promoCode' : promoCode,'discount' : discount,'sellerUsedCode' : sellerUsedCode,'totalCartAmount' : totalCartAmount , 'free_delivery_amount' : free_delivery_amount, 'delivery_cost' : delivery_cost , 'sellerId' : sellerId, 'cartList': JSON.stringify(cartList) });
}
//End

//End 
  ionViewDidLoad() {
    console.log('ionViewDidLoad MycartPage');
  }

  gotoSellerProfile(userId)
{
this.app.getRootNav().push(SellerprofilePage,{'userId' : userId});
}



}
