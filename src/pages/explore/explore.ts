import { Component, ViewChild, ViewContainerRef, AfterViewInit } from '@angular/core';
import { Slides,Platform,Segment,normalizeURL,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MyprofilePage } from '../myprofile/myprofile';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { HomePage } from '../home/home';
import { Crop } from '@ionic-native/crop';
import { JoinorderPage } from '../joinorder/joinorder';
import { ProductsearchPage } from '../productsearch/productsearch';
import { MyorderPage } from '../myorder/myorder';


@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html',
})
export class ExplorePage {

  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any;

  translationLet: any = []; 
  loading: Loading; 
  tempProductImg : any = "";
  userImgPath : any ='';
  prodCatImgPath : any ='';
  categoryList : any =[];
  itemList :any =[];
  cat_id : any ='';
  loadmoreData=0;
  tempArr :any =[];
  isActive: boolean = false;
  oldArr :any =[];
   isFetchingData: any =true;
  token :any ='';
  loginUserData : any = {'id' : '',address:'',latitude:'',longitude:''}; 
 
 

  totalCounts : any = [];
  prodImgPath : any ='';

  exploreObj : any ={'user_id' : '','category_id' : '','start' : 0,'limit' : 60 };

    constructor(private crop: Crop,public actionSheetCtrl: ActionSheetController,public platform: Platform,public configProvider : ConfigProvider,private camera: Camera,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

       this.prodCatImgPath = this.configProvider.getProdCatImgPath();
        this.prodImgPath = this.configProvider.getProdImgPath();


        this.translateService.get(['unable_to_crop_image','select_one_payment_method','explore_product','search_by_name','order_not_available','load_more','','cancel','post_new_product','take_photo','upload_from_gallery','something_wrong_txt','unable_to_fetch_gps','please_wait_gps_txt','prod_del_err','prod_del_success','yes_txt','no_txt','delete','delete_product_txt','likes','view_all_comment','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

       this.storage.get('authtoken').then((authtoken) => {
            this.token=authtoken;
        });
      
        this.storage.get('userDetails').then((userDetails) => {
            this.loginUserData=userDetails;
            this.exploreObj.user_id =this.loginUserData.id ;
            this.getExploreList(this.loginUserData.id);
         }); 

  }
  

  //Call Explore list

  getExploreList(user_id)
  {

 //Call service 
//this.showLoading();

this.exploreObj.start=0;
this.exploreObj.limit=60;  

this.serviceProvider.getExploreList(this.exploreObj,this.token).then((result:any) => {
   this.isFetchingData=false;
        //  this.loading.dismiss();
            if(result.code == 200){
              this.categoryList= result.data;

              if(result.itemList.length > 0 )
              {
              this.loadmoreData=1;  
              let chunk_size = 6;
              let index = 0;
              let arrayLength = result.itemList.length;
           
            for (index = 0; index < arrayLength; index += chunk_size) {
              let myChunk = result.itemList.slice(index, index+chunk_size);
              this.totalCounts.push(myChunk);
            }
              }else
              {
                 this.loadmoreData=0;
                 this.totalCounts =[];
              }
          } else
          {
             this.categoryList=[];
             this.totalCounts =[];
              this.loadmoreData=0
          }
          }, (err) => {
         // this.loading.dismiss();
          console.log('err '+err);
          }); 
  }


  //End 

infiniteScrollFun(infiniteScroll,category_id) {
    
    this.exploreObj.category_id =  category_id;
    this.exploreObj.start = parseInt(this.exploreObj.start)+parseInt(this.exploreObj.limit);
    this.exploreObj.limit= parseInt(this.exploreObj.limit)+60;

  setTimeout(() => {

           this.serviceProvider.getOrderCatWise(this.exploreObj,this.token).then((result:any) => {

              if(result.code == 200){
                    //Chunk records 
                    if(result.itemList.length > 0 )
                    {
                    this.tempArr =this.totalCounts.concat(result.itemList);
                    this.loadmoreData=1;
                    let chunk_size = 6;
                    let index = 0;
                    let arrayLength = this.tempArr.length;
                 
                  for (index = 0; index < arrayLength; index += chunk_size) {
                    let myChunk = this.tempArr.slice(index, index+chunk_size);
                   this.totalCounts.push(myChunk);
                  }
                   infiniteScroll.complete();

                    } //End 
         
                } else
                { 
                  this.loadmoreData=0;
                  infiniteScroll.complete();  
                }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 


   //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End


   //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

   //End



  ionViewDidLoad() {
    console.log('ionViewDidLoad ExplorePage');
  }

displayOrderList(exploreData)
{
  this.serviceProvider.getOrderCatWise(exploreData,this.token).then((result:any) => {
                this.totalCounts=[];
                  if(result.code == 200){
                    if(result.itemList.length > 0 )
                    {
                    let chunk_size = 6;
                    let index = 0;
                    let arrayLength = result.itemList.length;
                    
                 
                  for (index = 0; index < arrayLength; index += chunk_size) {
                    let myChunk = result.itemList.slice(index, index+chunk_size);
                  
                    this.totalCounts.push(myChunk);
                  }
                  this.loadmoreData =1;
                    }else
                    {
                      this.loadmoreData =0;
                    }
         
                } else
                {
                   this.totalCounts =[];
                }
                }, (err) => {
             //   this.loading.dismiss();
                }); 
}


  // On segment click
  selectedTab(index,catId) {
    this.slider.slideTo(index);
    this.getCurrentIndexVa(index);
  }

  // On slide changed

  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    let slides_count = this.segments.nativeElement.childElementCount;

    this.page = currentIndex.toString();
    if(this.page >= slides_count)
      this.page = (slides_count-1).toString();

   //Call service to call orders 
     this.exploreObj.start=0;
     this.exploreObj.limit=60; 
     this.exploreObj.category_id = this.categoryList[this.page].id;
     this.displayOrderList(this.exploreObj);
   //End 
    this.centerScroll();
  }

  // Center current scroll
  centerScroll(){
    if(!this.segments || !this.segments.nativeElement)
      return;

    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent/2) ;

    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }

  // Get size start to current
  sizeLeft(){
    let size = 0;
    for(let i = 0; i < this.page; i++){
      size+= this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }

  // Easing function
  easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }

  // Animate scroll
  smoothScrollTo(endX){
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;

    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }




//Common Pages 
   
    gotoHome()
   {
    this.app.getRootNav().setRoot(HomePage); 
   }

    gotoAccount()
    {
      this.app.getRootNav().setRoot(MyprofilePage); 
    }

        gotoUserOrder()
    {
     this.app.getRootNav().setRoot(MyorderPage);
    }

    
    //Go to cover photo

    gotoCoverPhoto()
    {
    
    if( (this.loginUserData.min_order_charge == '' || this.loginUserData.min_order_charge == null ) && (this.loginUserData.max_delivery_range == '' || this.loginUserData.max_delivery_range == null )  && (this.loginUserData.std_delivery_cost == ''  || this.loginUserData.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else if( (this.loginUserData.Paypal_method == 0) && (this.loginUserData.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {

    //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  

  }
}

  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

  //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

CropedImg(imageData)
{
if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg });
}

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
 //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }

  //Goto Join orders 

  gotoJoinOrder(order_id,distance)
  {
    this.navCtrl.push(JoinorderPage,{'order_id' : order_id,'distance' : distance});
  }

  getCurrentIndexVa(index){

    let countofAllElm = this.segments.nativeElement.childElementCount;
    for(let i=0;i<countofAllElm.toString(); i++){
      this.segments.nativeElement.children[i].classList.remove('segment-activated') 
    }
    this.segments.nativeElement.children[index].classList.add('segment-activated');
  }

  gotoSearch()
  {
    this.navCtrl.push(ProductsearchPage);
  }

}
