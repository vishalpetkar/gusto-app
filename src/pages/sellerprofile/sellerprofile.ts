import { Component } from '@angular/core';
import { ViewController,IonicPageModule,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ConfigProvider } from '../../providers/config/config';
import { ServiceProvider } from '../../providers/service/service';
import { ProductdetailsPage } from '../productdetails/productdetails';

@Component({
  selector: 'page-sellerprofile',
  templateUrl: 'sellerprofile.html',
})
export class SellerprofilePage {

	translationLet: any = []; 
    loading: Loading; 
  	userData : any ={'productList' : []} ;
     userReqObj : any ={};
  	token :any ='';
  	imagePath : any ='';
    prodImgPath : any ='';
 constructor(public configProvider :ConfigProvider,public viewCtrl: ViewController,public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   	
	this.imagePath = this.configProvider.getImagePath();
  this.prodImgPath = this.configProvider.getProdImgPath();
	this.translateService.get(['product_not_in_range','total_products','total_ratings','user_profile','service_error','please_enter_valid_phone','user_name_avail','user_name_already_exist','mobile_already_exist','emiil_already_exist','user_name_not_more_than','profile_update_success','profile_update_err','email_already_exist','something_wrong_txt','unAuthReq_msg','err_upload_file','cancel','upload_profile_image','take_photo','upload_from_gallery','please_enter_valid_mobile','please_enter_mobile','email_not_valid','error_txt','please_enter_name','please_enter_email','please_wait_txt','basic_info','ok_txt','edit_profile','update_profile','name','mobile_no','email','seller_profile']).subscribe((translation: [string]) => {
	    this.translationLet = translation;
	});
    
    this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
  	});


	 this.storage.get('userDetails').then((userDetails) => {
	      this.userReqObj.user_id=userDetails.id;
        this.userReqObj.seller_id=this.navParams.get('userId');
      this.getSellerProfile(this.userReqObj);
	  });
  }

  getSellerProfile(user_id)
  {
    this.showLoading();
    this.serviceProvider.getUserProfile(this.userReqObj,this.token).then((result:any) => {
            this.loading.dismiss();
            if(result.code == 200)
            { 
             	this.userData = result.data;
            }
           else
           {
            this.toastMsg(this.translationLet.service_error);
            }
          }, (err) => {
             this.loading.dismiss();
          }); 

  }
  

   //Show loading 

    showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerprofilePage');
  }

    gotoProductDetailPage(isOrderd,product_id,p_user_id){

      if(p_user_id != this.userReqObj.user_id)
      {
        if(isOrderd.isOrderdDistance == true)
        {
        this.navCtrl.push(ProductdetailsPage,{'product_id' : product_id, 'product_distance' : isOrderd.distance });  
      }else{
        this.toastMsg(this.translationLet.product_not_in_range);
      }
     }
  }
  

}
