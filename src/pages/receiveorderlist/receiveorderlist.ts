import { Component,ViewChild,ElementRef, Renderer } from '@angular/core';
import { Platform,Nav,MenuController ,ActionSheetController,normalizeURL,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { ExplorePage } from '../explore/explore';
import { ReceivePastPage } from '../receive-past/receive-past';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import { UpcomingPage } from '../upcoming/upcoming';

@Component({
  selector: 'page-receiveorderlist',
  templateUrl: 'receiveorderlist.html',
})
export class ReceiveorderlistPage {
 
  ReceiveUpcomingPage : any = ReceiveUpcomingPage;
  ReceivePastPage : any = ReceivePastPage;

  profileDetails : any = {
    name : '',
    profile_pic : '',
    mobile_no:'',
    address:'',
    email : '',
    id:'',
    min_order_charge:'',
    order_free_amt:'',
    //order_free_range:'',
    max_delivery_range:'',
    std_delivery_cost:'',
    Paypal_method: '',
    COD:'',
    order_take_away : ''
  };
    myorderCount :any =0;
    receiveOrderCount :any =0;
   token :any ='';
   translationLet: any; 
   loading: Loading;
   tempProductImg : any = "";

constructor(private crop: Crop,public actionSheetCtrl: ActionSheetController,public renderer: Renderer, public platform: Platform,private camera: Camera,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    
     this.translateService.get(['select_one_payment_method','unable_to_crop_image','my_order_history','upcoming','past_orders','cancel','post_new_product','take_photo','upload_from_gallery','min_order_for_home','min_order_for_free_delivery','order_within_meter','max_delivery_radius','cost_std_delivery','payment_change_success','COD','paypal','delivery_policies_success','please_enter_min_order','please_enter_number_only','please_enter_amt_free','please_enter_range_free','please_enter_max_range','please_enter_std_cost','error_txt','delivery_policies_err','my_product_list','view_product_list','not_mention_yet','unAuthReq_msg','something_wrong_txt','please_wait_txt','english_txt','italian_txt','sign_out_success','sign_out_msg','lang_change_success','sign_out','yes_txt','no_txt','user_account','my_order_history','view_order_history','cart','view_cart','notification','view_all_notification','delivery_polices','min_order_delivery','min_order_free_delivery','amount','range_in_meter','max_delivery_range','stadard_delivery_cost','save','payement_method','change_lang','change_pass','logout']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  

      this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 

     //Get details
    this.storage.get('userDetails').then((userDetails) => {
      this.profileDetails=userDetails;
       this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
    }); 
  }

  gotoHome()
  {
   this.app.getRootNav().setRoot(HomePage);
  }

  gotoAccount()
  {
    this.app.getRootNav().setRoot(MyprofilePage); 
  }
 

//Go to cover photo
    gotoCoverPhoto()
    {
       if(this.profileDetails.order_take_away ==1)
      {
  //Actionsheet start
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
    //Actionsheet end
      }else
      {

      if( (this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null ) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null )  && (this.profileDetails.std_delivery_cost == ''  || this.profileDetails.std_delivery_cost == null))
    {
     this.toastMsg(this.translationLet.delivery_policies_err);
    }else if( (this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0))
    {
     this.toastMsg(this.translationLet.select_one_payment_method);
     this.app.getRootNav().setRoot(MyprofilePage); 
    }else
    {
       //Actionsheet 
    
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translationLet.post_new_product,
      buttons: [
        {
          text: this.translationLet.take_photo,
          handler: () => {
          this.takePhoto();                                                                                                       
          }
        },{
          text: this.translationLet.upload_from_gallery,
          handler: () => {
           this.takePhotoGallery();
          }
        },{
          text: this.translationLet.cancel,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();  
  }
}
    } 
   
  takePhoto()
  {
 //End 
 this.tempProductImg='';
    const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

        //Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error =>  this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 
    
  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   //End  

  takePhotoGallery()
  {
     this.tempProductImg='';
     const options: CameraOptions = {
            quality: 90,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true,
            targetWidth: 900,
            targetHeight: 900,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
            encodingType: 0     // 0=JPG 1=PNG
        };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {
    
//Crop Image 
  this.crop.crop(imageData, {quality: 100})
  .then(
    newImage => this.CropedImg(newImage),
    error => this.toastMsg(this.translationLet.unable_to_crop_image)
  );
//End 

  }, (err) => {
   //this.toastMsg("Unable to capture image");
  }); 

  }
   
   //End  

CropedImg(imageData)
{
  if(this.platform.is('ios'))
  {
  this.tempProductImg =normalizeURL(imageData);
  }else
  {
  this.tempProductImg =imageData;
  }
    this.app.getRootNav().setRoot(CoverphotoPage,{'tempProductImg' : this.tempProductImg }); 
        
}

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

      gotMyOrder()
   {
    this.app.getRootNav().setRoot(UpcomingPage); 
   }

  }