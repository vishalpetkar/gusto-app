import { Component,ViewChild } from '@angular/core';
import { Platform,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';

/**
 * Generated class for the SharepeoplelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-sharepeoplelist',
  templateUrl: 'sharepeoplelist.html',
})
export class SharepeoplelistPage{
 
 translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  shareList :any =[];
  shareObj : any = {
    user_id:'',
    order_id : '',
    buyer_id:'',
    start : 0,
    limit : 10
  };
  tempArr :any = [];
  loadmoreData=0;
  isData : any = true;

constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    
    this.shareObj.start= 0;  
    this.shareObj.limit= 10;  

    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

      this.translateService.get(['share_frd_list','pull_to_refresh','no_one_share_order_yet','want_to_share_order','rejected','joined','pending','product_cart_deleted','delete_cart_msg','unable_update_product_note','product_note_updated','pleae_add_note','write_note','add_product_note','unable_update_quantity','product_update_quantity','unable_to_delete_cart','delete','submit','select_quantity','cancel','cart','add_notes','add_promo','proceed','prod_cart_empty','delivery_polices','yes_txt','no_txt','refreshing','please_wait_txt','load_more']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
     //Get details

     this.tempArr= JSON.parse(this.navParams.get("orderDetails"));
    
	    this.storage.get('userDetails').then((userDetails) => {
	        this.shareObj.user_id =userDetails.id; 
          this.shareObj.buyer_id =this.tempArr.buyer_id;
	        this.shareObj.order_id =this.tempArr.id;
	        this.buyerShareOrders();
	     }); 

  }


 /*Ionic refresher */
 doShareUserRefresh(refresher) {
    this.shareObj.start= 0;  
    this.shareObj.limit= 10;  
    this.buyerShareOrdersRefresh(refresher);
  }
 /* End */

 buyerShareOrdersRefresh (refresher)
{
this.shareObj.start=parseInt(this.shareObj.start);
this.shareObj.limit=parseInt(this.shareObj.limit);  
this.serviceProvider.buyershareList(this.shareObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.shareList =result.data;     
            this.loadmoreData=1;
            this.isData =true;
            refresher.complete();

          } else if(result.code == 500) {
             this.shareList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          } else
          {
             this.shareList=[];
             this.isData =false;
             this.loadmoreData=0;
             refresher.complete();
          }
          }, (err) => {
          console.log('err '+err);
          refresher.complete();
          }); 
}

buyerShareOrders()
{
this.shareObj.start=parseInt(this.shareObj.start);
this.shareObj.limit=parseInt(this.shareObj.limit);	
this.serviceProvider.buyershareList(this.shareObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.shareList =result.data;     
            this.loadmoreData=1;
            this.isData =true;

          } else if(result.code == 500) {
             this.shareList=[];
             this.isData =false;
             this.loadmoreData=0;
          } else
          {
             this.shareList=[];
             this.isData =false;
             this.loadmoreData=0;
          }
          }, (err) => {
          console.log('err '+err);
          }); 
}

//infiniteScrollFun

infiniteScrollFun(infiniteScroll) {

    this.shareObj.start = parseInt(this.shareObj.start)+parseInt(this.shareObj.limit);
    this.shareObj.limit= parseInt(this.shareObj.limit)+10;

  setTimeout(() => {

           this.serviceProvider.buyershareList(this.shareObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.shareList = this.shareList.concat(result.data);
            this.loadmoreData=1;
            if(this.shareList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

            infiniteScroll.complete();
          } else if(result.code == 500) {
              if(this.shareList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
            if(this.shareList.length > 0)
              {
               this.isData =true;
              }else{
               this.isData =false;
              }

             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 

//Toast Msg 

	toastMsg (msg){
	 let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 2000,
	      position: 'bottom'
	    });
	    toast.present(toast);
	  }

   //End

     //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

 //Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad SharepeoplelistPage');
  }

}
