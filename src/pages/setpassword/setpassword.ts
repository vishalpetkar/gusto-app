import { Component } from '@angular/core';
import { MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { SelectaddressPage } from '../selectaddress/selectaddress';
import { HomePage } from '../home/home';
import moment from 'moment';

@Component({
  selector: 'page-setpassword',
  templateUrl: 'setpassword.html',
})
export class SetpasswordPage {
  
  translationLet: any;	
  loading: Loading;

    loginData : any = {
	  mobile_no : '',
    name: '',
    password : '',
    created : '',
    device_token : '',
    email :''
    };

  confirmPass : any ='';


  constructor(public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  this.translateService.get(['User_name','user_name_avail','user_name_already_exist','mobile_already_exist','emiil_already_exist','email_not_valid','email_already_exist','please_enter_email','email','user_name_not_more_than','please_enter_user_name','pass_more_six','plese_enter_pass','plese_enter_pass','plese_enter_confirm_pass','pass_mismatch','wrong_pass','sign_in_success','something_wrong_txt','set_password','please_set_pass','number','password','confirm_pass','service_error','please_wait_txt','error_txt','ok_txt','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
  
 this.loginData.mobile_no=navParams.get('phoneNumber'); 

 //Onesignal device_token 
   this.storage.get('player_id').then((player_id) => {
    this.loginData.device_token=player_id;
  }); 
  //End  


  }
  
//Submit and validtions 

submit()
{
 if(this.validate())
  {
  this.showLoading();
  this.loginData.created = moment().format('YYYY-MM-DD HH:mm:ss');

  this.serviceProvider.setPassword(this.loginData).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
      if(result.status == 1)
      {
        this.navCtrl.push(SelectaddressPage,{'user_id' : result.data });
      }
      else{
        if(result.isAddress == true)
        {
          this.toastMsg(this.translationLet.sign_in_success);
          this.storage.set('authtoken', result.token);
          this.storage.set('userDetails',result.userRes);
          this.storage.set('user_id',result.userRes.id);
          this.storage.set('lang_id',result.userRes.language_id);
          this.storage.set('cartCount',result.userRes.cartCount);
          this.app.getRootNav().setRoot(HomePage);
        }
        else{
          this.navCtrl.push(SelectaddressPage,{'user_id' : result.data });
        }
      }
    } else{
      if(result.status == 2)
       this.toastMsg(this.translationLet.something_wrong_txt);
     else if(result.status == 6)
       this.toastMsg(this.translationLet.user_name_already_exist);
     else if(result.status == 7)
       this.toastMsg(this.translationLet.emiil_already_exist);
     else
       this.toastMsg(this.translationLet.wrong_pass);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
  }
}

validate()
{

  let emailValid = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; 
      

  if (this.loginData.name.trim()  == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_user_name);
          return false;
        }
   
     if (this.loginData.name.length > 30) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.user_name_not_more_than);
          return false;
        }
    
     if (this.loginData.email.trim()  == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_email);
          return false;
        }
      
       if (!emailValid.test(this.loginData.email ) && (this.loginData.email.trim() !='')) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.email_not_valid);
          return false;
        }
        
  if (this.loginData.password.trim() == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_pass);
          return false;
        }

 if (this.loginData.password.length < 6 ) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_more_six);
          return false;
        }

if (this.confirmPass.trim()== "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.plese_enter_confirm_pass);
          return false;
        }       

  if (this.loginData.password != this.confirmPass) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.pass_mismatch);
          return false;
        }            

  return true;
}

//End 


   //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

    //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 

  //Check User name unqiueness

  uniqueName(item)
  {
  
  if(item.trim().length > 0)
  {
  this.showLoading();
  this.serviceProvider.uniqueUserName(item.trim()).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
      this.toastMsg(this.translationLet.user_name_already_exist);
    } else{
      this.toastMsg(this.translationLet.user_name_avail);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
 }
 
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SetpasswordPage');
  }

}
