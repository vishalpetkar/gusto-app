import { Component, Renderer } from '@angular/core';
import { ViewController,IonicPageModule,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import moment from 'moment';

@Component({
  selector: 'page-acceptnewtime',
  templateUrl: 'acceptnewtime.html',
})
export class AcceptnewtimePage {

  translationLet: any = []; 
  loading: Loading; 
  token :any ='';
  tempObj :any ={};
  OrderObj : any = {'id' : '' ,'user_id' : '','rescheduled_time' : '','seller_id' : '','buyer_id':'','created' : '','is_free' : '' };
  reschuledDateTime :any ='';
  tempTime :any ='';

  oldDateTime :any =''
  oldDate : any ='' ;
  newDateTime :any =''
  newDate : any ='';

  orderStatusObj : any ={'orderTrackStatus' : [],'status' : 0};
  lang_id: any ='';

   constructor(public renderer: Renderer, public viewCtrl: ViewController,public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'acceptnewtime-popup', true);
     
      this.translateService.get(['done_txt','rescheduled_delivery_greater_than_buyer','rescheduled_delivery_differ_than_buyer','unAuthReq_msg','service_error','please_wait_txt','accept_with_new_time','time','cancel','submit']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 
    
    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id=lang_id;
     });  

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

     this.tempObj= JSON.parse(this.navParams.get('reschuledJSON'));
     this.tempTime =this.tempObj.order_time;
     this.OrderObj.id= this.tempObj.id;
     this.OrderObj.rescheduled_time= this.tempObj.order_time;
     this.OrderObj.order_date= this.tempObj.order_date;
     this.OrderObj.seller_id = this.tempObj.seller_id;
     this.OrderObj.buyer_id = this.tempObj.buyer_id;
     this.OrderObj.created = this.tempObj.created;
     this.OrderObj.is_free = this.tempObj.is_free;
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AcceptnewtimePage');
  }

cancel()
{
this.viewCtrl.dismiss(this.orderStatusObj);
}

submit(){

	if(this.OrderObj.rescheduled_time != this.tempTime)
{
        this.newDateTime = this.OrderObj.order_date + " " + this.OrderObj.rescheduled_time;
        this.newDate =moment(this.newDateTime).format('x');

        this.oldDateTime = this.OrderObj.order_date + " " + this.tempTime;
        this.oldDate =moment(this.oldDateTime).format('x');

         if(this.newDate < this.oldDate )
         {
         this.toastMsg(this.translationLet.rescheduled_delivery_greater_than_buyer);
          return false;
         }else
         {
        
  this.showLoading();
 
  this.reschuledDateTime = this.OrderObj.order_date + " " + this.OrderObj.rescheduled_time;
  this.OrderObj.rescheduled_date_time =moment(this.reschuledDateTime).format('YYYY-MM-DD HH:mm:ss');
  this.OrderObj.rescheduled_date = this.OrderObj.order_date;

  this.serviceProvider.acceptWithNewTime(this.OrderObj,this.token,this.lang_id).then((result:any) => {
            this.loading.dismiss();
            if(result.code == 200)
            { 
              this.orderStatusObj.status = result.status;
              this.orderStatusObj.orderTrackStatus = result.orderTrackStatus;
              this.viewCtrl.dismiss(this.orderStatusObj);
            }
           else
           {
            this.viewCtrl.dismiss(this.orderStatusObj);
            this.toastMsg(this.translationLet.service_error);
            }
          }, (err) => {
             this.loading.dismiss();
          }); 

}

}else{
  this.toastMsg(this.translationLet.rescheduled_delivery_differ_than_buyer);
}

  }


  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //Toast Msg 

  toastMsg (msg){
   let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom'
      });
      toast.present(toast);
    }

}
