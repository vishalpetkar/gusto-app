import { Component,ViewChild,ElementRef, Renderer } from '@angular/core';
import { ViewController,Platform,Nav,MenuController ,ModalController,ActionSheetController,normalizeURL,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import moment from 'moment';
import { UpdateOrderPage } from '../update-order/update-order';

@Component({
  selector: 'page-action-popover',
  templateUrl: 'action-popover.html',
})

export class ActionPopoverPage {
  tempObj : any ={'id' : '','seller_id':'','buyer_id':'','role':'','user_id' : '','record_id' : '','notes' : '','quantity' : ''};
  token :any ='';
  translationLet: any;  
  loading: Loading;
  dismissVal :any = {'status' : '','tempArr' : [],'action' :''};

  constructor(public viewCtrl: ViewController,public modalCtrl: ModalController,public actionSheetCtrl: ActionSheetController,public renderer: Renderer, public platform: Platform,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.tempObj = this.navParams.get("actionJSON");
      this.translateService.get(['prod_del_err','only_one_product_in_order','order_deleted_success','do_you_want_to_delete_order','cancel','delete','edit','order_time_elapsed_can_not_share_order','order_time_elapsed_can_not_send_order','unable_to_crop_image','yes_txt','no_txt','reject_received_order_from_your_side','do_you_want_reject_order','reject_order','send_order','sender_order_to_seller','order_send_to_seller_successfully','select_one_payment_method','accept_received_order','do_you_want_to_accept_order','order_received_sucessfully','do_you_want_to_accept_rescheduled_time','order_confrim_from_your_side','reject_received_order','do_you_want_to_reject_received_order','order_time_elapsed','service_error','mobile_no','refreshing','time','order_details','pull_to_refresh','delivery_cost_txt','free_delivery_txt','total_amt','order_delivery_on','buyer_product_notes','not_added_txt','order_receiver_details','name','surname','company','seller_info','share_order_with_frd','order_share_with_following_frd','view_all','order_summary','send_order_to_seller','order_not_send_due_to_min_policies','order_not_accept_yet','order_rescheduled_by_seller','seller_suggest_new_time','accept','reject','received_order_from_seller','accepted','rejected','order_status','please_wait_txt','cancel','post_new_product','take_photo','upload_from_gallery','save','payement_method']).subscribe((translation: [string]) => {
        this.translationLet = translation;
        });  

    this.storage.get('authtoken').then((authtoken) => {
      this.token=authtoken;
     }); 
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActionPopoverPage');
  }

  close()
  {
     this.dismissVal.tempArr =[];
     this.dismissVal.status =0;
     this.dismissVal.action =0;
     this.viewCtrl.dismiss(this.dismissVal);
  	
  }
 
 //editOrder

 editOrder()
 {  
    let modalOrder = this.modalCtrl.create(UpdateOrderPage,{'orderDetails' : this.tempObj});
    modalOrder.present();
    modalOrder.onDidDismiss(data => {
    this.viewCtrl.dismiss(data);
    //End 
    });
 }

 //Delete order

 deleteOrder()
 {
  let alert = this.alertCtrl.create({
    title: this.translationLet.delete,
    message: this.translationLet.do_you_want_to_delete_order,
    buttons: [
      {
        text: this.translationLet.yes_txt,
        handler: () => {
      //  Delete  
    this.showLoading();
    this.serviceProvider.delOrders(this.tempObj,this.token).then((result:any) => {
    this.loading.dismiss();
    if(result.code == 200)
    {
     this.toastMsg(this.translationLet.order_deleted_success);
     this.dismissVal.tempArr =[];
     this.dismissVal.status =1;
     this.dismissVal.action =0;

     this.viewCtrl.dismiss(this.dismissVal);

    }else if(result.code==400)
    {
     this.dismissVal.tempArr =[];
     this.dismissVal.status =0;
     this.dismissVal.action =0;
     this.viewCtrl.dismiss(this.dismissVal);
    this.toastMsg(this.translationLet.only_one_product_in_order);
    }
    else
    {
     this.dismissVal.tempArr =[];
     this.dismissVal.status =0;
     this.dismissVal.action =0;
     this.viewCtrl.dismiss(this.dismissVal);
    this.toastMsg(this.translationLet.prod_del_err);
    }
        }, (err) => {
          this.loading.dismiss();
        });
          }
        //End
      },{
        text: this.translationLet.no_txt,
        handler: () => {
        this.viewCtrl.dismiss();
        }
      }
    ]
  });
  alert.present();  
 }

  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

//Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }


}
