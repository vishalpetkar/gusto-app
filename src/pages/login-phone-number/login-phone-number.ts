import { Component } from '@angular/core';
import { MenuController ,IonicPage,App, ModalController,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { CountryCodePage } from '../country-code/country-code';
import { VarifyPage } from '../varify/varify';
import { SetpasswordPage } from '../setpassword/setpassword';

@Component({
  selector: 'page-login-phone-number',
  templateUrl: 'login-phone-number.html',
})
export class LoginPhoneNumberPage {
 

loginData : any = {
countryCode : '+39',
phoneNumber : '',
tempNumber : '',
};

countryList :any =[];

  translationLet: any;	
  defaultFlag : any ='';
  loading: Loading;

  constructor(public modalCtrl: ModalController,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
   this.defaultFlag = 'assets/images/country-icon1.png'; 

    this.translateService.get(['otp_send_msg','service_error','please_enter_valid_phone','please_wait_txt','error_txt','enter_phone_no_val','ok_txt','sign_up_number','enter_phone_txt','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
        this.translationLet = translation;
    });
   this.initCountryList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPhoneNumberPage');
  }

  //Get Country List 
 initCountryList()
 {

 	this.serviceProvider.getCountryList().then((result:any) => {
    if(result.code == 200){
      this.countryList =result.data;
    } else if(result.code == 500) {
      //this.showPopup(this.translationLet.error,result.message);
    } else if(result.code == 404) {
      //this.showPopup(this.translationLet.error,result.message);
    } 
    
    }, (err) => {
    });
 }
  
  //End

//Country model 

 openCountryCodeModal()
 {

 let modalCountry = this.modalCtrl.create(CountryCodePage, {'countryListObj':this.countryList});

  modalCountry.onDidDismiss(data => {
    console.log(data.country_icon);
     if(data.country_code != ''){
      this.loginData.countryCode =data.country_code;
      this.defaultFlag =data.country_icon;
     }
   });

    modalCountry.present();
 }
  

//End 

//Submit 

submit()
{
  if(this.validate())
  {
  //Call service and generate OTP 
  this.showLoading();

  this.loginData.phoneNumber =  this.loginData.countryCode+this.loginData.tempNumber;
  this.serviceProvider.varifyPhoneNumber(this.loginData).then((result:any) => {
     this.loading.dismiss();
    if(result.code == 200){
     if(result.status == 'New')
     {
     //Varify page 
     this.toastMsg(this.translationLet.otp_send_msg);
     this.navCtrl.push(VarifyPage,{'phoneNumber' : this.loginData.phoneNumber, 'otp' : result.otp });
     }else
     {
    //Set password page   
     this.navCtrl.push(SetpasswordPage,{"phoneNumber" : this.loginData.phoneNumber});
     }
    }else if(result.code == 201){
      this.toastMsg(result.error);
    }else{
     this.toastMsg(this.translationLet.service_error);
    } 
    }, (err) => {
       this.loading.dismiss();
    });
  
    //End  
  }
}

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 4000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

  //Show loading 

   showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
  //End

validate()
{
  if (this.loginData.tempNumber == "") {
          this.showPopup(this.translationLet.error_txt,this.translationLet.enter_phone_no_val);
          return false;
        }
  if (this.loginData.tempNumber.length != 10) {
          this.showPopup(this.translationLet.error_txt,this.translationLet.please_enter_valid_phone);
          return false;
        }
  return true;
}

  //Alert Popups 
    showPopup(title,text) {
    let alert = this.alertCtrl.create({
      title: title,
      message: text,
      buttons:  [
      { text: this.translationLet.ok_txt,
        cssClass:'button popup-btn'}]
    });
    alert.present();
  }
   //End 



}
