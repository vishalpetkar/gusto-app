import { Component,ViewChild } from '@angular/core';
import { Platform,Content,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import moment from 'moment';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';


@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})

export class CommentsPage {
   @ViewChild(Content) contentArea: Content;
  translationLet: any = [];	
  loading: Loading;	

  commentObj : any = {
    user_id:'',
    product_id : '',
    start : 0,
    limit : 10
  };
  
   userImgPath : any ='';
   prodImgPath : any ='';

  token :any ='';
  newDate :any ='';
  commentList : any =[] ;
  loadmoreData=0;
  isData : any = true ;

  tempUserImg : any =''; 
  tempUserName : any ='';

  //Add commentes 

  addComment :  any = {
	  product_id : '',
	  comment : '',
	  user_id : '',
	  created : ''
	  };
  
   userImg  : any ='';

   tempDate : any ='';
   all_months : any =[];

  constructor(public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
	    this.userImgPath = this.configProvider.getImagePath();
		  this.prodImgPath = this.configProvider.getProdImgPath();

		  this.commentObj.product_id =navParams.get('product_id');
		  this.addComment.product_id =navParams.get('product_id');

		   this.storage.get('authtoken').then((authtoken) => {
		      this.token=authtoken;
		  });

			 this.storage.get('lang_id').then((lang_id) => {
			      if(lang_id ==1)
				this.all_months=[{'text' : 'Jan', 'value' : 'Jan'},{'text' : 'Feb', 'value':'Feb'},{'text' : 'Mar', 'value':'March'},{'text' : 'Apr', 'value':'April'},{'text' : 'May', 'value':'May'},{'text' : 'Jun', 'value':'June'},{'text' : 'Jul', 'value':'July'},{'text' : 'Aug', 'value':'Aug'},{'text' : 'Sep', 'value':'Sep'},{'text' : 'Oct', 'value':'Oct'},{'text' : 'Nov', 'value':'Nov'},{'text' : 'Dec', 'value':'Dec'}];
			     else
				this.all_months=[{'text' : 'Jan', 'value' : 'Gennaio'},{'text' : 'Feb', 'value' : 'Febbraio'},{'text' : 'Mar', 'value' : 'Marzo'},{'text' : 'Apr', 'value' : 'Aprile'},{'text' : 'May', 'value' : 'Maggio'},{'text' : 'Jun', 'value' : 'Giugno'},{'text' : 'Jul', 'value' : 'Luglio'},{'text' : 'Aug', 'value' : 'Agosto'},{'text' : 'Sep', 'value' : 'Settembre'},{'text' : 'Oct', 'value' : 'Ottobre'},{'text' : 'Nov', 'value' : 'Novembre'},{'text' : 'Dec', 'value' : 'Dicembre'}];
		});

		 this.translateService.get(['please_enter_comment','comments','comment_not_avail','load_more','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
		        this.translationLet = translation;
		      }); 

		   //Get details
		  this.storage.get('userDetails').then((userDetails) => {
		  	this.tempUserName= userDetails.name;
		  	if(userDetails['profile_pic'] != '' && userDetails['profile_pic'] != null)
		      {
		       this.tempUserImg =this.userImgPath+userDetails['profile_pic']; 
		       this.userImg =userDetails['profile_pic'];
		      }else
		      {
		        this.tempUserImg = "assets/images/user-profile-default-img.jpg"; 
		        this.userImg ="";
		      }

		      this.commentObj.user_id=userDetails.id;
		      this.addComment.user_id =userDetails.id;
		      this.initCommentList();
		   }); 
	    
	    //End  

  }
  

  //Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}


  initCommentList()
  {
	this.commentObj.start=parseInt(this.commentObj.start);
	this.commentObj.limit=parseInt(this.commentObj.limit);	
	this.serviceProvider.getProdComments(this.commentObj,this.token).then((result:any) => {
		            if(result.code == 200){
	            this.commentList =result.data;     
	            this.loadmoreData=1;
	            this.isData =true;
	            setTimeout(() => { this.contentArea.scrollToBottom();	
	            	  }, 500);
	          } else if(result.code == 500) {
	             this.commentList=[];
	             this.isData =false;
	             this.loadmoreData=0;
	          } else
	          {
	             this.commentList=[];
	             this.isData =false;
	             this.loadmoreData=0;
	          }
	          }, (err) => {
	          console.log('err '+err);
	          }); 	
  }

  
  //End 

infiniteScrollFun(infiniteScroll) {

    this.commentObj.start = parseInt(this.commentObj.start)+parseInt(this.commentObj.limit);
    this.commentObj.limit= parseInt(this.commentObj.limit)+10;

  setTimeout(() => {

           this.serviceProvider.getProdComments(this.commentObj,this.token).then((result:any) => {
            if(result.code == 200){
            this.commentList = this.commentList.concat(result.data);
            this.loadmoreData=1;
            infiniteScroll.complete();
          } else if(result.code == 500) {
             this.loadmoreData=0;
              infiniteScroll.complete();
          } else
          {
             this.loadmoreData=0;
             infiniteScroll.complete();
          }
          }, (err) => {
          console.log('err '+err);
          }); 

   }, 500);

}
//End 


  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }


//Add comments 
 send ()
 {
 
 if(this.addComment.comment.trim() == '' )
 {
  this.toastMsg(this.translationLet.please_enter_comment);
 }else
 { 
 	this.addComment.created = moment().format('YYYY-MM-DD HH:mm:ss');
 	this.tempDate = moment().format('DD MMM YYYY h:mm a');
   //Save to DB 
	this.serviceProvider.addProdComments(this.addComment,this.token).then((result:any) => {
	            if(result.code == 200){
	            this.isData = true 	;
		        this.commentList.push({
		        comment_id:this.addComment.comment_id,
	            user_id: this.addComment.user_id,
	            comment: this.addComment.comment,
	            created:this.matchingPair(this.tempDate),
	            name : this.tempUserName,
	            profile_pic:this.userImg
	            
	          });
		       this.addComment.comment=''; 
               setTimeout(() => { this.contentArea.scrollToBottom();	
	            	  }, 500);     
		      
            
	          } else if(result.code == 500) {
	             this.toastMsg(this.translationLet.service_error);
	          } else
	          {
	            this.toastMsg(this.translationLet.unAuthReq_msg);
	          }
	          }, (err) => {
	          console.log('err '+err);
	          }); 	

  //End 

 }

 }

matchingPair(currentDate)
{
 for(let i=0;i<this.all_months.length;i++)
{
	if(currentDate.indexOf(this.all_months[i]['text']) != -1)
	{
		this.newDate= currentDate.replace(this.all_months[i]['text'],this.all_months[i]['value']);
	}
}
return this.newDate;

}

 //Toast Msg 

toastMsg (msg){
 let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.present(toast);
  }

//End

 //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

}
