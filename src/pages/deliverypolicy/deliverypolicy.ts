import { Component,ViewChild } from '@angular/core';
import { Platform,Slides,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-deliverypolicy',
  templateUrl: 'deliverypolicy.html',
})
export class DeliverypolicyPage {

deliveryPolicies : any =[];
translationLet: any = [];	

   constructor(public platform: Platform,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  
       this.translateService.get(['delivery_polices','min_order_for_free_delivery','max_delivery_radius','cost_std_delivery','min_order','min_order_del','free_policy','free_range','free_amt','max_del_range','std_charges']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

       this.deliveryPolicies=JSON.parse(navParams.get('userPolicies'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliverypolicyPage');
  }

}
