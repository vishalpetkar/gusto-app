import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-disclaimer',
  templateUrl: 'disclaimer.html',
})
export class DisclaimerPage {
 translationLet: any;	
  constructor(private translateService: TranslateService,public navCtrl: NavController, public navParams: NavParams) {
       this.translateService.get(['disclaimer']).subscribe((translation: [string]) => {
        this.translationLet = translation;
      });  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisclaimerPage');
  }

}
