import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { Platform, Nav, MenuController, ModalController, ActionSheetController, normalizeURL, IonicPage, App, NavController, NavParams, ToastController, AlertController, Events, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { SellerproductsPage } from '../sellerproducts/sellerproducts';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CoverphotoPage } from '../coverphoto/coverphoto';
import { Crop } from '@ionic-native/crop';
import { MyprofilePage } from '../myprofile/myprofile';
import { HomePage } from '../home/home';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
import moment from 'moment';
import { ExplorePage } from '../explore/explore';
import { ShareproductPage } from '../shareproduct/shareproduct';
import { SharepeoplelistPage } from '../sharepeoplelist/sharepeoplelist';
import { PaymentTransactionPage } from '../payment-transaction/payment-transaction';
import { UserRatingPage } from '../user-rating/user-rating';
import { ReceiveUpcomingPage } from '../receive-upcoming/receive-upcoming';
import { PopoverController } from 'ionic-angular';
import { ActionPopoverPage } from '../action-popover/action-popover';
import { UpdateReceiverPage } from '../update-receiver/update-receiver';
import { UpdatedeliverytimePage } from '../updatedeliverytime/updatedeliverytime';
import { UpcomingPage } from '../upcoming/upcoming';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
  selector: 'page-buyerorderdetails',
  templateUrl: 'buyerorderdetails.html',
})
export class BuyerorderdetailsPage {

  token: any = '';
  translationLet: any;
  loading: Loading;

  profileDetails: any = {
    name: '',
    profile_pic: '',
    mobile_no: '',
    address: '',
    email: '',
    id: '',
    min_order_charge: '',
    order_free_amt: '',
    // order_free_range:'',
    max_delivery_range: '',
    std_delivery_cost: '',
    Paypal_method: '',
    COD: '',
    order_take_away: ''
  };

  buyerpaidAmt: any = '';
  lang_id: any = '';
  all_months: any = [];
  newDate: any = '';
  tXDate: any = '';
  updateData: any = { 'order_id': '', 'user_id': '', 'seller_id': '', 'created': '', 'paypal_tx_id': '' };
  updateDataJoin: any = { 'order_id': '', 'user_id': '', 'seller_id': '', 'created': '', 'order_item_id': '', 'paypal_tx_id': '' };
  prodImgPath: any = '';
  userImgPath: any = '';
  isFetchingData: any = true;
  buyerOrderObj: any = { 'buyer_id': '', 'seller_id': '', 'id': '', 'created': '', 'is_free': '' };
  orderDetails: any = { 'orderTrack': [], 'productList': [] };
  delOrderObj: any = { 'order_id': '', 'buyer_id': '' };
  updateTimeObj: any = { 'id': '', 'user_id': '', 'delivery_time': '', 'delivery_date': '' };

  tempProductImg: any = "";
  myorderCount: any = 0;
  receiveOrderCount: any = 0;
  dateTimeOld: any = '';
  dateTimeNew: any = '';
  actionObj: any = { 'id': '', 'seller_id': '', 'buyer_id': '', 'role': '', 'user_id': '', 'record_id': '', 'notes': '', 'quantity': '' };
  paypalLang: any = '';

  constructor(private payPal: PayPal, public popoverCtrl: PopoverController, public modalCtrl: ModalController, private crop: Crop, public actionSheetCtrl: ActionSheetController, public renderer: Renderer, public platform: Platform, private camera: Camera, public configProvider: ConfigProvider, public serviceProvider: ServiceProvider, private menu: MenuController, public app: App, private translateService: TranslateService, public events: Events, public navCtrl: NavController, private storage: Storage, public toastCtrl: ToastController, public navParams: NavParams, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }

  // Popover 
  presentPopover(myEvent, buyer_id, role, record_id, indexVal, notes, quantity) {
    //Init variables 

    this.actionObj.buyer_id = buyer_id;
    this.actionObj.role = role;
    this.actionObj.record_id = record_id;
    this.actionObj.notes = notes;
    this.actionObj.quantity = quantity;

    let popover = this.popoverCtrl.create(ActionPopoverPage, { 'actionJSON': this.actionObj });
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(data => {
      if (data.action == 0 && data.status == 1) {
        //Delete updated 
        if (this.actionObj.role == 1)
          this.buyerOrderInit(this.buyerOrderObj);
        else
          this.app.getRootNav().setRoot(UpcomingPage);
      } else if (data.action == 1 && data.status == 1) {
        //Edit updated 
        /*this.orderDetails.productList[indexVal]['notes'] = data.tempArr['notes'];
        this.orderDetails.productList[indexVal]['quantity'] = data.tempArr['quantity'];
       this.orderDetails.totalAmount = this.getCartAmountCount(this.orderDetails.productList);
       this.orderDetails.totalAmount = parseFloat(this.orderDetails.totalAmount)+parseFloat(this.orderDetails.delivery_cost); */
        this.buyerOrderInit(this.buyerOrderObj);
      }
    })

  }

  getCartAmountCount(cartListObj) {
    let tempObj = 0;
    for (let i = 0; i < cartListObj.length; i++) {
      if (cartListObj[i]['isPromocodeused'] == 1)

        tempObj += (cartListObj[i]['item_price'] - (cartListObj[i]['item_price'] * (cartListObj[i]['discount_percentage'] / 100))) * cartListObj[i]['quantity'];
      else
        tempObj += cartListObj[i]['quantity'] * cartListObj[i]['item_price'];
    }
    return tempObj;
  }


  /*Ionic refresher */
  doBuyerOrderRefresh(refresher) {
    this.buyerOrderInitRefresh(this.buyerOrderObj, refresher);
  }


  buyerOrderInitRefresh(buyerDetails, refresher) {

    //this.showLoading();

    this.serviceProvider.getBuyerDetails(buyerDetails, this.token, this.lang_id).then((result: any) => {
      //        this.loading.dismiss();
      if (result.code == 200) {
        this.orderDetails = result.data;
        if (this.orderDetails.productList[0].asBuyer == 1) {
          this.buyerpaidAmt = this.orderDetails.productList[0].final_amount_paypal;
        }
        //this.userList = result.data.userList;
        refresher.complete();
      } else {
        this.orderDetails = [];
        refresher.complete();
        this.toastMsg(this.translationLet.service_error);
      }
    }, (err) => {
      //      this.loading.dismiss();
      refresher.complete();
      console.log('err ' + err);
    });
  }


  /* End */


  ionViewDidEnter() {
    this.prodImgPath = this.configProvider.getProdImgPath();
    this.userImgPath = this.configProvider.getImagePath();
    this.translateService.get(['seller_suggest_new_pick_up_time', 'seller_address', 'delivery', 'take_away', 'you_got_txt', 'off_to_all_products', 'call_now', 'minimum_amount_reached_txt', 'minimum_shared_order_txt', 'payment_failed_error', 'record_updated', 'pay_via', 'pay_via_paypal', 'order_reject', 'coupen_applied', 'buyer', 'joined_buyer', 'un_paid', 'paid', 'something_wrong_txt', 'order_deleted_success', 'delete', 'do_you_want_to_delete_order', 'order_time_updated_successfully', 'order_time_elapsed_can_not_share_order', 'order_time_elapsed_can_not_send_order', 'unable_to_crop_image', 'yes_txt', 'no_txt', 'reject_received_order_from_your_side', 'do_you_want_reject_order', 'reject_order', 'send_order', 'sender_order_to_seller', 'order_send_to_seller_successfully', 'select_one_payment_method', 'accept_received_order', 'do_you_want_to_accept_order', 'order_received_sucessfully', 'do_you_want_to_accept_rescheduled_time', 'order_confrim_from_your_side', 'reject_received_order', 'do_you_want_to_reject_received_order', 'order_time_elapsed', 'service_error', 'mobile_no', 'refreshing', 'time', 'order_details', 'pull_to_refresh', 'delivery_cost_txt', 'free_delivery_txt', 'total_amt', 'order_delivery_on', 'buyer_product_notes', 'not_added_txt', 'order_receiver_details', 'name', 'surname', 'company', 'seller_info', 'share_order_with_frd', 'order_share_with_following_frd', 'view_all', 'order_summary', 'send_order_to_seller', 'order_not_send_due_to_min_policies', 'order_not_accept_yet', 'order_rescheduled_by_seller', 'seller_suggest_new_time', 'accept', 'reject', 'received_order_from_seller', 'accepted', 'rejected', 'order_status', 'please_wait_txt', 'cancel', 'post_new_product', 'take_photo', 'upload_from_gallery', 'save', 'payement_method']).subscribe((translation: [string]) => {
      this.translationLet = translation;
    });

    this.storage.get('lang_id').then((lang_id) => {
      this.lang_id = lang_id;

      if (this.lang_id == 1) {
        this.paypalLang = 'en_US';
        this.all_months = [{ 'text': 'Jan', 'value': 'Jan' }, { 'text': 'Feb', 'value': 'Feb' }, { 'text': 'Mar', 'value': 'March' }, { 'text': 'Apr', 'value': 'April' }, { 'text': 'May', 'value': 'May' }, { 'text': 'Jun', 'value': 'June' }, { 'text': 'Jul', 'value': 'July' }, { 'text': 'Aug', 'value': 'Aug' }, { 'text': 'Sep', 'value': 'Sep' }, { 'text': 'Oct', 'value': 'Oct' }, { 'text': 'Nov', 'value': 'Nov' }, { 'text': 'Dec', 'value': 'Dec' }];
      }
      else {
        this.paypalLang = 'it_IT';
        this.all_months = [{ 'text': 'Gennaio', 'value': 'Jan' }, { 'text': 'Febbraio', 'value': 'Feb' }, { 'text': 'Marzo', 'value': 'Mar' }, { 'text': 'Aprile', 'value': 'Apr' }, { 'text': 'Maggio', 'value': 'May' }, { 'text': 'Giugno', 'value': 'Jun' }, { 'text': 'Luglio', 'value': 'Jul' }, { 'text': 'Agosto', 'value': 'Aug' }, { 'text': 'Settembre', 'value': 'Sep' }, { 'text': 'Ottobre', 'value': 'Oct' }, { 'text': 'Novembre', 'value': 'Nov' }, { 'text': 'Dicembre', 'value': 'Dec' },
        { 'text': 'Jan', 'value': 'Gennaio' }, { 'text': 'Feb', 'value': 'Febbraio' }, { 'text': 'Mar', 'value': 'Marzo' }, { 'text': 'Apr', 'value': 'Aprile' }, { 'text': 'May', 'value': 'Maggio' }, { 'text': 'Jun', 'value': 'Giugno' }, { 'text': 'Jul', 'value': 'Luglio' }, { 'text': 'Aug', 'value': 'Agosto' }, { 'text': 'Sep', 'value': 'Settembre' }, { 'text': 'Oct', 'value': 'Ottobre' }, { 'text': 'Nov', 'value': 'Novembre' }, { 'text': 'Dec', 'value': 'Dicembre' }];
      }
    });

    this.storage.get('authtoken').then((authtoken) => {
      this.token = authtoken;
    });

    //Get details
    this.storage.get('userDetails').then((userDetails) => {
      this.profileDetails = userDetails;
      this.myorderCount = userDetails.myorderCount;
      this.receiveOrderCount = userDetails.receiveOrderCount;
      this.buyerOrderObj.id = this.navParams.get('order_id');
      this.buyerOrderObj.buyer_id = userDetails.id;
      this.delOrderObj.buyer_id = userDetails.id;
      this.actionObj.id = this.navParams.get('order_id');
      this.actionObj.seller_id = this.navParams.get('seller_id');
      this.actionObj.user_id = userDetails.id;
      this.delOrderObj.order_id = this.navParams.get('order_id');
      this.buyerOrderObj.seller_id = this.navParams.get('seller_id');
      //Get Buyer order details
      this.buyerOrderInit(this.buyerOrderObj);
      //End 
    });
  }


  //Goto User Profile section 

  gotoSellerProfile(userId) {
    this.navCtrl.push(SellerprofilePage, { 'userId': userId });
  }

  //Buyer order details section 

  buyerOrderInit(buyerDetails) {

    //this.showLoading();

    this.serviceProvider.getBuyerDetails(buyerDetails, this.token, this.lang_id).then((result: any) => {
      this.isFetchingData = false;
      //        this.loading.dismiss();
      if (result.code == 200) {
        this.orderDetails = result.data;
        if (this.orderDetails.productList[0].asBuyer == 1) {
          this.buyerpaidAmt = this.orderDetails.productList[0].final_amount_paypal;
        }
        /*console.log(this.buyerpaidAmt);
        this.userList = result.data.userList; */
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
      } else {
        this.orderDetails = [];
        this.myorderCount = result.userDetails.myorderCount;
        this.receiveOrderCount = result.userDetails.receiveOrderCount;
        this.storage.set('userDetails', result.userDetails);
        this.toastMsg(this.translationLet.service_error);
      }
    }, (err) => {
      //      this.loading.dismiss();
      console.log('err ' + err);
    });
  }

  //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">' + this.translationLet.please_wait_txt + '</div></div></div>'
    });
    this.loading.present();
  }

  //End


  //End 

  translateDateFun(mixDate) {
    for (let i = 0; i < this.all_months.length; i++) {
      if (mixDate.indexOf(this.all_months[i]['text']) != -1) {
        this.newDate = mixDate.replace(this.all_months[i]['text'], this.all_months[i]['value']);
      }
    }
    return this.newDate;

  }

  //Send order to Seller 

  sendOrder() {
    //Check time elapsed 
    this.tXDate = this.translateDateFun(this.orderDetails.deliveryTime);
    this.dateTimeOld = moment(this.tXDate).format('x');
    this.dateTimeNew = moment().format('x');

    if (this.dateTimeOld < this.dateTimeNew) {
      this.toastMsg(this.translationLet.order_time_elapsed_can_not_send_order);
    } else {
      let alertSendOrder = this.alertCtrl.create({
        title: this.translationLet.send_order,
        message: this.translationLet.sender_order_to_seller,
        buttons: [
          {
            text: this.translationLet.no_txt,
            cssClass: 'btn-primary btn-round',
            handler: () => {
            }
          },
          {
            text: this.translationLet.yes_txt,
            cssClass: 'btn-primary btn-round',
            handler: () => {

              this.showLoading();
              this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
              this.serviceProvider.sendOrderToSeller(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
                this.loading.dismiss();
                if (result.code == 200) {
                  this.orderDetails.buyer_order_status = result.buyer_order_status;
                  this.toastMsg(this.translationLet.order_send_to_seller_successfully);
                } else {
                  this.toastMsg(this.translationLet.service_error);
                }
              }, (err) => {
                this.loading.dismiss();
              });


            }
          }

        ]
      });
      alertSendOrder.present();
    }
  }

  //End 

  gotoExplore() {
    this.app.getRootNav().setRoot(ExplorePage);
  }

  gotoHome() {

    this.app.getRootNav().setRoot(HomePage);
  }

  gotoAccount() {
    this.app.getRootNav().setRoot(MyprofilePage);
  }




  //Go to cover photo
  gotoCoverPhoto() {

    if (this.profileDetails.order_take_away == 1) {
      //Actionsheet start
      let actionSheet = this.actionSheetCtrl.create({
        title: this.translationLet.post_new_product,
        buttons: [
          {
            text: this.translationLet.take_photo,
            handler: () => {
              this.takePhoto();
            }
          }, {
            text: this.translationLet.upload_from_gallery,
            handler: () => {
              this.takePhotoGallery();
            }
          }, {
            text: this.translationLet.cancel,
            handler: () => {
            }
          }
        ]
      });
      actionSheet.present();
      //Actionsheet end
    } else {
      if ((this.profileDetails.min_order_charge == '' || this.profileDetails.min_order_charge == null) && (this.profileDetails.max_delivery_range == '' || this.profileDetails.max_delivery_range == null) && (this.profileDetails.std_delivery_cost == '' || this.profileDetails.std_delivery_cost == null)) {
        this.toastMsg(this.translationLet.delivery_policies_err);
      } else if ((this.profileDetails.Paypal_method == 0) && (this.profileDetails.COD == 0)) {
        this.toastMsg(this.translationLet.select_one_payment_method);
        this.app.getRootNav().setRoot(MyprofilePage);
      } else {
        //Actionsheet 

        let actionSheet = this.actionSheetCtrl.create({
          title: this.translationLet.post_new_product,
          buttons: [
            {
              text: this.translationLet.take_photo,
              handler: () => {
                this.takePhoto();
              }
            }, {
              text: this.translationLet.upload_from_gallery,
              handler: () => {
                this.takePhotoGallery();
              }
            }, {
              text: this.translationLet.cancel,
              handler: () => {
              }
            }
          ]
        });
        actionSheet.present();
      }
    }
  }

  takePhoto() {
    //End 
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };
    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }
  //End  

  takePhotoGallery() {
    this.tempProductImg = '';
    const options: CameraOptions = {
      quality: 90,
      saveToPhotoAlbum: true,
      allowEdit: false,
      correctOrientation: true,
      targetWidth: 900,
      targetHeight: 900,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: 0, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
      encodingType: 0     // 0=JPG 1=PNG
    };

    //Get picture function 
    this.camera.getPicture(options).then((imageData) => {

      //Crop Image 
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => this.CropedImg(newImage),
          error => this.toastMsg(this.translationLet.unable_to_crop_image)
        );
      //End 

    }, (err) => {
      //this.toastMsg("Unable to capture image");
    });

  }

  //End  

  CropedImg(imageData) {
    if (this.platform.is('ios')) {
      this.tempProductImg = normalizeURL(imageData);
    } else {
      this.tempProductImg = imageData;
    }
    this.app.getRootNav().setRoot(CoverphotoPage, { 'tempProductImg': this.tempProductImg });

  }

  //Toast Msg 

  toastMsg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present(toast);
  }

  //End 

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyerorderdetailsPage');
  }


  //Accept Deliver order 

  acceptDeliverOrder() {

    //Confirmation 
    let alertAcceptDeliver = this.alertCtrl.create({
      title: this.translationLet.accept_received_order,
      message: this.translationLet.do_you_want_to_accept_order,
      buttons: [
        {
          text: this.translationLet.no_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {
          }
        },
        {
          text: this.translationLet.yes_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {

            //Call service and accept order 
            this.showLoading();
            this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
            this.serviceProvider.acceptDeliverOrderBuyer(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.toastMsg(this.translationLet.order_received_sucessfully);
                this.orderDetails.order_delivery_Status = result.status;
                this.orderDetails.orderTrack = result.orderTrackStatus;
                this.storage.set('userDetails', result.userDetails);
                // User Rate
                let modalRate = this.modalCtrl.create(UserRatingPage, { 'user_id': this.buyerOrderObj.seller_id, 'product_id': this.orderDetails.prodId, 'id': this.buyerOrderObj.buyer_id });
                modalRate.present();
                modalRate.onDidDismiss(data => {
                  //Set to order page 
                  this.storage.get('userDetails').then((userDetails) => {
                    this.myorderCount = userDetails.myorderCount;
                    this.app.getRootNav().setRoot(UpcomingPage);
                  });
                  //End 
                });
                //End 
              } else {
                this.toastMsg(this.translationLet.service_error);
              }
            }, (err) => {
              this.loading.dismiss();
              console.log('err ' + err);
            });

            //End 

          }
        }

      ]
    });
    alertAcceptDeliver.present();

  }
  //End 

  //Accept order 

  acceptOrder() {
    this.tXDate = this.translateDateFun(this.orderDetails.rescheduled_date_time);
    this.dateTimeOld = moment(this.tXDate).format('x');

    this.dateTimeNew = moment().format('x');
    if (this.dateTimeOld < this.dateTimeNew) {
      this.toastMsg(this.translationLet.order_time_elapsed);
    } else {
      //Confirmation 
      let alertAccept = this.alertCtrl.create({
        title: this.translationLet.accept,
        message: this.translationLet.do_you_want_to_accept_rescheduled_time,
        buttons: [
          {
            text: this.translationLet.no_txt,
            cssClass: 'btn-primary btn-round',
            handler: () => {
            }
          },
          {
            text: this.translationLet.yes_txt,
            cssClass: 'btn-primary btn-round',
            handler: () => {

              //Call service and accept order 

              if (this.orderDetails.payment_status == 0 && this.orderDetails.payment_mode == 2) {
                this.payPal.init({
                  PayPalEnvironmentProduction: this.orderDetails.paypal_client_id,
                  PayPalEnvironmentSandbox: ''
                }).then(() => {
                  // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
                  this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
                    languageOrLocale: this.paypalLang
                    // Only needed if you get an "Internal Service Error" after PayPal login!
                    //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
                  })).then(() => {
                    let payment = new PayPalPayment(this.buyerpaidAmt, 'EUR', 'Description', 'sale');
                    this.payPal.renderSinglePaymentUI(payment).then((success) => {

                      if (success.response.state == 'approved') {
                        //Paypal mode 
                        this.updateData.order_id = this.orderDetails.orderIds;
                        this.updateData.user_id = this.buyerOrderObj.buyer_id;
                        this.updateData.seller_id = this.actionObj.seller_id;
                        this.updateData.created = moment().format('YYYY-MM-DD HH:mm:ss');
                        this.updateDataJoin.paypal_tx_id = success.response.state.id;
                        this.serviceProvider.updatePaypalData(this.updateData, this.token);
                        this.showLoading();
                        this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
                        this.buyerOrderObj.is_free = this.orderDetails.isFree;
                        this.serviceProvider.acceptOrderFromBuyer(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
                          this.loading.dismiss();
                          if (result.code == 200) {
                            this.toastMsg(this.translationLet.order_confrim_from_your_side);
                            this.buyerOrderInit(this.buyerOrderObj);
                            this.orderDetails.order_delivery_Status = result.status;
                            this.orderDetails.orderTrack = result.orderTrackStatus;
                          } else {
                            this.toastMsg(this.translationLet.service_error);
                          }
                        }, (err) => {
                          this.loading.dismiss();
                          console.log('err ' + err);
                        });
                      } else {
                        this.toastMsg(this.translationLet.payment_failed_error);
                      }
                    }, (err) => {
                      // Error or render dialog closed without being successful
                      this.toastMsg(JSON.stringify(err));
                    });
                  }, (err) => {
                    // Error in configuration
                    this.toastMsg(JSON.stringify(err));
                  });
                }, (err) => {
                  // Error in initialization, maybe PayPal isn't supported or something else
                  //this.toastMsg(JSON.stringify(err));
                });


              } else {
                this.showLoading();
                this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
                this.buyerOrderObj.is_free = this.orderDetails.isFree;
                this.serviceProvider.acceptOrderFromBuyer(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
                  this.loading.dismiss();
                  if (result.code == 200) {
                    this.toastMsg(this.translationLet.order_confrim_from_your_side);
                    this.buyerOrderInit(this.buyerOrderObj);
                    this.orderDetails.order_delivery_Status = result.status;
                    this.orderDetails.orderTrack = result.orderTrackStatus;
                  } else {
                    this.toastMsg(this.translationLet.service_error);
                  }
                }, (err) => {
                  this.loading.dismiss();
                  console.log('err ' + err);
                });

              }

              //End 

            }
          }

        ]
      });
      alertAccept.present();
    }
  }
  //End 

  //Reject order 

  rejectDeliverOrder() {
    let alertDeliverReject = this.alertCtrl.create({
      title: this.translationLet.reject_received_order,
      message: this.translationLet.do_you_want_to_reject_received_order,
      buttons: [
        {
          text: this.translationLet.no_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {
          }
        },
        {
          text: this.translationLet.yes_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {

            //Call service and accept order 
            this.showLoading();
            this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
            this.serviceProvider.rejectDeliverOrder(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.toastMsg(this.translationLet.reject_received_order_from_your_side);
                this.orderDetails.order_delivery_Status = result.status;
                this.orderDetails.orderTrack = result.orderTrackStatus;
                // User Rate
                let modalRate = this.modalCtrl.create(UserRatingPage, { 'user_id': this.buyerOrderObj.seller_id, 'product_id': this.orderDetails.prodId, 'id': this.buyerOrderObj.buyer_id });
                modalRate.present();
                modalRate.onDidDismiss(data => {
                  //Set to order page 
                  this.app.getRootNav().setRoot(UpcomingPage);
                  //End 
                });

              } else {
                this.toastMsg(this.translationLet.service_error);
              }
            }, (err) => {
              this.loading.dismiss();
              console.log('err ' + err);
            });

            //End 

          }
        }

      ]
    });
    alertDeliverReject.present();
  }


  //Rejection 
  rejectOrder() {
    let alertReject = this.alertCtrl.create({
      title: this.translationLet.reject,
      message: this.translationLet.do_you_want_reject_order,
      buttons: [
        {
          text: this.translationLet.no_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {
          }
        },
        {
          text: this.translationLet.yes_txt,
          cssClass: 'btn-primary btn-round',
          handler: () => {

            //Call service and accept order 
            this.showLoading();
            this.buyerOrderObj.created = moment().format('YYYY-MM-DD HH:mm:ss');
            this.serviceProvider.rejectOrderfromBuyer(this.buyerOrderObj, this.token, this.lang_id).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.toastMsg(this.translationLet.reject_order);
                this.orderDetails.order_delivery_Status = result.status;
                this.orderDetails.orderTrack = result.orderTrackStatus;
              } else {
                this.toastMsg(this.translationLet.service_error);
              }
            }, (err) => {
              this.loading.dismiss();
              console.log('err ' + err);
            });

            //End 

          }
        }

      ]
    });
    alertReject.present();
  }

  //End 


  //Goto Share a friend 

  gotoShareAfriend() {
    //Check time elapsed 
    this.tXDate = this.translateDateFun(this.orderDetails.deliveryTime);
    this.dateTimeOld = moment(this.tXDate).format('x');
    this.dateTimeNew = moment().format('x');
    if (this.dateTimeOld < this.dateTimeNew) {
      this.toastMsg(this.translationLet.order_time_elapsed_can_not_share_order);
    } else {
      this.navCtrl.push(ShareproductPage, { 'orderDetails': JSON.stringify(this.buyerOrderObj) });
    }
  }
  //End 

  gotosharedfriends() {
    this.navCtrl.push(SharepeoplelistPage, { 'orderDetails': JSON.stringify(this.buyerOrderObj) });
  }

  gotoUserOrder() {
    this.app.getRootNav().setRoot(ReceiveUpcomingPage);
  }

  gotMyOrder() {
    this.app.getRootNav().setRoot(UpcomingPage);
  }


  //Goto Order summary 
  gotoOrderSummary() {
    this.navCtrl.push(PaymentTransactionPage, { 'ordersSummary': JSON.stringify(this.buyerOrderObj) });
  }

  //End 
  delMainOrder() {
    let alert = this.alertCtrl.create({
      title: this.translationLet.delete,
      message: this.translationLet.do_you_want_to_delete_order,
      buttons: [
        {
          text: this.translationLet.yes_txt,
          handler: () => {
            //  Delete  
            this.showLoading();
            this.serviceProvider.delMainOrder(this.delOrderObj, this.token).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.app.getRootNav().setRoot(UpcomingPage);
                this.toastMsg(this.translationLet.order_deleted_success);
              } else {
                this.toastMsg(this.translationLet.something_wrong_txt);
              }
            }, (err) => {
              this.loading.dismiss();
            });
          }
          //End
        }, {
          text: this.translationLet.no_txt,
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


  //changeReceiver

  changeReceiver() {
    let modalReceiver = this.modalCtrl.create(UpdateReceiverPage, { 'receiverDetails': JSON.stringify(this.orderDetails) });
    modalReceiver.present();
    modalReceiver.onDidDismiss(data => {
      if (data != '') {
        this.orderDetails.delivery_surname = data.delivery_surname;
        this.orderDetails.delivery_name = data.delivery_name;
        this.orderDetails.delivery_mobile = data.delivery_mobile;
        this.orderDetails.delivery_company = data.delivery_company;
      }
      //End 
    });
  }

  //Update time

  gotoUpdateTime() {
    this.updateTimeObj.id = this.buyerOrderObj.id;
    this.updateTimeObj.user_id = this.profileDetails.id;
    this.updateTimeObj.delivery_time = this.orderDetails.delivery_time;
    this.updateTimeObj.delivery_date = this.orderDetails.delivery_date;
    let modalAcceptTime = this.modalCtrl.create(UpdatedeliverytimePage, { 'deliveryJSON': JSON.stringify(this.updateTimeObj) });
    modalAcceptTime.present();
    modalAcceptTime.onDidDismiss(data => {
      if (data != 0) {
        this.orderDetails.delivery_time = data.delivery_time;
        this.orderDetails.delivery_date = data.delivery_date;
        this.orderDetails.deliveryTime = this.translateDateFun(moment(data.schedule_datetime).format('DD MMM YYYY HH:mm'));
        this.toastMsg(this.translationLet.order_time_updated_successfully);
      }
      //End 
    });
  }
  //End 


  //Goto paypal -Pay when buyer order 

  gotoPaypal(client_id, order_id) {

    this.payPal.init({
      PayPalEnvironmentProduction: this.orderDetails.paypal_client_id,
      PayPalEnvironmentSandbox: ''
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        languageOrLocale: this.paypalLang
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.buyerpaidAmt, 'EUR', 'Pay for order', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then((success) => {
          if (success.response.state == 'approved') {
            //payment done 
            this.updateData.order_id = order_id;
            this.updateData.user_id = this.buyerOrderObj.buyer_id;
            this.updateData.seller_id = this.actionObj.seller_id;
            this.updateData.paypal_tx_id = success.response.state.id;
            this.updateData.created = moment().format('YYYY-MM-DD HH:mm:ss');
            this.showLoading();
            this.serviceProvider.updatePaypalData(this.updateData, this.token).then((result: any) => {
              this.loading.dismiss();
              if (result.code == 200) {
                this.buyerOrderInit(this.buyerOrderObj);
                this.toastMsg(this.translationLet.record_updated);
              } else {
                this.toastMsg(this.translationLet.something_wrong_txt);
              }
            }, (err) => {
              this.loading.dismiss();
            });
          } else {
            this.toastMsg(this.translationLet.payment_failed_error);
          }

        }, (err) => {
          // Error or render dialog closed without being successful
          this.toastMsg(JSON.stringify(err));
        });
      }, (err) => {
        // Error in configuration
        this.toastMsg(JSON.stringify(err));
      });
    }, (err) => {
      // Error in initialization, maybe PayPal isn't supported or something else
      //   alert("3"+JSON.stringify(err));
    });

  }

  gotoPaypalJoin(client_id, order_id, totalAmt, order_item_id) {
    try{
      console.log('this.payPal.version(); : ', this.payPal.version());
      
      this.payPal.init({
        PayPalEnvironmentProduction: this.orderDetails.paypal_client_id,
        PayPalEnvironmentSandbox: ''
      }).then(() => {
        
        // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
        this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
          languageOrLocale: this.paypalLang,
          recipientName: 'Test',
          line1: 'test',
          line2: 'test',
          city: 'Nagpur',
          state: 'Mh',
          postalCode: '440011',
          countryCode: 'IN'
          // Only needed if you get an "Internal Service Error" after PayPal login!
          //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
        })).then(() => {
          let payment = new PayPalPayment(totalAmt, 'EUR', 'Description', 'sale');
          this.payPal.renderSinglePaymentUI(payment).then((success) => {
            if (success.response.state == 'approved') {
              //payment done 
              this.updateDataJoin.order_id = order_id;
              this.updateDataJoin.paypal_tx_id = success.response.state.id;
              this.updateDataJoin.order_item_id = order_item_id;
              this.updateDataJoin.user_id = this.buyerOrderObj.buyer_id;
              this.updateDataJoin.seller_id = this.actionObj.seller_id;
              this.updateDataJoin.created = moment().format('YYYY-MM-DD HH:mm:ss');
  
              this.showLoading();
  
              this.serviceProvider.updatePaypalJoinData(this.updateDataJoin, this.token).then((result: any) => {
                this.loading.dismiss();
                if (result.code == 200) {
                  this.buyerOrderInit(this.buyerOrderObj);
                  this.toastMsg(this.translationLet.record_updated);
                } else {
                  this.toastMsg(this.translationLet.something_wrong_txt);
                }
              }, (err) => {
                this.loading.dismiss();
              });
  
            } else {
              this.toastMsg(this.translationLet.payment_failed_error);
            }
          }, (err) => {
            // Error or render dialog closed without being successful
            this.toastMsg(JSON.stringify(err));
          });
        }, (err) => {
          // Error in configuration
          this.toastMsg(JSON.stringify(err));
        });
      }, (err) => {
        // Error in initialization, maybe PayPal isn't supported or something else
        // this.toastMsg('Payment failed, try after some time'); 
      });
    } catch(error) {
      console.log(error);
      
    }
    



  }

}
                                                                                                                                                                