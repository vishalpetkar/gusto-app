import { Component,ViewChild } from '@angular/core';
import { Platform,IonicPageModule ,Slides,ModalController,ActionSheetController,Nav,MenuController ,IonicPage,App,NavController, NavParams,ToastController, AlertController, Events,LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { ServiceProvider } from '../../providers/service/service';
import { ConfigProvider } from '../../providers/config/config';
import { CommentsPage } from '../comments/comments';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DeliverypolicyPage } from '../deliverypolicy/deliverypolicy';
import { MycartPage } from '../mycart/mycart'; 
import { ProductRatingsPage } from '../product-ratings/product-ratings';
import { SellerprofilePage } from '../sellerprofile/sellerprofile';
//import { IonicImageLoader } from 'ionic-image-loader';
//import { ImgcacheService } from '../global/services';
import moment from 'moment';
@Component({
  selector: 'page-sellerproduct-details',
  templateUrl: 'sellerproduct-details.html',
})
export class SellerproductDetailsPage {
translationLet: any = [];	
  loading: Loading;	
  userImgPath : any ='';
  prodImgPath : any ='';
  token :any ='';
  productDetails :any = {userPolicies : [], isProductRate : 0};
  userPolicies : any =[];
  commentList :any =[];
  prodImgList :any =[];
  cartCount: any =0;
  rate : any = 0;
  isFetchingData :any =true;
  productObj :any = { id : '', user_id : ''}; 

    tempProdLikes : any = {
    'user_id'  :'',
    'product_id' : ''
  };
  
  isenabled:boolean=false;

  isDisabled : boolean =false;

  tempUserImg :any ='';

  addtoCartObj : any =
  {
   'product_id' : '',
   'user_id' : '',
   'created' : '',
   'quantity' : '1'
  }
 
  @ViewChild(Slides) slides: Slides;
  //constructor(public imgcacheService: ImgcacheService,public modalCtrl: ModalController,private socialSharing: SocialSharing,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    constructor(public modalCtrl: ModalController,private socialSharing: SocialSharing,public platform: Platform,public configProvider : ConfigProvider,public serviceProvider : ServiceProvider,private menu: MenuController,public app: App,private translateService: TranslateService,public events: Events,public navCtrl: NavController,private storage: Storage,public toastCtrl: ToastController,public navParams: NavParams,private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellerproductDetailsPage');
  }



initProductDetails()
{
//this.showLoading();
this.serviceProvider.getProdDetails(this.productObj,this.token).then((result:any) => {
          //this.loading.dismiss();
            this.isFetchingData =false;
            if(result.code == 200){
            this.productDetails =result.data.Product[0];
            this.productDetails.distance=this.navParams.get('product_distance');
            if(result.data.Product[0].user_photo != '')
            {
            this.productDetails.user_photo= this.userImgPath+result.data.Product[0].user_photo;
            }else
            {
 		     	this.productDetails.user_photo= 'assets/images/user-profile-default-thumb.jpg' ;
            }
            this.userPolicies = result.data.Product[0].userPolicies[0];     
            this.commentList =result.data.Comments[0];
            this.prodImgList =result.data.ProdImages[0];      

          } else if(result.code == 500) {
             this.productDetails=[];
             this.toastMsg(this.translationLet.service_error);
          } else
          {
             this.productDetails=[];
             this.toastMsg(this.translationLet.unAuthReq_msg);
          }
          }, (err) => {
      //  this.loading.dismiss();
          console.log('err '+err);
          }); 
}

//Goto User Profile section 

gotoSellerProfile(userId)
{
this.navCtrl.push(SellerprofilePage,{'userId' : userId});
}



//Goto social share 

gotoSocialShare(prodname, prodImg)
{

this.isenabled=true;
	// Share 
this.socialSharing.share(prodname, this.translationLet.share_social_msg,this.prodImgPath+prodImg).then(() => {
 this.isenabled=false;
}).catch(() => {
  this.isenabled=false;
  this.toastMsg(this.translationLet.unable_to_share);
}); 

}

//Goto likes 

gotoLikes(prodId,prodLikes)
{
//Call service to update in DB 
this.isDisabled=true;
this.tempProdLikes.user_id = this.productObj.user_id;
this.tempProdLikes.product_id = prodId;

this.serviceProvider.updateProdLikes(this.tempProdLikes,this.token).then((result:any) => {
          //this.loading.dismiss();
            if(result.code == 200){
            this.isDisabled=false; 
              if(prodLikes ==true)
                {
                this.productDetails.isLikeUser =false;
                this.productDetails.likeCount -=1 ;
                }
              else
              {
                this.productDetails.isLikeUser=true;
                 this.productDetails.likeCount +=1 ;
              }
                
          } else if(result.code == 500) {
            this.isDisabled=false; 
             this.toastMsg(this.translationLet.service_error);
          } else
          {
            this.isDisabled=false; 
            this.toastMsg(this.translationLet.unAuthReq_msg);
          }
          }, (err) => {
        //this.loading.dismiss();
          this.isDisabled=false; 
          console.log('err '+err);
          }); 
//End

} 
//End 

 //Show loading 

  showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      cssClass: 'loading-section',
      content: '<div class="custom-spinner-container"><div class="custom-spinner-box"><img src="assets/images/logo-color.png" style="max-width:50px;" /><div class="loading-text">'+this.translationLet.please_wait_txt+'</div></div></div>'
    });
    this.loading.present();
  }
  
//End

  gotoComments(product_id)
  {
    this.navCtrl.push(CommentsPage,{'product_id' : product_id});
  }
  

    //Toast Msg 

	toastMsg (msg){
	 let toast = this.toastCtrl.create({
	      message: msg,
	      duration: 2000,
	      position: 'bottom'
	    });
	    toast.present(toast);
	  }

   //End

slideChanged()
{
   let currentIndex = this.slides.getActiveIndex();
}

gotoDelPolicies (deliveryPolicies)
{

 this.navCtrl.push(DeliverypolicyPage, {'userPolicies' : JSON.stringify(deliveryPolicies)});
}

ionViewDidEnter()
{   
    this.userImgPath = this.configProvider.getImagePath();
    this.prodImgPath = this.configProvider.getProdImgPath();
    this.productObj.id=this.navParams.get('product_id');

    
    this.productDetails.user_photo= 'assets/images/user-profile-default-thumb.jpg' ;

     this.storage.get('authtoken').then((authtoken) => {
        this.token=authtoken;
    });

    this.translateService.get(['prd_added_cart','go_to_cart','prod_cart_err','unable_to_share','share_social_msg','add_cart','view_all_comment','user_comments','rate_the_product','likes','comments','share_product','delivery_polices','delivery_cost','view_product','unAuthReq_msg','service_error','food_not_avail','load_more','prod_not_added','please_wait_txt','delivery_policies_err']).subscribe((translation: [string]) => {
          this.translationLet = translation;
        }); 

     //Get details
    this.storage.get('userDetails').then((userDetails) => {
        this.productObj.user_id=userDetails.id;
        this.initProductDetails();
     }); 
}


  gotoRatings(prodRate,prod_id,average_rating)
  {
    
  if(prodRate == 0 )
    {
    let modalRate = this.modalCtrl.create(ProductRatingsPage,{'user_id' : this.productObj.user_id, 'product_id' : prod_id,'average_rating':average_rating});
    modalRate.present();
    modalRate.onDidDismiss(data => {
    this.productDetails.isProductRate =data.userRating;
    this.productDetails.average_rating =data.average_rating;
    //End 
    });
 }
}


}
