import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { ConfigProvider } from '../providers/config/config';
import { ServiceProvider } from '../providers/service/service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MyApp } from './app.component';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient  } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NativeGeocoder} from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { OneSignal } from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';
import { Media } from '@ionic-native/media';
import { MediaCapture} from '@ionic-native/media-capture';
import { FileTransfer} from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Keyboard } from '@ionic-native/keyboard';
/* Pages start */
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';
import { Crop } from '@ionic-native/crop';
import { Ionic2RatingModule } from 'ionic2-rating';
//import { IonicImageLoader } from 'ionic-image-loader';
import { Contacts} from '@ionic-native/contacts';

import { LoginPhoneNumberPage } from '../pages/login-phone-number/login-phone-number';
import { CountryCodePage } from '../pages/country-code/country-code';
import { SearchBuyerProductPage } from '../pages/search-buyer-product/search-buyer-product';
import { SelectaddressPage } from '../pages/selectaddress/selectaddress';
import { SetpasswordPage } from '../pages/setpassword/setpassword';
import { VarifyPage } from '../pages/varify/varify';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { CoverphotoPage } from '../pages/coverphoto/coverphoto';
import { AddproductformPage } from '../pages/addproductform/addproductform';
import { SellerproductsPage } from '../pages/sellerproducts/sellerproducts';
import { SellerprodeditPage } from '../pages/sellerprodedit/sellerprodedit';
import { SellerprodviewPage } from '../pages/sellerprodview/sellerprodview';
import { CommentsPage } from '../pages/comments/comments';
import { ProductdetailsPage } from '../pages/productdetails/productdetails';
import { DeliverypolicyPage } from '../pages/deliverypolicy/deliverypolicy'; 
import { LocationPage } from '../pages/location/location'; 
import { MycartPage } from '../pages/mycart/mycart'; 
import { DeliveryaddressPage } from '../pages/deliveryaddress/deliveryaddress'; 
import { SellerreceivedorderPage } from '../pages/sellerreceivedorder/sellerreceivedorder'; 
import { BuyerorderdetailsPage } from '../pages/buyerorderdetails/buyerorderdetails'; 
import { MyorderPage } from '../pages/myorder/myorder'; 
import { SuperTabsModule } from 'ionic2-super-tabs';
import { UpcomingPage } from '../pages/upcoming/upcoming';
import { PastorderPage } from '../pages/pastorder/pastorder';
import { ReceiveorderlistPage } from '../pages/receiveorderlist/receiveorderlist';
import { ProductRatingsPage } from '../pages/product-ratings/product-ratings';
import { UserRatingPage } from '../pages/user-rating/user-rating';
import { AcceptnewtimePage } from '../pages/acceptnewtime/acceptnewtime';
import { SellerviewdeliveryaddressPage } from '../pages/sellerviewdeliveryaddress/sellerviewdeliveryaddress';
import { SellerprofilePage } from '../pages/sellerprofile/sellerprofile';
import { NotificationPage } from '../pages/notification/notification';
import { ExplorePage } from '../pages/explore/explore';
import { JoinorderPage } from '../pages/joinorder/joinorder';
import { ShareproductPage } from '../pages/shareproduct/shareproduct';
import { SharedfriendsPage } from '../pages/sharedfriends/sharedfriends';
import { SharepeoplelistPage } from '../pages/sharepeoplelist/sharepeoplelist';
import { SellerproductDetailsPage } from '../pages/sellerproduct-details/sellerproduct-details';
import { SellerjoinproductsPage } from '../pages/sellerjoinproducts/sellerjoinproducts';
import { JoinsharedorderPage } from '../pages/joinsharedorder/joinsharedorder';
import { ProductsearchPage } from '../pages/productsearch/productsearch';
import { JoinorderhistoryPage } from '../pages/joinorderhistory/joinorderhistory';
import { ReceivePastPage } from '../pages/receive-past/receive-past';
import { ReceiveUpcomingPage } from '../pages/receive-upcoming/receive-upcoming';
import { ActionPopoverPage } from '../pages/action-popover/action-popover';
import { UpdateReceiverPage } from '../pages/update-receiver/update-receiver';
import { UpdateOrderPage } from '../pages/update-order/update-order';
import { UpdatedeliverytimePage } from '../pages/updatedeliverytime/updatedeliverytime';
//import { LazyImgComponent }   from '../global/components/';
//import { LazyLoadDirective }   from '../global/directives/';
//import { ImgcacheService }    from '../global/services/';
import { TransactionsPage } from '../pages/transactions/transactions';
import { TransactiondetailsPage } from '../pages/transactiondetails/transactiondetails';
import { IonicImageLoader } from 'ionic-image-loader';
import { PaymentTransactionPage } from '../pages/payment-transaction/payment-transaction';
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { VerifiedPasswordPage } from '../pages/verified-password/verified-password';
import { JoinOrderPastPage } from '../pages/join-order-past/join-order-past';
import { JoinOrderUpcomingPage } from '../pages/join-order-upcoming/join-order-upcoming';
import { JoinhistroydetailPage } from '../pages/joinhistroydetail/joinhistroydetail';
import { PrivacyPoliciesPage } from '../pages/privacy-policies/privacy-policies';
import { DisclaimerPage } from '../pages/disclaimer/disclaimer';
import { ContactusPage } from '../pages/contactus/contactus';
import { NgCalendarModule  } from 'ionic2-calendar';
import { PayPal} from '@ionic-native/paypal'

/* Pages End*/

export function HttpLoaderFactory(http: HttpClient) {
     return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [

    MyApp,
   HomePage,
    LoginPage,
    LoginPhoneNumberPage,
    CountryCodePage,
    SelectaddressPage,
    SetpasswordPage,
    VarifyPage,
    MyprofilePage,
    EditprofilePage,
    ChangepasswordPage,
    CoverphotoPage,
    AddproductformPage,
    SellerproductsPage,
    SellerprodeditPage,
    SellerprodviewPage,
    CommentsPage,
    ProductdetailsPage,
    DeliverypolicyPage, 
    LocationPage,
    RegistrationPage,
    MycartPage,
    MyorderPage,
    SellerreceivedorderPage,
    DeliveryaddressPage,
    UpcomingPage,
    PastorderPage,
    ReceiveorderlistPage,
    ProductRatingsPage,
    BuyerorderdetailsPage,
    SellerviewdeliveryaddressPage,
    AcceptnewtimePage,
    SellerprofilePage,
    NotificationPage,
    ExplorePage,
    JoinorderPage,
    ShareproductPage,
    SharedfriendsPage,
    SharepeoplelistPage,
    SellerproductDetailsPage,
    SellerjoinproductsPage,
    JoinsharedorderPage,
    TransactionsPage,
    ProductsearchPage,
    TransactiondetailsPage,
    PaymentTransactionPage,
    UserRatingPage,
    ResetpasswordPage,
    ForgotpasswordPage,
    VerifiedPasswordPage,
    JoinorderhistoryPage,
    JoinOrderPastPage,
    JoinOrderUpcomingPage,
    JoinhistroydetailPage,
    ReceiveUpcomingPage,
    ReceivePastPage,
    ActionPopoverPage,
    UpdateReceiverPage,
    UpdateOrderPage,
    UpdatedeliverytimePage,
    PrivacyPoliciesPage,
    ContactusPage,
    DisclaimerPage,
    SearchBuyerProductPage,
   //  LazyImgComponent,
   // LazyLoadDirective
  ],
  imports: [
    BrowserModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp,{ mode: 'md', scrollAssist: false, autoFocusAssist: true }),
    HttpModule,
    IonicImageLoader.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    SuperTabsModule.forRoot(),
    Ionic2RatingModule,
  //  IonicImageLoader.forRoot(),
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LoginPhoneNumberPage,
    CountryCodePage,
    SelectaddressPage,
    SetpasswordPage,
    VarifyPage,
    MyprofilePage,
    EditprofilePage,
    ChangepasswordPage,
    CoverphotoPage,
    AddproductformPage,
    SellerproductsPage,
    SellerprodeditPage,
    SellerprodviewPage,
    CommentsPage,
    ProductdetailsPage,
    DeliverypolicyPage,
    LocationPage,
    RegistrationPage,
    MycartPage,
    MyorderPage,
    SellerreceivedorderPage,
    DeliveryaddressPage,
    UpcomingPage,
    PastorderPage,
    ReceiveorderlistPage,
    ProductRatingsPage,
    BuyerorderdetailsPage,
    SellerviewdeliveryaddressPage,
    AcceptnewtimePage,
    SellerprofilePage,
    NotificationPage,
    ExplorePage,
    JoinorderPage,
    ShareproductPage,
    SharedfriendsPage,
    SharepeoplelistPage,
    SellerproductDetailsPage,
    SellerjoinproductsPage,
    JoinsharedorderPage,
    TransactionsPage,
    ProductsearchPage,
    TransactiondetailsPage,
    PaymentTransactionPage,
    UserRatingPage,
    ResetpasswordPage,
    ForgotpasswordPage,
    VerifiedPasswordPage,
    JoinorderhistoryPage,
    JoinOrderPastPage,
    JoinOrderUpcomingPage,
    JoinhistroydetailPage,
    ReceiveUpcomingPage,
    ReceivePastPage,
    ActionPopoverPage,
    UpdateReceiverPage,
    UpdateOrderPage,
    UpdatedeliverytimePage,
    PrivacyPoliciesPage,
    ContactusPage,
    DisclaimerPage,
    SearchBuyerProductPage,
   // LazyImgComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfigProvider,
    ServiceProvider,
    Facebook,
    InAppBrowser,
    NativeGeocoder,
    Geolocation,
    OneSignal,
    Network,
    Media,
    MediaCapture,
    Camera,
    FileTransfer,
    File,
    SocialSharing, 
    LocationAccuracy,
    Crop,
    IonicImageLoader,
    PayPal,
    Contacts,
    Keyboard,
   // ImgcacheService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
