import { Component,ViewChild } from '@angular/core';
import { Nav,Platform,App,AlertController,Events,ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { LoginPage } from '../pages/login/login';
import { TranslateService } from '@ngx-translate/core';
import { OneSignal } from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';
import { AddproductformPage } from '../pages/addproductform/addproductform';
import { JoinhistroydetailPage } from '../pages/joinhistroydetail/joinhistroydetail';
import { SharedfriendsPage } from '../pages/sharedfriends/sharedfriends';
import { SellerreceivedorderPage } from '../pages/sellerreceivedorder/sellerreceivedorder';
import { BuyerorderdetailsPage } from '../pages/buyerorderdetails/buyerorderdetails';
import { JoinorderPage } from '../pages/joinorder/joinorder';
import { timer } from 'rxjs/observable/timer';
import { ServiceProvider } from '../providers/service/service';
@Component({
templateUrl: 'app.html'
})
export class MyApp {
@ViewChild(Nav) nav: Nav;
rootPage: any = '';
userId :any ='';
translationLet: any;


constructor(public serviceProvider : ServiceProvider,private network: Network,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private storage: Storage,public toastCtrl: ToastController, private alertCtrl: AlertController,public app: App,public events: Events,private  translateService: TranslateService,private oneSignal: OneSignal) {

this.translateService.get(['error_txt','ok_txt',"no_network",'please_wait_txt','forgot_password','please_enter_valid_phone','plese_enter_pass','signup','do_not_have_an_account','wrong_pass','instagram_error','sign_in_success','something_wrong_txt','facebook_error','error_txt','enter_phone_no_val','login_txt_1','login_txt_2','login_txt_3','facebook','instagram','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
this.translationLet = translation;
});

this.initializeApp();

this.storage.get('user_id').then((user_id) => {
this.userId=user_id;
if (this.userId > 0) {
this.rootPage = HomePage;
} else {
this.rootPage = LoginPage;
this.translateService.setDefaultLang('it');
this.translateService.use('it');
}
});
}

initializeApp() {

this.platform.ready().then(() => {
this.statusBar.styleDefault();
this.splashScreen.hide();
/* Language translator  */
if (this.platform.is('android') || this.platform.is('ios') )
{
this.network.onDisconnect().subscribe(() => {
let alertNetPopup = this.alertCtrl.create({
title: this.translationLet.error_txt,
message: this.translationLet.no_network,
buttons:  [
{ text: this.translationLet.ok_txt,
cssClass:'btn-primary btn-round',
handler: () => {
this.platform.exitApp();
}
}]
});
alertNetPopup.present();
});

}

//Check language
this.storage.get('lang_id').then((lang_id) => {

if(lang_id > 0 )
{
if(lang_id ==1)
{
this.translateService.use('en');
}else{
this.translateService.use('it');
}

this.translateService.get(['error_txt','ok_txt',"no_network",'please_wait_txt','forgot_password','please_enter_valid_phone','plese_enter_pass','signup','do_not_have_an_account','wrong_pass','instagram_error','sign_in_success','something_wrong_txt','facebook_error','error_txt','enter_phone_no_val','login_txt_1','login_txt_2','login_txt_3','facebook','instagram','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
this.translationLet = translation;
});

}else
{
this.translateService.setDefaultLang('it');
this.translateService.use('it');
}
this.translateService.get(['error_txt','ok_txt',"no_network",'please_wait_txt','forgot_password','please_enter_valid_phone','plese_enter_pass','signup','do_not_have_an_account','wrong_pass','instagram_error','sign_in_success','something_wrong_txt','facebook_error','error_txt','enter_phone_no_val','login_txt_1','login_txt_2','login_txt_3','facebook','instagram','insert_phone_number','next_txt']).subscribe((translation: [string]) => {
this.translationLet = translation;
});
});
//End
/*One signal push notification*/
if (this.platform.is('android') || this.platform.is('ios') )
{
this.oneSignal.startInit('853b01e6-0c3d-4dcc-a4e3-b01f534fe109', '13961028158');
this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
this.oneSignal.handleNotificationReceived().subscribe(pushData => {
//Notification received
this.storage.get('user_id').then((user_id) => {

if (user_id > 0) {
if(pushData.payload.additionalData.notificationType == 1 || pushData.payload.additionalData.notificationType == 5 || pushData.payload.additionalData.notificationType == 6){
this.app.getRootNav().setRoot(SellerreceivedorderPage,{'order_id' : pushData.payload.additionalData.order_id, 'user_id' : pushData.payload.additionalData.seller_id, 'buyer_id' : pushData.payload.additionalData.buyer_id});
}
if(pushData.payload.additionalData.notificationType == 2 || pushData.payload.additionalData.notificationType ==3  || pushData.payload.additionalData.notificationType ==4 || pushData.payload.additionalData.notificationType ==7 || pushData.payload.additionalData.notificationType ==10 || pushData.payload.additionalData.notificationType ==16){
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.payload.additionalData.order_id, 'seller_id' : pushData.payload.additionalData.seller_id});
}

if(pushData.payload.additionalData.notificationType == 8 ){

this.app.getRootNav().push(JoinorderPage,{'order_id' : pushData.payload.additionalData.order_id});
}
if(pushData.payload.additionalData.notificationType ==9){
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.payload.additionalData.order_id, 'seller_id' : pushData.payload.additionalData.seller_id});
}
if(pushData.payload.additionalData.notificationType == 17 || pushData.payload.additionalData.notificationType == 11 || pushData.payload.additionalData.notificationType ==12){
this.app.getRootNav().setRoot(SellerreceivedorderPage,{'order_id' : pushData.payload.additionalData.order_id, 'user_id' : pushData.payload.additionalData.seller_id, 'buyer_id' : pushData.payload.additionalData.buyer_id});
}
if(pushData.payload.additionalData.notificationType == 15){
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.payload.additionalData.order_id, 'seller_id' : pushData.payload.additionalData.seller_id});
}

} else {
this.app.getRootNav().setRoot(LoginPage);
}
});
});
//End
//Notification Open
this.oneSignal.handleNotificationOpened().subscribe(pushData => {

this.storage.get('user_id').then((user_id) => {

if (user_id > 0) {
if(pushData.notification.payload.additionalData.notificationType == 1 || pushData.notification.payload.additionalData.notificationType == 5 || pushData.notification.payload.additionalData.notificationType == 6){
setTimeout(() => {
this.app.getRootNav().setRoot(SellerreceivedorderPage,{'order_id' : pushData.notification.payload.additionalData.order_id, 'user_id' : pushData.notification.payload.additionalData.seller_id, 'buyer_id' : pushData.notification.payload.additionalData.buyer_id});
},500);
}
if(pushData.notification.payload.additionalData.notificationType == 16  || pushData.notification.payload.additionalData.notificationType == 2 || pushData.notification.payload.additionalData.notificationType ==3  || pushData.notification.payload.additionalData.notificationType ==4 || pushData.notification.payload.additionalData.notificationType ==7 || pushData.notification.payload.additionalData.notificationType == 10 ){
setTimeout(() => {
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.notification.payload.additionalData.order_id, 'seller_id' : pushData.notification.payload.additionalData.seller_id});
},500);
}
if(pushData.notification.payload.additionalData.notificationType == 8 ){
setTimeout(() => {
this.app.getRootNav().push(JoinorderPage,{'order_id' : pushData.notification.payload.additionalData.order_id});
},500);
}
if(pushData.notification.payload.additionalData.notificationType ==9){
setTimeout(() => {
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.notification.payload.additionalData.order_id, 'seller_id' : pushData.notification.payload.additionalData.seller_id});
},500);
}
if(pushData.notification.payload.additionalData.notificationType == 17  || pushData.notification.payload.additionalData.notificationType == 11 || pushData.notification.payload.additionalData.notificationType ==12){
setTimeout(() => {
this.app.getRootNav().setRoot(SellerreceivedorderPage,{'order_id' : pushData.notification.payload.additionalData.order_id, 'user_id' : pushData.notification.payload.additionalData.seller_id, 'buyer_id' : pushData.notification.payload.additionalData.buyer_id});
},500);
}
if(pushData.notification.payload.additionalData.notificationType ==15){
setTimeout(() => {
this.app.getRootNav().push(BuyerorderdetailsPage,{'order_id' : pushData.notification.payload.additionalData.order_id, 'seller_id' : pushData.notification.payload.additionalData.seller_id});
},500);
}
}else
{
this.app.getRootNav().setRoot(LoginPage);
}
});

//End
});

//End
this.oneSignal.endInit();
//Get one signal push notification user id
this.oneSignal.getIds()
.then((ids) =>
{
this.storage.set('player_id', ids.userId);
});
//End
}

});
}
}