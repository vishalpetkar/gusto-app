import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

/*
  Generated class for the ConfigProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigProvider {
  /*apiPath:any="http://datalogysoftware.com/garson/restnewAPI/";
  flagImgPath:any="http://datalogysoftware.com/garson/img/upload/country/";
  userImgPath :any ="http://datalogysoftware.com/garson/img/upload/user_profile/orignal/";
  prodCatImgPath :any ="http://datalogysoftware.com/garson/img/upload/product_category/original/";
  productImgPath :any ="http://datalogysoftware.com/garson/img/upload/product/"; */

  apiPath:any="https://www.superisland.it/restnew21API/";
  flagImgPath:any="https://www.superisland.it/img/upload/country/";
  userImgPath :any ="https://www.superisland.it/img/upload/user_profile/orignal/";
  prodCatImgPath :any ="https://www.superisland.it/img/upload/product_category/original/";
  productImgPath :any ="https://www.superisland.it/img/upload/product/"; 


  constructor(public http: Http,private storage: Storage) {
    console.log('Hello ConfigProvider Provider');
  }
  
  getAPIpath(){
    return this.apiPath;
  }

  getFlagImgpath(){
    return this.flagImgPath;
  }
  
  getProdCatImgPath()
  {
  return this.prodCatImgPath;
  }
  
  getImagePath(){
    return this.userImgPath;
  }


  getProdImgPath(){
    return this.productImgPath;
  }

  
  

}
