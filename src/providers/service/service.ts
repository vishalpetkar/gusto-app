import { Injectable } from '@angular/core';
import { Http, Jsonp, Response, RequestOptions, Headers } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../config/config';

/*
  Generated class for the ProfileServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {

	apiPath:any='';
	token:any;
  userDetails:any;
  userid:any;
 
  lang:any='';
  imgPath:any='';

  constructor(public http: Http,private storage: Storage,public configProvider:ConfigProvider) {
  	this.apiPath = this.configProvider.getAPIpath();
    console.log('Hello ProfileServiceProvider Provider');
  }

//Get country List 
  
  getCountryList()
{
  this.lang ='en' ;
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getCountries',this.lang).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

getAppUserList()
{
  this.lang ='en' ;
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getAppUserList',this.lang).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

phoneBookContacts(userContacts)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'phoneBookContacts',JSON.stringify(userContacts)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Get category list 


getCatList()
{
  this.lang ='en' ;
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getCatList',this.lang).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });  
}



//Varify Phone number

  varifyPhoneNumber(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'varifyPhoneNumber',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Instagram login API 

  getInstagramUserInfo(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.get("https://api.instagram.com/v1/users/self/?access_token="+loginData.access_token).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Set Password 

setPassword(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'setPassword',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

loginUser(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'loginUser',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Facebook login 

facebookLogin(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'facebookLogin',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//InstaGram login

instaGramLogin(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'instaGramLogin',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Resend code 

resendCode(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'resendCode',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Resend code 

//Reset password 

resetPassword(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'resetPassword',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

resendCodeReset(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'resendCodeReset',loginData).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


// Update Delivery address


updateDeliveryAddress(deliveryDetails)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateDeliveryAddress',JSON.stringify(deliveryDetails)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 



//Update language with token 

updatelanguage(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatelanguage',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}

//Update updatePaymentMethod

updatePaymentMethod(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatePaymentMethod',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}

//Product count 

getProductCount(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getProductCount',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}

//Delete Product from cart  

delCartProduct(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delCartProduct',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}

//End 


//Delete Notication

delNotification(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delNotification',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}

//End 



//Change password 

changePassword(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'changePassword',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}
//End 

//Contcat us

contactus(userData,token)
{

 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'contactus',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 

}
//End 

 //Update profile section
 
 updateProfile(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateProfile',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Update policies section
 
 updatePolicies(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatePolicies',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End

//Update  time policies section
 
 updateTimePolicies(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateTimePolicies',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 



//DelProdImage

 delProdImage(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delProdImage',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Del Edit ProdImage

 delEditProdImage(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delEditProdImage',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Edit product functionality 

 editproductData(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'editproductData',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}



//View seller product

  sellerProdDetails(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'sellerProdDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Get Tx Details 

txDetails(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'txDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Add to cart functionality 

  addToCart(cart,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addToCart',JSON.stringify(cart),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Add as Order 

 sendOrderToSeller(product_data,token,lang_id){
  product_data.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'sendOrderToSeller',JSON.stringify(product_data),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Add as Temp order 
  addAsTempOrder(product_data,token){
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addAsTempOrder',JSON.stringify(product_data),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 



//Get seller payment method list 

 sellerPaymentMethod(product_data,token,lang_id){
  product_data.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'sellerPaymentMethod',JSON.stringify(product_data),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Get List of My cart 

  mycartList(cart,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'mycartList',JSON.stringify(cart),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 




//addProductData

 addProductData(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addProductData',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Get Prod comments 

getProdComments(user,token)
{

   let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getProdComments',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  }); 
}
//End 


//Add product comment 

addProdComments(user,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addProdComments',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 




//Product details 

getProdDetails(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getProdDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End


//Display Other records 

getHomeProducts(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getHomeProducts',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End

//Get Products to be searched 

getProducts(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getProducts',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Display Seller products 

getSellerProducts(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getSellerProducts',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//GetAll Tx 

getAllTx(user,token){
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getAllTx',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Display my products

 getAllProducts(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getAllProducts',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Display My orders

 myOrderList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'myOrderList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

 myJoinOrderList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'myJoinOrderList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

 myPastJoinOrderList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'myPastJoinOrderList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}




//Display My Past Order List

 myPastOrderList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'myPastOrderList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}



//My order details 

getBuyerDetails(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getBuyerDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//getJoinBuyerDetails

getJoinBuyerDetails(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getJoinBuyerDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Deliver Order 
deliverOrder(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'deliverOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Accept orders 

acceptOrder(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//AcceptAllOrder orders 

acceptAllOrder(user,token){
let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptAllOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      }); 
  });
}
//End 

//acceptAllDoneOrder orders 

acceptAllDoneOrder(user,token){
let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptAllDoneOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      }); 
  });
}
//End 



//Accept orders  from buyer

acceptOrderFromBuyer(user,token,lang_id){
 user.lang_id =lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptOrderFromBuyer',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Accept Received orders  from buyer

acceptDeliverOrderBuyer(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptDeliverOrderBuyer',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Reject received orders from buyer 

rejectDeliverOrder(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'rejectDeliverOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Reject orders 

rejectOrder(user,token,lang_id){
 user.lang_id = lang_id ;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'rejectOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 


//closeOrder orders 

closeOrder(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'closeOrder',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

getSellerProfile(seller_id) {
  console.log("seller_id", seller_id);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getSellerProfile',{seller_id}).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Get Seller profile 

getUserProfile(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getUserProfile',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Reject orders from buyer 

rejectOrderfromBuyer(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'rejectOrderfromBuyer',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

/*Notification List */

getNotification(user,token,lang_id){
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getNotification',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End

//Get Explore list 

getExploreList(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getExploreList',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Get Friend list 

getFriendList(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getFriendList',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Get Friend list 

shareFriendOrder(userData,token,lang_id)
{
  userData.lang_id = lang_id;
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'shareFriendOrder',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 
//Get contact data 
getContactData(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getContactData',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Get shareFriendOrderList

shareFriendOrderList(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'shareFriendOrderList',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 




//Get explore details 


getExploreDetails(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getExploreDetails',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//GetOrderCatWise

getOrderCatWise(userData,token)
{
 let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getOrderCatWise',JSON.stringify(userData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}



//Accept with another time 

acceptWithNewTime(user,token,lang_id){
  
  user.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'acceptWithNewTime',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Accept with another time 

updateDeliveryTime(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateDeliveryTime',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//My order details 

getSellerDetails(user,token,lang_id){
  user.lang_id = lang_id ;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getSellerDetails',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Display Receive order list 

 receiveOrderList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'receiveOrderList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//Receive order past list 
 receivePastList(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'receivePastList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}




//En 

//Get Edit data 
 getProductRecords(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getProductRecords',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 

//Get updateProdLikes
 updateProdLikes(user,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateProdLikes',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 


// Update BuyerDeliveryAddress


buyerDeliveryAddress(user,token)
{ 
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'buyerDeliveryAddress',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

buyershareList(user,token)
{ 
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'buyershareList',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//End 

//Update Product quanitity 

 updateQuantity(cartData,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateQuantity',JSON.stringify(cartData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//joinOrders

joinOrders(joinData,token,lang_id){
  joinData.lang_id = lang_id;
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'joinOrders',JSON.stringify(joinData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}




//Update Product Note 

 updateNotes(cartData,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateNotes',JSON.stringify(cartData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Update Promo code 

 updatePromo(cartData,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatePromo',JSON.stringify(cartData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

updateSellerPromo(cartData,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateSellerPromo',JSON.stringify(cartData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


//Add Cover photo 
 
 addProductCover(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addProductCover',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//ENd

//Add User rate 

addUserRate(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addUserRate',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Updatepaypal data

updatePaypalData(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatePaypalData',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

updatePaypalJoinData(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updatePaypalJoinData',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}



//End 

//Add product Rate
 
 addProductRate(user,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'addProductRate',JSON.stringify(user),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//ENd

//Del product 

 delProduct(product,token){
  
  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delProduct',JSON.stringify(product),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}
//End 



//Unique User Name
 uniqueUserName(userName){

  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'uniqueUserName',JSON.stringify(userName)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End

//Unique User Name login user 

 uniqueLoginUserName(userName,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'uniqueLoginUserName',JSON.stringify(userName),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End

//Order summary section

 getOrderSummary(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getOrderSummary',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

delMainOrder(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delMainOrder',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

// /delOrders
delOrders(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delOrders',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//updateOrder

updateOrder(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateOrder',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


// /delTransaction
delTransaction(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delTransaction',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

// /delPastOrders
delPastOrders(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'delPastOrders',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

// /getSellerAvailDates
getSellerAvailDates(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getSellerAvailDates',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 
// /getTimeSlots
getTimeSlots(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'getTimeByDate',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 


// /updateReceiverDetails
updateReceiverDetails(orderData,token){

  let optionsNew=this.createAuthHeader(token);
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'updateReceiverDetails',JSON.stringify(orderData),optionsNew).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}

//End 

//Forgot Pass
 forgotPass(loginData)
{
  return new Promise((resolve, reject) => {
     this.http.post(this.apiPath+'forgotPass',JSON.stringify(loginData)).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
  });
}


// Token carrier

  createAuthHeader(token){
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Bearer '+token);
    let options = new RequestOptions({ headers: headers });
    return options;  
  }

}
